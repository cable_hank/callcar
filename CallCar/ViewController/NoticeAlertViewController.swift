//
//  NoticeAlertViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/11/6.
//  Copyright © 2018 Cablesoft. All rights reserved.
//

import UIKit
class NoticeAlertViewController: UIViewController {
    
    var url = ""
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var alertViewHeightConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        img.downloaded(from: url) { () in
            let ratio = self.img.image!.size.width / self.img.image!.size.height
            self.alertViewHeightConstraint.constant = self.img.frame.width / ratio + 40
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.dismiss(animated: true, completion: nil)
    }
}
