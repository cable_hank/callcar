//
//  LaunchScreenTabBarViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/6/12.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit

class LaunchScreenTabBarViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if #available(iOS 13.0, *) {
            UIApplication.shared.statusBarStyle = .darkContent
        } else {
            UIApplication.shared.statusBarStyle = .default
        }
        UserDefaults.standard.setValue("0", forKey: UserInfo.statusColor.rawValue)
        if let uid = UserDefaults.standard.string(forKey: UserInfo.id.rawValue){
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                
                CallCarAPI.shared.getTokenlize(parameters: ["acc":UserDefaults.standard.getString(key: UserInfo.account.rawValue),"pw":UserDefaults.standard.getString(key: UserInfo.pw.rawValue)]) { (success, rtn, error, errorString) in
                    UserDefaults.standard.set(rtn, forKey: UserInfo.tokenlize.rawValue)
                    CallCarAPI.shared.loadMission(parameters: ["uid":uid])
                    CallCarAPI.shared.getForder(parameters: ["uid":uid]) { (success, res, error,sCode) in
                        if success{
                            RealmDB.shared.saveOrder(json: res)
                        }
                        CallCarAPI.shared.getPorder(parameters: ["uid":uid]) { (success, res1, error,mss) in
                            if success{
                                RealmDB.shared.saveHsOrder(json: res1)
                            }
                            let storyboard = UIStoryboard(name: "TabBar", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "TabBarVC")
                            self.present(vc, animated: false, completion: nil)
                        }
                    }
                }
                
            }
            
        }else{
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "loginNv")
                self.present(vc, animated: false, completion: nil)
            }
            
        }
    }

}
