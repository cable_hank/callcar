//
//  LoadViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/6/4.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
class LoadViewController: UIViewController {
    @IBOutlet weak var nvView: NVActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        nvView.startAnimating()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        nvView.stopAnimating()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
