//
//  OpinionAlertViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/11/13.
//  Copyright © 2018 Cablesoft. All rights reserved.
//

import UIKit
class OpinionAlertViewController: UIViewController,UITextViewDelegate {

    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textView.layer.borderColor = UIColor(hex: 0x919191).cgColor
        textView.layer.borderWidth = 0.5
        textView.layer.cornerRadius = 10
        
    }
    
    override func viewDidAppear(_ animated: Bool) {

        _ = textView.becomeFirstResponder()
    }
    

    @IBAction func cancel(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submit(_ sender: UIButton) {
        if textView.text != ""{
            let email = UserDefaults.standard.getString(key: UserInfo.account.rawValue)
            let uid = UserDefaults.standard.getString(key: UserInfo.id.rawValue)
            let phone = UserDefaults.standard.getString(key: UserInfo.mobile.rawValue)
            let version = UIDevice.current.systemVersion
            CallCarAPI.shared.sendEval(parameters: ["uid":uid,"content":textView.text,"phone":phone,"email":email,"os":"iOS","version":version]) { (success, res, error, mss) in
                self.view.endEditing(false)
                self.dismiss(animated: true, completion: nil)
            }
        }else{
            self.present(AlertViewModel.shared.showAlert(message: "請輸入意見回饋"), animated: true, completion: nil)
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        animateViewMoving(up: true, moveValue: 60)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        animateViewMoving(up: false, moveValue: 60)
    }
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement)
        UIView.commitAnimations()
    }
}
