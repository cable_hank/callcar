//
//  InvoiceWebViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2019/2/1.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import UIKit

class InvoiceWebViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var ac: UIActivityIndicatorView!
    
    var postDataString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ac.startAnimating()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let url = URL(string: APIRoute.searchInvoice.rawValue)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        let nowDate = Int(Date().timeIntervalSince1970)
        print(nowDate)
        var mid = "33801622"
        if Bundle.main.bundleIdentifier! != "com.iShareco.CallCar"{
            mid = "3140333"
        }
        let post: String = "MerchantID_=\(mid)&PostData_=\(postDataString)"
        let postData: Data = post.data(using: String.Encoding.ascii, allowLossyConversion: true)!
        
        request.httpBody = postData
        self.webView.loadRequest(request as URLRequest)
        
        
    }

}

extension InvoiceWebViewController:UIWebViewDelegate{
    func webViewDidFinishLoad(_ webView: UIWebView) {
        ac.stopAnimating()
        let url = webView.request?.url
        if let url = url{
            print(url.absoluteString)
            if let query = url.query{
                print(query)
            }
        }
        let headers = webView.request?.allHTTPHeaderFields
        for (key,value) in headers! {
            print("key \(key) value \(value)")
        }
        print("-----------------------------")
    }
}
