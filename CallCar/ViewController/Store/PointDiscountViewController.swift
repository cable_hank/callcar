//
//  PointDiscountViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2019/1/4.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import UIKit

class PointDiscountViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pointTextField: LeftIconTextField!
    @IBOutlet weak var couponTextField: LeftIconTextField!
    
    
    @IBOutlet weak var lastPointLabel: UILabel!
    
    @IBOutlet weak var myPointLabel: UILabel!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var listProductPrice: UILabel!
    @IBOutlet weak var listShipingRate: UILabel!
    
    @IBOutlet weak var listCoopun: UILabel!
    @IBOutlet weak var listTotalPrice: UILabel!
    
    @IBOutlet weak var noProductView: UIView!
    
    @IBOutlet weak var dtypeLabel: UILabel!
    @IBOutlet weak var dtypeViewLeading: NSLayoutConstraint!
    
    @IBOutlet weak var dtypePointButton: UIButton!
    
    @IBOutlet weak var dtypeCouponButton: UIButton!
    
    @IBOutlet weak var discountViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var couponFlag: UILabel!
    
    var rm_shopCarArr = [RM_ShopCar]()
    var myPoint = 0
    var discount = 0
    var maxPoint = 0
    var priceArr = [0,0,0,0]
    var storeOderModel:StoreOrderParameterModel?
    
    var dtype = "B"
    
    var bonus = 0
    var coupon = 0
    var timer = Timer()
    var isCouponOk = true
    
    var loadPriceOk = false
    var dateFormate = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "購物車"
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        tableView.separatorColor = UIColor(hex: 0xFBE5B2)
        dateFormate.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        RealmShop.shared.updateAllShopCarBonusTo0()
        
        CallCarAPI.shared.getBonus(parameters: ["uid":UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!]) { (success, res, sCode, errorString) in
            if sCode >= 900{
                self.goRoot()
                return
            }
            if success{
                UserDefaults.standard.set(res["effpoint"].stringValue, forKey: UserInfo.bonus.rawValue)
                UserDefaults.standard.set(res["uneffpoint"].stringValue, forKey: UserInfo.unbonus.rawValue)
            }
            
            self.myPoint = Int(UserDefaults.standard.getString(key: UserInfo.bonus.rawValue))!
            self.myPointLabel.text = "\(self.myPoint)"
            self.maxPoint = self.myPoint
            self.reloadTable()
        }
        
        
        //----先在ui跑一次 button的文字才不會出現怪動畫----
        dtypePointButton.isSelected = true
        dtypeCouponButton.isSelected = false
        self.view.layoutIfNeeded()
        //-------------------------------------------
        
    }
    
    
    @IBAction func editPointTextfield(_ sender: LeftIconTextField) {
        if sender.text == ""{
            bonus = 0
            setPrice()
            return
        }
        if sender.text == "0" || sender.text == "00"{
            
            bonus = 0
            sender.text = ""
            setPrice()
            return
        }
        if let point = Int(sender.text!){
            if point > maxPoint{
                sender.text = "\(maxPoint)"
                bonus = maxPoint
            }else{
                bonus = point
            }
        }
        setPrice()
    }
    @IBAction func backToStore(_ sender: BorderButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func selectDtype(_ sender: UIButton) {
        
        dtypePointButton.isSelected = sender.tag == 0
        dtypeCouponButton.isSelected = sender.tag == 1
        if sender.tag == 0{
            dtype = "B"
            self.dtypeLabel.text = "點數折抵"
            self.dtypeViewLeading.constant = 0
            discountViewHeight.constant = 150
        }else{
            dtype = "C"
            self.dtypeLabel.text = "優惠碼折抵"
            self.dtypeViewLeading.constant = -self.view.frame.width
            discountViewHeight.constant = 120
        }
        UIView.animate(withDuration: 0.5, delay: 0.01, usingSpringWithDamping: 0.8, initialSpringVelocity: 10.0, options: .curveEaseInOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
        setPrice()
    }
    
    @objc func checkCoupon(){
        CallCarAPI.shared.checkShopCoupon(parameters: "{\"code\":\"\(couponTextField.text!)\",\"uid\":\"\(UserDefaults.standard.getString(key: UserInfo.id.rawValue))\",\"appLocale\":\"TW\"}") { (success, res) in
            if success{
                self.couponFlag.text = "✔︎"
                self.couponFlag.textColor = UIColor(hex: 0x47B707)
                self.couponFlag.isHidden = false
                self.isCouponOk = true
            }else{
                self.couponFlag.text = "✖︎"
                self.couponFlag.textColor = UIColor(hex: 0xF97B62)
                self.couponFlag.isHidden = false
                self.isCouponOk = false
            }
            self.setPrice()
        }
    }
    
    @IBAction func editCouponTextField(_ sender: LeftIconTextField) {
        sender.text = sender.text?.uppercased()
        if sender.text!.count > 6{
            timer.invalidate()
            timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.checkCoupon), userInfo: nil, repeats: false)
        }else if sender.text!.count == 0{
            couponFlag.isHidden = true
            isCouponOk = true
        }else{
            couponFlag.text = "✖︎"
            couponFlag.textColor = UIColor(hex: 0xF97B62)
            couponFlag.isHidden = false
            isCouponOk = false
        }
    }
    
    @IBAction func next(_ sender: BorderButton) {
        if !loadPriceOk{
            return
        }
        CallCarAPI.shared.getBonus(parameters: ["uid":UserDefaults.standard.getString(key: UserInfo.id.rawValue)]) { (success, res, sCode, errorString) in
            if sCode >= 900{
                self.goRoot()
                return
            }
            if success{
                if res["effpoint"].stringValue == UserDefaults.standard.getString(key: UserInfo.bonus.rawValue){
                    if !self.isCouponOk && self.dtype == "C" {
                        self.present(AlertViewModel.shared.showAlert(message: "優惠碼有誤"), animated: true, completion: nil)
                        return
                    }
                    var fdtype = self.dtype
                    if self.priceArr[2] == 0{
                        fdtype = ""
                    }
                    var fcoupon = ""
                    if self.dtype == "C" && self.isCouponOk{
                        fcoupon = self.couponTextField.text!
                    }
                    var storeOrder = StoreOrderParameterModel(uid:UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!, oprice: self.priceArr[0], sprice: self.priceArr[1], tprice: self.priceArr[0] + self.priceArr[1], dtype: fdtype, damt: -self.priceArr[2], pamt: self.priceArr[3],coupon:fcoupon, oname: "", ophone: "", rname: "", rphone: "", zip: "", city: "", distinct: "", address: "", market: [],einv:StoreOrderParameterModel.einv(category:"",etype:"",ubn:"",Address:"",ctype:"",cnum:"",email:""), appLocale: "TW")
                    print(storeOrder)
                    for shopCar in self.rm_shopCarArr{
                        let market = StoreOrderParameterModel.market.init(marketid: shopCar.MarketID, mid: shopCar.FormatId, title: shopCar.Title1, format: shopCar.Format, uprice: shopCar.SalePrice, qty: shopCar.pCount, unit: shopCar.Unit, dtamt: shopCar.SalePrice * shopCar.pCount,damt: shopCar.shopCarBonus)
                        storeOrder.market.append(market)
                    }
                    
                    self.storeOderModel = storeOrder
                    print(storeOrder)
                    self.performSegue(withIdentifier: "next", sender: nil)
                }else{
                    self.present(AlertViewModel.shared.showAlert(message: "資料有誤"), animated: true, completion: nil)
                }
            }else{
                self.present(AlertViewModel.shared.showAlert(message: "請確認網路狀態"), animated: true, completion: nil)
            }
            
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "next"{
            let vc = segue.destination as! StoreCheckOutViewController
            vc.storeOderModel = self.storeOderModel
        }
    }
    
    @IBAction func hideKeyBoard(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    @IBAction func back(_ sender: BorderButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func reloadTable() {
        rm_shopCarArr = RealmShop.shared.getShopCars()
        var pArr:[String] = []
        for shopCar in rm_shopCarArr{
            pArr.append(shopCar.MarketID)
        }
        
        if self.rm_shopCarArr.count > 0{
            self.noProductView.isHidden = true
        }else{
            self.noProductView.isHidden = false
        }
        
        self.tableView.reloadData()
        print(pArr.description)
        
        CallCarAPI.shared.getMarketPs(parameters: pArr.description) { (success, productsModel) in
            if !success{
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) { // change 2 to desired number of seconds
                    self.reloadTable()
                }
                return
            }
            if let productsModel = productsModel{
                for pModel in productsModel.RtnObject{
                    for shopCar in self.rm_shopCarArr{
                        if pModel.MarketID == shopCar.MarketID{
                            var isDel = false
                            var pModelf:ProductsModel.RtnObject.Format?
                            let eDateTime = self.dateFormate.date(from: pModel.ETime)!
                            
                            if pModel.OnFlag != "1" || eDateTime < Date(){
                                isDel = true
                                RealmShop.shared.delShopCar(shopCar: shopCar)
                            }else{
                                for fModel in pModel.Format{
                                    if fModel.mid == shopCar.FormatId{
                                        pModelf = fModel
                                        if fModel.saleflag != "1"{
                                            isDel = true
                                            RealmShop.shared.delShopCar(shopCar: shopCar)
                                        }
                                    }
                                }
                            }
                            if !isDel{
                                RealmShop.shared.updateShopCar(marketModel: pModel, format: pModelf, shopCar: shopCar)
                            }
                        }
                    }
                }
                self.rm_shopCarArr = RealmShop.shared.getShopCars()
                if self.rm_shopCarArr.count > 0{
                    self.noProductView.isHidden = true
                }else{
                    self.noProductView.isHidden = false
                }
                self.setPrice()
                
                self.tableView.reloadData()
                UIView.animate(withDuration: 0.3) {
                    self.tableViewHeight.constant = CGFloat(self.rm_shopCarArr.count * 170)
                    self.view.layoutIfNeeded()
                }
                
            }
        }
        
        
    }
    
    
    func setPrice() {
        loadPriceOk = false
        var discount = 0
        for shopCar in self.rm_shopCarArr{
            discount = discount + shopCar.BonusLimit * shopCar.pCount
        }
        
        if discount < self.myPoint{
            self.maxPoint = discount
        }else{
            self.maxPoint = self.myPoint
        }
        
        self.lastPointLabel.text = "\(maxPoint)"
        
        var parameters = StorePriceParameterModel.init(uid: UserDefaults.standard.getString(key: UserInfo.id.rawValue), coupon: "", bonus: 0, market: [], appLocale: "TW")
        if isCouponOk && dtype == "C" && couponTextField.text! != ""{
            parameters.coupon = couponTextField.text!
//            parameters.bonus = 0
        }else if dtype == "B"{
            parameters.coupon = ""
//            parameters.bonus = bonus
        }else{
            parameters.coupon = ""
//            parameters.bonus = 0
        }
        for shopCar in rm_shopCarArr{
            let market = StorePriceParameterModel.market.init(mid: shopCar.MarketID, amt: shopCar.pCount)
            parameters.market.append(market)
            parameters.bonus = parameters.bonus + shopCar.shopCarBonus
        }
        
        let encoder = JSONEncoder()
        if let data = try? encoder.encode(parameters){
            let parameterString = String(data: data, encoding: .utf8)!
//            print(parameterString)
            
            CallCarAPI.shared.getShopPrice(parameters: parameterString) { (success, res) in
                if success{
                    self.priceArr[0] = res["RtnObject"]["OriginalPrice"].intValue
                    self.priceArr[1] = res["RtnObject"]["Shipping"].intValue
                    self.priceArr[2] = -res["RtnObject"]["Discount"].intValue
                    self.priceArr[3] = self.priceArr[0] + self.priceArr[1] + self.priceArr[2]
                    
                    self.listProductPrice.text = "\(self.priceArr[0])"
                    self.listShipingRate.text = "\(self.priceArr[1])"
                    self.listCoopun.text = "\(self.priceArr[2])"
                    self.listTotalPrice.text = "$\(self.priceArr[3])"
                    self.loadPriceOk = true
                }
            }
        }
        
    }
    
    @objc func delProduct(_ sender:UIButton){
        RealmShop.shared.delShopCar(shopCar: rm_shopCarArr[sender.tag])
        priceArr[1] = 0
        
        RealmShop.shared.updateAllShopCarBonusTo0()
        for cell in tableView.visibleCells{
            let cell = cell as! ShopCarProductTableViewCell
            cell.pointTextField.text = ""
        }
        reloadTable()
    }
    
}

extension PointDiscountViewController:UITableViewDataSource,UITableViewDelegate,ShopCarProductTableViewCellDelegate{
    func setPriceTotal() {
        setPrice()
    }
    
    func setPoint() {
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rm_shopCarArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ShopCarProductTableViewCell
        cell.delegate = self
        cell.setCell(shopCar: rm_shopCarArr[indexPath.row])
        cell.clostButton.addTarget(self, action: #selector(self.delProduct(_:)), for: .touchUpInside)
        cell.clostButton.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170
    }
}
