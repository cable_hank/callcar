//
//  StoreSuccessViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2019/1/4.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import UIKit

class StoreSuccessViewController: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "換購成功"
        navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        self.navigationController?.navigationBar.barTintColor = UIColor(hex: 0xFF8300)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        UserDefaults.standard.setValue("1", forKey: UserInfo.statusColor.rawValue)
        UIApplication.shared.statusBarStyle = .lightContent
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        self.navigationController?.popToRootViewController(animated: false)
        UserDefaults.standard.setValue("0", forKey: UserInfo.statusColor.rawValue)
        if #available(iOS 13.0, *) {
            UIApplication.shared.statusBarStyle = .darkContent
        } else {
            UIApplication.shared.statusBarStyle = .default
        }
    }
    @IBAction func goHome(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
        UserDefaults.standard.setValue("0", forKey: UserInfo.statusColor.rawValue)
        if #available(iOS 13.0, *) {
            UIApplication.shared.statusBarStyle = .darkContent
        } else {
            UIApplication.shared.statusBarStyle = .default
        }
        
    }
}
