//
//  StoreCheckOutViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2019/1/4.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import UIKit
import SwiftyJSON
class StoreCheckOutViewController: UIViewController {
    @IBOutlet weak var okButton: BorderButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var level1TextField: LeftIconTextField!
    @IBOutlet weak var level2TextField: LeftIconTextField!
    @IBOutlet weak var receiptLabel: UILabel!
    @IBOutlet weak var userName: LeftIconTextField!
    @IBOutlet weak var userMobile: LeftIconTextField!
    @IBOutlet weak var name: LeftIconTextField!
    @IBOutlet weak var mobile: LeftIconTextField!
    @IBOutlet weak var addr: LeftIconTextField!
    @IBOutlet weak var invoiceViewLeading: NSLayoutConstraint!
    @IBOutlet weak var invoiceViewHeight: NSLayoutConstraint!
    @IBOutlet weak var invoiceButton1: UIButton!
    @IBOutlet weak var invoiceButton2: UIButton!
    @IBOutlet weak var invoiceButton3: UIButton!
    @IBOutlet weak var donateInvoiceButton1: UIButton!
    @IBOutlet weak var donateInvoiceButton2: UIButton!
    @IBOutlet weak var personalInvoiceButton1: UIButton!
    @IBOutlet weak var personalInvoiceButton2: UIButton!
    @IBOutlet weak var personalInvoiceButton3: UIButton!
    
    @IBOutlet weak var donateInvoiceTips: UILabel!
    @IBOutlet weak var donateInvoiceCode: LeftIconTextField!
    @IBOutlet weak var personalInvoiceCode: LeftIconTextField!
    
    @IBOutlet weak var ubnNumber: LeftIconTextField!
    
    @IBOutlet weak var emailTextField: LeftIconTextField!
    @IBOutlet weak var personalError: UILabel!
    @IBOutlet weak var donateInvError: UILabel!
    @IBOutlet weak var ubnError: UILabel!
    @IBOutlet weak var isSaveUserButton: UIButton!
    @IBOutlet weak var emailError: UILabel!
    
    @IBOutlet weak var invView1: UIView!
    @IBOutlet weak var invView2: UIView!
    
    @IBOutlet weak var orderTitleTopConstraint: NSLayoutConstraint!
    
    
    var textFieldY:CGFloat = 0
    var isKeyboardShown = false
    var rm_shopCarArr = [RM_ShopCar]()
    var postalModel = [PostalCodeModel]()
    var level2Model = [PostalCodeModel.districts]()
    let level1_PickerView = UIPickerView()
    let level2_PickerView = UIPickerView()
    var storeOderModel:StoreOrderParameterModel?
    var invoiceType = 0
    var donateInvoiceType = 0
    var personalInvoiceType = 0
    var resJson:JSON? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "商品結帳"
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        invoiceButton1.isSelected = true
        invoiceButton2.isSelected = false
        invoiceButton3.isSelected = false
        self.view.layoutIfNeeded()
        
        tableView.separatorColor = UIColor(hex: 0xFBE5B2)
        
        level1_PickerView.delegate = self
        level1_PickerView.dataSource = self
        level2_PickerView.delegate = self
        level2_PickerView.dataSource = self
        level1_PickerView.tag = 0
        level2_PickerView.tag = 1
        level1TextField.inputView = level1_PickerView
        level2TextField.inputView = level2_PickerView
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        rm_shopCarArr = RealmShop.shared.getShopCars()
        tableView.reloadData()
        UIView.animate(withDuration: 0.3) {
            self.tableViewHeight.constant = CGFloat(self.rm_shopCarArr.count * 80)
            self.view.layoutIfNeeded()
        }
        
        postalModel = LoadJson().getPostalCodeModel(path: "postalCode")
        level2Model = postalModel[0].districts
        
        
        userName.text = UserDefaults.standard.getString(key: UserInfo.lname.rawValue) + UserDefaults.standard.getString(key: UserInfo.fname.rawValue)
        userMobile.text = UserDefaults.standard.getString(key: UserInfo.mobile.rawValue)
        name.text = UserDefaults.standard.getString(key: UserInfo.lname.rawValue) + UserDefaults.standard.getString(key: UserInfo.fname.rawValue)
        mobile.text = UserDefaults.standard.getString(key: UserInfo.mobile.rawValue)
        emailTextField.text = UserDefaults.standard.getString(key: UserInfo.account.rawValue)
        priceLabel.text = "付款金額$\(storeOderModel!.pamt)"
        //付款金額0
        if storeOderModel!.pamt == 0{
            invView1.isHidden = true
            invView2.isHidden = true
            orderTitleTopConstraint.priority = UILayoutPriority(rawValue: 750)
        }
        
        if let rm_shopUser = RealmShop.shared.getShopUser(){
            //            emailTextField.text = rm_shopUser.email
            //            userName.text = rm_shopUser.userName
            //            userMobile.text = rm_shopUser.userMobile
            name.text = rm_shopUser.name
            mobile.text = rm_shopUser.mobile
            addr.text = rm_shopUser.addr
            
            level1TextField.text =  postalModel[rm_shopUser.level1].name
            level1_PickerView.selectRow(rm_shopUser.level1, inComponent: 0, animated: false)
            level2Model = postalModel[rm_shopUser.level1].districts
            level2_PickerView.selectRow(rm_shopUser.level2, inComponent: 0, animated: false)
            level2TextField.text = "\(postalModel[rm_shopUser.level1].districts[rm_shopUser.level2].name)\(postalModel[rm_shopUser.level1].districts[rm_shopUser.level2].zip)"
           
        }
    }
    
    
    @IBAction func agreeButton(_ sender: UIButton) {
        if sender.tag == 0{
            sender.isSelected = !sender.isSelected
        }else if sender.tag == 1{
            sender.isSelected = !sender.isSelected
            if sender.isSelected{
                okButton.isEnabled = true
                okButton.backgroundColor = UIColor(hex: 0xFF5C00)
            }else{
                okButton.isEnabled = false
                okButton.backgroundColor = UIColor(hex: 0xAAAAAA)
            }
        }
    }
    //切換發票種類
    @IBAction func invioceType(_ sender: UIButton) {
        self.view.endEditing(true)
        invoiceType = sender.tag
        invoiceButton1.isSelected = sender.tag == 0
        invoiceButton2.isSelected = sender.tag == 1
        invoiceButton3.isSelected = sender.tag == 2
        invoiceViewLeading.constant = -self.view.frame.width * CGFloat(sender.tag)
        UIView.animate(withDuration: 0.5, delay: 0.01, usingSpringWithDamping: 0.8, initialSpringVelocity: 10.0, options: .curveEaseInOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    //切換捐贈發票種類
    @IBAction func donateInvoice(_ sender: UIButton) {
        self.view.endEditing(true)
        donateInvoiceType = sender.tag
        donateInvoiceButton1.isSelected = sender.tag == 0
        donateInvoiceButton2.isSelected = sender.tag == 1
        donateInvoiceTips.isHidden = sender.tag == 1
        donateInvoiceCode.isHidden = sender.tag == 0
        donateInvoiceCode.isError = false
        donateInvError.isHidden = true
        donateInvoiceCode.layer.borderColor = UIColor(hex: 0xCCCCCC).cgColor
        
    }
    
    //切換個人電子發票種類
    @IBAction func personalInvoice(_ sender: UIButton) {
        self.view.endEditing(true)
        personalInvoiceType = sender.tag
        personalInvoiceCode.text = ""
        personalInvoiceButton1.isSelected = sender.tag == 0
        personalInvoiceButton2.isSelected = sender.tag == 1
        personalInvoiceButton3.isSelected = sender.tag == 2
        
        personalInvoiceCode.isHidden = sender.tag == 0
        personalInvoiceCode.isError = false
        if sender.tag == 1{
            personalInvoiceCode.placeholder = "輸入手機條碼號"
        }else if sender.tag == 2{
            personalInvoiceCode.placeholder = "輸入自然人憑證條碼號"
        }
        
        personalError.isHidden = true
        personalInvoiceCode.layer.borderColor = UIColor(hex: 0xCCCCCC).cgColor
    }
    
    @IBAction func textFieldChange(_ sender: LeftIconTextField) {
        sender.errorFunc(isError: false)
    }
    
    @IBAction func emailTextFieldChange(_ sender: LeftIconTextField) {
        if sender.isValidEmail(){
            sender.errorFunc(isError: false)
            emailError.isHidden = true
        }else{
            sender.errorFunc(isError: true)
            emailError.isHidden = false
        }
    }
    
    
    @IBAction func invTextFieldChange(_ sender: LeftIconTextField) {
        sender.errorFunc(isError: false)
        if sender.tag == 0{
            donateInvError.isHidden = true
        }else if sender.tag == 1{
            personalError.isHidden = true
        }else{
            ubnError.isHidden = true
        }
    }
    
    @IBAction func invoiceService(_ sender: UIButton) {
        
        let url = URL(string: "https://www.einvoice.nat.gov.tw")!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    @IBAction func service(_ sender: UIButton) {
        let url = URL(string: "https://www.callcar.com.tw/index.html#Reward_point")!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
        
    }
    
    @IBAction func ok(_ sender: BorderButton) {
        
        if var storeOderModel = storeOderModel{
            if invoiceType == 0{
                storeOderModel.einv.category = "B2C"
                storeOderModel.einv.etype = "1"
                storeOderModel.einv.ubn = ""
                storeOderModel.einv.ctype = ""
                if donateInvoiceType == 0{
                    storeOderModel.einv.cnum = "999"
                }else{
                    if donateInvoiceCode.text! == ""{
                        donateInvError.isHidden = false
                        donateInvError.text = "*此欄位為必填，請輸入愛心碼"
                        donateInvoiceCode.errorFunc(isError: true)
                        return
                    }else if donateInvoiceCode.text!.count < 3{
                        donateInvError.isHidden = false
                        donateInvError.text = "*愛心碼不可低於3碼"
                        donateInvoiceCode.errorFunc(isError: true)
                        return
                    }else{
                        donateInvError.isHidden = true
                        donateInvoiceCode.isError = false
                        donateInvoiceCode.layer.borderColor = UIColor(hex: 0xCCCCCC).cgColor
                    }
                    storeOderModel.einv.cnum = donateInvoiceCode.text!
                }
            }else if invoiceType == 1{
                storeOderModel.einv.category = "B2C"
                storeOderModel.einv.etype = "0"
                storeOderModel.einv.ubn = ""
                if personalInvoiceType == 0{
                    storeOderModel.einv.ctype = "2"
                    storeOderModel.einv.cnum = UserDefaults.standard.getString(key: UserInfo.id.rawValue)
                }else if personalInvoiceType == 1{
                    if personalInvoiceCode.text! == ""{
                        personalError.isHidden = false
                        personalError.text = "*此欄位為必填，請輸入手機條碼號"
                        personalInvoiceCode.errorFunc(isError: true)
                        return
                    }else{
                        personalError.isHidden = true
                        personalInvoiceCode.isError = false
                        personalInvoiceCode.layer.borderColor = UIColor(hex: 0xCCCCCC).cgColor
                    }
                    storeOderModel.einv.ctype = "0"
                    storeOderModel.einv.cnum = personalInvoiceCode.text!
                }else{
                    if personalInvoiceCode.text! == ""{
                        personalError.isHidden = false
                        personalError.text = "*此欄位為必填，請輸入自然人憑證條碼號"
                        personalInvoiceCode.errorFunc(isError: true)
                        return
                    }else{
                        personalError.isHidden = true
                        personalInvoiceCode.isError = false
                        personalInvoiceCode.layer.borderColor = UIColor(hex: 0xCCCCCC).cgColor
                    }
                    storeOderModel.einv.ctype = "1"
                    storeOderModel.einv.cnum = personalInvoiceCode.text!
                }
            }else{
                if ubnNumber.text! == ""{
                    ubnError.isHidden = false
                    ubnError.text = "*此欄位為必填，請輸入統一編號"
                    ubnNumber.errorFunc(isError: true)
                    return
                }else if ubnNumber.text!.count != 8{
                    ubnError.isHidden = false
                    ubnError.text = "統一編號必須為8碼"
                    ubnNumber.errorFunc(isError: true)
                    return
                }else{
                    ubnError.isHidden = true
                    ubnNumber.isError = false
                    ubnNumber.layer.borderColor = UIColor(hex: 0xCCCCCC).cgColor
                }
                storeOderModel.einv.category = "B2B"
                storeOderModel.einv.etype = ""
                storeOderModel.einv.ubn = ubnNumber.text!
                storeOderModel.einv.ctype = ""
                storeOderModel.einv.cnum = ""
            }
            
            if emailTextField.text == ""{
                emailError.isHidden = false
                emailTextField.errorFunc(isError: true)
                return
            }
            
            if userName.text == ""{
                userName.errorFunc(isError: true)
                return
            }
            if userMobile.text == "" || userMobile.text!.count != 10 || userMobile.text!.prefix(2) != "09"{
                
                userMobile.errorFunc(isError: true)
                return
            }
            if name.text == ""{
                name.errorFunc(isError: true)
                return
            }
            if mobile.text == "" || mobile.text!.count != 10{
                mobile.errorFunc(isError: true)
                return
            }
            
            if level1_PickerView.selectedRow(inComponent: 0) == 0{
                level1TextField.errorFunc(isError: true)
                return
            }
            if level2_PickerView.selectedRow(inComponent: 0) == 0{
                level2TextField.errorFunc(isError: true)
                return
            }
            if addr.text == ""{
                addr.errorFunc(isError: true)
                return
            }
            
            storeOderModel.einv.Address = ""
            storeOderModel.einv.email = emailTextField.text!
            storeOderModel.oname = userName.text!
            storeOderModel.ophone = userMobile.text!
            storeOderModel.rname = name.text!
            storeOderModel.rphone = mobile.text!
            storeOderModel.address = addr.text!
            storeOderModel.zip = level2Model[level2_PickerView.selectedRow(inComponent: 0)].zip
            storeOderModel.city = postalModel[level1_PickerView.selectedRow(inComponent: 0)].name
            storeOderModel.distinct = level2Model[level2_PickerView.selectedRow(inComponent: 0)].name
            
            
            
            let encoder = JSONEncoder()
            if let data = try? encoder.encode(storeOderModel){
                let parameterString = String(data: data, encoding: .utf8)!
                print(parameterString)
                let loadvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "load")
                self.present(loadvc, animated: false, completion: nil)
                CallCarAPI.shared.addStoreOrder(parameters: parameterString) { (success, res) in
                    print(res)
                    self.dismiss(animated: false, completion: nil)
                    if success{
                        if self.isSaveUserButton.isSelected{
                            let rm_userInfo = RM_ShopUserInfo()
//                            rm_userInfo.email = self.emailTextField.text!
//                            rm_userInfo.userName = self.userName.text!
//                            rm_userInfo.userMobile = self.userMobile.text!
                            rm_userInfo.name = self.name.text!
                            rm_userInfo.mobile = self.mobile.text!
                            rm_userInfo.level1 = self.level1_PickerView.selectedRow(inComponent: 0)
                            rm_userInfo.level2 = self.level2_PickerView.selectedRow(inComponent: 0)
                            rm_userInfo.addr = self.addr.text!
                            rm_userInfo.allAddr = "\(self.postalModel[rm_userInfo.level1].districts[rm_userInfo.level2].zip)\(self.postalModel[rm_userInfo.level1].name)\(self.postalModel[rm_userInfo.level1].districts[rm_userInfo.level2].name)\(rm_userInfo.addr)"
                            RealmShop.shared.saveShopUser(shopUser: rm_userInfo)
                        }
                        if self.storeOderModel!.pamt == 0 && res["RtnObject"].stringValue == ""{
                            RealmShop.shared.delAllShopCars()
                            self.performSegue(withIdentifier: "complete", sender: nil)
                        }else{
                            self.resJson = res["RtnObject"]
                            //                        self.performSegue(withIdentifier: "next", sender: nil)
                            self.performSegue(withIdentifier: "creditCard", sender: nil)
                        }
                        
                    }else{
                        self.present(AlertViewModel.shared.showAlert(message: res["DisplayMsg"].stringValue), animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "creditCard"{
            let vc = segue.destination as! CreditCardWebViewController
            if let resJson = resJson{
                vc.post = resJson["trade"].stringValue
                vc.orderId = resJson["order"].stringValue
            }
        }
    }
    
    @IBAction func textFieldDidBegin(_ sender: UITextField) {
        let relativePositionView = UIApplication.shared.keyWindow
        let relativeFrame = sender.superview?.convert(sender.frame, to: relativePositionView)
        let yPosition = relativeFrame?.minY;
        textFieldY = yPosition! + sender.frame.height * 3
        
    }
    
    @objc func keyboardWillShow(note: NSNotification) {
        if isKeyboardShown {
            return
        }
        let keyboardAnimationDetail = note.userInfo as! [String: AnyObject]
        let duration = TimeInterval(truncating: keyboardAnimationDetail[UIResponder.keyboardAnimationDurationUserInfoKey]! as! NSNumber)
        let keyboardFrameValue = keyboardAnimationDetail[UIResponder.keyboardFrameBeginUserInfoKey]! as! NSValue
        let keyboardFrame = keyboardFrameValue.cgRectValue
        
        if self.view.frame.height - keyboardFrame.size.height > textFieldY{
            return
        }
        
        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: -(keyboardFrame.size.height - (self.view.frame.height - self.textFieldY)))
        })
        isKeyboardShown = true
    }
    
    @objc func keyboardWillHide(note: NSNotification) {
        let keyboardAnimationDetail = note.userInfo as! [String: AnyObject]
        let duration = TimeInterval(truncating: keyboardAnimationDetail[UIResponder.keyboardAnimationDurationUserInfoKey]! as! NSNumber)
        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy:  -self.view.frame.origin.y)
        })
        isKeyboardShown = false
    }
}

extension StoreCheckOutViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rm_shopCarArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")! as! CheckOutProductTableViewCell
//        cell.productName.adjustsFontSizeToFitWidth = true
        cell.productName.text = "\(rm_shopCarArr[indexPath.row].Title1)"
        cell.countLabel.text = "*\(rm_shopCarArr[indexPath.row].pCount)"
        cell.imgView.kf.setImage(with: URL(string: rm_shopCarArr[indexPath.row].ImageUrl))
        if rm_shopCarArr[indexPath.row].Format == ""{
            cell.formatName.isHidden = true
        }else{
            cell.formatName.isHidden = false
            cell.formatName.text = rm_shopCarArr[indexPath.row].Format
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
}

extension StoreCheckOutViewController:UIPickerViewDataSource,UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 0{
            return postalModel.count
        }else{
            return level2Model.count
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 0{
            return postalModel[row].name
        }else{
            return "\(level2Model[row].name)\(level2Model[row].zip)"
        }
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 0{
            level2_PickerView.selectRow(0, inComponent: 0, animated: false)
            level1TextField.errorFunc(isError: false)
            level1TextField.text =  postalModel[row].name
            level2Model = postalModel[row].districts
            level2TextField.text =  postalModel[row].districts[0].name
        }else{
            level2TextField.errorFunc(isError: false)
            level2TextField.text =  "\(level2Model[row].name)\(level2Model[row].zip)"
        }

    }

}
