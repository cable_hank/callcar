//
//  CreditCardWebViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2019/3/29.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import UIKit

class CreditCardWebViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var ActivityIndicator: UIActivityIndicatorView!
    
    var post = ""
    var orderId = ""
    var isOK = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }
    override func viewWillAppear(_ animated: Bool) {
        let url = URL(string: APIRoute.lanCard.rawValue)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        let postData: Data = post.data(using: String.Encoding.ascii, allowLossyConversion: true)!
        
        request.httpBody = postData
        self.webView.loadRequest(request as URLRequest)
        
    }
}

extension CreditCardWebViewController:UIWebViewDelegate{
    func webViewDidFinishLoad(_ webView: UIWebView) {
        ActivityIndicator.stopAnimating()
        let url = webView.request?.url
        if let url = url{
            print(url.absoluteString)
            if let query = url.query{
                if query.lastIndex(of: "Status") != nil{
                    isOK = true
                    print(query)
                    if query == "Status=SUCCESS"{
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                            CallCarAPI.shared.checkShopOrder(parameter: self.orderId) { (success) in
                                if success{
                                    RealmShop.shared.delAllShopCars()
                                    self.performSegue(withIdentifier: "next", sender: nil)
                                }else{
                                    let alert = UIAlertController(title: "提示", message: "信用卡付款失敗", preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "確定", style: .default, handler: { (a) in
                                        self.navigationController?.popViewController(animated: true)
                                    }))
                                    self.present(alert, animated: true, completion: nil)
                                }
                            }
                        }
                    }else{
                        let alert = UIAlertController(title: "提示", message: "信用卡付款失敗", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "確定", style: .default, handler: { (a) in
                            self.navigationController?.popViewController(animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
            if !isOK{
                if url.absoluteString == "https://www.callcar.com.tw/pay2go/return.html"{
                    CallCarAPI.shared.checkShopOrder(parameter: orderId) { (success) in
                        if !success{
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                }
            }
        }
//        print("-----------------------------")
    }
}
