//
//  StoreViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/12/26.
//  Copyright © 2018 Cablesoft. All rights reserved.
//

import UIKit

class StoreViewController: UIViewController {

    @IBOutlet weak var lineImage: UIImageView!
    @IBOutlet weak var segementTitle1: UILabel!
    @IBOutlet weak var segementTitle2: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var topViewHeigjt: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var point: UILabel!
    @IBOutlet weak var lastPoint: UILabel!
    @IBOutlet weak var lastDate: UILabel!
    @IBOutlet var storeHeaderView: UIView!
    @IBOutlet var categoryHeaderView: UIView!
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    @IBOutlet weak var noProductLabel: UILabel!
    @IBOutlet weak var storeSwitchButton: UIButton!
    
    @IBOutlet weak var storeHisSwitchButton: UIButton!
    var productArr:[MarketModel.RtnObject] = []
    var product:ProductModel?
    var shopHsArr:[RM_ShopHs] = []
    var selectIndex = 0
    var ono = ""
    var shopCarButton = BadgeButton()
    var categorySelect = 0
    var categoryArr:[CategoryModel.RtnObject] = []
    var productFlag = false
    var productHsFlag = false
    
    var isStoreSwitch = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.title = "換購商城"
        shopCarButton = BadgeButton(type: .custom)
        shopCarButton.badgeBackgroundColor = UIColor(hex: 0xFF5C00)
        shopCarButton.setImage(UIImage(named: "cart_default"), for: .normal)
        shopCarButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        shopCarButton.addTarget(self, action: #selector(goShopCar), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: shopCarButton)
        self.navigationItem.setRightBarButtonItems([item1], animated: true)
        
        let leftInfoButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        leftInfoButton.setImage(UIImage(named: "info_orange"), for: .normal)
        leftInfoButton.addTarget(self, action: #selector(goInfoUrl), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: leftInfoButton)
        self.navigationItem.setLeftBarButtonItems([item2], animated: true)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-12-31"
        lastDate.text = formatter.string(from: Date())
        
        
        
    }
    
    @IBAction func storeSwitch(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3) {
            if sender == self.storeSwitchButton && !self.isStoreSwitch{
                self.isStoreSwitch = !self.isStoreSwitch
                self.noProductLabel.isHidden = self.productFlag
                self.noProductLabel.text = "目前尚無商品"
                self.view.backgroundColor = UIColor(hex: 0xF7F7F7)
                self.tableView.backgroundColor = UIColor(hex: 0xF7F7F7)
                self.segementTitle1.textColor = UIColor(hex: 0xFF8300)
                self.segementTitle2.textColor = UIColor(hex: 0x999999)
                self.lineImage.transform = self.lineImage.transform.translatedBy(x: -self.lineImage.frame.width, y: 0)
                self.lineImage.image = UIImage(named: "LineDarkorange")
                //            self.topViewHeigjt.constant = 80
                self.topView.isHidden = false
                self.tableView.reloadData()
                self.view.layoutIfNeeded()
            }else if sender == self.storeHisSwitchButton && self.isStoreSwitch{
                self.isStoreSwitch = !self.isStoreSwitch
                self.noProductLabel.isHidden = self.productHsFlag
                self.noProductLabel.text = "目前尚無換購紀錄"
                self.view.backgroundColor = UIColor(hex: 0xF2F2F2)
                self.tableView.backgroundColor = UIColor(hex: 0xF2F2F2)
                self.segementTitle1.textColor = UIColor(hex: 0x999999)
                self.segementTitle2.textColor = UIColor(hex: 0x14729A)
                self.lineImage.transform = self.lineImage.transform.translatedBy(x: self.lineImage.frame.width, y: 0)
                self.lineImage.image = UIImage(named: "LineDarkblue")
                //            self.topViewHeigjt.constant = 0
                self.topView.isHidden = true
                self.tableView.reloadData()
                self.view.layoutIfNeeded()
            }
        }
        
    }
    
    @objc func goInfoUrl(){
        let url = URL(string: "https://www.callcar.com.tw/index.html#Reward_point")!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    @objc func goShopCar(){
        
        self.performSegue(withIdentifier: "shopCar", sender: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if #available(iOS 13.0, *) {
            UIApplication.shared.statusBarStyle = .darkContent
        } else {
            UIApplication.shared.statusBarStyle = .default
        }
        UserDefaults.standard.setValue("0", forKey: UserInfo.statusColor.rawValue)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black]
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.navigationBar.shadowImage = UIImage()

        
        let loadvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "load")
        loadvc.modalPresentationStyle = .overFullScreen
        self.present(loadvc, animated: false, completion: nil)
        CallCarAPI.shared.getCategory { (success, res) in
            if success {
                print("----")
                self.categoryArr = res!.RtnObject
                self.categoryCollectionView.reloadData()
                CallCarAPI.shared.getCategorySale(parameters: self.categoryArr[self.categorySelect].cid, success: { (success, data) in
                    if let markets = data,success{
                        self.productArr = markets.RtnObject
                    }
                    self.productFlag = self.productArr.count > 0
                    CallCarAPI.shared.findStoreOrder(parameter: UserDefaults.standard.getString(key:UserInfo.id.rawValue)) { (success, res) in
                        if let orderHs = res,success{
                            print("findStoreOrder")
                            RealmShop().saveShopHs(OrderHs: orderHs.RtnObject)
                        }
                        self.shopHsArr = RealmShop().getShopHs()
                        self.productHsFlag = self.shopHsArr.count > 0
                        print(self.shopHsArr.count)
                        self.tableView.reloadData()
                        if self.isStoreSwitch{
                            self.noProductLabel.isHidden = self.productFlag
                            self.noProductLabel.text = "目前尚無商品"
                        }else{
                            self.noProductLabel.isHidden = self.productHsFlag
                            self.noProductLabel.text = "目前尚無換購紀錄"
                        }
                        self.dismiss(animated: true, completion: nil)
                    }
                })
            }else{
                self.dismiss(animated: true, completion: nil)
                let alert = AlertViewModel.shared.showAlert(message: "讀取商品失敗")
                self.present(alert, animated: true, completion: nil)
            }
            
        }
        
        let shopCars = RealmShop.shared.getShopCars()
        shopCarButton.badge =  shopCars.count
//        for shopCar in shopCars{
//            CallCarAPI.shared.checkMarketId(parameters: shopCar.MarketID) { (isOK) in
//                if !isOK{
//                    RealmShop().delShopCar(shopCar: shopCar)
//                    self.shopCarButton.badge = RealmShop.shared.getShopCars().count
//                }
//            }
//        }
        CallCarAPI.shared.getBonus(parameters: ["uid":UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!]) { (success, res, sCode, errorString) in
            if sCode >= 900{
                self.goRoot()
                return
            }
            if success{
                UserDefaults.standard.set(res["effpoint"].stringValue, forKey: UserInfo.bonus.rawValue)
                UserDefaults.standard.set(res["uneffpoint"].stringValue, forKey: UserInfo.unbonus.rawValue)
            }
            
            self.point.text = UserDefaults.standard.string(forKey: UserInfo.bonus.rawValue)
            self.lastPoint.text = UserDefaults.standard.string(forKey: UserInfo.unbonus.rawValue)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print(Bundle.main.bundleIdentifier!)
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.topView.bounds
        gradientLayer.colors = [UIColor(hex: 0xF5A623).cgColor,UIColor(hex: 0xF76B1C).cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.1)
        gradientLayer.zPosition = -1
        //        self.topView.layer.addSublayer(gradientLayer)
        self.topView.layer.insertSublayer(gradientLayer, at: 0)
        
        topView.clipsToBounds = true
        topView.layer.cornerRadius = 5
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showProduct"{
            let vc = segue.destination as! ProductViewController
            vc.product = self.product?.RtnObject
        }else if segue.identifier == "showRecord"{
            let vc = segue.destination as! StoreRecordViewController
            vc.ono = ono
        }else if segue.identifier == "shopCar"{
            
        }
    }
    
    @objc func selectProductButton(_ sender: BorderCornerButton){
        selectIndex = sender.tag
        if isStoreSwitch{
            CallCarAPI.shared.getMarketP(parameters: productArr[selectIndex].MarketID) { (success, productModel) in
                if success {
                    self.product = productModel
                    self.performSegue(withIdentifier: "showProduct", sender: nil)
                }
            }
        }else{
            ono = shopHsArr[sender.tag].ono
            self.performSegue(withIdentifier: "showRecord", sender: nil)
        }
    }
    
    @IBAction func segementChange(_ sender: UISegmentedControl) {
        
        UIView.animate(withDuration: 0.3) {
            if sender.selectedSegmentIndex == 0{
                self.noProductLabel.isHidden = self.productFlag
                self.noProductLabel.text = "目前尚無商品"
                self.view.backgroundColor = UIColor(hex: 0xF7F7F7)
                self.tableView.backgroundColor = UIColor(hex: 0xF7F7F7)
                self.segementTitle1.textColor = UIColor(hex: 0xFF8300)
                self.segementTitle2.textColor = UIColor(hex: 0x999999)
                self.lineImage.transform = self.lineImage.transform.translatedBy(x: -self.lineImage.frame.width, y: 0)
                self.lineImage.image = UIImage(named: "LineDarkorange")
                //            self.topViewHeigjt.constant = 80
                self.topView.isHidden = false
            }else{
                self.noProductLabel.isHidden = self.productHsFlag
                self.noProductLabel.text = "目前尚無換購紀錄"
                self.view.backgroundColor = UIColor(hex: 0xF2F2F2)
                self.tableView.backgroundColor = UIColor(hex: 0xF2F2F2)
                self.segementTitle1.textColor = UIColor(hex: 0x999999)
                self.segementTitle2.textColor = UIColor(hex: 0x14729A)
                self.lineImage.transform = self.lineImage.transform.translatedBy(x: self.lineImage.frame.width, y: 0)
                self.lineImage.image = UIImage(named: "LineDarkblue")
                //            self.topViewHeigjt.constant = 0
                self.topView.isHidden = true
            }
            self.tableView.reloadData()
            self.view.layoutIfNeeded()
        }
    }
    func goPage(id:String) {
        CallCarAPI.shared.getMarketP(parameters: id) { (success, productModel) in
            if success {
                self.product = productModel
                self.performSegue(withIdentifier: "showProduct", sender: nil)
            }
        }
    }
}

extension StoreViewController:UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 0
        }else{
            if isStoreSwitch{
                print("productArr.count:\(productArr.count)")
                return productArr.count
            }else{
                print("shopHsArr.count:\(shopHsArr.count)")
                return shopHsArr.count
            }
        }
            
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 && isStoreSwitch{
            return tableView.cellForRow(at: indexPath)!
        }else{
            if isStoreSwitch{
                let cell = tableView.dequeueReusableCell(withIdentifier: "storeCell") as! StoreTableViewCell
                cell.setCell(product: productArr[indexPath.row])
                cell.goButton.tag = indexPath.row
                cell.goButton.addTarget(self, action:  #selector(self.selectProductButton(_:)), for: .touchUpInside)
                
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "recordCell") as! RecordTableViewCell
                cell.setCell(shopHs: shopHsArr[indexPath.row])
                cell.button.tag = indexPath.row
                
                cell.button.addTarget(self, action:  #selector(self.selectProductButton(_:)), for: .touchUpInside)
                return cell
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if !isStoreSwitch{
            return nil
        }
        if section == 0{
            return storeHeaderView
        }else{
            return categoryHeaderView
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if !isStoreSwitch{
            return 0
        }
        if section == 0{
            return storeHeaderView.frame.height
        }else{
            return 42
        }
    }
    
    
}

extension StoreViewController:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "category", for: indexPath) as! StoreCategoryCollectionViewCell
        cell.categoryTitle.text = categoryArr[indexPath.row].desc
        if indexPath.row == categorySelect{
            cell.selectView.isHidden = false
            cell.categoryTitle.textColor = .white
        }else{
            cell.selectView.isHidden = true
            cell.categoryTitle.textColor = UIColor(hex: 0x999999)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        categorySelect = indexPath.row
        collectionView.reloadData()
        CallCarAPI.shared.getCategorySale(parameters: self.categoryArr[indexPath.row].cid, success: { (success, data) in
            if let markets = data,success{
                self.productArr = markets.RtnObject
                self.noProductLabel.isHidden = self.productArr.count > 0
                self.tableView.reloadData()
            }
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 95, height: 40)
    }
    
    
}
