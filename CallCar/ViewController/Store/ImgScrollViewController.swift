//
//  ImgScrollViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2019/3/12.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import UIKit

class ImgScrollViewController: UIViewController {

    @IBOutlet weak var pageScrollView: PageZoomImgScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    var imgArr:[String] = []
    var goPage = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        pageScrollView.imgUrlArr = imgArr
        pageControl.numberOfPages = imgArr.count
        for subScroll in pageScrollView.scrollViewArr{
            subScroll.delegate = self
        }
        
        scrollToPage(page: CGFloat(goPage))
        
    }
    
    @IBAction func close(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    

}
extension ImgScrollViewController:UIScrollViewDelegate{
    
    func scrollToPage(page:CGFloat){
        var frame = pageScrollView.frame
        frame.origin.x = frame.width * page
        frame.origin.y = 0
        pageControl.currentPage = Int(page)
        pageScrollView.scrollRectToVisible(frame, animated: false)
    }
    
    @objc func scrollViewCarousel(){
        if pageControl.currentPage == imgArr.count-1{
            scrollToPage(page:0)
        }else{
            scrollToPage(page:CGFloat(pageControl.currentPage+1))
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if imgArr.count > 0{
            pageControl.currentPage = self.pageScrollView.getPage()
        }
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return pageScrollView.imgViewArr[pageScrollView.getPage()]
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        pageScrollView.isPagingEnabled = scale == 1
    }
}
