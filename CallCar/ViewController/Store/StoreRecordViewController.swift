//
//  StoreRecordViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2019/1/7.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import UIKit

class StoreRecordViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var orderStatusLabel: UILabel!
    @IBOutlet weak var orderTotalPrice: UILabel!
    @IBOutlet weak var statisticsTotalPrice: UILabel!
    @IBOutlet weak var statisticsFarePrice: UILabel!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var discountPrice: UILabel!
    @IBOutlet weak var payPrice: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var addr: UILabel!
    @IBOutlet weak var cancelButton: BorderButton!
    @IBOutlet weak var noInvLabel: UILabel!
    @IBOutlet weak var invStackView: UIStackView!
    @IBOutlet weak var searchInvBtn: BorderCornerButton!
    
    @IBOutlet weak var discountView: UIView!
    @IBOutlet weak var discountViewHeight: NSLayoutConstraint!
    @IBOutlet weak var discountPointStackView: UIStackView!
    @IBOutlet weak var discountCouponStackView: UIStackView!
    
    @IBOutlet weak var point: UILabel!
    @IBOutlet weak var coupon: UILabel!
    
    @IBOutlet weak var orderStatusButton: BorderButton!
    
    
    var ono = ""
    var invPostData = ""
    var rm_shopHs = RM_ShopHs()
    
    var openStatusText = ""
    var closeStatusText = ""
    var statusOpen = false
    var canCancel = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        self.title = "換購資訊"
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        tableView.separatorColor = UIColor(hex: 0xFBE5B2)
        if let shopHs = RealmShop().getShopHs(ono: ono){
            rm_shopHs = shopHs
            tableViewHeight.constant = CGFloat(rm_shopHs.odetail.count * 90 + 2)
            
//            var orderstatus = ""
//            for process in shopHs.process{
//                if shopHs.oprocess == "\(process.Seq)"{
//                    orderstatus = "【\(process.Stage)】"
//                    break
//                }
//            }
//            if shopHs.ostatus == "X"{
//                cancelButton.isHidden = true
//                orderstatus = "【已取消】"
//            }
            for odetail in rm_shopHs.odetail{
                if let p = Int(odetail.process){
                    if p > 20{
                        cancelButton.borderColor = UIColor(hex: 0xF5A623)
                        cancelButton.setTitleColor(UIColor(hex: 0xF5A623), for: .normal)
                        cancelButton.setTitle("退貨說明", for: .normal)
                        canCancel = false
                    }
                }
            }
            
            
            if shopHs.ostatus == "0"{
                orderStatusButton.borderColor = UIColor(hex: 0x14729A)
                orderStatusButton.setTitle("訂單已成立", for: .normal)
                orderStatusButton.setTitleColor(UIColor(hex: 0x14729A), for: .normal)
                orderStatusButton.backgroundColor = .white
            }else{
                orderStatusButton.borderColor = UIColor(hex: 0x7F7F7F)
                orderStatusButton.setTitle("已取消", for: .normal)
                orderStatusButton.setTitleColor(.white, for: .normal)
                orderStatusButton.backgroundColor = UIColor(hex: 0x7F7F7F)
                
            }
            openStatusText = "訂單編號:\(rm_shopHs.ono)"
            closeStatusText = "訂單編號:\(rm_shopHs.ono)\n\(rm_shopHs.process[0].Stage):\(rm_shopHs.process[0].StageTime.suffix(11))"
            for process in rm_shopHs.process{
                if process.Display == "1"{
                    openStatusText = "\(openStatusText)\n\(process.Stage):\(process.StageTime.suffix(11))"
                }
            }
            orderStatusLabel.text = closeStatusText
            
            orderTotalPrice.text = "$\(rm_shopHs.orderp)"
            statisticsTotalPrice.text = "\(rm_shopHs.orderp)"
            statisticsFarePrice.text = "\(rm_shopHs.shipp)"
            discountPrice.text = "-\(rm_shopHs.damt)"
            payPrice.text = "$\(rm_shopHs.payamt)"
            name.text = rm_shopHs.rname
            phone.text = rm_shopHs.rmobile
            addr.text = rm_shopHs.raddress
            
            //發票
            if rm_shopHs.invoice == "0"{
                noInvLabel.isHidden = false
                noInvLabel.text = "出貨後方開立發票"
                invStackView.isHidden = true
            }else if rm_shopHs.invoice == "X"{
                noInvLabel.isHidden = true
                noInvLabel.text = "發票已作廢"
                invStackView.isHidden = true
            }else{
                noInvLabel.isHidden = false
                noInvLabel.text = ""
                invStackView.isHidden = false
            }
            
            //折抵
            if rm_shopHs.dtype == ""{
                discountView.isHidden = true
                discountViewHeight.constant = 0
            }else if rm_shopHs.dtype == "B"{
                discountView.isHidden = false
                discountViewHeight.constant = 70
                discountPointStackView.isHidden = false
                discountCouponStackView.isHidden = true
                point.text = "\(rm_shopHs.damt)"
                discountLabel.text = "點數折抵"
            }else{
                discountView.isHidden = false
                discountViewHeight.constant = 70
                discountPointStackView.isHidden = true
                discountCouponStackView.isHidden = false
                coupon.text = rm_shopHs.coupon
                discountLabel.text = "優惠碼折抵"
            }
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "searchInv"{
            let vc = segue.destination as! InvoiceWebViewController
            vc.postDataString = invPostData
        }
    }
    
    @IBAction func invSearch(_ sender: BorderCornerButton) {
        CallCarAPI.shared.searchStoreInv(parameters: ono) { (success, res) in
            if !success {
                self.present(AlertViewModel.shared.showAlert(message: res["DisplayMsg"].stringValue), animated: true, completion: nil)
                return
            }
            print(res)
            self.invPostData = res["RtnObject"].stringValue
            self.performSegue(withIdentifier: "searchInv", sender: nil)
        }
    }
    
    
    @IBAction func openStatus(_ sender: UIButton) {
        sender.transform = sender.transform.rotated(by: CGFloat.pi)
        if !statusOpen{
            orderStatusLabel.text = openStatusText
        }else{
            orderStatusLabel.text = closeStatusText
        }
        statusOpen = !statusOpen
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    
    @IBAction func cancelOrder(_ sender: BorderButton) {
        if !canCancel{
            return
        }
        
        var mid = "MS3207428026"
        if Bundle.main.bundleIdentifier! != "com.iShareco.CallCar"{
            mid = "MS35951169"
        }
        let alertContorller = UIAlertController(title: "提示", message: "是否取消訂單？", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "稍後再說", style: .default, handler: nil)
        let cancelOrderAction = UIAlertAction(title: "取消訂單", style: .default) { (alertAction) in
            let loadvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "load")
            loadvc.modalPresentationStyle = .overFullScreen
            self.present(loadvc, animated: false, completion: nil)
            if self.rm_shopHs.orderp == 0{
                CallCarAPI.shared.cancelStoreOrder(parameters: "{\"no\":\"\(self.ono)\",\"appLocale\":\"TW\"}") { (success, json) in
                    self.dismiss(animated: true, completion: nil)
                    if success{ self.navigationController?.popToRootViewController(animated: true)
                    }else{ self.present(AlertViewModel.shared.showAlert(message: json["DisplayMsg"].stringValue), animated: true, completion: nil)
                    }
                }
            }else{
                CallCarAPI.shared.shopUncharge(parameters: "{\"no\":\"\(self.ono)\"}", success: { (success, res) in
                    if success {
                        if res["type"].stringValue == "0"{
                            CallCarAPI.shared.lanCancel(parameters: "MerchantID_=\(mid)&\(res["content"].stringValue)", success: { (success,resLan,mss) in
                                CallCarAPI.shared.shopRefundrecord(parameters: "{\"no\":\"\(self.ono)\",\"log\":\(resLan)}")
                                if success{
                                    CallCarAPI.shared.cancelStoreOrder(parameters: "{\"no\":\"\(self.ono)\",\"appLocale\":\"TW\"}") { (success, json) in
                                        self.dismiss(animated: true, completion: nil)
                                        if success{ self.navigationController?.popToRootViewController(animated: true)
                                        }else{ self.present(AlertViewModel.shared.showAlert(message: json["DisplayMsg"].stringValue), animated: true, completion: nil)
                                        }
                                    }
                                }else{
                                    self.dismiss(animated: true, completion: nil)
                                    self.present(AlertViewModel.shared.showAlert(message: mss), animated: true, completion: nil)
                                }
                            })
                        }else if res["type"].stringValue == "-1"{
                            self.dismiss(animated: true, completion: nil)
                            self.present(AlertViewModel.shared.showAlert(message: "每日21:00至隔日12:00，為藍新金流帳務處理時段。請於12:00過後取消訂單"), animated: true, completion: nil)
                        }else{
                            CallCarAPI.shared.lanShopUncharge(parameters: "MerchantID_=\(mid)&\(res["content"].stringValue)", success: { (success,unchRes,mss) in
                                CallCarAPI.shared.shopRefundrecord(parameters: "{\"no\":\"\(self.ono)\",\"log\":\(unchRes)}")
                                if success{
                                    CallCarAPI.shared.cancelStoreOrder(parameters: "{\"no\":\"\(self.ono)\",\"appLocale\":\"TW\"}") { (success, json) in
                                        
                                        self.dismiss(animated: true, completion: nil)
                                        if success{ self.navigationController?.popToRootViewController(animated: true)
                                        }else{ self.present(AlertViewModel.shared.showAlert(message: json["DisplayMsg"].stringValue), animated: true, completion: nil)
                                        }
                                    }
                                }else{
                                    self.dismiss(animated: true, completion: nil)
                                    self.present(AlertViewModel.shared.showAlert(message: mss), animated: true, completion: nil)
                                }
                            })
                        }
                    }else{
                        
                        self.dismiss(animated: true, completion: nil)
                        self.present(AlertViewModel.shared.showAlert(message: "此訂單狀態無法取消"), animated: true, completion: nil)
                    }
                })
            }
            
            
            
            
        }
        
        alertContorller.addAction(cancelAction)
        alertContorller.addAction(cancelOrderAction)
        self.present(alertContorller, animated: true, completion: nil)
    }
}

extension StoreRecordViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rm_shopHs.odetail.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ShopOrderTableViewCell
        let odetail = rm_shopHs.odetail[indexPath.row]
        for process in rm_shopHs.process{
            if process.Seq == Int(odetail.process)!{
                cell.setCell(rmOdetail: odetail, rmProcess: process)
                break
            }
        }
        
        
        return cell
    }
    
    
}
