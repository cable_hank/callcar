//
//  ProductViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/12/28.
//  Copyright © 2018 Cablesoft. All rights reserved.
//

import UIKit
import SwiftyJSON
class ProductViewController: UIViewController {

    @IBOutlet weak var productImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    @IBOutlet weak var sideTitleLabel: UILabel!
    @IBOutlet weak var countTextField: LeftIconTextField!
    @IBOutlet weak var normTextField: LeftIconTextField!
    @IBOutlet weak var normHeight: NSLayoutConstraint!
    
    @IBOutlet weak var normStackView: UIStackView!
    
    @IBOutlet weak var bottonSubView: UIView!
    @IBOutlet weak var priceStackView: UIStackView!
    
    @IBOutlet weak var buttonWidthCt: NSLayoutConstraint!
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var bottomViewHeightCt: NSLayoutConstraint!
    @IBOutlet weak var subViewHeightCt: NSLayoutConstraint!
    
    @IBOutlet weak var pageScrollView: PageScrollView!
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var videoButon: BorderButton!
    
    @IBOutlet weak var videoHeight: NSLayoutConstraint!
    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var normIcon: UIButton!
    
    @IBOutlet weak var salePrice1: UILabel!
    @IBOutlet weak var oldPrice1: UILabel!
    @IBOutlet weak var salePrice2: UILabel!
    @IBOutlet weak var oldPrice2: UILabel!
    
    @IBOutlet weak var lowerPrice: UILabel!
    
    @IBOutlet weak var addCountButton: UIButton!
    
    @IBOutlet weak var lessCountButon: UIButton!
    @IBOutlet weak var addShopCarButton: BorderButton!
    
    @IBOutlet weak var nextButton: BorderButton!
    @IBOutlet weak var addShopCarWidth: NSLayoutConstraint!
    
    @IBOutlet weak var tipsLabel: UILabel!
    @IBOutlet weak var tipsImage: UIImageView!
    
    @IBOutlet weak var bonusLimitLabel: UILabel!
    
    @IBOutlet weak var addView: CornerView!
    
    let normPickerView = UIPickerView()
    
    var product:ProductModel.RtnObject?
    var format:[ProductModel.RtnObject.Format] = []
    var shopCars = [RM_ShopCar]()
    
    var isStretch = false
    var isKeyboardShown = false
    var productNormArr:[String] = []
    var timer:Timer? = nil
    var shopViewTimer:Timer? = nil
    var imgArr:[String] = []
    var otherHeight:CGFloat = 0
    var shopCarButton = BadgeButton()
    var maxBuy = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = product?.Title1
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        
        shopCarButton = BadgeButton(type: .custom)
        shopCarButton.badgeBackgroundColor = UIColor(hex: 0xFF5C00)
        shopCarButton.setImage(UIImage(named: "cart_default"), for: .normal)
        shopCarButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        shopCarButton.addTarget(self, action: #selector(shopCar), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: shopCarButton)
        self.navigationItem.setRightBarButtonItems([item1], animated: true)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil)
        
        normPickerView.dataSource = self
        normPickerView.delegate = self
        normTextField.inputView = normPickerView
        
        titleLabel.adjustsFontSizeToFitWidth = true
        
        //預設bottonView layout---------
        bottonSubView.isHidden = true
        buttonWidthCt.constant = self.view.frame.width - 20
        subViewHeightCt.constant = 0
        bottomViewHeightCt.constant = 60
        //-----------------------------
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setView()
        setShopCarsBadge()
        timerSwitch(true)
        pageControl.numberOfPages = imgArr.count
        pageScrollView.imgUrlArr = imgArr
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if buttonWidthCt.constant == self.view.frame.width - 20{
            addShopCarWidth.constant = 0
            addShopCarButton.isHidden = false
        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        timerSwitch(false)
        self.view.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "webView"{
            let vc = segue.destination  as! VideoWebViewController
            vc.urlString = product!.Video
        }else if segue.identifier == "imgScroll"{
            let vc = segue.destination  as! ImgScrollViewController
            vc.imgArr = imgArr
            vc.goPage = pageScrollView.getPage()
        }
    }
    
    func setShopCarsBadge(){
        shopCars = RealmShop.shared.getShopCars()
        shopCarButton.badge =  shopCars.count
    }
    
    func setView() {
        titleLabel.text = product?.Title1
        sideTitleLabel.text = product?.Title1
        subTitleLabel.text = product?.Title2
        textView.attributedText = product!.SaleInfo.htmlToAttributedString
        salePrice1.text = "優惠價:\(product!.SalePrice)"
        salePrice2.text = "優惠價:\(product!.SalePrice)"
        oldPrice1.attributedText = NSAttributedString(string: "定價:\(product!.Price)", attributes: [NSAttributedString.Key.strikethroughStyle:NSUnderlineStyle.single.rawValue])
        oldPrice2.attributedText = NSAttributedString(string: "定價:\(product!.Price)", attributes: [NSAttributedString.Key.strikethroughStyle:NSUnderlineStyle.single.rawValue])
        bonusLimitLabel.isHidden = product!.BonusLimit == 0
        bonusLimitLabel.text = "【最多可折抵點數\(product!.BonusLimit)點】"
        lowerPrice.layer.borderWidth = 1
        lowerPrice.layer.borderColor = UIColor(hex: 0xFF8300).cgColor
        lowerPrice.text = "折後價:$\(product!.SalePrice - product!.BonusLimit)"
        if product!.Video == ""{
            videoButon.isHidden = true
            videoHeight.constant = 0
        }
        
        if product!.Format.count > 0{
            format = []
            for f in product!.Format{
                if f.saleflag == "1" && f.soldq < f.saleq{
                    format.append(f)
                }
            }
            if format.count > 1{
                format.insert(ProductModel.RtnObject.Format(mid: "", format: "請選擇", saleflag: "",saleq: 0,soldq: 0), at: 0)
            }
            if format.count > 0{
                
                normPickerView.selectRow(0, inComponent: 0, animated: true)
                normTextField.text = format[0].format
                normPickerView.reloadInputViews()
            }
        }else{
            normStackView.isHidden = true
            normIcon.isHidden = true
            normHeight.constant = 0
            otherHeight = -40
        }
        imgArr.removeAll()
        for image in product!.Images{
            imgArr.append(image.Url)
        }
        if format.count == 0{
            nextButton.isEnabled = false
            nextButton.backgroundColor = .lightGray
            nextButton.setTitle("已售完", for: .normal)
        }
        getMaxBuy()
        setCountButtonColor(nowCount:1)
    }
    
    func getMaxBuy(){
        countTextField.text = "1"
        if normTextField.text == "請選擇" && normPickerView.selectedRow(inComponent: 0) == 0{
            maxBuy = product!.MaxBuy
            return
        }
        if format.count == 0{
            if let shopcar = RealmShop.shared.isRepeat(marketId: product!.MarketID, formatId: ""){
                maxBuy = shopcar.MaxBuy - shopcar.pCount
            }else{
                maxBuy = product!.MaxBuy
            }
            return
        }
        let qMaxBuy = format[normPickerView.selectedRow(inComponent: 0)].saleq - format[normPickerView.selectedRow(inComponent: 0)].soldq
        print("soldq:\(format[normPickerView.selectedRow(inComponent: 0)].soldq)")
        print("saleq:\(format[normPickerView.selectedRow(inComponent: 0)].saleq)")
        print("qMaxBuy:\(qMaxBuy)")
        if let shopcar = RealmShop.shared.isRepeat(marketId: product!.MarketID, formatId: format[normPickerView.selectedRow(inComponent: 0)].mid){
            maxBuy = shopcar.MaxBuy - shopcar.pCount
        }else{
            maxBuy = product!.MaxBuy
        }
        if maxBuy > qMaxBuy{
            maxBuy = qMaxBuy
        }
        print(maxBuy)
    }
    
    func timerSwitch(_ bool:Bool){
        if bool{
            timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.scrollViewCarousel), userInfo: nil, repeats: true)
        }else{
            timer?.invalidate()
            timer = nil
        }
    }
    
    @IBAction func watchVideo(_ sender: UIButton) {
        performSegue(withIdentifier: "webView", sender: nil)

    }
    
    func setCountButtonColor(nowCount:Int){
        if normTextField.text == "請選擇" && normPickerView.selectedRow(inComponent: 0) == 0 || maxBuy == 0{
            lessCountButon.setTitleColor(.gray, for: .normal)
            addCountButton.setTitleColor(.gray, for: .normal)
            lessCountButon.isEnabled = false
            addCountButton.isEnabled = false
            return
        }
        
        if nowCount == 1{
            lessCountButon.setTitleColor(.gray, for: .normal)
            lessCountButon.isEnabled = false
        }else{
            lessCountButon.setTitleColor(UIColor(hex: 0xF76B1C), for: .normal)
            lessCountButon.isEnabled = true
        }
        
        if nowCount == maxBuy{
            addCountButton.setTitleColor(.gray, for: .normal)
            addCountButton.isEnabled = false
        }else{
            addCountButton.setTitleColor(UIColor(hex: 0xF76B1C), for: .normal)
            addCountButton.isEnabled = true
        }
        
    }
    
    @IBAction func contSet(_ sender: UIButton) {
        if normTextField.text == "請選擇" && normPickerView.selectedRow(inComponent: 0) == 0 {
            return
        }
        let count = Int(countTextField.text!)!
        if maxBuy == 0{
            return
        }
        if sender.tag == 0{
            if count == 1{
                return
            }
            countTextField.text = "\(count - 1)"
            setCountButtonColor(nowCount:count-1)
        }else{
            if count == maxBuy{  //最大
                return
            }
            setCountButtonColor(nowCount:count+1)
            countTextField.text = "\(count + 1)"
            
        }
    }
    
    @IBAction func addShopCar(_ sender: BorderButton) {
        if (format.count > 1 && normPickerView.selectedRow(inComponent: 0) == 0){
            
            self.present(AlertViewModel.shared.showAlert(message: "請選擇規格"), animated: true, completion: nil)
            return
        }
        if  normTextField.text == "請選擇" && !normStackView.isHidden{
            
            self.present(AlertViewModel.shared.showAlert(message: "此商品目前無法購買"), animated: true, completion: nil)
            return
        }
        
        
        if !RealmShop.shared.saveShopCar(marketModel: product!, format: getFormat(),count: Int(countTextField.text!)!,canExceed:false){
            tipsLabel.text = "已超過商品數量"
            tipsImage.image = UIImage(named: "Notice")
            addView.isHidden = false
            shopViewTimer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.hideAddView), userInfo: nil, repeats: false)
            return
        }
        bottomSwitch(open:false)
        tipsLabel.text = "已加入購物車"
        tipsImage.image = UIImage(named: "cart_added")
        addView.isHidden = false
        setShopCarsBadge()
        getMaxBuy()
        setCountButtonColor(nowCount: 1)
        shopViewTimer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.hideAddView), userInfo: nil, repeats: false)
    }
    @objc func hideAddView(){
        addView.isHidden = true
    }
    
    @objc func shopCar(){
        self.view.endEditing(true)
        bottomSwitch(open: false)
        self.performSegue(withIdentifier: "next", sender: nil)
    }
    
    func bottomSwitch(open:Bool) {
        if open{
            self.bottonSubView.isHidden = false
            self.buttonWidthCt.constant = self.view.frame.width - 20 - 110
            self.addShopCarWidth.constant = 100
            self.subViewHeightCt.constant = 170 + self.otherHeight
            self.bottomViewHeightCt.constant = 230 + self.otherHeight
            
        }else{
            self.view.endEditing(true)
            if self.isStretch{
                self.bottonSubView.isHidden = true
                self.buttonWidthCt.constant = self.view.frame.width - 20
                self.addShopCarWidth.constant = 0
                self.subViewHeightCt.constant = 0
                self.bottomViewHeightCt.constant = 60
            }
        }
        UIView.animate(withDuration: 0.5, delay: 0.01, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, options: .curveEaseIn, animations: {
            self.view.layoutIfNeeded()
        })
        
        isStretch = open
    }
    
    func getFormat() -> ProductModel.RtnObject.Format?{
        var format1:ProductModel.RtnObject.Format?
        if format.count > 0{
            if normTextField.text == "請選擇"{
                return nil
            }else{
                format1 = format[normPickerView.selectedRow(inComponent: 0)]
            }
        }else{
            format1 = nil
        }
        return format1
    }
    
    @IBAction func goShopCar(_ sender: BorderButton) {
        if self.isStretch{
            if format.count > 0 && normPickerView.selectedRow(inComponent: 0) == 0 && normTextField.text == "請選擇"{
                self.present(AlertViewModel.shared.showAlert(message: "請選擇規格"), animated: true, completion: nil)
                return
            }
            if  normTextField.text == "請選擇" && !normStackView.isHidden{
                
                self.present(AlertViewModel.shared.showAlert(message: "此商品目前無法購買"), animated: true, completion: nil)
                return
            }
            if !RealmShop.shared.saveShopCar(marketModel: product!, format: getFormat(),count: Int(countTextField.text!)!,canExceed:true){
                
            }
            setShopCarsBadge()
            getMaxBuy()
            self.view.endEditing(true)
            bottomSwitch(open:false)
            self.performSegue(withIdentifier: "next", sender: nil)
        }else{
             bottomSwitch(open:true)
        }
    }
    @IBAction func closeBottonView(_ sender: UIButton) {
        bottomSwitch(open:false)
    }
    
    @IBAction func tapScrollView(_ sender: Any) {
        bottomSwitch(open:false)
        self.view.endEditing(true)
    }
    
    
    @objc func keyboardWillShow(note: NSNotification) {
        if isKeyboardShown {
            return
        }
        let keyboardAnimationDetail = note.userInfo as! [String: AnyObject]
        let duration = TimeInterval(truncating: keyboardAnimationDetail[UIResponder.keyboardAnimationDurationUserInfoKey]! as! NSNumber)
        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: -(self.normPickerView.frame.height))
        })
        isKeyboardShown = true
    }
    
    @objc func keyboardWillHide(note: NSNotification) {
        let keyboardAnimationDetail = note.userInfo as! [String: AnyObject]
        let duration = TimeInterval(truncating: keyboardAnimationDetail[UIResponder.keyboardAnimationDurationUserInfoKey]! as! NSNumber)
        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy:  -self.view.frame.origin.y)
        })
        isKeyboardShown = false
    }
    @IBAction func presentScrollImg(_ sender: UIButton) {
        performSegue(withIdentifier: "imgScroll", sender: nil)
    }
    @IBAction func tapImgScroll(_ sender: UITapGestureRecognizer) {
        performSegue(withIdentifier: "imgScroll", sender: nil)
    }
    
}

extension ProductViewController:UIPickerViewDataSource,UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return format.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return format[row].format
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        normTextField.text = format[row].format
        getMaxBuy()
        setCountButtonColor(nowCount:1)
    }
    
}

extension ProductViewController:UIScrollViewDelegate{
    
    func scrollToPage(page:CGFloat){
        var frame = pageScrollView.frame
        frame.origin.x = frame.width * page
        frame.origin.y = 0
        pageControl.currentPage = Int(page)
        pageScrollView.scrollRectToVisible(frame, animated: true)
    }
    
    @objc func scrollViewCarousel(){
        if pageControl.currentPage == imgArr.count-1{
            scrollToPage(page:0)
        }else{
           scrollToPage(page:CGFloat(pageControl.currentPage+1))
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if imgArr.count > 0{
            pageControl.currentPage = self.pageScrollView.getPage()
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        timerSwitch(false)
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        timerSwitch(true)
    }
    
}
