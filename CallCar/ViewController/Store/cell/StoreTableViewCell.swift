//
//  StoreTableViewCell.swift
//  CallCar
//
//  Created by Cablesoft on 2018/12/26.
//  Copyright © 2018 Cablesoft. All rights reserved.
//

import UIKit
import Kingfisher
class StoreTableViewCell: UITableViewCell {
    
    @IBOutlet weak var productImageView: UIImageView!
    
    @IBOutlet weak var goButton: CornerButton!
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var oldPrice: UILabel!
    
    @IBOutlet weak var old2Price: UILabel!
    @IBOutlet weak var newPrice: UILabel!
    @IBOutlet weak var soldOutView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        soldOutView.layer.cornerRadius = 30
        soldOutView.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setCell(product:MarketModel.RtnObject) {
        self.title.text = product.Title1
        if product.Images.count > 0{
            var haveMain = false
            for img in product.Images{
                if img.Main == "1"{
                    self.productImageView.kf.setImage(with:URL(string: img.Url))
                    haveMain = true
                    break
                }
            }
            if !haveMain{
                self.productImageView.kf.setImage(with:URL(string: product.Images[0].Url))
            }
        }
        self.oldPrice.text = "定價:\(product.Price)"
        self.old2Price.text = "優惠價:\(product.SalePrice)"
        self.newPrice.text = "折後價:\(product.SalePrice - product.BonusLimit)"
        soldOutView.isHidden = product.OnFlag != "0"
        
    }
    
}
