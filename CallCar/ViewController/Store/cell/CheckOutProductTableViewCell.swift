//
//  CheckOutProductTableViewCell.swift
//  CallCar
//
//  Created by Cablesoft on 2019/1/24.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import UIKit

class CheckOutProductTableViewCell: UITableViewCell {
    
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var formatName: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
