//
//  ShopCarProductTableViewCell.swift
//  CallCar
//
//  Created by Cablesoft on 2019/1/23.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import UIKit
import Kingfisher
protocol ShopCarProductTableViewCellDelegate {
    func setPriceTotal()
    func setPoint()
}

class ShopCarProductTableViewCell: UITableViewCell {

    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var format: UILabel!
    @IBOutlet weak var clostButton: UIButton!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var addCountButton: UIButton!
    @IBOutlet weak var takeCountButton: UIButton!
    @IBOutlet weak var countTextField: LeftIconTextField!
    
    @IBOutlet weak var canUsePoint: UILabel!
    
    @IBOutlet weak var pointTextField: LeftIconTextField!
    
    var delegate:ShopCarProductTableViewCellDelegate?
    
    var shopModel = RM_ShopCar()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    func setCell(shopCar:RM_ShopCar) {
        shopModel = shopCar
        productImage.kf.setImage(with: URL(string: shopCar.ImageUrl))
        productTitle.text = shopCar.Title1
        if shopCar.Format == ""{
            format.isHidden = true
        }else{
            format.text = shopCar.Format
        }
        price.text = "商品金額:$\(shopCar.SalePrice * shopCar.pCount)"
        countTextField.text = "\(shopCar.pCount)"
        canUsePoint.isHidden = shopCar.BonusLimit == 0
        canUsePoint.text = "【最多可折抵點數\(shopCar.BonusLimit * Int(countTextField.text!)!)點】"
        setCountButtonColor(count:shopCar.pCount)
    }
    
    func setCountButtonColor(count:Int){
        if count == 1{
            takeCountButton.setTitleColor(.gray, for: .normal)
            takeCountButton.isEnabled = false
        }else{
            takeCountButton.setTitleColor(UIColor(hex: 0xF76B1C), for: .normal)
            takeCountButton.isEnabled = true
        }
        
        if count == shopModel.MaxBuy{
            addCountButton.setTitleColor(.gray, for: .normal)
            addCountButton.isEnabled = false
        }else{
            addCountButton.setTitleColor(UIColor(hex: 0xF76B1C), for: .normal)
            addCountButton.isEnabled = true
        }
    }
    
    @IBAction func addCount(_ sender: UIButton) {
        var count = Int(countTextField.text!)!
        if count == shopModel.MaxBuy{
            return
        }
        count = count + 1
        setCountButtonColor(count:count)
        countTextField.text = "\(count)"
        price.text = "商品金額:$\(shopModel.SalePrice * count)"
        canUsePoint.text = "【最多可折抵點數\(shopModel.BonusLimit * count)點】"
        RealmShop.shared.updateCount(shopCar: shopModel, count: count)
        delegate?.setPriceTotal()
    }
    
    @IBAction func takeCount(_ sender: UIButton) {
        var count = Int(countTextField.text!)!
        if count == 1{
            return
        }
        count = count - 1
        setCountButtonColor(count:count)
        countTextField.text = "\(count)"
        price.text = "商品金額:$\(shopModel.SalePrice * count)"
        canUsePoint.text = "【最多可折抵點數\(shopModel.BonusLimit * count)點】"
        let maxPoint = shopModel.BonusLimit * count
        if let point = Int(pointTextField.text!){
            if point > maxPoint{
                pointTextField.text = "\(maxPoint)"
                RealmShop.shared.updateShopCarBonus(shopCar: shopModel, bonus: maxPoint)
            }
        }
        RealmShop.shared.updateCount(shopCar: shopModel, count: count)
        delegate?.setPriceTotal()
    }
    
    @IBAction func editingTextField(_ sender: LeftIconTextField) {
        
        let rm_shopCar = RealmShop.shared.getShopCars()
        var sb = 0
        for shopCar in rm_shopCar{
            if shopCar.FormatId != shopModel.FormatId{
                sb = sb + shopCar.shopCarBonus
            }
        }
        let myPoint = Int(UserDefaults.standard.getString(key: UserInfo.bonus.rawValue))! - sb
        let maxPoint = shopModel.pCount * shopModel.BonusLimit
        if sender.text == ""{
            RealmShop.shared.updateShopCarBonus(shopCar: shopModel, bonus: 0)
            delegate?.setPriceTotal()
            return
        }
        if sender.text == "0" || sender.text == "00"{
            
            sender.text = ""
            RealmShop.shared.updateShopCarBonus(shopCar: shopModel, bonus: 0)
            delegate?.setPriceTotal()
            return
        }
        if let point = Int(sender.text!){
            if maxPoint > myPoint{
                if point > myPoint{
                    sender.text = "\(myPoint)"
                }
            }else{
                if point > maxPoint{
                    sender.text = "\(maxPoint)"
                }
            }
            RealmShop.shared.updateShopCarBonus(shopCar: shopModel, bonus: Int(sender.text!)!)
            delegate?.setPriceTotal()
        }
    }
    
    
}
