//
//  RecordTableViewCell.swift
//  CallCar
//
//  Created by Cablesoft on 2019/1/31.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import UIKit
import Kingfisher

class RecordTableViewCell: UITableViewCell {

    @IBOutlet weak var productImage: UIImageView!
    
    @IBOutlet weak var productTitle: UILabel!
    
    @IBOutlet weak var productFormat: UILabel!
    
    @IBOutlet weak var productCount: UILabel!
    
    @IBOutlet weak var productPrice: UILabel!
    
    @IBOutlet weak var limtCount: UILabel!
    @IBOutlet weak var shopCarCount: UILabel!
    
    @IBOutlet weak var shopCarPrice: UILabel!
    
    @IBOutlet weak var marketId: UILabel!
    
    @IBOutlet weak var buyDate: UILabel!
    
    @IBOutlet weak var limitViewHeight: NSLayoutConstraint!
    @IBOutlet weak var button: CornerButton!
    
    @IBOutlet weak var limitView: UIView!
    
    @IBOutlet weak var orderStatusButton: BorderButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setCell(shopHs:RM_ShopHs) {
        productImage.kf.setImage(with:URL(string: shopHs.odetail[0].img))
        if shopHs.ostatus == "0"{
            orderStatusButton.borderColor = UIColor(hex: 0x14729A)
            orderStatusButton.setTitle("訂單已成立", for: .normal)
            orderStatusButton.setTitleColor(UIColor(hex: 0x14729A), for: .normal)
            orderStatusButton.backgroundColor = .white
        }else{
            orderStatusButton.borderColor = UIColor(hex: 0x7F7F7F)
            orderStatusButton.setTitle("已取消", for: .normal)
            orderStatusButton.setTitleColor(.white, for: .normal)
            orderStatusButton.backgroundColor = UIColor(hex: 0x7F7F7F)
        }
        productTitle.text = shopHs.odetail[0].title
        productFormat.text = shopHs.odetail[0].format
        productFormat.isHidden = shopHs.odetail[0].format == ""
        productCount.text = "x\(shopHs.odetail[0].amt)"
        productPrice.text = "$\(shopHs.odetail[0].totalp)"
        if shopHs.odetail.count == 1{
            limitView.isHidden = true
            limitViewHeight.constant = 0
        }else{
            limitView.isHidden = false
            limitViewHeight.constant = 30
            limtCount.text = "還有\(shopHs.odetail.count-1)個商品"
        }
        shopCarCount.text = "共\(shopHs.odetail.count)個商品"
        shopCarPrice.text = "$\(shopHs.payamt)"
        
//        var orderstatus = ""
//        for process in shopHs.process{
//            if shopHs.oprocess == "\(process.Seq)"{
//                orderstatus = "【\(process.Stage)】"
//                break
//            }
//        }
//        if shopHs.ostatus == "X"{
//            orderstatus = "【已取消】"
//        }
        marketId.text = "訂單編號:\(shopHs.ono)"
        buyDate.text = "訂購日期:\(shopHs.otime.replacingOccurrences(of: "T", with: " "))"
    }
}
