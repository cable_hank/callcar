//
//  ShopOrderTableViewCell.swift
//  CallCar
//
//  Created by Cablesoft on 2019/2/12.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import UIKit

class ShopOrderTableViewCell: UITableViewCell {

    @IBOutlet weak var shopOrderImage: UIImageView!
    @IBOutlet weak var shopOrderTitle: UILabel!
    @IBOutlet weak var shopOrderFormat: UILabel!
    @IBOutlet weak var shopOrderCount: UILabel!
    @IBOutlet weak var shopOrderPrice: UILabel!
    @IBOutlet weak var usePoint: UILabel!
    @IBOutlet weak var ststus: BorderCornerButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(rmOdetail:rm_odetail,rmProcess:rm_process) {
        shopOrderTitle.text = rmOdetail.title
        shopOrderFormat.text = rmOdetail.format
        shopOrderCount.text = "x\(rmOdetail.amt)"
        shopOrderPrice.text = "商品金額:$\(rmOdetail.totalp)"
        usePoint.text = "點數折抵:\(rmOdetail.damt)"
        shopOrderImage.downloaded(from: rmOdetail.img)
        if let process = Int(rmOdetail.process){
            ststus.isHidden = process <= 20
            ststus.setTitle(rmProcess.Stage, for: .normal)
            if process == 30{
                ststus.backgroundColor = UIColor(hex: 0x14729A)
            }else{
                ststus.backgroundColor = UIColor(hex: 0xF97B62)
            }
            
            
        }
        
        
    }

}
