//
//  VideoWebViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2019/1/16.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import UIKit
class VideoWebViewController: UIViewController,UIWebViewDelegate {
    
    var urlString = ""
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var acView: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "商品影片"
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        loadYoutube(videoID: urlString)
        acView.startAnimating()
    }
    
    func loadYoutube(videoID:String) {
        guard let youtubeURL = URL(string: videoID) else { return }
        webView.loadRequest( URLRequest(url: youtubeURL) )
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews();
        UIApplication.shared.isStatusBarHidden = false
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        print("webViewDidFinishLoad")
        acView.stopAnimating()
    }
    
}
