//
//  CheckAlertViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/10/23.
//  Copyright © 2018 Cablesoft. All rights reserved.
//

import UIKit

protocol checkAlertDelegate {
    func checkAlertSubmit()
}

class CheckAlertViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var carInfoTitle: UILabel!
    @IBOutlet weak var startTItle: UILabel!
    @IBOutlet weak var endTitle: UILabel!
    @IBOutlet weak var flightInfoTitle: UILabel!
    @IBOutlet weak var peopleCountTitle: UILabel!
    @IBOutlet weak var bagCountTitle: UILabel!
    @IBOutlet weak var nameTitle: UILabel!
    @IBOutlet weak var phoneTitle: UILabel!
    @IBOutlet weak var priceTitle: UILabel!
    @IBOutlet weak var carInfo: UILabel!
    @IBOutlet weak var start: UILabel!
    @IBOutlet weak var end: UILabel!
    @IBOutlet weak var flightInfo: UILabel!
    @IBOutlet weak var peopleCount: UILabel!
    @IBOutlet weak var bagCount: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var vanTypeStackView: UIStackView!
    @IBOutlet weak var vanTypeLabel: UILabel!
    
    var delegate:checkAlertDelegate?
    var orderModel = OrderModel()
    var isCheck = false
    var dno = ""
    var color = UIColor(hex: 0x55C1EA)
    var isGo = false
    var isJp = false
    var country = "TW"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isJp{
            country = "JP"
        }else{
            country = "TW"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print(orderModel.getParameters())
        if orderModel.cpflag == "S"{
            titleLabel.text = "共乘訂單確認"
            priceTitle.text = "共乘價格試算"
            color = UIColor(hex: 0x20B6FC)
            vanTypeStackView.isHidden = true
            
        }else if orderModel.cpflag == "P"{
            titleLabel.text = "專接訂單確認"
            priceTitle.text = "專接價格"
            color = UIColor(hex: 0x3575C0)
            vanTypeStackView.isHidden = true
        }else{
            if isJp{
                titleLabel.text = "日本共乘訂單確認"
                priceTitle.text = "日本共乘價格"
                color = UIColor(hex: 0xFC989F)
                vanTypeStackView.isHidden = false
                if dno == ""{
                    vanTypeLabel.text = "發起"
                }else{
                    vanTypeLabel.text = "併車"
                }
            }else{
                titleLabel.text = "TaiVAN大共乘訂單確認"
                priceTitle.text = "TaiVAN大共乘價格"
                color = UIColor(hex: 0xF5A623)
                vanTypeStackView.isHidden = false
                if dno == ""{
                    vanTypeLabel.text = "發起"
                }else{
                    vanTypeLabel.text = "併車"
                }
            }
        }
        
        topView.backgroundColor = color
        carInfoTitle.textColor = color
        startTItle.textColor = color
        endTitle.textColor = color
        flightInfoTitle.textColor = color
        peopleCountTitle.textColor = color
        bagCountTitle.textColor = color
        nameTitle.textColor = color
        phoneTitle.textColor = color
        priceTitle.textColor = color
        carInfo.text = "\(toDateFormaaterSlash(dateString: orderModel.sdate)) \(toTimeFormaater(timeString: orderModel.ptime)) \(orderModel.cartype)"
        start.text = orderModel.paddress
        end.text = orderModel.taddress
        flightInfo.text = "\(toDateFormaaterSlash(dateString: orderModel.fdate)) \(toTimeFormaater(timeString: orderModel.ftime)) \(orderModel.fno)"
//        flightInfo.text = orderModel.fno
        peopleCount.text = "\(orderModel.pcnt)"
        bagCount.text = "\(orderModel.bcnt)"
        name.text = orderModel.pname
        phone.text = orderModel.pmobile
        price.text = "\(orderModel.price)"
    }
    
    @IBAction func check(_ sender: UIButton) {
        isCheck = !isCheck
        if isCheck{
            okButton.backgroundColor = color
        }else{
            okButton.backgroundColor = UIColor(hex: 0x929292)
        }
        sender.isSelected = isCheck
    }
    
    @IBAction func service(_ sender: UIButton) {
        var urlString = ""
        if orderModel.cpflag == "S"{
            urlString = "https://www.callcar.com.tw/service-carpool.html#carpool-faq"
        }else if orderModel.cpflag == "P"{
            urlString = "https://www.callcar.com.tw/service-vip.html#vip-faq"
        }else{
            urlString = "https://www.callcar.com.tw/service-vanpool.html#vanpool-faq"
        }
        let url = URL(string: urlString)!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func privacy(_ sender: UIButton) {
        let url = URL(string: "https://www.callcar.com.tw/policy.html")!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func ok(_ sender: UIButton) {
        if isCheck{
            if !isGo{
                isGo = true
                if dno != "" && orderModel.cpflag != "P"{
                    orderModel.pcartype = "1"
                    var parameters = orderModel.getPickParameters()
                    parameters["country"] = country
                    print(parameters)
                    CallCarAPI.shared.addPickOrder(parameters: ["data":convertDictionaryToString(parameters),"dno":dno,"time":orderModel.ptime]) { (success, res, error,mss,sCode) in
                        if sCode >= 900{
                            self.goRoot()
                            return
                        }
                        if error != nil {
                            self.isGo = false
                            self.present(AlertViewModel.shared.showAlert(message: "請檢察網路狀態"), animated: true, completion: nil)
                            return
                        }
                        //                    self.submitButton.isEnabled = true
                        if success {
                            if self.orderModel.stype == "I" && self.orderModel.cpflag == "S"{
                                let dateTime = "\(self.orderModel.sdate)\(self.orderModel.ptime)"
                                UserNotificationTools().addNotification(identifier: res,dateTime: dateTime, badgeInt: UIApplication.shared.applicationIconBadgeNumber)
                            }
                            if UserDefaults.standard.string(forKey: UserInfo.isAlertStoreKit.rawValue) == nil{
                                UserDefaults.standard.set("false", forKey: UserInfo.isAlertStoreKit.rawValue)
                            }
                            self.dismiss(animated: false, completion: nil)
                            self.delegate?.checkAlertSubmit()
                        }else{
                            self.isGo = false
                            self.present(AlertViewModel.shared.showAlert(message: mss), animated: true, completion: nil)
                        }
                    }
                }else{
                    orderModel.pcartype = "0"
                    var parameters = orderModel.getParameters()
                    parameters["country"] = country
                    print(parameters)
                    CallCarAPI.shared.addOrder(parameters: parameters) { (success, res, error,mss,sCode) in
                        if sCode >= 900{
                            self.goRoot()
                            return
                        }
                        if error != nil {
                            self.isGo = false
                            self.present(AlertViewModel.shared.showAlert(message: "請檢察網路狀態"), animated: true, completion: nil)
                            return
                        }
                        //                    self.submitButton.isEnabled = true
                        if success {
                            if self.orderModel.stype == "I" && self.orderModel.cpflag == "S"{
                                let dateTime = "\(self.orderModel.sdate)\(self.orderModel.ptime)"
                                UserNotificationTools().addNotification(identifier: res,dateTime: dateTime, badgeInt: UIApplication.shared.applicationIconBadgeNumber)
                            }
                            if UserDefaults.standard.string(forKey: UserInfo.isAlertStoreKit.rawValue) == nil{
                                UserDefaults.standard.set("false", forKey: UserInfo.isAlertStoreKit.rawValue)
                            }
                            self.dismiss(animated: false, completion: nil)
                            self.delegate?.checkAlertSubmit()
                        }else{
                            self.isGo = false
                            self.present(AlertViewModel.shared.showAlert(message: mss), animated: true, completion: nil)
                        }
                    }
                }
            }
        }else{
            self.present(AlertViewModel.shared.showAlert(message: "請確認服務規定及隱私權政策"), animated: true, completion: nil)
        }
            
    }
    
    
    
}
