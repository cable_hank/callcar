//
//  VipDiscriptionViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2019/5/27.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import UIKit

class VipDiscriptionViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func urlButton(_ sender: Any) {
        let url = URL(string: "https://www.callcar.com.tw/service-vip.html#vip-faq")!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    
    @IBAction func cancel(_ sender: CornerButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
