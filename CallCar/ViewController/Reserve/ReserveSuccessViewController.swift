//
//  ReserveSuccessViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/7.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit

class ReserveSuccessViewController: UIViewController {
    
    var orderModel = OrderModel()
    
    @IBOutlet weak var addrLabel: UILabel!
    @IBOutlet weak var addAddrButton: BorderCornerButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "預約成功"
        navigationItem.hidesBackButton = true
    }

    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        if orderModel.stype == "O"{
            addrLabel.text = orderModel.paddress
        }else{
            addrLabel.text = orderModel.taddress
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UserDefaults.standard.setValue("0", forKey: UserInfo.statusColor.rawValue)
        if #available(iOS 13.0, *) {
            UIApplication.shared.statusBarStyle = .darkContent
        } else {
            UIApplication.shared.statusBarStyle = .default
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.navigationController?.popToRootViewController(animated: false)
    }
    
    @IBAction func addAddr(_ sender: BorderCornerButton) {
        if orderModel.stype == "O"{
            print(orderModel.paddress)
            RealmDB.shared.saveAddr(addr: orderModel.paddress, lat: orderModel.plat, lng: orderModel.plng)
        }else{
            print(orderModel.taddress)
            RealmDB.shared.saveAddr(addr: orderModel.taddress, lat: orderModel.tlat, lng: orderModel.tlng)
        }
        addAddrButton.isEnabled = false
        addAddrButton.backgroundColor = UIColor(hex: 0x14729A)
        addAddrButton.setTitleColor(.white, for: .normal)
        addAddrButton.setTitle("已加入常用地址", for: .normal)
    }
    
    @IBAction func goHome(_ sender: BorderCornerButton) {
        self.navigationController?.popToRootViewController(animated: true)?.first?.navigationController?.popViewController(animated: true)
    }
    @IBAction func goToFAQ(_ sender: UIButton) {
        var url = URL(string: "https://www.callcar.com.tw/service-carpool.html#carpool-faq")!
        if orderModel.cpflag == "P"{
            url = URL(string: "https://www.callcar.com.tw/service-vip.html#vip-faq")!
        }
        if orderModel.cpflag == "Ｖ"{
            url = URL(string: "https://www.callcar.com.tw/service-vanpool.html#vanpool-faq")!
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
}
