//
//  NewSetAddrViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2019/11/4.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import UIKit
import GooglePlaces
protocol NewSendAddrDelegate {
    func addr(_ position:CLLocationCoordinate2D,addr:String)
}
class NewSetAddrViewController: UIViewController {
    
    var addrArr = [RM_AddrModel]()
    var searchBar = UISearchBar()
    var resultView: UITextView?
    var fetcher: GMSAutocompleteFetcher?
    var text = ""
    var predictions: [GMSAutocompletePrediction] = []
    var addrDelegate:NewSendAddrDelegate?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addrFetcherView: UIView!
    @IBOutlet weak var addrFetcherTableView: UITableView!
    @IBOutlet weak var noSearchLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            tableView.contentInsetAdjustmentBehavior = .never
        } else {
            self.automaticallyAdjustsScrollViewInsets = false
        }
        print("isNew")
        // Put the search bar in the navigation bar.
        searchBar.delegate = self
        searchBar.sizeToFit()
        searchBar.placeholder = "請輸入地址"
        searchBar.text = text
        searchBar.enablesReturnKeyAutomatically = true
//        searchBar.setValue("取消", forKey:"_cancelButtonText")
        navigationItem.titleView = searchBar
        fetcher = GMSAutocompleteFetcher(bounds: nil, filter: nil)
        fetcher?.delegate = self
        
        definesPresentationContext = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        addrArr = RealmDB.shared.getAddr()
        tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
}

// Handle the user's selection.
extension NewSetAddrViewController:UISearchBarDelegate,GMSAutocompleteFetcherDelegate{
    func didFailAutocompleteWithError(_ error: Error) {
        
    }
    
    
    func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {
        self.predictions = predictions
        print(self.predictions.count)
        noSearchLabel.isHidden = self.predictions.count != 0
        addrFetcherTableView.isHidden = self.predictions.count == 0
        noSearchLabel.text = "沒有與「\(searchBar.text!)」相符的搜尋結果"
        addrFetcherTableView.reloadData()
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        addrFetcherView.isHidden = true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        noSearchLabel.isHidden = true
        addrFetcherView.isHidden = false
        fetcher?.sourceTextHasChanged(searchBar.text)
    }
    
    
    
    
    
}

extension NewSetAddrViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == addrFetcherTableView{
            return predictions.count
        }else{
            return addrArr.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == addrFetcherTableView{
            let cell = tableView.dequeueReusableCell(withIdentifier: "fetcher")! as! AddrFetcherTableViewCell
            cell.titleLabel.text = predictions[indexPath.row].attributedFullText.string
            cell.subTitleLabel.text = predictions[indexPath.row].attributedPrimaryText.string
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "oldAddrCell")! as! AddrTableViewCell
            cell.addrLabel.text = addrArr[indexPath.row].addr
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if tableView == addrFetcherTableView{
            return nil
        }
        return "常用地址"
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == addrFetcherTableView{
            
            let placeClient = GMSPlacesClient()
            placeClient.lookUpPlaceID(predictions[indexPath.row].placeID) { (place, error) in
                if let place = place{
                    if self.predictions[indexPath.row].attributedFullText.string.prefix(2) == "台灣"{
                        self.addrDelegate?.addr(place.coordinate,addr:String(self.predictions[indexPath.row].attributedFullText.string.dropFirst().dropFirst()))
                    }else{
                         self.addrDelegate?.addr(place.coordinate,addr:self.predictions[indexPath.row].attributedFullText.string)
                    }
                   
                }
                self.dismiss(animated: true, completion: nil)
            }
        }else{
            let location = CLLocationCoordinate2D(latitude: addrArr[indexPath.row].lat, longitude: addrArr[indexPath.row].lng)
            addrDelegate?.addr(location,addr:"")
            dismiss(animated: true, completion: nil)
        }
    }
    
}
