//
//  ReserveCarInfoViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/22.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit
import SwiftyJSON
class ReserveCarInfoViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noteTextView: UITextView!
    
    @IBOutlet weak var flighiNoLabel: UILabel!
    @IBOutlet weak var terminalLabel: UILabel!
    @IBOutlet weak var startCityLabel: UILabel!
    @IBOutlet weak var endCityLabel: UILabel!
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var endTimeLabel: UILabel!
    
    var timeList = [CarTimeModel]()
    var orderModel = OrderModel()
    var flightCardInfo = [String]()
    
    
    var timerForShowScrollIndicator: Timer?
    var selectIndexPath:IndexPath?
    var dno = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.separatorStyle = .none
        
        self.navigationItem.title = "選擇上車時間"
        let backButton = UIBarButtonItem()
        backButton.title = " "
        backButton.tintColor = .white
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        noteTextView.layer.cornerRadius = 10
    }

    override func viewWillAppear(_ animated: Bool) {
        selectIndexPath = nil
        if flightCardInfo.count > 0{
            flighiNoLabel.text = flightCardInfo[0]
            terminalLabel.text = flightCardInfo[1]
            startCityLabel.text = flightCardInfo[2]
            endCityLabel.text = flightCardInfo[3]
            startTimeLabel.text = self.toTimeFormaater(timeString: flightCardInfo[4])
            endTimeLabel.text = self.toTimeFormaater(timeString: flightCardInfo[5])
        }
        print(timeList.count)
        tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        startTimerForShowScrollIndicator()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        stopTimerForShowScrollIndicator() 
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func selectRadio(_ sender:UIButton) {
        print(sender.tag)
        
    }

    @IBAction func next(_ sender: BorderCornerButton) {
        if orderModel.sdate == "" || orderModel.ptime == "" || selectIndexPath == nil{
            self.present(AlertViewModel.shared.showAlert(message: "請選擇車班時間"), animated: true, completion: nil)
            return
        }
        if orderModel.stype == "I"{
            let fdt = "\(orderModel.fdate)\(orderModel.ftime)"
            let sdt = "\(orderModel.sdate)\(orderModel.ptime)"
            print("sdt:\(sdt) fdt:\(fdt)")
            let format = DateFormatter()
            format.dateFormat = "yyyyMMddHHmm"
            let dfdt = format.date(from: fdt)!
            let dsdt = format.date(from: sdt)!
            if dfdt + 90 * 60 > dsdt{
                let alertController = UIAlertController(title: "風險提示", message: "依據媒合時間，共乘服務最多等候5分鐘為限，請保留足夠緩衝時間，我們將不承擔航班延誤所產生之搭乘權益受損。", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "確定預約", style: .default) { (alert) in
                    self.performSegue(withIdentifier: "goCheck", sender: nil)
                }
                let cancelAction = UIAlertAction(title: "取消預約", style: .cancel, handler: nil)
                alertController.addAction(okAction)
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion: nil)
                return
            }
        }else{
            let fdt = "\(orderModel.fdate)\(orderModel.ftime)"
            let sdt = "\(orderModel.sdate)\(orderModel.ptime)"
            print("sdt:\(sdt) fdt:\(fdt)")
            let format = DateFormatter()
            format.dateFormat = "yyyyMMddHHmm"
            let dfdt = format.date(from: fdt)!
            let dsdt = format.date(from: sdt)!
            print("dsdt:\(dsdt + 180 * 60) dfdt:\(dfdt)")
            if dsdt + 180 * 60 > dfdt{
                let alertController = UIAlertController(title: "風險提示", message: "共乘服務採期望區間預約，服務前三日將會提供指定上車時間，請保留足夠前往機場交通緩衝時間，我們將不承擔因交通時間不足所產生之搭乘權益受損。", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "確定預約", style: .default) { (alert) in
                    self.performSegue(withIdentifier: "goCheck", sender: nil)
                }
                let cancelAction = UIAlertAction(title: "取消預約", style: .cancel, handler: nil)
                alertController.addAction(okAction)
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion: nil)
                return
            }
        }
        self.performSegue(withIdentifier: "goCheck", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goCheck"{
            let vc = segue.destination as! CheckOutViewController
            vc.orderModel = orderModel
            vc.dno = dno
        }
        
    }
    
    /// Show always scroll indicator in table view
    @objc func showScrollIndicatorsInContacts() {
        UIView.animate(withDuration: 0.001) {
            self.tableView.flashScrollIndicators()
        }
    }
    
    /// Start timer for always show scroll indicator in table view
    func startTimerForShowScrollIndicator() {
        self.timerForShowScrollIndicator = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.showScrollIndicatorsInContacts), userInfo: nil, repeats: true)
    }
    
    /// Stop timer for always show scroll indicator in table view
    func stopTimerForShowScrollIndicator() {
        self.timerForShowScrollIndicator?.invalidate()
        self.timerForShowScrollIndicator = nil
    }
    
    
}

extension ReserveCarInfoViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return timeList.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if timeList[indexPath.row].dno == ""{
            let cell = tableView.dequeueReusableCell(withIdentifier: "carTimeCell")! as! CarTimeTableViewCell
            cell.timeLabel.text = self.toTimeFormaater(timeString: timeList[indexPath.row].carTime)
            cell.dateLabel.text = self.toDateFormaater(dateString: timeList[indexPath.row].carDate)
            if let _ =  self.selectIndexPath, indexPath.row == selectIndexPath?.row {
                cell.radioButton.isSelected = true
                cell.carImg.alpha = 1
                cell.timeLabel.font = UIFont.systemFont(ofSize: 20)
                cell.dateLabel.font = UIFont.systemFont(ofSize: 16)
                cell.carWidthConstraint.constant = 40
                cell.cornerBackgroundView.backgroundColor = UIColor(hex: 0xF7F7F7)
                orderModel.sdate = timeList[indexPath.row].carDate
                orderModel.ptime = timeList[indexPath.row].carTime
                dno = timeList[indexPath.row].dno
            } else {
                cell.radioButton.isSelected = false
                cell.carImg.alpha = 0.5
                cell.timeLabel.font = UIFont.systemFont(ofSize: 18)
                cell.dateLabel.font = UIFont.systemFont(ofSize: 14)
                cell.carWidthConstraint.constant = 30
                cell.cornerBackgroundView.backgroundColor = .white
                
            }
            
            cell.radioButton.tag = indexPath.row
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "pickCarTimeCell")! as! PickCarTimeTableViewCell
            cell.timeLabel.text = self.toTimeFormaater(timeString: timeList[indexPath.row].carTime)
            cell.dateLabel.text = self.toDateFormaater(dateString: timeList[indexPath.row].carDate)
            cell.bagCount.text = timeList[indexPath.row].rbcnt
            cell.personCount.text = timeList[indexPath.row].rpcnt
            cell.stopPlace.text = "\(timeList[indexPath.row].city)\(timeList[indexPath.row].distinct)"
            if timeList[indexPath.row].carType == "四人座"{
                cell.carSeat.text = "四人座"
                cell.carImg.image = UIImage(named: "carpool_four")
            }else{
                cell.carSeat.text = "七人座"
                cell.carImg.image = UIImage(named: "carpool_seven")
            }
            if let _ =  self.selectIndexPath, indexPath.row == selectIndexPath?.row {
                cell.radioButton.isSelected = true
                cell.carImg.alpha = 1
                cell.timeLabel.font = UIFont.systemFont(ofSize: 20)
                cell.dateLabel.font = UIFont.systemFont(ofSize: 16)
                cell.cornerView.borderColor = UIColor(hex: 0x20B6FC)
                orderModel.sdate = timeList[indexPath.row].carDate
                orderModel.ptime = timeList[indexPath.row].carTime
                dno = timeList[indexPath.row].dno
            } else {
                cell.radioButton.isSelected = false
                cell.carImg.alpha = 0.5
                cell.timeLabel.font = UIFont.systemFont(ofSize: 18)
                cell.dateLabel.font = UIFont.systemFont(ofSize: 14)
                cell.cornerView.borderColor = UIColor(hex: 0xCCCCCC)
                
            }
            
            cell.radioButton.tag = indexPath.row
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectIndexPath = indexPath
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if timeList[indexPath.row].dno == ""{
            return 60
        }else{
            return 80
        }
    }
}

