//
//  ReserveDetailViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/7.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit
import SwiftyJSON
class ReserveDetailViewController: UIViewController {
    @IBOutlet weak var personCountTextField: LeftIconTextField!
    @IBOutlet weak var baggageCountTextField: LeftIconTextField!
    @IBOutlet weak var onCarDateTextField: LeftIconTextField!
    @IBOutlet weak var flightDateTextField: LeftIconTextField!
    @IBOutlet weak var flightNoTextField: LeftIconTextField!
    
    @IBOutlet weak var flightCardView: FlightCardView!
    @IBOutlet weak var errorFlightNoMssLabel: UILabel!
    
    @IBOutlet weak var flighiNoLabel: UILabel!
    @IBOutlet weak var terminalLabel: UILabel!
    @IBOutlet weak var startCityLabel: UILabel!
    @IBOutlet weak var endCityLabel: UILabel!
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var endTimeLabel: UILabel!
    @IBOutlet weak var loadActivity: UIActivityIndicatorView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var errorImg: UIImageView!
    
    var personCountPickerView = UIPickerView()
    var baggagePickerView = UIPickerView()
    var onCarDatePicker = UIDatePicker()
    var flightDatePicker = UIDatePicker()
    
    
    let personCountArray = ["1","2","3","4","5","6","7"]
    let baggageCountArray = ["1","2","3","4","5","6","7"]
    var flightCardInfo = ["","","","","","","",""]
    let formatter = DateFormatter()
    let formatterForApi = DateFormatter()
    var orderModel = OrderModel()
    var timeList = [CarTimeModel]()
    var isFlightNoOk = false
    var timer = Timer()
    var counter = 0.0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = UIColor(hex: 0x20B6FC)
        textView.layer.cornerRadius = 10
        
        personCountPickerView.dataSource = self
        personCountPickerView.delegate = self
        baggagePickerView.dataSource = self
        baggagePickerView.delegate = self
        personCountPickerView.tag = 0
        baggagePickerView.tag = 1
        onCarDatePicker.tag = 0
        flightDatePicker.tag = 1
        
//        personCountTextField.backgroundColor = .clear
//        baggageCountTextField.backgroundColor = .clear
        
        onCarDatePicker.datePickerMode = .date
        flightDatePicker.datePickerMode = .date
        onCarDatePicker.addTarget(self,action: #selector(self.datePickerChanged),for: .valueChanged)
        flightDatePicker.addTarget(self,action: #selector(self.datePickerChanged),for: .valueChanged)
        formatter.dateFormat = "yyyy/MM/dd"
        formatterForApi.dateFormat = "yyyyMMdd"
        
        personCountTextField.inputView = personCountPickerView
        baggageCountTextField.inputView = baggagePickerView
        onCarDateTextField.inputView = onCarDatePicker
        flightDateTextField.inputView = flightDatePicker
        
        let backButton = UIBarButtonItem()
        backButton.title = " "
        backButton.tintColor = .white
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil)
    }

    func addDay(day:Int,date:Date) -> Date{
        return Calendar.current.date(byAdding: Calendar.Component.day, value: day, to: date)!
    }
    
    func changeDatePicker(date:Date){
        if orderModel.stype == "O"{
            flightDatePicker.minimumDate = date
            flightDatePicker.maximumDate = addDay(day: 1, date: date)
        }else{
            flightDatePicker.minimumDate = addDay(day: -1, date: date)
            flightDatePicker.maximumDate = date
        }
        flightDatePicker.date = date
        flightDateTextField.text = formatter.string(from: date)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.barTintColor = UIColor(hex: 0x20B6FC)
        if orderModel.stype == "I"{
            self.title = "預約共乘(往市區)"
        }else{
            self.title = "預約共乘(往機場)"
        }
        UIApplication.shared.statusBarStyle = .lightContent
        
        UserDefaults.standard.setValue("1", forKey: UserInfo.statusColor.rawValue)
        self.tabBarController?.tabBar.isHidden = true
        onCarDatePicker.minimumDate = Date()
//        onCarDatePicker.minimumDate = Date(timeIntervalSinceNow: 60*60*24*5)
        onCarDatePicker.maximumDate = Date(timeIntervalSinceNow: 60*60*24*90)
        
        changeDatePicker(date:onCarDatePicker.date)
        
        
        onCarDateTextField.text = formatter.string(from: onCarDatePicker.date)
        if orderModel.stype == "O"{
            print("出境")
        }else if orderModel.stype == "I"{
            print("入境")
        }
        if orderModel.cpflag == "S"{
            print("共乘")
        }else if orderModel.cpflag == "P"{
            print("專接")
        }
        print("pcity:\(orderModel.pcity)")
        print("pdistinct:\(orderModel.pdistinct)")
        print("pvillage:\(orderModel.pvillage)")
        print("paddress:\(orderModel.paddress)")
        print("plat:\(orderModel.plat)")
        print("plng:\(orderModel.plng)")
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func datePickerChanged(datePicker:UIDatePicker) {
        print("datePicker.tag \(datePicker.tag)")
        if datePicker.tag == 0{
            onCarDateTextField.text = formatter.string(from: datePicker.date)
            changeDatePicker(date:datePicker.date)
        }else{
            flightDateTextField.text = formatter.string(from: datePicker.date)
        }
    }
    
    
    @IBAction func editFlightNo(_ sender: LeftIconTextField) {
//        print(sender.text!)
        sender.text = sender.text?.uppercased()
//        print(formatter.string(from: flightDatePicker.date))
        if sender.text != ""{
            playActivity(isPlay: true)
            self.errorImg.isHidden = true
            self.flightCardView.isHidden = true
            self.isFlightNoOk = false
            timer.invalidate()
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.getFlightInfo), userInfo: nil, repeats: false)
        }
    }
    
    @IBAction func editFlightDate(_ sender: LeftIconTextField) {
        playActivity(isPlay: true)
        DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
            self.getFlightInfo()
        }
        
    }
    
    func playActivity(isPlay:Bool){
        if isPlay{
            loadActivity.startAnimating()
        }else{
            loadActivity.stopAnimating()
        }
    }
    
    @objc func getFlightInfo() {
        if flightNoTextField.text!.count < 3{
            self.errorImg.isHidden = false
            self.errorImg.blink()
            self.flightCardView.isHidden = true
            self.isFlightNoOk = false
            playActivity(isPlay: false)
            return
        }
        isFlightNoOk = false
        flightCardView.isHidden = true
        errorImg.isHidden = true
        CallCarAPI.shared.checkFlight(parameters: ["no":flightNoTextField.text!,"date":formatterForApi.string(from: flightDatePicker.date),"type":orderModel.stype], success: { (success, res,sCode) in
            if sCode >= 900{
                self.goRoot()
                return
            }
//            print(res)
            if success{
                print(self.orderModel.airport)
                if res.count == 0{
                    self.playActivity(isPlay: false)
                    self.errorImg.isHidden = false
                    self.errorImg.blink()
                    self.flightCardView.isHidden = true
                    self.isFlightNoOk = false
                    return
                }else if res[0]["da"].stringValue != self.orderModel.airport && res[0]["aa"].stringValue != self.orderModel.airport{
                    
                    self.playActivity(isPlay: false)
                    self.errorImg.isHidden = false
                    self.errorImg.blink()
                    self.flightCardView.isHidden = true
                    self.isFlightNoOk = false
                    return
                }else{
                    var airportAddr = ""
                    if self.orderModel.airport == "TPE"{
                        if res[0]["ter"].stringValue == "1"{
                            self.terminalLabel.text = "第一航廈"
                            self.orderModel.terminal = "T1"
                            self.flightCardInfo[1] = "第一航廈"
                            airportAddr = "桃園國際機場 第一航廈"
                        }else if res[0]["ter"].stringValue == "2"{
                            self.terminalLabel.text = "第二航廈"
                            self.orderModel.terminal = "T2"
                            self.flightCardInfo[1] = "第二航廈"
                            airportAddr = "桃園國際機場 第二航廈"
                        }else if res[0]["ter"].stringValue == "3"{
                            self.terminalLabel.text = "第三航廈"
                            self.orderModel.terminal = "T3"
                            self.flightCardInfo[1] = "第三航廈"
                            airportAddr = "桃園國際機場 第三航廈"
                        }else{
                            self.terminalLabel.text = ""
                            self.orderModel.terminal = ""
                            self.flightCardInfo[1] = ""
                            airportAddr = "桃園國際機場"
                        }
                    }else if self.orderModel.airport == "KHH"{
                        self.terminalLabel.text = "高雄機場"
                        self.orderModel.terminal = ""
                        self.flightCardInfo[1] = "高雄機場"
                        airportAddr = "高雄國際機場"
                    }else{
                        self.terminalLabel.text = ""
                        self.orderModel.terminal = ""
                        self.flightCardInfo[1] = ""
                        airportAddr = ""
                    }
                    if self.orderModel.stype == "O"{
                        self.orderModel.taddress = airportAddr
                    }else{
                        self.orderModel.paddress = airportAddr
                    }
                    self.flighiNoLabel.text = res[0]["fno"].stringValue
                    self.startCityLabel.text = res[0]["da"].stringValue
                    self.endCityLabel.text = res[0]["aa"].stringValue
                    
                    let startTimeText = res[0]["dt"].stringValue
                    self.startTimeLabel.text = self.toTimeFormaater(timeString: startTimeText)
                    
                    let endTimeText = res[0]["at"].stringValue
                    self.endTimeLabel.text = self.toTimeFormaater(timeString: endTimeText)
                    
                    self.flightCardInfo[0] = res[0]["fno"].stringValue
                    self.flightCardInfo[2] = res[0]["da"].stringValue
                    self.flightCardInfo[3] = res[0]["aa"].stringValue
                    self.flightCardInfo[4] = res[0]["dt"].stringValue
                    self.flightCardInfo[5] = res[0]["at"].stringValue
                    self.flightCardInfo[6] = res[0]["fullat"].stringValue
                    self.flightCardInfo[7] = res[0]["fulldt"].stringValue
                    
                    self.playActivity(isPlay: false)
                    self.errorImg.isHidden = true
                    self.flightCardView.isHidden = false
                    self.isFlightNoOk = true
                }
                
                
            }else{
                self.playActivity(isPlay: false)
                self.errorImg.isHidden = false
                self.errorImg.blink()
                self.flightCardView.isHidden = true
                self.isFlightNoOk = false
            }
        })
    }
    
    @IBAction func personUp(_ sender: UIButton) {
        if personCountTextField.text! == "7"{
            return
        }
        personCountTextField.text = String(Int(personCountTextField.text!)!+1)
        baggageCountTextField.text = personCountTextField.text!
    }
    
    @IBAction func personDown(_ sender: UIButton) {
        if personCountTextField.text! == "1"{
            return
        }
        personCountTextField.text = String(Int(personCountTextField.text!)!-1)
        baggageCountTextField.text = personCountTextField.text!
    }
    
    @IBAction func bagUp(_ sender: UIButton) {
        if baggageCountTextField.text! == "7" || Int(personCountTextField.text!)!+1 == Int(baggageCountTextField.text!)!{
            return
        }
        baggageCountTextField.text = String(Int(baggageCountTextField.text!)!+1)
        
    }
    
    @IBAction func bagDown(_ sender: UIButton) {
        if baggageCountTextField.text! == "1"{
            return
        }
        baggageCountTextField.text = String(Int(baggageCountTextField.text!)!-1)
        
    }
    
    
    
    @IBAction func next(_ sender: BorderCornerButton) {
        print(isFlightNoOk)
        guard isFlightNoOk else{
            self.present(AlertViewModel.shared.showAlert(message: "請輸入正確的航班資訊！"), animated: true, completion: nil)
            return
        }
        sender.isEnabled = false
        timeList = [CarTimeModel]()
        var placeArr = [Double]()
        if orderModel.stype == "I"{
            placeArr = [orderModel.tlat,orderModel.tlng]
        }else if orderModel.stype == "O"{
            placeArr = [orderModel.plat,orderModel.plng]
        }
        var fdate = ""
        var ftime = ""
        if orderModel.stype == "I"{
            fdate = String(flightCardInfo[6].prefix(8))
            ftime = String(flightCardInfo[6].suffix(4))
        }else{
            fdate = String(flightCardInfo[7].prefix(8))
            ftime = String(flightCardInfo[7].suffix(4))
        }
        
        orderModel.pcnt = Int(personCountTextField.text!)!
        orderModel.bcnt = Int(baggageCountTextField.text!)!
        orderModel.fdate = fdate
        orderModel.ftime = ftime
        print(fdate)
        print(ftime)
        
        orderModel.cpflag = "S"
        var parmeters:[String:Any] = ["fdate":fdate,"ftime":ftime,"stype":orderModel.cpflag,"ftype":orderModel.stype,"lat":placeArr[0],"lng":placeArr[1],"airport":orderModel.airport,"ter":orderModel.terminal,"pcnt":orderModel.pcnt,"bcnt":orderModel.bcnt]
        if orderModel.stype == "O"{
            parmeters["city"] = orderModel.pcity
            parmeters["dist"] = orderModel.pdistinct
        }else{
            parmeters["city"] = orderModel.tcity
            parmeters["dist"] = orderModel.tdistinct
        }
        print(parmeters)
        CallCarAPI.shared.getCarTime(parameters: parmeters, success: { (success, res,error,errorString,sCode) in
            if sCode >= 900{
                self.goRoot()
                return
            }
            sender.isEnabled = true
            if self.isApiError(error: error){
                let alertController = UIAlertController(title: "提示",message: "連線失敗",preferredStyle: .alert)
                let okActtion = UIAlertAction(title: "確定", style: .default) { (UIAlertAction) in
                    let storyboard = UIStoryboard(name: "TabBar", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "TabBarVC")
                    self.present(vc, animated: false, completion: nil)
                }
                alertController.addAction(okActtion)
                self.present(alertController, animated: true, completion: nil)
                return
            }
            if !success {
                self.present(AlertViewModel.shared.showAlert(message: errorString), animated: true, completion: nil)
                return
            }
            print(res)
            for r in res.arrayValue{
                let carTime = CarTimeModel(carDate: r["date"].stringValue, carTime: r["time"].stringValue, dno:r["dno"].stringValue, city: r["city"].stringValue, distinct: r["distinct"].stringValue, rbcnt: r["rbcnt"].stringValue, rpcnt: r["rpcnt"].stringValue,carType: r["cartype"].stringValue,rno:r["rno"].arrayValue[0].stringValue)
                self.timeList.append(carTime)
            }
            self.performSegue(withIdentifier: "goCarInfo", sender: nil)
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print(orderModel.fdate)
        if segue.identifier == "goCarInfo" {
            
//            orderModel.sdate = formatterForApi.string(from: flightDatePicker.date) //乘車日期 該抓航班日還是乘車日？？？？
//            orderModel.fdate = formatterForApi.string(from: flightDatePicker.date)
//            orderModel.ftime = startTimeLabel.text!.replacingOccurrences(of: ":", with: "")
            orderModel.pcnt = Int(personCountTextField.text!)!
            orderModel.bcnt = Int(baggageCountTextField.text!)!
            orderModel.fno = flightNoTextField.text!
            
            let vc = segue.destination as! ReserveCarInfoViewController
            
            vc.orderModel = orderModel
            vc.flightCardInfo = flightCardInfo
            vc.timeList = timeList
            
            
        }else if segue.identifier == "goVip"{
            let vc = segue.destination as! VipReserveDetailViewController
            vc.orderModel = orderModel
        }
    }
    
    
    
    
    var textFieldY:CGFloat = 0
    var isKeyboardShown = false
    
    @IBAction func textFieldDidBegin(_ sender: UITextField) {
        let relativePositionView = UIApplication.shared.keyWindow
        let relativeFrame = sender.superview?.convert(sender.frame, to: relativePositionView)
        let yPosition = relativeFrame?.minY;
        textFieldY = yPosition! + sender.frame.height * 3
        
    }
    
    @objc func keyboardWillShow(note: NSNotification) {
        if isKeyboardShown {
            return
        }
        let keyboardAnimationDetail = note.userInfo as! [String: AnyObject]
        let duration = TimeInterval(truncating: keyboardAnimationDetail[UIResponder.keyboardAnimationDurationUserInfoKey]! as! NSNumber)
        let keyboardFrameValue = keyboardAnimationDetail[UIResponder.keyboardFrameBeginUserInfoKey]! as! NSValue
        let keyboardFrame = keyboardFrameValue.cgRectValue
        
        if self.view.frame.height - keyboardFrame.size.height > textFieldY{
            return
        }
        
        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: -(keyboardFrame.size.height - (self.view.frame.height - self.textFieldY)))
        })
        isKeyboardShown = true
    }
    
    @objc func keyboardWillHide(note: NSNotification) {
        let keyboardAnimationDetail = note.userInfo as! [String: AnyObject]
        let duration = TimeInterval(truncating: keyboardAnimationDetail[UIResponder.keyboardAnimationDurationUserInfoKey]! as! NSNumber)
        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy:  -self.view.frame.origin.y)
        })
        isKeyboardShown = false
    }
}

extension ReserveDetailViewController:UIPickerViewDataSource,UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 0{
            return self.personCountArray.count
        }else{
            return self.baggageCountArray.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 0{
            return self.personCountArray[row]
        }else{
            return self.baggageCountArray[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print("select row tag \(pickerView.tag)")
        if pickerView.tag == 0{
            self.personCountTextField.text = personCountArray[row]
            self.baggageCountTextField.text = baggageCountArray[row]
            baggagePickerView.selectRow(row, inComponent: 0, animated: false)
        }else{
            self.baggageCountTextField.text = baggageCountArray[row]
        }
        
    }
    
    
}
