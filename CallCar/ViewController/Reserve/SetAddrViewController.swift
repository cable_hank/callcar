//
//  SetAddrViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/18.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit
import GooglePlaces
protocol SendAddrDelegate {
    func addr(_ position:CLLocationCoordinate2D,addr:String)
}
class SetAddrViewController: UIViewController {
    
    var addrArr = [RM_AddrModel]()
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var resultView: UITextView?
    
    var text = ""
    
    @IBOutlet weak var tableView: UITableView!
    var addrDelegate:SendAddrDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            tableView.contentInsetAdjustmentBehavior = .never
        } else {
            self.automaticallyAdjustsScrollViewInsets = false
        }
        
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        searchController = UISearchController(searchResultsController: resultsViewController)
        
        searchController?.searchResultsUpdater = resultsViewController
        // Put the search bar in the navigation bar.
        searchController?.searchBar.delegate = self
        searchController?.searchBar.sizeToFit()
        searchController?.searchBar.placeholder = "請輸入地址"
        searchController?.searchBar.text = text
        searchController?.searchBar.enablesReturnKeyAutomatically = true
        searchController?.searchBar.setValue("取消", forKey:"_cancelButtonText")
        navigationItem.titleView = searchController?.searchBar
        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        definesPresentationContext = true
        
        // Prevent the navigation bar from being hidden when searching.
        searchController?.hidesNavigationBarDuringPresentation = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        addrArr = RealmDB.shared.getAddr()
        tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
}

// Handle the user's selection.
extension SetAddrViewController: GMSAutocompleteResultsViewControllerDelegate,UISearchBarDelegate{
    
    
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didAutocompleteWith place: GMSPlace) {
        searchController?.isActive = false
        // Do something with the selected place.
//        print("Place name: \(place.name)")
//        print("Place address: \(place.formattedAddress)")
//        print("Place lat: \(place.coordinate.latitude) long:\(place.coordinate.longitude)")
        if let addr = place.formattedAddress{
            
            addrDelegate?.addr(place.coordinate,addr:addr)
        }
        dismiss(animated: true, completion: nil)
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didFailAutocompleteWithError error: Error){
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    func didRequestAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
         UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
         UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    
}

extension SetAddrViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addrArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "oldAddrCell")! as! AddrTableViewCell
        cell.addrLabel.text = addrArr[indexPath.row].addr
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "常用地址"
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let location = CLLocationCoordinate2D(latitude: addrArr[indexPath.row].lat, longitude: addrArr[indexPath.row].lng)
        addrDelegate?.addr(location,addr:"")
        dismiss(animated: true, completion: nil)
    }
    
}
