//
//  CheckOutViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/24.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit

class CheckOutViewController: UIViewController,CreditCardValueDelegate,InvoiceDelegate,checkAlertDelegate {

    @IBOutlet weak var nameTextField: LeftIconTextField!
    @IBOutlet weak var phoneTextField: LeftIconTextField!
    @IBOutlet weak var couponTextField: LeftIconTextField!
    @IBOutlet weak var couponMssLabel: UILabel!
    
    @IBOutlet weak var carFeeLabel: UILabel!
    @IBOutlet weak var promoteFeeLabel: UILabel!
    @IBOutlet weak var nightFeeLabel: UILabel!
    @IBOutlet weak var baggageFeeLabel: UILabel!
    @IBOutlet weak var priceFeeLael: UILabel!
    @IBOutlet weak var cardNoLabel: UILabel!
    @IBOutlet weak var setCardButton: BorderCornerButton!
    @IBOutlet weak var setInvoiceButton: BorderCornerButton!
    
    @IBOutlet weak var nameTitleLabel: UILabel!
    @IBOutlet weak var mobileTitleLabel: UILabel!
    @IBOutlet weak var payInfoTitle: UILabel!
    @IBOutlet weak var invTitleLabel: UILabel!
    @IBOutlet weak var copTitleLabel: UILabel!
    @IBOutlet weak var priceInfoTitle: UILabel!
    @IBOutlet weak var totalTitleLabel: UILabel!
    @IBOutlet weak var submitButton: BorderCornerButton!
    
    @IBOutlet weak var priceTitleLabel: UILabel!
    
    
    @IBOutlet weak var vanDamtTitleLabel: UILabel!
    
    @IBOutlet weak var vanDamtPriceLabel: UILabel!
    @IBOutlet weak var invoiceNoLabel: UILabel!
    @IBOutlet weak var invoiceAddrLabel: UILabel!
    @IBOutlet weak var bagPriceTitleLabel: UILabel!
    @IBOutlet weak var bagPriceLabel: UILabel!
    
    @IBOutlet weak var invoiceView: UIView!
    @IBOutlet weak var invEditButtonView: CornerView!
    @IBOutlet weak var cardEditButtonView: CornerView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var invoiceTipsLabel: UILabel!
    @IBOutlet weak var showSharePriceInfoButton: UIButton!
    
    @IBOutlet weak var dButton0: UIButton!
    @IBOutlet weak var dButton1: UIButton!
    @IBOutlet weak var dButton2: UIButton!
    
    @IBOutlet weak var dView: UIView!
    @IBOutlet weak var dPoint: UILabel!
    @IBOutlet weak var dImg: UIImageView!
    
    @IBOutlet weak var dLabel: UILabel!
    
    @IBOutlet weak var bonusTips: UIButton!
    @IBOutlet weak var vanBottomView: UIView!
    
    @IBOutlet weak var vanBottomViewHeight: NSLayoutConstraint!
    @IBOutlet weak var vanTypeLabel: UILabel!
    
    @IBOutlet weak var vanTipsLabel: UILabel!
    
    @IBOutlet weak var goVanBottomView: UIStackView!
    
    var dtype = "C"
    var couponOk = true
    var cid = ""
    var iid = ""
    var dno = ""
    var depositLimit = 0
    var bonusLimit = 0
    var deposit = ""
    var bonus = ""
    var orderModel = OrderModel()
    var priceArr:[String:Int] = ["Price":0,"CarFee":0,"NightFee":0,"BaggageFee":0,"PromoteAmt":0,"PromoteDiscount":0,"UserDiscount":0]
    let sColor = UIColor(hex: 0x55C1EA)
    let vColor = UIColor(hex: 0x3575C0)
    var vanColor = UIColor(hex: 0xFF8300)
    var isJp = false
    var country = "TW"
    
    @IBOutlet weak var invoiceViewHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "付款資訊"
        let backButton = UIBarButtonItem()
        backButton.title = " "
        backButton.tintColor = .white
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil)
        if isJp{
            vanColor = UIColor(hex: 0xFC989F)
            country = "JP"
        }else{
            vanColor = UIColor(hex: 0xFF8300)
            country = "TW"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if dno != ""{
            print("這是檢車")
        }
        print("cpflag:\(orderModel.cpflag)")
        if orderModel.cpflag == "S"{
            showSharePriceInfoButton.isHidden = false
            nameTitleLabel.textColor = sColor
            mobileTitleLabel.textColor = sColor
            payInfoTitle.textColor = sColor
            invTitleLabel.textColor = sColor
            copTitleLabel.textColor = sColor
            priceInfoTitle.textColor = sColor
            priceInfoTitle.text = "共乘價格試算"
            totalTitleLabel.textColor = sColor
            submitButton.backgroundColor = UIColor(hex: 0x20B6FC)
            priceFeeLael.textColor = sColor
            priceTitleLabel.text = "基本車資"
            bagPriceTitleLabel.isHidden = false
            bagPriceLabel.isHidden = false
            vanDamtTitleLabel.isHidden = true
            vanDamtPriceLabel.isHidden = true
            dButton0.setImage(UIImage(named: "carpool_no"), for: .normal)
            dButton0.setImage(UIImage(named: "carpool_yes"), for: .selected)
            dButton1.setImage(UIImage(named: "carpool_no"), for: .normal)
            dButton1.setImage(UIImage(named: "carpool_yes"), for: .selected)
            dButton2.setImage(UIImage(named: "carpool_no"), for: .normal)
            dButton2.setImage(UIImage(named: "carpool_yes"), for: .selected)
            vanBottomView.isHidden = true
            vanBottomViewHeight.constant = 0
            goVanBottomView.isHidden = true
        }else if orderModel.cpflag == "P"{
            showSharePriceInfoButton.isHidden = true
            nameTitleLabel.textColor = vColor
            mobileTitleLabel.textColor = vColor
            payInfoTitle.textColor = vColor
            invTitleLabel.textColor = vColor
            copTitleLabel.textColor = vColor
            priceInfoTitle.textColor = vColor
            priceInfoTitle.text = "專接價格試算"
            totalTitleLabel.textColor = vColor
            submitButton.backgroundColor = vColor
            priceFeeLael.textColor = vColor
            
            if orderModel.cartype == "四人座"{
                priceTitleLabel.text = "四人座車資"
            }else{
                priceTitleLabel.text = "七人座車資"
            }
            bagPriceTitleLabel.isHidden = true
            bagPriceLabel.isHidden = true
            vanDamtTitleLabel.isHidden = true
            vanDamtPriceLabel.isHidden = true
            dButton0.setImage(UIImage(named: "vip_no"), for: .normal)
            dButton0.setImage(UIImage(named: "vip_yes"), for: .selected)
            dButton1.setImage(UIImage(named: "vip_no"), for: .normal)
            dButton1.setImage(UIImage(named: "vip_yes"), for: .selected)
            dButton2.setImage(UIImage(named: "vip_no"), for: .normal)
            dButton2.setImage(UIImage(named: "vip_yes"), for: .selected)
            vanBottomView.isHidden = true
            vanBottomViewHeight.constant = 0
            
            if (orderModel.cartype == "四人座" && orderModel.bcnt <= 2 && orderModel.pcnt <= 2) || (orderModel.cartype == "七人座" && orderModel.bcnt <= 4 && orderModel.pcnt <= 4){
                let sdate = "\(orderModel.fdate)\(orderModel.ptime)".formateDate(format: "yyyyMMddHHmm")!
                if Date(timeIntervalSinceNow: 60*60*24*15) <  sdate{
                    goVanBottomView.isHidden = false
                }else{
                    goVanBottomView.isHidden = true
                }
            }else{
                goVanBottomView.isHidden = true
            }
        }else{
            if isJp{
                priceInfoTitle.text = "日本共乘價格試算"
                vanTypeLabel.textColor = UIColor(hex: 0x33C5D4)
            }else{
                priceInfoTitle.text = "TaiVAN大共乘價格試算"
                vanTypeLabel.textColor = UIColor(hex: 0xFF5C00)
            }
            submitButton.backgroundColor = vanColor
            showSharePriceInfoButton.isHidden = true
            nameTitleLabel.textColor = vanColor
            mobileTitleLabel.textColor = vanColor
            payInfoTitle.textColor = vanColor
            invTitleLabel.textColor = vanColor
            copTitleLabel.textColor = vanColor
            priceInfoTitle.textColor = vanColor
            totalTitleLabel.textColor = vanColor
            priceFeeLael.textColor = vanColor
            
            if orderModel.cartype == "四人座"{
                priceTitleLabel.text = "四人座車資"
            }else{
                priceTitleLabel.text = "七人座車資"
            }
            bagPriceTitleLabel.isHidden = true
            bagPriceLabel.isHidden = true
            vanDamtTitleLabel.isHidden = false
            vanDamtPriceLabel.isHidden = false
            dButton0.setImage(UIImage(named: "vanpool_no"), for: .normal)
            dButton0.setImage(UIImage(named: "vanpool_yes"), for: .selected)
            dButton1.setImage(UIImage(named: "vanpool_no"), for: .normal)
            dButton1.setImage(UIImage(named: "vanpool_yes"), for: .selected)
            dButton2.setImage(UIImage(named: "vanpool_no"), for: .normal)
            dButton2.setImage(UIImage(named: "vanpool_yes"), for: .selected)
            vanBottomView.isHidden = false
            vanBottomViewHeight.constant = 75
            if dno != ""{
//                vanTipsLabel.text = "TaiVAN大共乘併車享有優惠價六折的優惠"
                vanTypeLabel.text = "併車"
            }else{
//                vanTipsLabel.text = "推廣期間發起人預約即享九折優惠，媒合成功將再次降價"
                vanTypeLabel.text = "發起"
            }
            goVanBottomView.isHidden = true
        }
        
        getUserInfo()
        getPrice(dtype: "C")
        
        CallCarAPI.shared.getCard(parameters: ["uid":orderModel.uid!]) { (success, res, error) in
            if success{
                self.cardNo(no: res["CNo"].stringValue, date: res["Expire"].stringValue, thirdNo: res["Code"].stringValue,id:res["ID"].stringValue)
            }else{
                self.cardEditButtonView.isHidden = true
            }
        }
        CallCarAPI.shared.getMarketing(parameters: ["uid":orderModel.uid!]) { (success, res, error,sCode) in
            if success{
                self.deposit = res["deposit"].stringValue
                self.bonus = res["bonus"].stringValue
            }
        }
    }
    
    func cardNo(no: String, date: String, thirdNo: String,id: String) {
        print(no)
        cardNoLabel.isHidden = false
        if no.count > 5{
            cardNoLabel.text = "\(no.prefix(5))****-****\(no.suffix(5))"
        }else{
            cardNoLabel.text = ""
        }
        setCardButton.isHidden = true
        cid = id
        cardEditButtonView.isHidden = false
        
    }
    func invoiceValue(invoiceAddr: String, invoiceNo: String,id:String) {
        print("iid:\(id)")
        iid = id
        invoiceNoLabel.text = "統一編號：\(invoiceNo)"
        invoiceAddrLabel.text = invoiceAddr
        invoiceTipsLabel.isHidden = true
        setInvoiceButton.isHidden = true
        invoiceNoLabel.isHidden = false
        invoiceAddrLabel.isHidden = false
        invEditButtonView.isHidden = false
        invoiceViewHeightConstraint.constant = setInvoiceButton.frame.height + 5
        invoiceView.layoutIfNeeded()
    }
    
    
    
    func getPrice(dtype:String){
        var couponP = ""
        var bonusP = 0
        var depositP = 0
        if dtype == "C"{
            if couponOk{
                couponP = couponTextField.text!
            }else{
                couponP = ""
            }
        }else if dtype == "B"{
            bonusP = bonusLimit
        }else{
            depositP = depositLimit
        }
        var parameters:[String:Any] = ["":""]
        if orderModel.cpflag == "S"{
            parameters = ["date":orderModel.sdate,
                          "time":orderModel.ptime,
                          "share":orderModel.cpflag,
                          "pcnt":orderModel.pcnt,
                          "bcnt":orderModel.bcnt,
                          "coupon":couponP,
                          "bonus":bonusP,
                          "deposit":depositP,
                          "dno":dno,
                          "uid":orderModel.uid!,
                          "cname":"",
                          "dname":"",
                          "airport":"",
                          "ctype":""]
        }else if orderModel.cpflag == "P"{
            var cname = ""
            var dname = ""
            if orderModel.stype == "I"{
                cname = orderModel.tcity
                dname = orderModel.tdistinct
            }else{
                cname = orderModel.pcity
                dname = orderModel.pdistinct
            }
            parameters = ["date":orderModel.sdate,
                          "time":orderModel.ptime,
                          "share":orderModel.cpflag,
                          "pcnt":orderModel.pcnt,
                          "bcnt":orderModel.bcnt,
                          "coupon":couponP,
                          "bonus":bonusP,
                          "deposit":depositP,
                          "dno":"",
                          "uid":orderModel.uid!,
                          "cname":cname,
                          "dname":dname,
                          "airport":orderModel.airport,
                          "ctype":orderModel.cartype,]
        }else{
            var cname = ""
            var dname = ""
            if orderModel.stype == "I"{
                cname = orderModel.tcity
                dname = orderModel.tdistinct
            }else{
                cname = orderModel.pcity
                dname = orderModel.pdistinct
            }
            var van = ""
            if dno == ""{
                van = "M"
            }else{
                van = "S"
            }
            parameters = ["date":orderModel.sdate,
                          "time":orderModel.ptime,
                          "share":orderModel.cpflag,
                          "pcnt":orderModel.pcnt,
                          "bcnt":orderModel.bcnt,
                          "coupon":couponP,
                          "bonus":bonusP,
                          "deposit":depositP,
                          "dno":dno,
                          "uid":orderModel.uid!,
                          "cname":cname,
                          "dname":dname,
                          "airport":orderModel.airport,
                          "ctype":orderModel.cartype,
                          "van":van]
        }
        parameters["country"] = country
        print("getPrice parameters: \(parameters)")
        CallCarAPI.shared.getPrice(parameters: parameters) { (success, res, sCode) in
            if sCode >= 900{
                self.goRoot()
                return
            }
            print("RtnObject:\(res)")
            self.priceArr["Price"] = res["Price"].intValue
            self.priceArr["CarFee"] = res["CarFee"].intValue
            self.priceArr["NightFee"] = res["NightFee"].intValue
            self.priceArr["BaggageFee"] = res["BaggageFee"].intValue
            self.priceArr["PromoteAmt"] = res["PromoteAmt"].intValue
            self.priceArr["UserDiscount"] = res["UserDiscount"].intValue
            self.priceArr["PromoteDiscount"] = res["PromoteDiscount"].intValue
            if self.bonusLimit == 0{
                self.bonusLimit = res["BonusLimit"].intValue
            }
            if self.depositLimit == 0{
                self.depositLimit = res["DepositLimit"].intValue
            }
//            print(self.priceArr)
            self.setPrice()
        }
    }
    
    func getUserInfo(){
        CallCarAPI.shared.getUserInfo(parameters: ["id":orderModel.uid!]) { (success, res, sCode) in
            if sCode >= 900{
                self.goRoot()
                return
            }
//            print("RtnObject:\(res)")
            self.nameTextField.text = "\(res["lname"].stringValue)\(res["fname"].stringValue)"
            self.phoneTextField.text = res["mobile"].stringValue
        }
    }
    func setPrice(){
        carFeeLabel.text = String(priceArr["CarFee"]!)
        if priceArr["UserDiscount"]! != 0{
            promoteFeeLabel.text = "-\(priceArr["UserDiscount"]!)"
        }else{
            promoteFeeLabel.text = String(priceArr["UserDiscount"]!)
        }
        if priceArr["PromoteDiscount"] != 0{
            vanDamtPriceLabel.text = "-\(priceArr["PromoteDiscount"]!)"
            if orderModel.cpflag == "V"{
                vanDamtTitleLabel.isHidden = false
                vanDamtPriceLabel.isHidden = false
                if dno == ""{
                    vanDamtTitleLabel.text = "發起折扣"
                }else{
                    vanDamtTitleLabel.text = "併車折扣"
                }
            }
        }else{
            if orderModel.cpflag == "V"{
                vanDamtTitleLabel.isHidden = true
                vanDamtPriceLabel.isHidden = true
            }
        }
        nightFeeLabel.text = String(priceArr["NightFee"]!)
        baggageFeeLabel.text = String(priceArr["BaggageFee"]!)
        priceFeeLael.text = String(priceArr["Price"]!)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func editCouponCode(_ sender: LeftIconTextField) {
//        print("code:\(sender.text!) date:\(orderModel.sdate)")
        sender.text = sender.text?.uppercased()
        guard sender.text != "" else{
            self.couponOk = true
            couponMssLabel.isHidden = true
            sender.errorFunc(isError: false)
            return
        }
        var parameters = ["code":sender.text!,"date":orderModel.sdate,"cflag":orderModel.cpflag,"city":orderModel.pcity,"distinct":orderModel.pdistinct,"country":country]
        if orderModel.stype == "I"{
            parameters = ["code":sender.text!,"date":orderModel.sdate,"cflag":orderModel.cpflag,"city":orderModel.tcity,"distinct":orderModel.tdistinct,"country":country]
        }
        print(parameters)
        CallCarAPI.shared.checkCoupon(parameters: parameters, success: { (success, res,sCode) in
            if sCode >= 900{
                self.goRoot()
                return
            }
            if success{
                self.couponOk = true
                self.couponMssLabel.text = "✔︎"
                self.couponMssLabel.textColor = UIColor(hex: 0x47B707)
                sender.errorFunc(isError: false)
            }else{
                self.couponOk = false
                self.couponMssLabel.text = "✖︎"
                self.couponMssLabel.textColor = UIColor(hex: 0xF97B62)
                sender.errorFunc(isError: true)
            }
            self.getPrice(dtype: "C")
            self.couponMssLabel.isHidden = false
        })
    }
    
    @IBAction func showCreditCard(_ sender: BorderCornerButton) {
        self.performSegue(withIdentifier: "presentCreditCard", sender: nil)
    }
    @IBAction func showInvoice(_ sender: BorderCornerButton) {
        self.performSegue(withIdentifier: "presentInvoice", sender: nil)
    }
    
    func checkAlertSubmit() {
        self.performSegue(withIdentifier: "realSubmit", sender: nil)
    }
    
    func submit(){
        if dtype == "C"{
            guard couponOk else {
                self.present(AlertViewModel.shared.showAlert(message: "請輸入正確的優惠碼"), animated: true, completion: nil)
                return
            }
        }
        guard nameTextField.text!.count != 0 else {
            self.present(AlertViewModel.shared.showAlert(message: "請輸入聯絡姓名"), animated: true, completion: nil)
            return
        }
        guard phoneTextField.text!.count != 0 else {
            self.present(AlertViewModel.shared.showAlert(message: "請輸入聯絡電話"), animated: true, completion: nil)
            return
        }
        
        guard phoneTextField.text!.count == 10 && phoneTextField.text!.prefix(2) == "09" else {
            self.present(AlertViewModel.shared.showAlert(message: "請輸入正確手機號碼格式"), animated: true, completion: nil)
            return
        }
        guard cid != "" else {
            self.present(AlertViewModel.shared.showAlert(message: "請設定信用卡"), animated: true, completion: nil)
            return
        }
        
        submitButton.isEnabled = false
        orderModel.idata = iid
        orderModel.price = priceArr["Price"]!
        orderModel.dtype = dtype
        
        orderModel.udamt = priceArr["UserDiscount"]!
        orderModel.pdamt = priceArr["PromoteDiscount"]!
        if dtype == "C"{
            if couponTextField.text != ""{
                orderModel.coupon = couponTextField.text!
            }else{
                orderModel.dtype = ""
            }
        }else if dtype == "R"{
            if depositLimit == 0{
                orderModel.dtype = ""
            }
        }else if dtype == "B"{
            if bonusLimit == 0{
                orderModel.dtype = ""
            }
        }
        orderModel.ctype = "F"
        orderModel.cdata = cid
        orderModel.max = "0"
        orderModel.damt = priceArr["PromoteAmt"]!
        orderModel.pname = nameTextField.text!
        orderModel.pmobile = phoneTextField.text!
        print("dno:\(dno)")
        self.performSegue(withIdentifier: "submit", sender: nil)
        self.submitButton.isEnabled = true
    }
    
    @IBAction func submit(_ sender: BorderCornerButton) {
        CallCarAPI.shared.getMarketing(parameters: ["uid":orderModel.uid!]) { (success, res, error,sCode) in
            if sCode >= 900{
                self.goRoot()
                return
            }
            if self.isApiError(error: error){
                let alertController = UIAlertController(title: "提示",message: "連線失敗",preferredStyle: .alert)
                let okActtion = UIAlertAction(title: "確定", style: .default) { (UIAlertAction) in
                    let storyboard = UIStoryboard(name: "TabBar", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "TabBarVC")
                    self.present(vc, animated: false, completion: nil)
                }
                alertController.addAction(okActtion)
                self.present(alertController, animated: true, completion: nil)
                return
            }
            if success{
                if self.deposit == res["deposit"].stringValue && self.bonus == res["bonus"].stringValue{
                    self.submit()
                }else{
                    let alertController = UIAlertController(title: "預約失敗",message: "資料認證失敗！將返回首頁。",preferredStyle: .alert)
                    let okActtion = UIAlertAction(title: "確定", style: .default) { (UIAlertAction) in
                        let storyboard = UIStoryboard(name: "TabBar", bundle: nil)
                        let rootVC = storyboard.instantiateViewController(withIdentifier: "TabBarVC")
                        self.present(rootVC, animated: false, completion: nil)
                        
                    }
                    alertController.addAction(okActtion)
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "presentCreditCard"{
            let vc = segue.destination as! CreditCardAlertViewController
            vc.delegate = self
            vc.cid = cid
        }else if segue.identifier == "presentInvoice"{
            let vc = segue.destination as! InvoiceAlertViewController
            vc.delegate = self
        }else if segue.identifier == "submit"{
            let vc = segue.destination as! CheckAlertViewController
            vc.delegate = self
            vc.orderModel = orderModel
            vc.dno = dno
            vc.isJp = isJp
        }else if segue.identifier == "realSubmit"{
            let vc = segue.destination as! ReserveSuccessViewController
            vc.orderModel = orderModel
        }else if segue.identifier == "alert"{
            let vc = segue.destination as! vanInitiateAlertViewController
            vc.delegate = self
            vc.isTopTips = false
        }
    }
    @IBAction func goVan(_ sender: CornerButton) {
        self.performSegue(withIdentifier: "alert", sender: nil)
        
    }
    
    @IBAction func cardEditButton(_ sender: UIButton) {
         self.performSegue(withIdentifier: "presentCreditCard", sender: nil)
    }
    
    @IBAction func invEditButton(_ sender: UIButton) {
        self.performSegue(withIdentifier: "presentInvoice", sender: nil)
    }
    
    @IBAction func selectDtype(_ sender: UIButton) {
        if sender.tag == 0{
            dButton0.isSelected = true
            dButton1.isSelected = false
            dButton2.isSelected = false
            dtype = "C"
            dView.isHidden = true
            bonusTips.isHidden = true
        }else if sender.tag == 1{
            dButton0.isSelected = false
            dButton1.isSelected = true
            dButton2.isSelected = false
            dtype = "B"
            dImg.image = UIImage(named: "ListPoint")
            dPoint.textColor = UIColor(hex: 0xFF8300)
            dLabel.textColor = UIColor(hex: 0xFF8300)
            dLabel.text = "乘車點數"
            dPoint.text = "\(bonusLimit)"
            dView.isHidden = false
            bonusTips.isHidden = false
        }else if sender.tag == 2{
            dButton0.isSelected = false
            dButton1.isSelected = false
            dButton2.isSelected = true
            dtype = "R"
            dImg.image = UIImage(named: "ListCash")
            dPoint.textColor = UIColor(hex: 0xF9D34F)
            dLabel.textColor = UIColor(hex: 0xF9D34F)
            dLabel.text = "儲值金"
            dPoint.text = "\(depositLimit)"
            dView.isHidden = false
            bonusTips.isHidden = true
        }
        getPrice(dtype: dtype)
        print(dtype)
    }
    
    
    func convertDictionaryToString(_ dictionary: [String : Any]) -> String {
        var result: String = ""
        
        do {
            // 將dictionary轉成JSON data
            let data: Data = try JSONSerialization.data(withJSONObject: dictionary, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            // 將data格式化成json格式
            let decoded = try JSONSerialization.jsonObject(with: data, options: [])
            
            if let dictFromJSON = decoded as? [String : Any] {
                // 將Data轉成字串
                let jsonData: Data = try JSONSerialization.data(withJSONObject: dictFromJSON, options: JSONSerialization.WritingOptions.prettyPrinted)
                result = String(data: jsonData, encoding: .utf8)!
            }
        } catch {
            print("convertDictionaryToString: \(error.localizedDescription)")
        }
        return result
    }
    
    
    
    var textFieldY:CGFloat = 0
    var isKeyboardShown = false
    
    @IBAction func textFieldDidBegin(_ sender: UITextField) {
        let relativePositionView = UIApplication.shared.keyWindow
        let relativeFrame = sender.superview?.convert(sender.frame, to: relativePositionView)
        let yPosition = relativeFrame?.minY;
        textFieldY = yPosition! + sender.frame.height * 3
        
    }
    
    @objc func keyboardWillShow(note: NSNotification) {
        if isKeyboardShown {
            return
        }
        let keyboardAnimationDetail = note.userInfo as! [String: AnyObject]
        let duration = TimeInterval(truncating: keyboardAnimationDetail[UIResponder.keyboardAnimationDurationUserInfoKey]! as! NSNumber)
        let keyboardFrameValue = keyboardAnimationDetail[UIResponder.keyboardFrameBeginUserInfoKey]! as! NSValue
        let keyboardFrame = keyboardFrameValue.cgRectValue
        
        if self.view.frame.height - keyboardFrame.size.height > textFieldY{
            return
        }
        
        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: -(keyboardFrame.size.height - (self.view.frame.height - self.textFieldY)))
        })
        isKeyboardShown = true
    }
    
    @objc func keyboardWillHide(note: NSNotification) {
        let keyboardAnimationDetail = note.userInfo as! [String: AnyObject]
        let duration = TimeInterval(truncating: keyboardAnimationDetail[UIResponder.keyboardAnimationDurationUserInfoKey]! as! NSNumber)
        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy:  -self.view.frame.origin.y)
        })
        isKeyboardShown = false
    }
}

extension CheckOutViewController:vanInitiateAlertViewControllerDelegate{
    func vanInitiateAlertOK() {
        self.navigationController?.navigationBar.barTintColor = UIColor(hex: 0xF5A623)
        //--------上面貼下來的---------
        showSharePriceInfoButton.isHidden = true
        nameTitleLabel.textColor = vanColor
        mobileTitleLabel.textColor = vanColor
        payInfoTitle.textColor = vanColor
        invTitleLabel.textColor = vanColor
        copTitleLabel.textColor = vanColor
        priceInfoTitle.textColor = vanColor
        priceInfoTitle.text = "TaiVAN大共乘價格試算"
        totalTitleLabel.textColor = vanColor
        submitButton.backgroundColor = UIColor(hex: 0xF5A623)
        priceFeeLael.textColor = vanColor
        
        if orderModel.cartype == "四人座"{
            priceTitleLabel.text = "四人座車資"
        }else{
            priceTitleLabel.text = "七人座車資"
        }
        bagPriceTitleLabel.isHidden = true
        bagPriceLabel.isHidden = true
        vanDamtTitleLabel.isHidden = false
        vanDamtPriceLabel.isHidden = false
        dButton0.setImage(UIImage(named: "vanpool_no"), for: .normal)
        dButton0.setImage(UIImage(named: "vanpool_yes"), for: .selected)
        dButton1.setImage(UIImage(named: "vanpool_no"), for: .normal)
        dButton1.setImage(UIImage(named: "vanpool_yes"), for: .selected)
        dButton2.setImage(UIImage(named: "vanpool_no"), for: .normal)
        dButton2.setImage(UIImage(named: "vanpool_yes"), for: .selected)
        vanBottomView.isHidden = false
        vanBottomViewHeight.constant = 75
        if dno != ""{
//            vanTipsLabel.text = "TaiVAN大共乘併車享有優惠價六折的優惠"
            vanTypeLabel.text = "併車"
        }else{
//            vanTipsLabel.text = "推廣期間發起人預約即享九折優惠，媒合成功將再次降價"
            vanTypeLabel.text = "發起"
        }
        goVanBottomView.isHidden = true
        //----------上面貼下來的------------
        orderModel.cpflag = "V"
        getPrice(dtype: dtype)
    }
    
    
}
