//
//  VanCarInfoViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2019/6/5.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import UIKit

class VanCarInfoViewController: UIViewController {

    @IBOutlet weak var flightNoLabel: UILabel!
    @IBOutlet weak var terminalLabel: UILabel!
    @IBOutlet weak var startCityLabel: UILabel!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var endCityLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var carDateLabel: UILabel!
    @IBOutlet weak var carTimeTextField: LeftIconTextField!
    @IBOutlet weak var carTimeView: UIView!
    @IBOutlet weak var carTimvViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var tipsLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var tipsHeightconstraint: NSLayoutConstraint!
    @IBOutlet weak var onCarTitleLabel: UILabel!
    @IBOutlet weak var textfieldIconImg: UIImageView!
    
    
    @IBOutlet weak var flightTitleLabel: UILabel!
    @IBOutlet weak var dateTitleLabel: UILabel!
    @IBOutlet weak var vanTypeTitleLabel: UILabel!
    @IBOutlet weak var nextButton: BorderCornerButton!
    
    var timeDatePicker = UIDatePicker()
    
    var timeList = [CarTimeModel]()
    var orderModel = OrderModel()
    var flightCardInfo = [String]()
    var minDateString = ""
    var maxDateString = ""
    var isJp = false
    var mainColor = UIColor(hex: 0xF5A623)
    
    let formatter = DateFormatter()
    let formatterForApi = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "選擇上車時間"
        let backButton = UIBarButtonItem()
        backButton.title = " "
        backButton.tintColor = .white
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        formatter.dateFormat = "HH:mm"
        formatterForApi.dateFormat = "HHmm"
        carTimeTextField.inputView = timeDatePicker
        timeDatePicker.datePickerMode = .time
        timeDatePicker.minuteInterval = 10
        timeDatePicker.locale = Locale(identifier: "en_GB")
        timeDatePicker.addTarget(self,action: #selector(self.datePickerChanged),for: .valueChanged)
        
        textView.layer.cornerRadius = 10
        
        if isJp{
            mainColor = UIColor(hex: 0xFC989F)
        }else{
            mainColor = UIColor(hex: 0xF5A623)
        }
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil)
    }
    
    @objc func datePickerChanged(datePicker:UIDatePicker){
        carTimeTextField.text = formatter.string(from: datePicker.date)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        flightTitleLabel.textColor = mainColor
        dateTitleLabel.textColor = mainColor
        nextButton.backgroundColor = mainColor
        if isJp{
            vanTypeTitleLabel.textColor = UIColor(hex: 0x33C5D4)
        }else{
            vanTypeTitleLabel.textColor = UIColor(hex: 0xFF5C00)
        }
        
        if flightCardInfo.count > 0{
            flightNoLabel.text = flightCardInfo[0]
            terminalLabel.text = flightCardInfo[1]
            startCityLabel.text = flightCardInfo[2]
            endCityLabel.text = flightCardInfo[3]
            startDateLabel.text = self.toTimeFormaater(timeString: flightCardInfo[4])
            endDateLabel.text = self.toTimeFormaater(timeString: flightCardInfo[5])
        }
        var text = ""
        if orderModel.stype == "O"{
            text = "1往機場(出國)上車時間選擇，請給予足夠交通時間，也可增加共乘媒合降價機率。\n2.司機將依指定上車時間，最多等候 30 分鐘為限。\n3.航班表定抵達時間為 22:00-05:59 將加收夜間費用 200元（目前不收取）。"
            
            onCarTitleLabel.text = "選擇上車時間"
//            tipsHeightconstraint.constant = 70
            textView.text = text
            carTimeTextField.isHidden = false
            carTimvViewConstraint.constant = 100
            textfieldIconImg.isHidden = false
            
            timeDatePicker.minimumDate = self.formatterForApi.date(from: "\(minDateString.prefix(3))0")
            timeDatePicker.maximumDate = self.formatterForApi.date(from: "\(maxDateString.prefix(3))0")
            timeDatePicker.date = timeDatePicker.minimumDate!
            carTimeTextField.text = formatter.string(from: self.timeDatePicker.minimumDate!)
        }else{
            text = "1.往市區（回國）上車時間不需要選擇（無提供指定時間接機服務）。\n2.將以《訂單登錄航班編號班機》實際抵達時間起算，等候 60 分鐘為限。\n3.航班表定抵達時間為 22:00-05:59 將加收夜間費用 200元（目前不收取）。\n4.航班延遲超過 5 小時，視同服務已使用，補償措施請洽詢航空公司。"
            onCarTitleLabel.text = "航班抵達日期"
//            tipsHeightconstraint.constant = 130
            textView.text = text
            carTimeTextField.isHidden = true
            carTimvViewConstraint.constant = 50
            textfieldIconImg.isHidden = true
        }
        
        carDateLabel.text = self.toDateFormaater(dateString: orderModel.sdate)
        print(["fdate":orderModel.fdate,"ftime":orderModel.ftime,"tdate":orderModel.sdate])
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func next(_ sender: BorderCornerButton) {
        if orderModel.stype == "O"{
            orderModel.ptime = carTimeTextField.text!.replacingOccurrences(of: ":", with: "")
        }else{
            orderModel.ptime = flightCardInfo[5]
        }
        self.performSegue(withIdentifier: "goCheck", sender: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goCheck"{
            let vc = segue.destination as! CheckOutViewController
            vc.orderModel = orderModel
            vc.isJp = isJp
        }
    }
    var textFieldY:CGFloat = 0
    var isKeyboardShown = false
    
    @IBAction func textFieldDidBegin(_ sender: UITextField) {
        let relativePositionView = UIApplication.shared.keyWindow
        let relativeFrame = sender.superview?.convert(sender.frame, to: relativePositionView)
        let yPosition = relativeFrame?.minY;
        textFieldY = yPosition! + sender.frame.height * 3
        
    }
    
    @objc func keyboardWillShow(note: NSNotification) {
        if isKeyboardShown {
            return
        }
        let keyboardAnimationDetail = note.userInfo as! [String: AnyObject]
        let duration = TimeInterval(truncating: keyboardAnimationDetail[UIResponder.keyboardAnimationDurationUserInfoKey]! as! NSNumber)
        let keyboardFrameValue = keyboardAnimationDetail[UIResponder.keyboardFrameBeginUserInfoKey]! as! NSValue
        let keyboardFrame = keyboardFrameValue.cgRectValue
        
        if self.view.frame.height - keyboardFrame.size.height > textFieldY{
            return
        }
        
        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: -(keyboardFrame.size.height - (self.view.frame.height - self.textFieldY)))
        })
        isKeyboardShown = true
    }
    
    @objc func keyboardWillHide(note: NSNotification) {
        let keyboardAnimationDetail = note.userInfo as! [String: AnyObject]
        let duration = TimeInterval(truncating: keyboardAnimationDetail[UIResponder.keyboardAnimationDurationUserInfoKey]! as! NSNumber)
        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy:  -self.view.frame.origin.y)
        })
        isKeyboardShown = false
    }
}
