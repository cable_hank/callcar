//
//  vanInitiateAlertViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2019/6/10.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import UIKit

protocol vanInitiateAlertViewControllerDelegate {
    func vanInitiateAlertOK()
}

class vanInitiateAlertViewController: UIViewController {

    @IBOutlet weak var topTips: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var submitButton: CornerButton!
    
    var isTopTips = true
    var delegate:vanInitiateAlertViewControllerDelegate?
    var isJp = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if !isTopTips{
            topTips.text = ""
        }
        if isJp{
            topView.backgroundColor = UIColor(hex: 0xFC989F)
            submitButton.backgroundColor = UIColor(hex: 0xFC989F)
        }else{
            topView.backgroundColor = UIColor(hex: 0xF5A623)
            submitButton.backgroundColor = UIColor(hex: 0xF5A623)
        }
    }
    
    @IBAction func ok(_ sender: CornerButton) {
        self.dismiss(animated: true) {
            self.delegate?.vanInitiateAlertOK()
        }
    }
    
    @IBAction func cancel(_ sender: BorderCornerButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
