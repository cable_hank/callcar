//
//  VanPickCarInfoViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2019/6/10.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import UIKit

class VanPickCarInfoViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var flighiNoLabel: UILabel!
    @IBOutlet weak var terminalLabel: UILabel!
    @IBOutlet weak var startCityLabel: UILabel!
    @IBOutlet weak var endCityLabel: UILabel!
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var endTimeLabel: UILabel!
    
    
    
    @IBOutlet weak var flightTitle: UILabel!
    @IBOutlet weak var carInfoTitle: UILabel!
    @IBOutlet weak var vanTypeLabel: UILabel!
    @IBOutlet weak var vanType: UILabel!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var faVanButton: CornerButton!
    @IBOutlet weak var nextButton: BorderCornerButton!
    
    var timeList = [CarTimeModel]()
    var orderModel = OrderModel()
    var flightCardInfo = [String]()
    var minDateString = ""
    var maxDateString = ""
    
    var timerForShowScrollIndicator: Timer?
    var selectIndexPath:IndexPath?
    var dno = ""
    var isJp = false
    var country = "TW"
    var mainColor = UIColor(hex: 0xF5A623)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.separatorStyle = .none
        
        self.navigationItem.title = "選擇上車時間"
        let backButton = UIBarButtonItem()
        backButton.title = " "
        backButton.tintColor = .white
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        if isJp{
            mainColor = UIColor(hex: 0xFC989F)
            country = "JP"
        }else{
            mainColor = UIColor(hex: 0xF5A623)
            country = "TW"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        flightTitle.textColor = mainColor
        carInfoTitle.textColor = mainColor
        nextButton.backgroundColor = mainColor
        if isJp{
            vanTypeLabel.text = "日本共乘類型"
            vanType.textColor = UIColor(hex: 0x33C5D4)
            bottomView.backgroundColor = UIColor(hex: 0x33C5D4)
            faVanButton.setTitleColor(UIColor(hex: 0x33C5D4), for: .normal)
        }else{
            vanTypeLabel.text = "TaiVAN大共乘類型"
            vanType.textColor = UIColor(hex: 0xFF5C00)
            bottomView.backgroundColor = UIColor(hex: 0xFF5C00)
            faVanButton.setTitleColor(UIColor(hex: 0xFF5C00), for: .normal)
        }
        
        selectIndexPath = nil
        if flightCardInfo.count > 0{
            flighiNoLabel.text = flightCardInfo[0]
            terminalLabel.text = flightCardInfo[1]
            startCityLabel.text = flightCardInfo[2]
            endCityLabel.text = flightCardInfo[3]
            startTimeLabel.text = self.toTimeFormaater(timeString: flightCardInfo[4])
            endTimeLabel.text = self.toTimeFormaater(timeString: flightCardInfo[5])
        }
        print(timeList.count)
        tableView.reloadData()
        if orderModel.stype == "I"{
            carInfoTitle.text = "車班資訊\n(依航班實際抵達時間起等候60分鐘)"
        }else{
            carInfoTitle.text = "車班資訊"
        }
    } 
    
    override func viewDidAppear(_ animated: Bool) {
        startTimerForShowScrollIndicator()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        stopTimerForShowScrollIndicator()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func selectRadio(_ sender:UIButton) {
        print(sender.tag)
        
    }
    
    @IBAction func next(_ sender: BorderCornerButton) {
        if orderModel.sdate == "" || orderModel.ptime == "" || selectIndexPath == nil{
            self.present(AlertViewModel.shared.showAlert(message: "請選擇車班時間"), animated: true, completion: nil)
            return
        }
//        if orderModel.stype == "I"{
//            let fdt = "\(orderModel.fdate)\(orderModel.ftime)"
//            let sdt = "\(orderModel.sdate)\(orderModel.ptime)"
//            print("sdt:\(sdt) fdt:\(fdt)")
//            let format = DateFormatter()
//            format.dateFormat = "yyyyMMddHHmm"
//            let dfdt = format.date(from: fdt)!
//            let dsdt = format.date(from: sdt)!
//            if dfdt + 90 * 60 > dsdt{
//                let alertController = UIAlertController(title: "風險提示", message: "依據媒合時間，共乘服務最多等候5分鐘為限，請保留足夠緩衝時間，我們將不承擔航班延誤所產生之搭乘權益受損。", preferredStyle: .alert)
//                let okAction = UIAlertAction(title: "確定預約", style: .default) { (alert) in
//                    self.performSegue(withIdentifier: "goCheck", sender: nil)
//                }
//                let cancelAction = UIAlertAction(title: "取消預約", style: .cancel, handler: nil)
//                alertController.addAction(okAction)
//                alertController.addAction(cancelAction)
//                self.present(alertController, animated: true, completion: nil)
//                return
//            }
//        }
        self.performSegue(withIdentifier: "goCheck", sender: nil)
    }
    
    @IBAction func fa(_ sender: CornerButton) {
        CallCarAPI.shared.getVANOTime(parameters: ["fdate":self.orderModel.fdate,"ftime":self.orderModel.ftime,"tdate":self.orderModel.sdate,"city":self.orderModel.pcity,"dist":self.orderModel.pdistinct,"airport":self.orderModel.airport,"country":country]) { (success, res, sCode, mss) in
            if sCode >= 900{
                self.goRoot()
                return
            }
            if success{
                print(res)
                self.minDateString = res["btime"].stringValue
                self.maxDateString = res["etime"].stringValue
                
                self.performSegue(withIdentifier: "alert", sender: nil)
            }else{
                if mss == ""{
                    self.present(AlertViewModel.shared.showAlert(message: "根據航班時間，此時段不提供服務"), animated: true, completion: nil)
                }else{
                    self.present(AlertViewModel.shared.showAlert(message: mss), animated: true, completion: nil)
                }
            }
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goCheck"{
            let vc = segue.destination as! CheckOutViewController
            vc.orderModel = orderModel
            vc.dno = dno
            vc.isJp = isJp
        }else if segue.identifier == "fa"{
            let vc = segue.destination as! VanCarInfoViewController
            vc.orderModel = orderModel
            vc.flightCardInfo = flightCardInfo
            vc.minDateString = minDateString
            vc.maxDateString = maxDateString
            vc.timeList = timeList
            vc.isJp = isJp
        }else if segue.identifier == "alert"{
            let vc = segue.destination as! vanInitiateAlertViewController
            vc.delegate = self
            vc.isTopTips = false
            vc.isJp = isJp
        }
    }
    
    /// Show always scroll indicator in table view
    @objc func showScrollIndicatorsInContacts() {
        UIView.animate(withDuration: 0.001) {
            self.tableView.flashScrollIndicators()
        }
    }
    
    /// Start timer for always show scroll indicator in table view
    func startTimerForShowScrollIndicator() {
        self.timerForShowScrollIndicator = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.showScrollIndicatorsInContacts), userInfo: nil, repeats: true)
    }
    
    /// Stop timer for always show scroll indicator in table view
    func stopTimerForShowScrollIndicator() {
        self.timerForShowScrollIndicator?.invalidate()
        self.timerForShowScrollIndicator = nil
    }
    
    
}

extension VanPickCarInfoViewController: UITableViewDataSource,UITableViewDelegate,vanInitiateAlertViewControllerDelegate{
    func vanInitiateAlertOK() {
        self.performSegue(withIdentifier: "fa", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return timeList.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "pickCarTimeCell")! as! PickCarTimeTableViewCell
        cell.timeLabel.isHidden = orderModel.stype == "I"
        cell.timeLabel.text = self.toTimeFormaater(timeString: timeList[indexPath.row].carTime)
        cell.dateLabel.text = self.toDateFormaater(dateString: timeList[indexPath.row].carDate)
        cell.bagCount.text = timeList[indexPath.row].rbcnt
        cell.personCount.text = timeList[indexPath.row].rpcnt
        cell.stopPlace.text = "\(timeList[indexPath.row].city)\(timeList[indexPath.row].distinct)"
        cell.shareCode.text = "\(timeList[indexPath.row].rno.prefix(4))****\(timeList[indexPath.row].rno.suffix(3))"
        if timeList[indexPath.row].carType == "四人座"{
            cell.carSeat.text = "四人座"
            if isJp{
                cell.carImg.image = UIImage(named: "car_four_jp")
            }else{
                cell.carImg.image = UIImage(named: "Frame_four")
            }
        }else{
            cell.carSeat.text = "七人座"
            if isJp{
                cell.carImg.image = UIImage(named: "car_seven_jp")
            }else{
                cell.carImg.image = UIImage(named: "Frame_seven")
            }
        }
        if let _ =  self.selectIndexPath, indexPath.row == selectIndexPath?.row {
            cell.radioButton.isSelected = true
            cell.cornerView.borderColor = UIColor(hex: 0xF5A623)
            orderModel.sdate = timeList[indexPath.row].carDate
            orderModel.ptime = timeList[indexPath.row].carTime
            dno = timeList[indexPath.row].dno
        } else {
            cell.radioButton.isSelected = false
            cell.cornerView.borderColor = UIColor(hex: 0xCCCCCC)
            
        }
        print(cell.cornerView.frame.height)
        cell.radioButton.tag = indexPath.row
        
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectIndexPath = indexPath
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
