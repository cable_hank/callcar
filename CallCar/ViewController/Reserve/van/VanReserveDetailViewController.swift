//
//  VanReserveDetailViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2019/5/28.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import UIKit

class VanReserveDetailViewController: UIViewController {
    
    @IBOutlet weak var onCarDateTextField: LeftIconTextField!
    @IBOutlet weak var carTypeTextField: LeftIconTextField!
    @IBOutlet weak var personCountTextField: LeftIconTextField!
    @IBOutlet weak var bagCountTextField: LeftIconTextField!
    @IBOutlet weak var flightDateTextField: LeftIconTextField!
    @IBOutlet weak var flightNoTextField: LeftIconTextField!
    
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var flightNoLabel: UILabel!
    @IBOutlet weak var terminalLabel: UILabel!
    @IBOutlet weak var startCityLabel: UILabel!
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var endCityLabel: UILabel!
    @IBOutlet weak var endTimeLabel: UILabel!
    @IBOutlet weak var loadActivity: UIActivityIndicatorView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var flightView: FlightCardView!
    @IBOutlet weak var errorImg: UIImageView!
    
    
    @IBOutlet weak var onCarTitleLabel: UILabel!
    @IBOutlet weak var personCountTitleLabel: UILabel!
    @IBOutlet weak var bagCountTitleLabel: UILabel!
    @IBOutlet weak var flightDateTitleLabel: UILabel!
    @IBOutlet weak var carTypeTitleLabel: UILabel!
    @IBOutlet weak var flightNoTitleLabel: UILabel!
    @IBOutlet weak var tipsTitleLabel: UILabel!
    @IBOutlet weak var nextButton: BorderCornerButton!
    @IBOutlet weak var personCountUp: UIButton!
    @IBOutlet weak var personCountDown: UIButton!
    @IBOutlet weak var bagCountUp: UIButton!
    @IBOutlet weak var bagCountDown: UIButton!
    @IBOutlet weak var carTypeImg: UIImageView!
    @IBOutlet weak var calendarImg1: UIImageView!
    @IBOutlet weak var calendarImg2: UIImageView!
    
    
    var carTypePickerView = UIPickerView()
    var personCountPickerView = UIPickerView()
    var baggagePickerView = UIPickerView()
    var onCarDatePicker = UIDatePicker()
    var flightDatePicker = UIDatePicker()
    
    let carTypeArray = ["四人座","七人座"]
    var personCountArray = ["1","2","3","4","5","6","7"]
    var baggageCountArray = ["1","2","3","4","5","6","7"]
    var flightCardInfo = ["","","","","","","",""]
    
    var timeList = [CarTimeModel]()
    let formatter = DateFormatter()
    let formatterForApi = DateFormatter()
    var orderModel = OrderModel()
    var isFlightNoOk = false
    var timer = Timer()
    var isJp = false
    var country = "TW"
    
    var minDateString = ""
    var maxDateString = ""
    var mainColor = UIColor(hex: 0xF5A623)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        personCountPickerView.dataSource = self
        personCountPickerView.delegate = self
        baggagePickerView.dataSource = self
        baggagePickerView.delegate = self
        carTypePickerView.dataSource = self
        carTypePickerView.delegate = self
        
        onCarDateTextField.inputView = onCarDatePicker
        carTypeTextField.inputView = carTypePickerView
        personCountTextField.inputView = personCountPickerView
        bagCountTextField.inputView = baggagePickerView
        flightDateTextField.inputView = flightDatePicker
        //        personCountTextField.backgroundColor = .clear
        //        bagCountTextField.backgroundColor = .clear
        
        onCarDatePicker.tag = 0
        personCountPickerView.tag = 1
        baggagePickerView.tag = 2
        onCarDatePicker.tag = 0
        flightDatePicker.tag = 1
        
        formatter.dateFormat = "yyyy/MM/dd"
        formatterForApi.dateFormat = "yyyyMMdd"
        onCarDatePicker.datePickerMode = .date
        flightDatePicker.datePickerMode = .date
        onCarDatePicker.addTarget(self,action: #selector(self.datePickerChanged),for: .valueChanged)
        flightDatePicker.addTarget(self,action: #selector(self.datePickerChanged),for: .valueChanged)
        onCarDateTextField.text = formatter.string(from: onCarDatePicker.date)
        flightDateTextField.text = formatter.string(from: flightDatePicker.date)
        
        textView.layer.cornerRadius = 10
        let backButton = UIBarButtonItem()
        backButton.title = " "
        backButton.tintColor = .white
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        if isJp{
            self.title = "預約日本共乘"
            country = "JP"
        }else{
            self.title = "預約TaiVAN大共乘"
            country = "TW"
        }
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil)
    }
    
    func addDay(day:Int,date:Date) -> Date{
        return Calendar.current.date(byAdding: Calendar.Component.day, value: day, to: date)!
    }
    
    func changeDatePicker(date:Date){
        if orderModel.stype == "O"{
            flightDatePicker.minimumDate = date
            flightDatePicker.maximumDate = addDay(day: 1, date: date)
        }else{
            flightDatePicker.minimumDate = addDay(day: -1, date: date)
            flightDatePicker.maximumDate = date
        }
        flightDatePicker.date = date
        flightDateTextField.text = formatter.string(from: date)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.barTintColor = mainColor
        onCarTitleLabel.textColor = mainColor
        personCountTitleLabel.textColor = mainColor
        bagCountTitleLabel.textColor = mainColor
        flightDateTitleLabel.textColor = mainColor
        carTypeTitleLabel.textColor = mainColor
        flightNoTitleLabel.textColor = mainColor
        tipsTitleLabel.textColor = mainColor
        nextButton.backgroundColor = mainColor
        personCountUp.setTitleColor(mainColor, for: .normal)
        personCountDown.setTitleColor(mainColor, for: .normal)
        bagCountUp.setTitleColor(mainColor, for: .normal)
        bagCountDown.setTitleColor(mainColor, for: .normal)
        if isJp{
            if orderModel.stype == "I"{
                self.title = "預約日本共乘(往市區)"
            }else{
                self.title = "預約日本共乘(往機場)"
            }
            calendarImg1.image = UIImage(named: "jp_date")
            calendarImg2.image = UIImage(named: "jp_date")
            carTypeImg.image = UIImage(named: "sel_jp")
        }else{
            if orderModel.stype == "I"{
                self.title = "預約TaiVAN大共乘(往市區)"
            }else{
                self.title = "預約TaiVAN大共乘(往機場)"
            }
            calendarImg1.image = UIImage(named: "vanpool_date")
            calendarImg2.image = UIImage(named: "vanpool_date")
            carTypeImg.image = UIImage(named: "sel_vanpool")
        }
        
        print(orderModel.stype)
        UserDefaults.standard.setValue("1", forKey: UserInfo.statusColor.rawValue)
        UIApplication.shared.statusBarStyle = .lightContent
        self.tabBarController?.tabBar.isHidden = true
        if isJp{
            onCarDatePicker.minimumDate = Date(timeIntervalSinceNow: 60*60*24*5)
        }else{
            onCarDatePicker.minimumDate = Date(timeIntervalSinceNow: 60*60*24*3)
        }
        onCarDatePicker.maximumDate = Date(timeIntervalSinceNow: 60*60*24*90)
        changeDatePicker(date:onCarDatePicker.date)
        
        
        onCarDateTextField.text = formatter.string(from: onCarDatePicker.date)
        
        if orderModel.stype == "O"{
            print("出境")
        }else if orderModel.stype == "I"{
            print("入境")
        }
        if orderModel.cpflag == "S"{
            print("共乘")
        }else if orderModel.cpflag == "P"{
            print("專接")
        }else{
            print("專接共乘")
        }
    }
    
    @objc func datePickerChanged(datePicker:UIDatePicker) {
        if datePicker.tag == 0{
            onCarDateTextField.text = formatter.string(from: datePicker.date)
            changeDatePicker(date:datePicker.date)
        }else{
            flightDateTextField.text = formatter.string(from: datePicker.date)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func next(_ sender: BorderCornerButton) {
        if self.isFlightNoOk{
            var fdate = ""
            var ftime = ""
            if self.orderModel.stype == "I"{
                self.orderModel.sdate = String(self.flightCardInfo[6].prefix(8))
                fdate = String(self.flightCardInfo[6].prefix(8))
                ftime  = String(self.flightCardInfo[6].suffix(4))
            }else{
                self.orderModel.sdate = self.formatterForApi.string(from: self.onCarDatePicker.date)
                fdate = String(self.flightCardInfo[7].prefix(8))
                ftime  = String(self.flightCardInfo[7].suffix(4))
            }
            self.orderModel.fdate = fdate
            self.orderModel.ftime = ftime
            self.orderModel.pcnt = Int(self.personCountTextField.text!)!
            self.orderModel.bcnt = Int(self.bagCountTextField.text!)!
            self.orderModel.fno = self.flightNoTextField.text!
            self.orderModel.cartype = self.carTypeTextField.text!
            self.orderModel.cpflag = "V"
            
            var placeArr = [Double]()
            if orderModel.stype == "I"{
                placeArr = [orderModel.tlat,orderModel.tlng]
            }else if orderModel.stype == "O"{
                placeArr = [orderModel.plat,orderModel.plng]
            }
            
            var parmeters:[String:Any] = ["fdate":fdate,"ftime":ftime,"stype":orderModel.cpflag,"ftype":orderModel.stype,"lat":placeArr[0],"lng":placeArr[1],"airport":self.orderModel.airport,"ter":orderModel.terminal,"pcnt":orderModel.pcnt,"bcnt":orderModel.bcnt,"fno":self.orderModel.fno,"country":country]
            if orderModel.stype == "O"{
                parmeters["city"] = orderModel.pcity
                parmeters["dist"] = orderModel.pdistinct
            }else{
                parmeters["city"] = orderModel.tcity
                parmeters["dist"] = orderModel.tdistinct
            }
            print("--------------------")
            print(parmeters)
            print("--------------------")
            
            let loadvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "load")
            loadvc.modalPresentationStyle = .overFullScreen
            self.present(loadvc, animated: false, completion: nil)
            CallCarAPI.shared.gVanTimeCar(parameters: parmeters, success: { (success, res,error,errorString,sCode) in
                if sCode >= 900{
                    self.dismiss(animated: false, completion: nil)
                    self.goRoot()
                    return
                }
                sender.isEnabled = true
                if self.isApiError(error: error){
                    
                    self.dismiss(animated: false, completion: nil)
                    let alertController = UIAlertController(title: "提示",message: "連線失敗",preferredStyle: .alert)
                    let okActtion = UIAlertAction(title: "確定", style: .default) { (UIAlertAction) in
                        let storyboard = UIStoryboard(name: "TabBar", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "TabBarVC")
                        self.present(vc, animated: false, completion: nil)
                    }
                    alertController.addAction(okActtion)
                    self.present(alertController, animated: true, completion: nil)
                    return
                }
                if !success {
                    self.dismiss(animated: false, completion: nil)
                    self.present(AlertViewModel.shared.showAlert(message: errorString), animated: true, completion: nil)
                    return
                }
                print("-------------------")
                print(res.arrayValue.count)
                self.timeList = []
                if res.arrayValue.count > 0{
                    for r in res.arrayValue{
                        let carTime = CarTimeModel(carDate: r["date"].stringValue, carTime: r["time"].stringValue, dno:r["dno"].stringValue, city: r["city"].stringValue, distinct: r["distinct"].stringValue, rbcnt: r["rbcnt"].stringValue, rpcnt: r["rpcnt"].stringValue,carType: r["cartype"].stringValue,rno:r["rno"].arrayValue[0].stringValue)
                        self.timeList.append(carTime)
                    }
                    
                    self.dismiss(animated: false, completion: nil)
                    self.performSegue(withIdentifier: "pick", sender: nil)
                    return
                }else{
                    CallCarAPI.shared.chkVip(parameters: ["date":self.formatterForApi.string(from: self.onCarDatePicker.date),"country":self.country]) { (success, res, error, mss,sCode) in
                        if sCode >= 900{
                            
                            self.dismiss(animated: false, completion: nil)
                            self.goRoot()
                            return
                        }
                        print(res)
                        if self.isApiError(error: error){
                            
                            self.dismiss(animated: false, completion: nil)
                            let alertController = UIAlertController(title: "提示",message: "連線失敗",preferredStyle: .alert)
                            let okActtion = UIAlertAction(title: "確定", style: .default) { (UIAlertAction) in
                                let storyboard = UIStoryboard(name: "TabBar", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "TabBarVC")
                                self.present(vc, animated: false, completion: nil)
                            }
                            alertController.addAction(okActtion)
                            self.present(alertController, animated: true, completion: nil)
                            return
                        }
                        if error != nil{
                            self.dismiss(animated: false, completion: nil)
                            self.present(AlertViewModel.shared.showAlert(message: "請確認網路狀態"), animated: true, completion: nil)
                            return
                        }
                        self.dismiss(animated: false, completion: nil)
                        if success{ print(["fdate":fdate,"ftime":ftime,"tdate":self.orderModel.sdate,"city":self.orderModel.pcity,"dist":self.orderModel.pdistinct,"airport":self.orderModel.airport])
                            if self.orderModel.stype == "O"{
                                CallCarAPI.shared.getVANOTime(parameters: ["fdate":self.orderModel.fdate,"ftime":self.orderModel.ftime,"tdate":self.orderModel.sdate,"city":self.orderModel.pcity,"dist":self.orderModel.pdistinct,"airport":self.orderModel.airport,"country":self.country]) { (success, res, sCode, mss) in
                                    if sCode >= 900{
                                        self.goRoot()
                                        return
                                    }
                                    if success{
                                        print(res)
                                        self.minDateString = res["btime"].stringValue
                                        self.maxDateString = res["etime"].stringValue
                                        
                                        self.performSegue(withIdentifier: "alert", sender: nil)
                                    }else{
                                        if mss == ""{
                                            self.present(AlertViewModel.shared.showAlert(message: "根據航班時間，此時段不提供服務"), animated: true, completion: nil)
                                        }else{
                                            self.present(AlertViewModel.shared.showAlert(message: mss), animated: true, completion: nil)
                                        }
                                    }
                                }
                            }else{
                                self.performSegue(withIdentifier: "goCarInfo", sender: nil)
                            }
                            
                            
                        }else{
                            self.present(AlertViewModel.shared.showAlert(message: mss), animated: true, completion: nil)
                            return
                        }
                        
                    }
                }
                
            })
        }else{
            self.present(AlertViewModel.shared.showAlert(message: "請輸入正確的航班編號與日期"), animated: true, completion: nil)
        }
        
    }
    
    @IBAction func personUp(_ sender: UIButton) {
        if carTypeTextField.text == "四人座"{
            if personCountTextField.text! == "2"{
                return
            }
            
            personCountTextField.text = String(Int(personCountTextField.text!)!+1)
            if bagCountTextField.text! == "2"{
                return
            }
            bagCountTextField.text = personCountTextField.text!
        }else{
            if personCountTextField.text! == "5"{
                return
            }
            personCountTextField.text = String(Int(personCountTextField.text!)!+1)
            if bagCountTextField.text! == "5"{
                return
            }
            bagCountTextField.text = personCountTextField.text!
        }
        
    }
    
    @IBAction func personDown(_ sender: UIButton) {
        if personCountTextField.text! == "1"{
            return
        }
        personCountTextField.text = String(Int(personCountTextField.text!)!-1)
        bagCountTextField.text = personCountTextField.text!
    }
    
    @IBAction func bagUp(_ sender: UIButton) {
        
        if carTypeTextField.text == "四人座"{
            if bagCountTextField.text! == "2"{
                return
            }
        }else{
            if bagCountTextField.text! == "5"{
                return
            }
        }
        bagCountTextField.text = String(Int(bagCountTextField.text!)!+1)
        
    }
    
    @IBAction func bagDown(_ sender: UIButton) {
        if bagCountTextField.text! == "1"{
            return
        }
        bagCountTextField.text = String(Int(bagCountTextField.text!)!-1)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goCarInfo"{
            let vc = segue.destination as! VanCarInfoViewController
            vc.orderModel = orderModel
            vc.flightCardInfo = flightCardInfo
            vc.minDateString = minDateString
            vc.maxDateString = maxDateString
            vc.timeList = timeList
            vc.isJp = isJp
        }else if segue.identifier == "alert"{
            let vc = segue.destination as! vanInitiateAlertViewController
            vc.delegate = self
            vc.isJp = isJp
        }else if segue.identifier == "pick"{
            let vc = segue.destination as! VanPickCarInfoViewController
            vc.orderModel = orderModel
            vc.flightCardInfo = flightCardInfo
            vc.timeList = timeList
            vc.isJp = isJp
        }
    }
    @IBAction func editFlightDate(_ sender: LeftIconTextField) {
        playActivity(isPlay: true)
        DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
            self.getFlightInfo()
        }
        
    }
    @IBAction func editFlightNo(_ sender: LeftIconTextField) {
        sender.text = sender.text?.uppercased()
        if sender.text != ""{
            playActivity(isPlay: true)
            self.errorImg.isHidden = true
            self.flightView.isHidden = true
            self.isFlightNoOk = false
            timer.invalidate()
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.getFlightInfo), userInfo: nil, repeats: false)
        }
    }
    
    func playActivity(isPlay:Bool){
        if isPlay{
            loadActivity.startAnimating()
        }else{
            loadActivity.stopAnimating()
        }
    }
    
    @objc func getFlightInfo() {
        print("stype:\(orderModel.stype)")
        if flightNoTextField.text!.count < 3{
            self.errorImg.isHidden = false
            self.errorImg.blink()
            self.flightView.isHidden = true
            self.isFlightNoOk = false
            playActivity(isPlay: false)
            return
        }
        isFlightNoOk = false
        flightView.isHidden = true
        self.errorImg.isHidden = true
        CallCarAPI.shared.checkFlight(parameters: ["no":flightNoTextField.text!,"date":formatterForApi.string(from: flightDatePicker.date),"type":orderModel.stype,"country":country], success: { (success, res,sCode) in
            if sCode >= 900{
                self.goRoot()
                return
            }
            print(res)
            if success{
                if res.count == 0{
                    self.playActivity(isPlay: false)
                    self.errorImg.isHidden = false
                    self.errorImg.blink()
                    self.flightView.isHidden = true
                    self.isFlightNoOk = false
                    return
                }
                if res[0]["da"].stringValue != self.orderModel.airport && res[0]["aa"].stringValue != self.orderModel.airport{
                    print("中獎")
                    self.playActivity(isPlay: false)
                    self.errorImg.isHidden = false
                    self.errorImg.blink()
                    self.flightView.isHidden = true
                    self.isFlightNoOk = false
                    return
                }
                self.playActivity(isPlay: false)
                self.errorImg.isHidden = true
                self.flightView.isHidden = false
                self.isFlightNoOk = true
                
                print(res[0]["der"].stringValue)
                var airportAddr = ""
                
                switch self.orderModel.airport {
                case "TPE":
                    if res[0]["ter"].stringValue == "1"{
                        self.terminalLabel.text = "第一航廈"
                        self.orderModel.terminal = "T1"
                        self.flightCardInfo[1] = "第一航廈"
                        airportAddr = "桃園國際機場 第一航廈"
                    }else if res[0]["ter"].stringValue == "2"{
                        self.terminalLabel.text = "第二航廈"
                        self.orderModel.terminal = "T2"
                        self.flightCardInfo[1] = "第二航廈"
                        airportAddr = "桃園國際機場 第二航廈"
                    }else if res[0]["ter"].stringValue == "3"{
                        self.terminalLabel.text = "第三航廈"
                        self.orderModel.terminal = "T3"
                        self.flightCardInfo[1] = "第三航廈"
                        airportAddr = "桃園國際機場 第三航廈"
                    }else{
                        self.terminalLabel.text = ""
                        self.orderModel.terminal = ""
                        self.flightCardInfo[1] = ""
                        airportAddr = "桃園國際機場"
                    }
                case "KHH":
                    self.terminalLabel.text = "高雄機場"
                    self.orderModel.terminal = ""
                    self.flightCardInfo[1] = "高雄機場"
                    airportAddr = "高雄國際機場"
                case "KIX":
                    self.terminalLabel.text = "關西國際機場"
                    self.orderModel.terminal = ""
                    self.flightCardInfo[1] = "關西國際機場"
                    airportAddr = "關西國際機場"
                case "NGO":
                    self.terminalLabel.text = "中部國際機場"
                    self.orderModel.terminal = ""
                    self.flightCardInfo[1] = "中部國際機場"
                    airportAddr = "中部國際機場"
                case "HND":
                    self.terminalLabel.text = "東京國際機場"
                    self.orderModel.terminal = ""
                    self.flightCardInfo[1] = "東京國際機場"
                    airportAddr = "東京國際機場"
                case "NRT":
                    self.terminalLabel.text = "成田國際機場"
                    self.orderModel.terminal = ""
                    self.flightCardInfo[1] = "成田國際機場"
                    airportAddr = "成田國際機場"
                case "CTS":
                    self.terminalLabel.text = "新千歲機場"
                    self.orderModel.terminal = ""
                    self.flightCardInfo[1] = "新千歲機場"
                    airportAddr = "新千歲機場"
                default:
                    break
                }
                
                if self.orderModel.stype == "O"{
                    self.orderModel.taddress = airportAddr
                }else{
                    self.orderModel.paddress = airportAddr
                }
                self.flightNoLabel.text = res[0]["fno"].stringValue
                self.startCityLabel.text = res[0]["da"].stringValue
                self.endCityLabel.text = res[0]["aa"].stringValue
                self.startTimeLabel.text = self.toTimeFormaater(timeString: res[0]["dt"].stringValue)
                self.endTimeLabel.text = self.toTimeFormaater(timeString: res[0]["at"].stringValue)
                
                self.flightCardInfo[0] = res[0]["fno"].stringValue
                self.flightCardInfo[2] = res[0]["da"].stringValue
                self.flightCardInfo[3] = res[0]["aa"].stringValue
                self.flightCardInfo[4] = res[0]["dt"].stringValue
                self.flightCardInfo[5] = res[0]["at"].stringValue
                self.flightCardInfo[6] = res[0]["fullat"].stringValue
                self.flightCardInfo[7] = res[0]["fulldt"].stringValue
                
            }else{
                self.playActivity(isPlay: false)
                self.errorImg.isHidden = false
                self.errorImg.blink()
                self.flightView.isHidden = true
                self.isFlightNoOk = false
            }
        })
    }
    
    
    var textFieldY:CGFloat = 0
    var isKeyboardShown = false
    
    @IBAction func textFieldDidBegin(_ sender: UITextField) {
        let relativePositionView = UIApplication.shared.keyWindow
        let relativeFrame = sender.superview?.convert(sender.frame, to: relativePositionView)
        let yPosition = relativeFrame?.minY;
        textFieldY = yPosition! + sender.frame.height * 3
        
    }
    
    @objc func keyboardWillShow(note: NSNotification) {
        if isKeyboardShown {
            return
        }
        let keyboardAnimationDetail = note.userInfo as! [String: AnyObject]
        let duration = TimeInterval(truncating: keyboardAnimationDetail[UIResponder.keyboardAnimationDurationUserInfoKey]! as! NSNumber)
        let keyboardFrameValue = keyboardAnimationDetail[UIResponder.keyboardFrameBeginUserInfoKey]! as! NSValue
        let keyboardFrame = keyboardFrameValue.cgRectValue
        
        if self.view.frame.height - keyboardFrame.size.height > textFieldY{
            return
        }
        
        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: -(keyboardFrame.size.height - (self.view.frame.height - self.textFieldY)))
        })
        isKeyboardShown = true
    }
    
    @objc func keyboardWillHide(note: NSNotification) {
        let keyboardAnimationDetail = note.userInfo as! [String: AnyObject]
        let duration = TimeInterval(truncating: keyboardAnimationDetail[UIResponder.keyboardAnimationDurationUserInfoKey]! as! NSNumber)
        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy:  -self.view.frame.origin.y)
        })
        isKeyboardShown = false
    }
}

extension VanReserveDetailViewController:UIPickerViewDataSource,UIPickerViewDelegate,vanInitiateAlertViewControllerDelegate{
    func vanInitiateAlertOK() {
        self.performSegue(withIdentifier: "goCarInfo", sender: nil)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 0 {
            return carTypeArray.count
        }else if pickerView.tag == 1{
            return personCountArray.count
        }else{
            return baggageCountArray.count
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 0{
            return self.carTypeArray[row]
        }else if pickerView.tag == 1{
            return self.personCountArray[row]
        }else{
            return self.baggageCountArray[row]
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 0{
            self.carTypeTextField.text = carTypeArray[row]
            if carTypeArray[row] == "四人座"{
                personCountArray = ["1","2","3","4","5"]
                baggageCountArray = ["1","2","3","4","5"]
            }else{
                personCountArray = ["1","2","3","4","5","6","7"]
                baggageCountArray = ["1","2","3","4","5","6","7"]
            }
            personCountTextField.text = "1"
            bagCountTextField.text = "1"
            personCountPickerView.selectRow(0, inComponent: 0, animated: false)
            baggagePickerView.selectRow(0, inComponent: 0, animated: false)
            personCountPickerView.reloadComponent(0)
            baggagePickerView.reloadComponent(0)
            
        }else if pickerView.tag == 1{
            self.personCountTextField.text = personCountArray[row]
            self.bagCountTextField.text = baggageCountArray[row]
            baggagePickerView.selectRow(row, inComponent: 0, animated: false)
        }else{
            self.bagCountTextField.text = baggageCountArray[row]
        }
    }
    
}
