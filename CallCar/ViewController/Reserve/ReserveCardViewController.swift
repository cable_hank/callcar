//
//  ReserveCardViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2019/5/14.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import UIKit
import Firebase

class ReserveCardViewController: UIViewController,EvaluationAlertDelegate {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    
    var type = 0
    var noticeURL = ""
    var isloadingOK = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.setValue("0", forKey: UserInfo.statusColor.rawValue)
        
        //STOMAP SDK
        if #available(iOS 10.0, *) {
            let id = UserDefaults.standard.getString(key: UserInfo.id.rawValue)
            STOMAPButterManager.shared().start(withApiKey: "7aafc69cc581497abc0439dc25da0528", aliasKey: id, aliasType: STOMAPButterflySDKAliasTypeOTHER)
            STOMAPButterManager.shared().delegate = self
        }
        
        
        //教學頁
//        if UserDefaults.standard.string(forKey: UserInfo.isTeach.rawValue) == nil{
//            UserDefaults.standard.set(true, forKey: UserInfo.isTeach.rawValue)
//            self.performSegue(withIdentifier: "showTeach", sender: nil)
//        }
        
        
        titleLabel.text = "Hi!\(UserDefaults.standard.getString(key: UserInfo.fname.rawValue))~歡迎您的預約搭乘"
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.tabBarController?.tabBar.isHidden = false
        isloadingOK = false
        
        if checkInternetFunction(){
            CallCarAPI.shared.testAPI(parameters: [:]) { (success, res, error, mss) in
                if !success{
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "MaintainVC")
                    self.present(vc, animated: false, completion: nil)
                }
            }
        }
        CallCarAPI.shared.getTokenlize(parameters: ["acc":UserDefaults.standard.getString(key: UserInfo.account.rawValue),"pw":UserDefaults.standard.getString(key: UserInfo.pw.rawValue)]) { (success, rtn, error, errorString) in
            print(rtn)
            if rtn == "UnAuthorize"{
                RealmDB.shared.deleteAll()
                Messaging.messaging().unsubscribe(fromTopic: "iOS")
                Messaging.messaging().unsubscribe(fromTopic: UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!)
                UserNotificationTools().deleteAllNotifications()
                AnalyticsConfiguration.shared().setAnalyticsCollectionEnabled(false)
                UserDefaults.standard.removeObject(forKey: UserInfo.account.rawValue)
                UserDefaults.standard.removeObject(forKey: UserInfo.mobile.rawValue)
                UserDefaults.standard.removeObject(forKey: UserInfo.fname.rawValue)
                UserDefaults.standard.removeObject(forKey: UserInfo.lname.rawValue)
                UserDefaults.standard.removeObject(forKey: UserInfo.id.rawValue)
                UserDefaults.standard.removeObject(forKey: UserInfo.noticeDate.rawValue)
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
                let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDel.window?.rootViewController = vc
                return
            }
            UserDefaults.standard.set(rtn, forKey: UserInfo.tokenlize.rawValue)
            self.isloadingOK = true
            //取通知
            CallCarAPI.shared.getBulletin(parameters: ["uid":UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!,"platform":"iOS"]) { (success, res, error, errormss,sCode) in

                if success {
                    RM_BulletinModel().save(json: res)
                }
                if let tabItems = self.tabBarController?.tabBar.items {
                    let tabItem = tabItems[3]
                    if RM_BulletinModel().getIfIsLookFalse(){
                        tabItem.badgeValue = "!"
                    }else{
                        tabItem.badgeValue = nil
                    }
                }
            }
            
            //firebaseToken
            InstanceID.instanceID().instanceID(handler: { (result, error) in
                if let error = error {
                    print("Error fetching remote instange ID: \(error)")
                } else if let result = result {
                    print("Remote instance ID token: \(result.token)")
                    Messaging.messaging().subscribe(toTopic: "iOS")
                    Messaging.messaging().subscribe(toTopic: "TW")
                    Messaging.messaging().subscribe(toTopic: UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!)
                    let parameters:[String:Any] = ["token":result.token,"uid":Int(UserDefaults.standard.getString(key: UserInfo.id.rawValue))!,"os":"iOS","osver":UIDevice.current.systemVersion,"appver":Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String,"brand":"apple","model":UIDevice.current.model]
                    CallCarAPI.shared.updToken(parameters: parameters) { (success, res, error, mss,sCode) in
                        
                    }
                }
            })
            //評價
            if let isAlertStoreKit = UserDefaults.standard.string(forKey: UserInfo.isAlertStoreKit.rawValue){
                if isAlertStoreKit == "false"{
                    UserDefaults.standard.set("true", forKey: UserInfo.isAlertStoreKit.rawValue)
                    let storyboard = UIStoryboard(name: "TabBar", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "EvalutionAlert") as! EvaluationAlertViewController
                    vc.delegate = self
                    //                vc.modalPresentationStyle = .overFullScreen
                    self.present(vc, animated: true, completion: nil)
                }else{
                    self.alertNotice()
                }
            }else{
                if UserDefaults.standard.getString(key: UserInfo.missionOrder.rawValue) == "1"{
                    
                    UserDefaults.standard.set("true", forKey: UserInfo.isAlertStoreKit.rawValue)
                    let storyboard = UIStoryboard(name: "TabBar", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "EvalutionAlert") as! EvaluationAlertViewController
                    vc.delegate = self
                    //                vc.modalPresentationStyle = .overFullScreen
                    self.present(vc, animated: true, completion: nil)
                }else{
                    self.alertNotice()
                }
            }
            
        }
        
    }
    
    func showNotice() {
        alertNotice()
    }
    func alertNotice() {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMdd"
        let nowDateString = formatter.string(from: Date())
        if let noticeDateString = UserDefaults.standard.string(forKey: UserInfo.noticeDate.rawValue){
            if nowDateString != noticeDateString{
                UserDefaults.standard.set(nowDateString, forKey: UserInfo.noticeDate.rawValue)
                CallCarAPI.shared.getPromote(parameters: [:]) { (success, res, error, errmss) in
                    if success{
                        self.noticeURL = res
                        self.performSegue(withIdentifier: "noticePresent", sender: nil)
                    }
                }
            }
        }else{
            UserDefaults.standard.set(nowDateString, forKey: UserInfo.noticeDate.rawValue)
            CallCarAPI.shared.getPromote(parameters: [:]) { (success, res, error, errmss) in
                if success{
                    self.noticeURL = res
                    self.performSegue(withIdentifier: "noticePresent", sender: nil)
                }
            }
        }
    }
    
    @IBAction func goReserve(_ sender: UIButton) {
        if !isloadingOK{
            return
        }
        print(sender.tag)
//        if sender.tag == 2{
//            let alert = UIAlertController(title: "提示", message: "即將於7月4日開放預約", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "確定", style: .default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
//            return
//        }
        type = sender.tag
        self.performSegue(withIdentifier: "goReserve", sender: nil)
    }
    
    @IBAction func instruction(_ sender: UIButton) {
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goReserve"{
            let vc = segue.destination as! ReserveViewController
            print("reserveType:\(type)")
            vc.reserveType = type
        }else if segue.identifier == "noticePresent"{
            let vc = segue.destination as! NoticeAlertViewController
            //            print(noticeURL)
            vc.url = noticeURL
        }
    }
    

}

extension ReserveCardViewController:STOMAPButterManagerDelegate{
    func getLocationStatusIsAlways(_ value: Bool) {

    }

    func getResult(_ result: String) {

    }

    func getStatus(_ status: String) {

    }

    func getScanStatus(_ status: String) {

    }

    func getScanUUIDList(_ uuidLists: [Any]) {

    }

    func getScanFinalList(_ finalLists: [Any]) {

    }

    func getUpdateTime(_ updateTime: Int32) {

    }


}
