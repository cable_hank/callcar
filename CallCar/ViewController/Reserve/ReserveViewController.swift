//
//  ReserveViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/7.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit
import GoogleMaps
import SwiftyJSON
import NVActivityIndicatorView


class ReserveViewController: UIViewController,NewSendAddrDelegate,CLLocationManagerDelegate{
    var myLocationManager = CLLocationManager()
    
    let GeocodingAPIKey = "AIzaSyCmDSfdCv8XurR2G-ECzynxA_FgtdWFw60"
    let airport = ["桃園機場"]
    var pickerView = UIPickerView()
    let shareColor = UIColor(hex: 0x20B6FC)
    let singleColor = UIColor(hex: 0x3575C0)
    let vanColor = UIColor(hex: 0xF5A623)
    let jpColor = UIColor(hex: 0xFC989F)
    var isOK = false
    var isGoAirPort = true
    var posServiceModel:PosServiceModel = PosServiceModel.shared
    var orderModel:OrderModel = OrderModel()
    var locationModel = LocationModel()
    var addr = ""
//    var isPosition = false
    var airPortModelArr = [AirPortModel]()
    var airPortRow = 0
    var isRetry = false
    var noticeURL = ""
    var isUpdUserLoc = true
    var presentAddr = ""
    var level4 = ""
    var reserveType = 0  //0:共乘 1:專接 2:taivan 3:日本
    var laungue = "zh-Tw"
    var country = "TW"
    var isLoadFirstAirport = false
    
    
    @IBOutlet weak var tipsView: ShadowCornerView!
    @IBOutlet weak var tipsLabel: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var startLocationTextField: LeftTextUITextField!
    @IBOutlet weak var endLocationTextField: LeftTextUITextField!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var nvView: NVActivityIndicatorView!
    @IBOutlet weak var loadCar: UIImageView!
    @IBOutlet weak var vipView: CornerView!
    @IBOutlet weak var vipPrice: UILabel!
    
    @IBOutlet weak var startConstraint: NSLayoutConstraint!
    @IBOutlet weak var endConstraint: NSLayoutConstraint!
    @IBOutlet weak var tipsWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var pinImg: UIImageView!
    @IBOutlet weak var vipPriceView: UIView!
    @IBOutlet weak var vipPrice1: UILabel!
    @IBOutlet weak var vipPrice2: UILabel!
    @IBOutlet weak var vipPriceHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = " "
        self.navigationController?.navigationBar.barTintColor = shareColor
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        
        
        let camera = GMSCameraPosition.camera(withLatitude: 25.0461205936379, longitude: 121.517856605351, zoom: 16.0)
        mapView.camera = camera
        mapView.mapType = .normal
        mapView.settings.myLocationButton = true
        
        //修改textField時改為pickerView
        pickerView.delegate = self
        pickerView.dataSource = self
        endLocationTextField.inputView = pickerView
        startLocationTextField.inputView = UIView()
        
        // 設置委任對象
        myLocationManager.delegate = self
        // 距離篩選器 用來設置移動多遠距離才觸發委任方法更新位置
        myLocationManager.distanceFilter = kCLLocationAccuracyNearestTenMeters
        // 取得自身定位位置的精確度
        myLocationManager.desiredAccuracy = kCLLocationAccuracyBest
        if let location = myLocationManager.location {
            mapView.animate(toLocation: location.coordinate)
            mapView.animate(toZoom: 16)
        }
        
        vipPrice.adjustsFontSizeToFitWidth = true
        
        if reserveType == 0{
            submitButton.setTitle("  預約共乘", for: .normal)
            pinImg.image = UIImage(imageLiteralResourceName: "pin_blue")
            chageColor(color: shareColor)
        }else if reserveType == 1{
            submitButton.setTitle("  預約專接", for: .normal)
            pinImg.image = UIImage(imageLiteralResourceName: "pin_darkblue")
            chageColor(color: singleColor)
        }else if reserveType == 2{
            submitButton.setTitle("  預約TaiVAN大共乘", for: .normal)
            pinImg.image = UIImage(imageLiteralResourceName: "pin_orange")
            chageColor(color: vanColor)
        }else{
            submitButton.setTitle("  預約日本共乘", for: .normal)
            pinImg.image = UIImage(imageLiteralResourceName: "pin_jp")
            chageColor(color: jpColor)
            laungue = "ja-JP"
            country = "JP"
//            camera = GMSCameraPosition.camera(withLatitude: 35.678277, longitude: 139.769415, zoom: 16.0)
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // 首次使用 向使用者詢問定位自身位置權限
        if CLLocationManager.authorizationStatus() == .notDetermined {
            // 取得定位服務授權
            myLocationManager.requestAlwaysAuthorization()
            // 開始定位自身位置
//            myLocationManager.startUpdatingLocation()
            
        }else if CLLocationManager.authorizationStatus() == .denied {
            // 提示可至[設定]中開啟權限
//            let alertController = UIAlertController(title: "定位權限已關閉",message:"如要變更權限，請至 設定 > 隱私權 > 定位服務 開啟",preferredStyle: .alert)
//            let okAction = UIAlertAction(
//                title: "確認", style: .default, handler:nil)
//            alertController.addAction(okAction)
//            self.present(alertController,animated: true, completion: nil)
        }else if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            
            mapView.isMyLocationEnabled = true
            myLocationManager.startUpdatingLocation()
        }else if CLLocationManager.authorizationStatus() == .authorizedAlways{
            
            mapView.isMyLocationEnabled = true
            myLocationManager.startUpdatingLocation()
        }
        
    }

    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        if !isPosition{
//            let currentLocation: CLLocation = locations[0] as CLLocation
//            print("Current Location: \(currentLocation.coordinate.latitude), \(currentLocation.coordinate.longitude)")
//
//            if let location = locations.first {
//                mapView.animate(toLocation: location.coordinate)
//                mapView.animate(toZoom: 16)
//                myLocationManager.stopUpdatingLocation()
//
//                jsonToAddr(location.coordinate)
//                isPosition = true
//            }
//        }
    }
    
    @IBAction func next(_ sender: UIButton) {
        if isOK {
            orderModel = OrderModel()
            orderModel.uid = Int(UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!)
//            print("uid:\(UserDefaults.standard.string(forKey: UserInfo.id.rawValue))")
            if isGoAirPort{
                orderModel.stype = "O"
                
                orderModel.pcity = locationModel.city
                orderModel.pdistinct = locationModel.distinct
                orderModel.pvillage = locationModel.village
                orderModel.paddress = locationModel.address
                orderModel.plat = locationModel.lat
                orderModel.plng = locationModel.lng
                
                orderModel.tcity = airPortModelArr[self.airPortRow].city
                orderModel.tdistinct = airPortModelArr[self.airPortRow].distinct
                orderModel.tvillage = airPortModelArr[self.airPortRow].village
                orderModel.taddress = airPortModelArr[self.airPortRow].address
                orderModel.tlat = airPortModelArr[self.airPortRow].lat
                orderModel.tlng = airPortModelArr[self.airPortRow].lng
            }else{
                orderModel.stype = "I"
                
                orderModel.tcity = locationModel.city
                orderModel.tdistinct = locationModel.distinct
                orderModel.tvillage = locationModel.village
                orderModel.taddress = locationModel.address
                orderModel.tlat = locationModel.lat
                orderModel.tlng = locationModel.lng
                
                orderModel.pcity = airPortModelArr[self.airPortRow].city
                orderModel.pdistinct = airPortModelArr[self.airPortRow].distinct
                orderModel.pvillage = airPortModelArr[self.airPortRow].village
                orderModel.paddress = airPortModelArr[self.airPortRow].address
                orderModel.plat = airPortModelArr[self.airPortRow].lat
                orderModel.plng = airPortModelArr[self.airPortRow].lng
                
            }
            orderModel.airport = airPortModelArr[self.airPortRow].code
            print(orderModel.airport)
            
            if reserveType == 0{
                orderModel.cpflag = "S"
                self.navigationController?.navigationBar.barTintColor = shareColor
                self.performSegue(withIdentifier: "goShare", sender: nil)
            }else if reserveType == 1{
                orderModel.cpflag = "P"
                self.navigationController?.navigationBar.barTintColor = singleColor
                self.performSegue(withIdentifier: "goVip", sender: nil)
            }else if reserveType == 2{
                orderModel.cpflag = "V"
                self.navigationController?.navigationBar.barTintColor = vanColor
                self.performSegue(withIdentifier: "goVan", sender: nil)
            }else{
                orderModel.cpflag = "V"
                self.navigationController?.navigationBar.barTintColor = vanColor
                self.performSegue(withIdentifier: "goVan", sender: nil)
            }
        }else{
            self.present(AlertViewModel.shared.showAlert(message: "請選擇正確位置"), animated: true, completion: nil)
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        if #available(iOS 13.0, *) {
            UIApplication.shared.statusBarStyle = .darkContent
        } else {
            UIApplication.shared.statusBarStyle = .default
        }
        UserDefaults.standard.setValue("0", forKey: UserInfo.statusColor.rawValue)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.tabBarController?.tabBar.isHidden = false
        getAirPort()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    override func viewDidDisappear(_ animated: Bool) {
        myLocationManager.stopUpdatingLocation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //輸入地址
    @IBAction func addrEditAlert(_ sender: LeftTextUITextField){
        //取消修改狀態 在dismiss後 才不會按兩次才跳出
        self.view.endEditing(true)
        self.performSegue(withIdentifier: "showSetAddr2", sender: nil)
    }
    @IBAction func addrEditAlertEnd(_ sender: LeftTextUITextField) {
        //取消修改狀態 在dismiss後 才不會按兩次才跳出
//        if !isGoAirPort{
//            self.view.endEditing(true)
//            self.performSegue(withIdentifier: "showSetAddr", sender: nil)
//        }
    }
    
    func getAirPort() {
        CallCarAPI.shared.getAirPort(parameters: ["country":country]) { (success, res, sCode, mss) in
//            print(success)
            if sCode >= 900{
                self.goRoot()
                return
            }
            print(res)
            if success {
                self.airPortModelArr = [AirPortModel]()
                for airport in res.arrayValue{
                    if airport["country"].stringValue == self.country{
                        var aModel = AirPortModel()
                        aModel.code = airport["code"].stringValue
                        aModel.name = airport["name"].stringValue
                        aModel.share = airport["share"].stringValue
                        aModel.vip = airport["vip"].stringValue
                        aModel.lat = Double(airport["lat"].stringValue)!
                        aModel.lng = Double(airport["lng"].stringValue)!
                        aModel.city = airport["city"].stringValue
                        aModel.distinct = airport["distinct"].stringValue
                        aModel.village = airport["village"].stringValue
                        aModel.address = airport["address"].stringValue
                        aModel.country = airport["country"].stringValue
                        self.airPortModelArr.append(aModel)
                    }
                }
                
                self.endLocationTextField.text = self.airPortModelArr[self.pickerView.selectedRow(inComponent: 0)].name
                if !self.isLoadFirstAirport{
                    self.airportCamera(airportName:self.endLocationTextField.text!)
                    self.isLoadFirstAirport = true
                }
//                self.endLocationTextField.text = self.airPortModelArr[0].name
                self.pickerView.reloadAllComponents()
            }else{
                print("retry~~")
                DispatchQueue.main.asyncAfter(deadline: .now() + 5) { // change 2 to desired number of seconds
                    self.getAirPort()
                }
            }
        }
    }
    
    
    //segmented
    @IBAction func segmentedChange(_ sender: UISegmentedControl) {
        isGoAirPort = sender.selectedSegmentIndex == 0
        let startC = startConstraint.constant
        let endC = endConstraint.constant
        UIView.animate(withDuration: 0.3, animations: {
            self.startLocationTextField.transform = self.startLocationTextField.transform.scaledBy(x: 1.05, y: 1.05)
            self.endLocationTextField.transform = self.endLocationTextField.transform.scaledBy(x: 0.95, y: 0.95)
            self.startConstraint.constant = endC
            self.endConstraint.constant = startC
            self.view.layoutIfNeeded()
        }) { (isComplete) in
            UIView.animate(withDuration: 0.3, animations: {
                self.startLocationTextField.transform = CGAffineTransform.identity
                self.endLocationTextField.transform = CGAffineTransform.identity
            })
        }
        
        let sl = startLocationTextField.leftText
        startLocationTextField.leftText = endLocationTextField.leftText
        endLocationTextField.leftText = sl
        self.view.endEditing(true)
    }
    //0 隱藏 1 無提供服務 2 有專接沒共乘
    func tipsViewSet(type:Int){
        if type == 0{
            startLocationTextField.text = addr
            tipsView.isHidden = true
        }else if type == 1 {
            tipsLabel.text = "很抱歉，此地點未提供服務"
            tipsWidthConstraint.constant = 185
            tipsView.isHidden = false
        }else if type == 2{
            tipsLabel.text = "此地點僅提供專接服務"
            tipsWidthConstraint.constant = 160
            tipsView.isHidden = false
        }else if type == 3{
            startLocationTextField.text = addr
            tipsLabel.text = "\(airPortModelArr[self.airPortRow].name)無提供共乘服務"
            tipsWidthConstraint.constant = 200
            tipsView.isHidden = false
        }else if type == 4{
            startLocationTextField.text = addr
            tipsLabel.text = "\(airPortModelArr[self.airPortRow].name)無提供專接服務"
            tipsWidthConstraint.constant = 200
            tipsView.isHidden = false
        }else if type == 5{
            startLocationTextField.text = "Google圖資不完整，請選擇鄰近地點！"
            tipsLabel.text = "Google圖資資料不完整"
            tipsWidthConstraint.constant = 160
            tipsView.isHidden = false
        }
    }
    
    func serviceType(){
        if airPortModelArr.count == 0{
            vipPriceView.isHidden = true
            vipPriceHeight.constant = 0
            
            self.present(AlertViewModel.shared.showAlert(message: "未取得機場資訊"), animated: true, completion: nil)
            return
        }
        if reserveType == 0{
            vipPriceView.isHidden = true
            vipPriceHeight.constant = 0
            vipView.backgroundColor = shareColor
            if level4 == ""{
                vipView.isHidden = true
                tipsViewSet(type:5)
                submitButton.backgroundColor = .lightGray
                self.isOK = false
            }else{
                if airPortModelArr[self.airPortRow].share == "0"{
                    tipsViewSet(type:3)
                    submitButton.backgroundColor = .lightGray
                    self.isOK = false
                }else{
                    if posServiceModel.share.Status == "True" {
                        if posServiceModel.share.Promote != ""{
                            vipView.isHidden = false
                            vipPrice.text = posServiceModel.share.Promote
                        }else{
                            vipView.isHidden = true
                        }
                        tipsViewSet(type:0)
                        submitButton.backgroundColor = shareColor
                        self.isOK = true
                    }else if posServiceModel.vip.Status == "True" {
                        vipView.isHidden = true
                        tipsViewSet(type:2)
                        submitButton.backgroundColor = .lightGray
                        self.isOK = false
                    }else{
                        vipView.isHidden = true
                        tipsViewSet(type:1)
                        submitButton.backgroundColor = .lightGray
                        self.isOK = false
                    }
                    //                print("share status:\(posServiceModel.share.Status)")
                    //                print("share price:\(posServiceModel.share.Price)")
                }
            }
            
        }else if reserveType == 1{
            vipView.backgroundColor = singleColor
            if airPortModelArr[self.airPortRow].vip == "0"{
                vipPriceView.isHidden = true
                vipPriceHeight.constant = 0
                tipsViewSet(type:3)
                submitButton.backgroundColor = .lightGray
                self.isOK = false
            }else{
                if posServiceModel.vip.Status == "True" {
                    vipPriceView.isHidden = false
                    vipPriceHeight.constant = 30
                    if posServiceModel.vip.Promote != ""{
                        vipView.isHidden = false
                        vipPrice.text = posServiceModel.vip.Promote
                    }else{
                        vipView.isHidden = true
                    }
                    tipsViewSet(type:0)
                    submitButton.backgroundColor = singleColor
                    vipPrice1.text = posServiceModel.vip.Price
                    vipPrice2.text = "$\(posServiceModel.vip.PromotePrice)"
                    self.isOK = true
                }else{
                    vipPriceView.isHidden = true
                    vipPriceHeight.constant = 0
                    tipsViewSet(type:1)
                    vipView.isHidden = true
                    submitButton.backgroundColor = .lightGray
                    self.isOK = false
                }
                print("vip status:\(posServiceModel.vip.Status)")
                print("vip price:\(posServiceModel.vip.Price)")
                print("vip Promote:\(posServiceModel.vip.Promote)")
                print("vip PromotePrice:\(posServiceModel.vip.PromotePrice)")
            }
            
        }else if reserveType == 2{
            //專接共乘
            vipView.backgroundColor = vanColor
            if airPortModelArr[self.airPortRow].vip == "0"{
                vipPriceView.isHidden = true
                vipPriceHeight.constant = 0
                tipsViewSet(type:3)
                submitButton.backgroundColor = .lightGray
                self.isOK = false
            }else{
                if posServiceModel.tshare.Status == "True" {
                    tipsViewSet(type:0)
                    submitButton.backgroundColor = vanColor
                    self.isOK = true
                }else{
                    vipPriceView.isHidden = true
                    vipPriceHeight.constant = 0
                    tipsViewSet(type:1)
                    vipView.isHidden = true
                    submitButton.backgroundColor = .lightGray
                    self.isOK = false
                }
                
            }
        }else if reserveType == 3{
            //日本共乘
            vipView.backgroundColor = jpColor
            if airPortModelArr[self.airPortRow].vip == "0"{
                vipPriceView.isHidden = true
                vipPriceHeight.constant = 0
                tipsViewSet(type:3)
                submitButton.backgroundColor = .lightGray
                self.isOK = false
            }else{
                if posServiceModel.tshare.Status == "True" {
                    tipsViewSet(type:0)
                    submitButton.backgroundColor = jpColor
                    self.isOK = true
                }else{
                    vipPriceView.isHidden = true
                    vipPriceHeight.constant = 0
                    tipsViewSet(type:1)
                    vipView.isHidden = true
                    submitButton.backgroundColor = .lightGray
                    self.isOK = false
                }
                
            }
        }
        
    }
    
    func setLocationText(text:String){
        startLocationTextField.text = text
    }
    
    func chageColor(color:UIColor){
        submitButton.backgroundColor = color
        segmentedControl.tintColor = color
        startLocationTextField.color = color
        endLocationTextField.color = color
        self.navigationController?.navigationBar.barTintColor = color
    }
    
    //addr delegate function
    func addr(_ position: CLLocationCoordinate2D,addr: String) {
        print(addr)
        self.presentAddr = addr
        mapView.camera = GMSCameraPosition.camera(withTarget: position, zoom: 16.0)
//        jsonToAddr(position,addr:addr)
    }
    
    func jsonToAddr(_ position:CLLocationCoordinate2D){
        load(isLoad:true)
        guard airPortModelArr.count != 0 else{
            isRetry = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) { // change 2 to desired number of seconds
                self.jsonToAddr(self.mapView.camera.target)
            }
            return
        }
        isRetry = false
        posServiceModel = PosServiceModel.shared
        self.isOK = false
        let parmeters = ["language":laungue,"region":laungue,"latlng":"\(position.latitude),\(position.longitude)","key":GeocodingAPIKey,"result_type":"street_address|administrative_area_level_4"]
        print("latlng:\(position.latitude),\(position.longitude)")
        NetworkTools.requestData(.get, URLString: "https://maps.googleapis.com/maps/api/geocode/json", parameters: parmeters) { (response,error) in
            guard let res = response else {
                guard error == nil else{
                    self.present(AlertViewModel.shared.showAlert(message: "請檢察網路狀態"), animated: true, completion: nil)
                    return
                }
                return
            }
            let json = JSON(res)
            var level_1 = ""  //市
            var level_3 = ""  //區
            var level_4 = ""  //里
            var route = ""    //路
            var number = ""   //號
            print(json)
            if self.reserveType != 3{
                
                var streetAddressResult:JSON? = nil
                for result in json["results"].arrayValue{
                    if result["types"][0].stringValue == "street_address"{
                        if result["geometry"]["location_type"].stringValue == "RANGE_INTERPOLATED"{
                            if  result["formatted_address"].stringValue.lastIndex(of: "鄉道") == nil && result["formatted_address"].stringValue.lastIndex(of: "縣道") == nil && result["formatted_address"].stringValue.lastIndex(of: "公路") == nil && result["formatted_address"].stringValue.lastIndex(of: "高架道路") == nil && result["formatted_address"].stringValue.lastIndex(of: "甲線") == nil{
                                streetAddressResult = result
                                break
                            }else{
                                if streetAddressResult != nil{
                                    if streetAddressResult!["formatted_address"].stringValue.suffix(1) != "號"{
                                        print("do")
                                        streetAddressResult = result
                                        break
                                    }
                                }
                            }
                            
                        }else{
                            streetAddressResult = result
                        }
                    }
                }
                
                if let streetResult = streetAddressResult{
                    if self.presentAddr == "" || self.presentAddr.suffix(1) != "號" || self.presentAddr.lastIndex(of: "鄉道") != nil || self.presentAddr.lastIndex(of: "縣道") != nil || self.presentAddr.lastIndex(of: "公路") != nil || self.presentAddr.lastIndex(of: "高架道路") != nil || self.presentAddr.lastIndex(of: "甲線") != nil{
                        self.addr = streetResult["formatted_address"].stringValue
                    }else{
                        self.addr = self.presentAddr
                        self.presentAddr = ""
                    }
                    var replaceCountry = ""
                    var replacePostalCode = ""
                    for streetAddr in streetResult["address_components"].arrayValue{
                        if streetAddr["types"][0] == "administrative_area_level_1"{
                            level_1 = streetAddr["long_name"].stringValue
                        }else if streetAddr["types"][0] == "administrative_area_level_3"{
                            level_3 = streetAddr["long_name"].stringValue
                        }else if streetAddr["types"][0] == "administrative_area_level_4"{
                            level_4 = streetAddr["long_name"].stringValue
                        }else if streetAddr["types"][0] == "country"{
                            replaceCountry = streetAddr["long_name"].stringValue
                        }else if streetAddr["types"][0] == "postal_code"{
                            replacePostalCode = streetAddr["long_name"].stringValue
                        }
                    }
                    self.addr = self.addr.replacingOccurrences(of: "\(replacePostalCode)\(replaceCountry)", with: "")
                    let address_components = streetResult["address_components"].arrayValue
                    if level_1 == "" {
                        if address_components[4]["long_name"].stringValue == "台灣"{
                            level_1 = address_components[3]["long_name"].stringValue
                        }else{
                            level_1 = address_components[4]["long_name"].stringValue
                        }
                    }
                    
                    if level_4 == ""{
                        for result in json["results"].arrayValue{
                            if result["types"][0].stringValue == "administrative_area_level_4"{
                                for r4 in result["address_components"].arrayValue{
                                    if r4["types"][0].stringValue == "administrative_area_level_4"{
                                        level_4 = r4["long_name"].stringValue
                                    }else if r4["types"][0].stringValue == "administrative_area_level_3"{
                                        level_3 = r4["long_name"].stringValue
                                        
                                    }
                                }
                            }
                        }
                    }
                    if level_3 == "松山鎮"{
                        level_3 = "松山區"
                    }
                    self.addr = self.addr.replacingOccurrences(of: "松山鎮", with: "松山區")
                    if level_1 == "" || level_3 == ""{
                        self.tipsViewSet(type:0)
                        self.setLocationText(text: "定位錯誤，請移至其他地方")
                        self.submitButton.backgroundColor = .lightGray
                        self.isOK = false
                        self.load(isLoad:false)
                        self.vipView.isHidden = true
                        self.vipPriceView.isHidden = true
                        return
                    }
                    route = address_components[1]["long_name"].stringValue
                    number = "\(address_components[0]["long_name"].stringValue)號"
                    
                    
                    print(self.addr)
                    if self.addr.suffix(1) != "號" || self.addr.lastIndex(of: "鄉道") != nil{
                        self.tipsViewSet(type:0)
                        self.setLocationText(text: "定位錯誤，請移至其他地方")
                        self.submitButton.backgroundColor = .lightGray
                        self.isOK = false
                        self.load(isLoad:false)
                        self.vipView.isHidden = true
                        self.vipPriceView.isHidden = true
                        return
                    }
                    self.locationModel.city = level_1
                    self.locationModel.distinct = level_3
                    self.locationModel.village = level_4
                    self.locationModel.address = self.addr
                    self.locationModel.lat = position.latitude
                    self.locationModel.lng = position.longitude
                    
                    self.setLocationText(text: self.addr)
                    self.level4 = level_4
                }else{
                    self.tipsViewSet(type:0)
                    self.setLocationText(text: "定位錯誤，請移至其他地方")
                    self.submitButton.backgroundColor = .lightGray
                    self.isOK = false
                    self.load(isLoad:false)
                    self.vipView.isHidden = true
                    self.vipPriceView.isHidden = true
                    return
                }
            }else{
                var streetAddressResult:JSON? = nil
                var postal_code = ""
                for result in json["results"].arrayValue{
                    if result["types"][0].stringValue == "street_address"{
                        streetAddressResult = result
                        break
                    }
                }
                if let streetResult = streetAddressResult{
                    for streetAddr in streetResult["address_components"].arrayValue{
                        if streetAddr["types"][0] == "administrative_area_level_1"{
                            level_1 = streetAddr["long_name"].stringValue
                        }else if streetAddr["types"][0] == "locality"{
                            level_3 = streetAddr["long_name"].stringValue
                        }else if streetAddr["types"][0] == "postal_code"{
                            postal_code = streetAddr["long_name"].stringValue
                        }
                    }
                    self.addr = streetResult["formatted_address"].stringValue.replacingOccurrences(of: "日本、〒\(postal_code) ", with: "")
                    self.locationModel.city = level_1
                    self.locationModel.distinct = level_3
                    self.locationModel.village = level_4
                    self.locationModel.address = self.addr
                    self.locationModel.lat = position.latitude
                    self.locationModel.lng = position.longitude
                    
                    self.setLocationText(text: self.addr)
                    self.level4 = level_4
                }else{
                    self.tipsViewSet(type:0)
                    self.setLocationText(text: "定位錯誤，請移至其他地方")
                    self.submitButton.backgroundColor = .lightGray
                    self.isOK = false
                    self.load(isLoad:false)
                    self.vipView.isHidden = true
                    self.vipPriceView.isHidden = true
                    return
                }
            }
            
            
            print(["city":level_1,"dist":level_3,"vill":level_4,"airport":self.airPortModelArr[self.airPortRow].code,"country":self.country])
            CallCarAPI.shared.checkService(parameters: ["city":level_1,"dist":level_3,"vill":level_4,"airport":self.airPortModelArr[self.airPortRow].code,"country":self.country], success: { (success, res,error) in
                guard error == nil else{
                    self.present(AlertViewModel.shared.showAlert(message: "請檢察網路狀態"), animated: true, completion: nil)
                    self.load(isLoad:false)
                    return
                }
                if success {
                    print(res)
                    self.posServiceModel.jsonToModel(res)
                    self.serviceType()
                }
                self.load(isLoad:false)
            })
            if self.isUpdUserLoc{
                CallCarAPI.shared.updUserLoc(parameters: ["uid":Int(UserDefaults.standard.getString(key: UserInfo.id.rawValue))!,"city":level_1,"dist":level_3,"vill":level_4,"lat":position.latitude,"lng":position.longitude], success: { (success, res, error, mss) in
                    
                })
                self.isUpdUserLoc = false
            }
            
        }
    }
    
    func load(isLoad:Bool){
        if isLoad{
            self.nvView.startAnimating()
            self.loadCar.isHidden = false
        }else{
            self.nvView.stopAnimating()
            self.loadCar.isHidden = true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showSetAddr2"{
            let nv = segue.destination as! UINavigationController
            let vc = nv.topViewController as! NewSetAddrViewController
            vc.addrDelegate = self
            if startLocationTextField.text?.suffix(1) == "號"{
                vc.text = startLocationTextField.text!
            }else{
                vc.text = ""
            }
        }else if segue.identifier == "goShare"{
            let vc = segue.destination as! ReserveDetailViewController
            vc.orderModel = self.orderModel
        }else if segue.identifier == "goVip"{
            let vc = segue.destination as! VipReserveDetailViewController
            vc.orderModel = self.orderModel
        }else if segue.identifier == "goVan"{
            let vc = segue.destination as! VanReserveDetailViewController
            vc.orderModel = self.orderModel
            if reserveType == 3{
                vc.mainColor = jpColor
                vc.isJp = true
            }else{
                vc.mainColor = vanColor
                vc.isJp = false
            }
        }else if segue.identifier == "noticePresent"{
            let vc = segue.destination as! NoticeAlertViewController
//            print(noticeURL)
            vc.url = noticeURL
        }
    }
    
    
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension ReserveViewController:GMSMapViewDelegate{
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        if !isRetry{
            jsonToAddr(position.target)
        }
    }
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        self.view.endEditing(true)
    }
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        self.view.endEditing(true)
    }
    
}

extension ReserveViewController:UIPickerViewDataSource,UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return airPortModelArr.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return airPortModelArr[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        endLocationTextField.text = airPortModelArr[row].name
//        print(airPortModelArr[row].address)
//        print(airPortModelArr[row].city)
//        print(airPortModelArr[row].distinct)
        print(airPortModelArr[row].city)
        airportCamera(airportName:airPortModelArr[row].name)
        airPortRow = row
        jsonToAddr(mapView.camera.target)
    }
    
    func airportCamera(airportName:String) {
        switch airportName {
        case "關西國際機場":
            mapView.camera = GMSCameraPosition.camera(withLatitude: 34.67196845236622, longitude: 135.49987826496363, zoom: 16.0)
        case "桃園國際機場":
            print("123")
        case "高雄國際機場":
            print("123")
        case "中部國際機場":
            mapView.camera = GMSCameraPosition.camera(withLatitude: 35.1811004999, longitude: 136.9067886999, zoom: 16.0)
        case "東京國際機場":
            mapView.camera = GMSCameraPosition.camera(withLatitude: 35.68167202738757, longitude: 139.76976424455643, zoom: 16.0)
        case "成田國際機場":
            mapView.camera = GMSCameraPosition.camera(withLatitude: 35.68167202738757, longitude: 139.76976424455643, zoom: 16.0)
        case "新千歲機場":
            mapView.camera = GMSCameraPosition.camera(withLatitude: 43.062288750694805, longitude: 141.35436471551657, zoom: 16.0)
        default:
            break
        }
    }
    
}



