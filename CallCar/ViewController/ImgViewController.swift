//
//  ImgViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2019/12/3.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import UIKit
import Kingfisher
class ImgViewController: UIViewController {

    var imgUrl = ""
    @IBOutlet weak var imgView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imgView.kf.setImage(with: URL(string: imgUrl))
        // Do any additional setup after loading the view.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.dismiss(animated: true, completion: nil)
    }

}
