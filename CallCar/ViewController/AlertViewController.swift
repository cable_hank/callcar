//
//  AlertViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/15.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit

class AlertViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func ok(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
