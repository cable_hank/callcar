//
//  VanBeanCarViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2019/9/27.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import UIKit

class VanBeanCarViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var flightDirection: UILabel!
    
    @IBOutlet weak var airport: UILabel!
    @IBOutlet weak var flightTime: UILabel!
    @IBOutlet weak var flightDate: UILabel!
    @IBOutlet weak var carInfoTitle: UILabel!
    
    @IBOutlet weak var title1: UILabel!
    @IBOutlet weak var title2: UILabel!
    @IBOutlet weak var submitButton: BorderCornerButton!
    
    
    var timeList = [CarTimeModel]()
//    var orderModel = OrderModel()
    var orderModel = RM_OrderModel()
//    var flightCardInfo = [String]()
    var minDateString = ""
    var maxDateString = ""
    
    var timerForShowScrollIndicator: Timer?
    var selectIndexPath:IndexPath?
    var dno = ""
    var parameter:[String:Any] = ["sdno":"","srno":"","tdno":"","stime":""]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.separatorStyle = .none
        
        self.navigationItem.title = "選擇上車時間"
        let backButton = UIBarButtonItem()
        backButton.title = " "
        backButton.tintColor = .white
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        parameter["sdno"] = orderModel.dno
        parameter["srno"] = orderModel.rno
        airport.text = orderModel.airport
        if orderModel.stype == "O"{ //出國
            flightDirection.text = "往機場"
        }else{
            flightDirection.text  = "往市區"
        }
        flightTime.text = String(orderModel.ftime.suffix(5))
        flightDate.text = String(orderModel.ftime.prefix(10))
        UIApplication.shared.statusBarStyle = .lightContent
        
        UserDefaults.standard.setValue("1", forKey: UserInfo.statusColor.rawValue)
        if orderModel.country == "JP"{
            self.navigationController?.navigationBar.barTintColor = UIColor(hex: 0xFC989F)
            title1.textColor = UIColor(hex: 0xFC989F)
            title2.textColor = UIColor(hex: 0xFC989F)
            submitButton.backgroundColor =  UIColor(hex: 0xFC989F)
        }else{
            self.navigationController?.navigationBar.barTintColor = UIColor(hex: 0xF5A623)
            title1.textColor = UIColor(hex: 0xF5A623)
            title2.textColor = UIColor(hex: 0xF5A623)
            submitButton.backgroundColor =  UIColor(hex: 0xF5A623)
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        selectIndexPath = nil
        print(timeList.count)
        tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        startTimerForShowScrollIndicator()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        stopTimerForShowScrollIndicator()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func selectRadio(_ sender:UIButton) {
        print(sender.tag)
        
    }
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func next(_ sender: BorderCornerButton) {
        if parameter["stime"] as! String == "" || selectIndexPath == nil{
            self.present(AlertViewModel.shared.showAlert(message: "請選擇車班時間"), animated: true, completion: nil)
            return
        }
        let stime = parameter["stime"] as! String
        let stimeDate = stime.formateDate(format: "yyyyMMdd HHmm")!
        parameter["stime"] = stimeDate.formateString(format: "yyyy-MM-dd HH:mm:ss")
        print(parameter)
        let vc = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "load")
        self.present(vc, animated: false, completion: nil)
        CallCarAPI.shared.cVANOrder(parameters: parameter) { (success, res, error, errorString, sCode) in
            self.dismiss(animated: false, completion: nil)
            if success{
                let alertController = UIAlertController(title: "提示", message: "併車成功", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "確定", style: .default) { (alert) in
                    self.dismiss(animated: true, completion: nil)
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
                
            }else{
                self.present(AlertViewModel.shared.showAlert(message: "併車失敗"), animated: true, completion: nil)
            }
        }
//        if orderModel.stype == "I"{
//            let fdt = "\(orderModel.fdate)\(orderModel.ftime)"
//            let sdt = "\(orderModel.sdate)\(orderModel.ptime)"
//            print("sdt:\(sdt) fdt:\(fdt)")
//            let format = DateFormatter()
//            format.dateFormat = "yyyyMMddHHmm"
//            let dfdt = format.date(from: fdt)!
//            let dsdt = format.date(from: sdt)!
//            if dfdt + 90 * 60 > dsdt{
//                let alertController = UIAlertController(title: "風險提示", message: "依據媒合時間，共乘服務最多等候5分鐘為限，請保留足夠緩衝時間，我們將不承擔航班延誤所產生之搭乘權益受損。", preferredStyle: .alert)
//                let okAction = UIAlertAction(title: "確定預約", style: .default) { (alert) in
//                    self.performSegue(withIdentifier: "goCheck", sender: nil)
//                }
//                let cancelAction = UIAlertAction(title: "取消預約", style: .cancel, handler: nil)
//                alertController.addAction(okAction)
//                alertController.addAction(cancelAction)
//                self.present(alertController, animated: true, completion: nil)
//                return
//            }
//        }
//        self.performSegue(withIdentifier: "goCheck", sender: nil)
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "goCheck"{
//            let vc = segue.destination as! CheckOutViewController
//            vc.orderModel = orderModel
//            vc.dno = dno
//        }else if segue.identifier == "fa"{
//            let vc = segue.destination as! VanCarInfoViewController
//            vc.orderModel = orderModel
//            vc.flightCardInfo = flightCardInfo
//            vc.minDateString = minDateString
//            vc.maxDateString = maxDateString
//            vc.timeList = timeList
//        }else if segue.identifier == "alert"{
//            let vc = segue.destination as! vanInitiateAlertViewController
//            vc.delegate = self
//            vc.isTopTips = false
//        }
    }
    
    /// Show always scroll indicator in table view
    @objc func showScrollIndicatorsInContacts() {
        UIView.animate(withDuration: 0.001) {
            self.tableView.flashScrollIndicators()
        }
    }
    
    /// Start timer for always show scroll indicator in table view
    func startTimerForShowScrollIndicator() {
        self.timerForShowScrollIndicator = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.showScrollIndicatorsInContacts), userInfo: nil, repeats: true)
    }
    
    /// Stop timer for always show scroll indicator in table view 10 24 tr875
    func stopTimerForShowScrollIndicator() {
        self.timerForShowScrollIndicator?.invalidate()
        self.timerForShowScrollIndicator = nil
    }
    
    
}

extension VanBeanCarViewController: UITableViewDataSource,UITableViewDelegate,vanInitiateAlertViewControllerDelegate{
    func vanInitiateAlertOK() {
        self.performSegue(withIdentifier: "fa", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return timeList.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "pickCarTimeCell")! as! PickCarTimeTableViewCell
        cell.timeLabel.isHidden = orderModel.stype == "I"
        cell.timeLabel.text = self.toTimeFormaater(timeString: timeList[indexPath.row].carTime)
        cell.dateLabel.text = self.toDateFormaater(dateString: timeList[indexPath.row].carDate)
        cell.bagCount.text = timeList[indexPath.row].rbcnt
        cell.personCount.text = timeList[indexPath.row].rpcnt
        cell.stopPlace.text = "\(timeList[indexPath.row].city)\(timeList[indexPath.row].distinct)"
        
        cell.shareCode.text = "\(timeList[indexPath.row].rno.prefix(4))****\(timeList[indexPath.row].rno.suffix(3))"
        if timeList[indexPath.row].carType == "四人座"{
            cell.carSeat.text = "四人座"
            if orderModel.country == "JP"{
                cell.carImg.image = UIImage(named: "car_four_jp")
            }else{
                cell.carImg.image = UIImage(named: "Frame_four")
            }
        }else{
            cell.carSeat.text = "七人座"
            if orderModel.country == "JP"{
                cell.carImg.image = UIImage(named: "car_seven_jp")
            }else{
                cell.carImg.image = UIImage(named: "Frame_seven")
            }
        }
        if let _ =  self.selectIndexPath, indexPath.row == selectIndexPath?.row {
            cell.radioButton.isSelected = true
            cell.cornerView.borderColor = UIColor(hex: 0xF5A623)
            parameter["stime"] = "\(timeList[indexPath.row].carDate) \(timeList[indexPath.row].carTime)"
            parameter["tdno"] = timeList[indexPath.row].dno
            dno = timeList[indexPath.row].dno
        } else {
            cell.radioButton.isSelected = false
            cell.cornerView.borderColor = UIColor(hex: 0xCCCCCC)
            
        }
        print(cell.cornerView.frame.height)
        cell.radioButton.tag = indexPath.row
        
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectIndexPath = indexPath
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
