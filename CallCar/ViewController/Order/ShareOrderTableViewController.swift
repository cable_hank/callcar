//
//  ShareOrderTableViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/31.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit
import RealmSwift

class ShareOrderTableViewController: UITableViewController {

    @IBOutlet weak var rnoLabel: UILabel!
    @IBOutlet weak var tdateLabel: UILabel!
    @IBOutlet weak var ttimeLabel: UILabel!
    @IBOutlet weak var paddressLabel: UILabel!
    @IBOutlet weak var taddressLabel: UILabel!
    @IBOutlet weak var dnameLabel: UILabel!
    @IBOutlet weak var dmobileLabel: UILabel!
    @IBOutlet weak var carNoLabel: UILabel!
    @IBOutlet weak var pCountLabel: UILabel!
    @IBOutlet weak var bCountLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var couponNoLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var oldPriceLabel: UILabel!
    @IBOutlet weak var bonusLabel: UILabel!
    @IBOutlet weak var noDriverInfoView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var cancelLabel: UILabel!
    @IBOutlet weak var vanCarTableView: UITableView!
    @IBOutlet weak var priceTitle: UILabel!
    
    var rno = ""
    var isNew = true
    var isCancel = true
    var vanArr = List<rm_dispatch>()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.separatorStyle = .none
        self.vanCarTableView.separatorStyle = .none
        if isNew{
            self.tableView.backgroundView = UIImageView(image: UIImage(named: "bg_2"))
        }
        self.navigationItem.title = "訂單詳情"
        //backbutton設定
        let backButton = UIBarButtonItem()
        backButton.title = " "
        backButton.tintColor = .black
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
    }

    override func viewWillAppear(_ animated: Bool) {
        print("rno:\(rno)")
        print("isNew:\(isNew)")
        if isNew{
            if let order = RealmDB.shared.getOrderByNo(rno: rno){
                rnoLabel.text = order.rno
                tdateLabel.text = yyyyMMddToMM_Slash_dd(dateString:order.tdate)
                if order.stime != ""{
                    ttimeLabel.text = order.stime
                }else{
                    ttimeLabel.text = toTimeFormaater(timeString: order.ttime)
                }
                paddressLabel.text = order.paddress
                taddressLabel.text = order.taddress
                dnameLabel.text = order.dname
                dmobileLabel.text = order.dphone
                carNoLabel.text = order.carno
                pCountLabel.text = String(order.pcnt)
                bCountLabel.text = String(order.bcnt)
                nameLabel.text = order.pname
                mobileLabel.text = order.pphone
                
                vanArr = order.dispatch
                vanCarTableView.reloadData()
                
                if order.coupon == ""{
                    couponNoLabel.text = "無"
                }else{
                    couponNoLabel.text = order.coupon
                }
                print(order.pstage)
                print(order.pstageo)
                if order.pstage == "已預約"{
                    noDriverInfoView.isHidden = false
                }else if order.pstage == "已取消"{
                    topView.backgroundColor = UIColor(hex: 0xF7F7F7)
                    cancelLabel.isHidden = false
                }
                priceLabel.text = "$\(String(order.price))"
                bonusLabel.text = String(order.bonus)
                if order.price1 == 0{
                    oldPriceLabel.isHidden = true
                }else{
                    oldPriceLabel.attributedText = NSAttributedString(string: "$\(String(order.price1))", attributes: [NSAttributedString.Key.strikethroughStyle:NSUnderlineStyle.single.rawValue])
                }
                if order.noshow == "N"{
                    priceTitle.text = "出車處理費"
                }else{
                    priceTitle.text = "車資"
                }
                
            }else{
                self.present(AlertViewModel.shared.showAlert(message: "訂單資料錯誤"), animated: true, completion: nil)
            }
        }else{
            if let order = RealmDB.shared.getHsOrderByNo(rno: rno){
                rnoLabel.text = order.rno
                tdateLabel.text = yyyyMMddToMM_Slash_dd(dateString:order.tdate)
                if order.stime != ""{
                    ttimeLabel.text = order.stime
                }else{
                    ttimeLabel.text = toTimeFormaater(timeString: order.ttime)
                }
                paddressLabel.text = order.paddress
                taddressLabel.text = order.taddress
                dnameLabel.text = order.dname
                dmobileLabel.text = order.dphone
                carNoLabel.text = order.carno
                pCountLabel.text = String(order.pcnt)
                bCountLabel.text = String(order.bcnt)
                nameLabel.text = order.pname
                mobileLabel.text = order.pphone
                if order.coupon == ""{
                    couponNoLabel.text = "無"
                }else{
                    couponNoLabel.text = order.coupon
                }
                print(order.pstage)
                if order.pstage == "已預約"{
                    noDriverInfoView.isHidden = false
                }else if order.pstage == "已取消"{
                    topView.backgroundColor = UIColor(hex: 0xF7F7F7)
                    cancelLabel.isHidden = false
                }
                priceLabel.text = "$\(String(order.price))"
                bonusLabel.text = String(order.bonus)
                if order.price1 == 0{
                    oldPriceLabel.isHidden = true
                }else{
                    oldPriceLabel.attributedText = NSAttributedString(string: "$\(String(order.price1))", attributes: [NSAttributedString.Key.strikethroughStyle:NSUnderlineStyle.single.rawValue])
            }
//                if order.noshow == "N"{
//                    priceTitle.text = "出車處理費"
//                }else{
//                    priceTitle.text = "車資"
//                }
                print(order.pstage)
            }else{
                self.present(AlertViewModel.shared.showAlert(message: "訂單資料錯誤"), animated: true, completion: nil)
            }
        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.navigationController?.popViewController(animated: false);
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.tag == 1{
            return 52
        }else{
            if indexPath.row == 5{
                if vanArr.count == 0{
                    return 0
                }
                return CGFloat((vanArr.count * 52) + 40)
            }
            if (!isNew && indexPath.row == 1) || (isCancel && indexPath.row == 1) || (dnameLabel.text == "" && dmobileLabel.text == "" && indexPath.row == 1 && noDriverInfoView.isHidden){
                return 0
            }
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "VanCarTableViewCell")! as! VanCarTableViewCell
            if vanArr.count == 1{
                cell.topLineView.isHidden = true
                cell.bottomLineView.isHidden = true
            }else if indexPath.row + 1 == vanArr.count{
                cell.topLineView.isHidden = false
                cell.bottomLineView.isHidden = true
            }else if indexPath.row == 0{
                cell.topLineView.isHidden = true
                cell.bottomLineView.isHidden = false
            }else{
                cell.topLineView.isHidden = false
                cell.bottomLineView.isHidden = false
            }
            if rno == vanArr[indexPath.row].rno{
                cell.vanCornerView.backgroundColor = UIColor(hex: 0x20B6FC)
            }else{
                cell.vanCornerView.backgroundColor = UIColor(hex: 0x999999)
            }
            cell.addr.text = "\(vanArr[indexPath.row].city)\(vanArr[indexPath.row].distinct)"
            cell.pCount.text = "\(vanArr[indexPath.row].pcnt)"
            cell.bCount.text = "\(vanArr[indexPath.row].bcnt)"
            return cell
        }else{
            return super.tableView(tableView, cellForRowAt: indexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 1{
            return vanArr.count
        }else{
            return super.tableView(tableView, numberOfRowsInSection: section)
        }
    }
    

}
