//
//  VipOrderTableViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/31.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit

class VipOrderTableViewController: UITableViewController {
    
    @IBOutlet weak var rnoLabel: UILabel!
    @IBOutlet weak var tdateLabel: UILabel!
    @IBOutlet weak var ttimeLabel: UILabel!
    @IBOutlet weak var paddressLabel: UILabel!
    @IBOutlet weak var taddressLabel: UILabel!
    @IBOutlet weak var dnameLabel: UILabel!
    @IBOutlet weak var dmobileLabel: UILabel!
    @IBOutlet weak var carNoLabel: UILabel!
    @IBOutlet weak var pCountLabel: UILabel!
    @IBOutlet weak var bCountLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var couponNoLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var carTypeLabel: UILabel!
    @IBOutlet weak var bonusLabel: UILabel!
    
    @IBOutlet weak var noDriverInfoView: UIView!
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var cancelLabel: UILabel!
    @IBOutlet weak var priceTitle: UILabel!
    var rno = ""
    var isNew = true
    var isCancel = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.separatorStyle = .none
        
        if isNew{
            self.tableView.backgroundView = UIImageView(image: UIImage(named: "bg_2"))
        }
        self.navigationItem.title = "訂單詳情"
        //backbutton設定
        let backButton = UIBarButtonItem()
        backButton.title = " "
        backButton.tintColor = .black
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }

    override func viewWillAppear(_ animated: Bool) {
        print("rno:\(rno)")
        print("isNew:\(isNew)")
        if isNew{
            if let order = RealmDB.shared.getOrderByNo(rno: rno){
                rnoLabel.text = order.rno
                tdateLabel.text = toDateFormaaterSlash(dateString: order.tdate)
                if order.stime != ""{
                    ttimeLabel.text = order.stime
                }else{
                    ttimeLabel.text = toTimeFormaater(timeString: order.ttime)
                }
                paddressLabel.text = order.paddress
                taddressLabel.text = order.taddress
                dnameLabel.text = order.dname
                dmobileLabel.text = order.dphone
                carNoLabel.text = order.carno
                pCountLabel.text = String(order.pcnt)
                bCountLabel.text = String(order.bcnt)
                nameLabel.text = order.pname
                mobileLabel.text = order.pphone
                couponNoLabel.text = order.coupon
                
                if order.coupon == ""{
                    couponNoLabel.text = "無"
                }else{
                    couponNoLabel.text = order.coupon
                }
                
                if order.pstage == "已取消"{
                    topView.backgroundColor = UIColor(hex: 0xF7F7F7)
                    cancelLabel.isHidden = false
                }
                
                if order.dname == "" && order.dphone == ""{
                    noDriverInfoView.isHidden = false
                }else{
                    noDriverInfoView.isHidden = true
                }
                bonusLabel.text = String(order.bonus)
                priceLabel.text = "$\(String(order.price))"
                carTypeLabel.text = order.cartype
                
                if order.noshow == "N"{
                    priceTitle.text = "出車處理費"
                }else{
                    priceTitle.text = "車資"
                }
                
            }else{
                self.present(AlertViewModel.shared.showAlert(message: "訂單資料錯誤"), animated: true, completion: nil)
            }
        }else{
            if let order = RealmDB.shared.getHsOrderByNo(rno: rno){
                rnoLabel.text = order.rno
                tdateLabel.text = toDateFormaaterSlash(dateString: order.tdate)
                if order.stime != ""{
                    ttimeLabel.text = order.stime
                }else{
                    ttimeLabel.text = toTimeFormaater(timeString: order.ttime)
                }
                paddressLabel.text = order.paddress
                taddressLabel.text = order.taddress
                dnameLabel.text = order.dname
                dmobileLabel.text = order.dphone
                carNoLabel.text = order.carno
                pCountLabel.text = String(order.pcnt)
                bCountLabel.text = String(order.bcnt)
                nameLabel.text = order.pname
                mobileLabel.text = order.pphone
                couponNoLabel.text = order.coupon
                
                if order.coupon == ""{
                    couponNoLabel.text = "無"
                }else{
                    couponNoLabel.text = order.coupon
                }
                if order.pstage == "已預約"{
                    noDriverInfoView.isHidden = false
                }else if order.pstage == "已取消"{
                    topView.backgroundColor = UIColor(hex: 0xF7F7F7)
                    cancelLabel.isHidden = false
                }
                bonusLabel.text = String(order.bonus)
                priceLabel.text = "$\(String(order.price))"
                
                carTypeLabel.text = order.cartype
                if order.noshow == "N"{
                    priceTitle.text = "出車處理費"
                }else{
                    priceTitle.text = "車資"
                }
            }else{
                self.present(AlertViewModel.shared.showAlert(message: "訂單資料錯誤"), animated: true, completion: nil)
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.navigationController?.popViewController(animated: false);
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (!isNew && indexPath.row == 1) || (isCancel && indexPath.row == 1) || (dnameLabel.text == "" && dmobileLabel.text == "" && indexPath.row == 1 && noDriverInfoView.isHidden){
            return 0
        }
        return super.tableView(tableView, heightForRowAt: indexPath)
    }
}
