//
//  VanOrderTableViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2019/6/10.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import UIKit
import RealmSwift

class VanOrderTableViewController: UITableViewController {

    @IBOutlet weak var rnoLabel: UILabel!
    @IBOutlet weak var tdateLabel: UILabel!
    @IBOutlet weak var ttimeLabel: UILabel!
    @IBOutlet weak var paddressLabel: UILabel!
    @IBOutlet weak var taddressLabel: UILabel!
    @IBOutlet weak var dnameLabel: UILabel!
    @IBOutlet weak var dmobileLabel: UILabel!
    @IBOutlet weak var carNoLabel: UILabel!
    @IBOutlet weak var pCountLabel: UILabel!
    @IBOutlet weak var bCountLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var couponNoLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var carTypeLabel: UILabel!
    @IBOutlet weak var bonusLabel: UILabel!
    @IBOutlet weak var noDriverInfoView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var cancelLabel: UILabel!
    @IBOutlet weak var vanTitle: UILabel!
    @IBOutlet weak var vanTypeLabel: UILabel!
    
    @IBOutlet weak var vanCarTableView: UITableView!
    @IBOutlet weak var priceTitle: UILabel!
    
    @IBOutlet weak var topColorView: UIView!
    @IBOutlet weak var typeLabel1: UILabel!
    @IBOutlet weak var tips1: UILabel!
    @IBOutlet weak var tips2: UILabel!
    @IBOutlet weak var dphoneTitle: UILabel!
    
    var rno = ""
    var isNew = true
    var isCancel = true
    var rm_order = RM_OrderModel()
    var isloadRM = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.separatorStyle = .none
        self.vanCarTableView.separatorStyle = .none
        if isNew{
            self.tableView.backgroundView = UIImageView(image: UIImage(named: "bg_2"))
        }
        self.navigationItem.title = "訂單詳情"
        //backbutton設定
        let backButton = UIBarButtonItem()
        backButton.title = " "
        backButton.tintColor = .black
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("rno:\(rno)")
        print("isNew:\(isNew)")
        if isNew{
            if let order = RealmDB.shared.getOrderByNo(rno: rno){
                rm_order = order
                if order.country == "JP"{
                    topColorView.backgroundColor = UIColor(hex: 0xFC989F)
                    typeLabel1.textColor = UIColor(hex: 0xFC989F)
                    tips1.textColor = UIColor(hex: 0xFC989F)
                    tips2.textColor = UIColor(hex: 0xFC989F)
                    vanTitle.textColor = UIColor(hex: 0xFC989F)
                    vanTitle.text = "日本共乘"
                    vanTypeLabel.textColor = UIColor(hex: 0x33C5D4)
                }
                
                rnoLabel.text = order.rno
                tdateLabel.text = toDateFormaaterSlash(dateString: order.tdate)
                if order.stime != ""{
                    ttimeLabel.text = order.stime
                }else{
                    ttimeLabel.text = toTimeFormaater(timeString: order.ttime)
                }
                paddressLabel.text = order.paddress
                taddressLabel.text = order.taddress
                dnameLabel.text = order.dname
                dmobileLabel.text = order.dphone
                carNoLabel.text = order.carno
                pCountLabel.text = String(order.pcnt)
                bCountLabel.text = String(order.bcnt)
                nameLabel.text = order.pname
                mobileLabel.text = order.pphone
                couponNoLabel.text = order.coupon
                vanCarTableView.reloadData()
                
                if order.vanflag == "S"{
                    vanTypeLabel.text = "併車"
                    vanTitle.text = "併車"
                }else{
                    vanTypeLabel.text = "發起"
                    vanTitle.text = "發起"
                }
                
                if order.coupon == ""{
                    couponNoLabel.text = "無"
                }else{
                    couponNoLabel.text = order.coupon
                }
                
                if order.pstage == "已取消"{
                    topView.backgroundColor = UIColor(hex: 0xF7F7F7)
                    cancelLabel.isHidden = false
                }
                
                if order.dname == "" && order.dphone == ""{
                    noDriverInfoView.isHidden = false
                }else{
                    noDriverInfoView.isHidden = true
                    if order.country == "JP"{
                        dphoneTitle.text = "客服電話"
                    }else{
                        dphoneTitle.text = "司機電話"
                    }
                }
                bonusLabel.text = String(order.bonus)
                priceLabel.text = "$\(String(order.price))"
                carTypeLabel.text = order.cartype
                
                if order.noshow == "N"{
                    priceTitle.text = "出車處理費"
                }else{
                    priceTitle.text = "車資"
                }
                
            }else{
                self.present(AlertViewModel.shared.showAlert(message: "訂單資料錯誤"), animated: true, completion: nil)
            }
        }else{
            if let order = RealmDB.shared.getHsOrderByNo(rno: rno){
                rnoLabel.text = order.rno
                tdateLabel.text = toDateFormaaterSlash(dateString: order.tdate)
                if order.stime != ""{
                    ttimeLabel.text = order.stime
                }else{
                    ttimeLabel.text = toTimeFormaater(timeString: order.ttime)
                }
                paddressLabel.text = order.paddress
                taddressLabel.text = order.taddress
                dnameLabel.text = order.dname
                dmobileLabel.text = order.dphone
                carNoLabel.text = order.carno
                pCountLabel.text = String(order.pcnt)
                bCountLabel.text = String(order.bcnt)
                nameLabel.text = order.pname
                mobileLabel.text = order.pphone
                couponNoLabel.text = order.coupon
                
                if order.vanflag == "S"{
                    vanTypeLabel.text = "併車"
                    vanTitle.text = "併車"
                }else{
                    vanTypeLabel.text = "發起"
                    vanTitle.text = "發起"
                }
                if order.coupon == ""{
                    couponNoLabel.text = "無"
                }else{
                    couponNoLabel.text = order.coupon
                }
                if order.pstage == "已預約"{
                    noDriverInfoView.isHidden = false
                }else if order.pstage == "已取消"{
                    topView.backgroundColor = UIColor(hex: 0xF7F7F7)
                    cancelLabel.isHidden = false
                }
                bonusLabel.text = String(order.bonus)
                priceLabel.text = "$\(String(order.price))"
                
                carTypeLabel.text = order.cartype
                if order.noshow == "N"{
                    priceTitle.text = "出車處理費"
                }else{
                    priceTitle.text = "車資"
                }
            }else{
                self.present(AlertViewModel.shared.showAlert(message: "訂單資料錯誤"), animated: true, completion: nil)
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.navigationController?.popViewController(animated: false);
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.tag == 1{
            return 52
        }else{
            if indexPath.row == 5{
                if rm_order.dispatch.count == 0{
                    return 0
                }
                return CGFloat((rm_order.dispatch.count * 52) + 40)
            }
            if (!isNew && indexPath.row == 1) || (isCancel && indexPath.row == 1) || (dnameLabel.text == "" && dmobileLabel.text == "" && indexPath.row == 1 && noDriverInfoView.isHidden){
                return 0
            }
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "VanCarTableViewCell")! as! VanCarTableViewCell
            if rm_order.dispatch.count == 1{
                cell.topLineView.isHidden = true
                cell.bottomLineView.isHidden = true
            }else if indexPath.row + 1 == rm_order.dispatch.count{
                cell.topLineView.isHidden = false
                cell.bottomLineView.isHidden = true
            }else if indexPath.row == 0{
                cell.topLineView.isHidden = true
                cell.bottomLineView.isHidden = false
            }else{
                cell.topLineView.isHidden = false
                cell.bottomLineView.isHidden = false
            }
            if rno == rm_order.dispatch[indexPath.row].rno{
                if rm_order.country == "JP"{
                    cell.vanCornerView.backgroundColor = UIColor(hex: 0xFC989F)
                }else{
                    cell.vanCornerView.backgroundColor = UIColor(hex: 0xF5A623)
                }
            }else{
                cell.vanCornerView.backgroundColor = UIColor(hex: 0x999999)
            }
            cell.addr.text = "\(rm_order.dispatch[indexPath.row].city)\(rm_order.dispatch[indexPath.row].distinct)"
            cell.pCount.text = "\(rm_order.dispatch[indexPath.row].pcnt)"
            cell.bCount.text = "\(rm_order.dispatch[indexPath.row].bcnt)"
            return cell
        }else{
            return super.tableView(tableView, cellForRowAt: indexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 1{
            return rm_order.dispatch.count
        }else{
            return super.tableView(tableView, numberOfRowsInSection: section)
        }
    }
    
    
}
