//
//  CancelOrderViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2019/1/24.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import UIKit

protocol CancelOrderViewControllerDelegate {
    func cancelReload()
}

class CancelOrderViewController: UIViewController {

    @IBOutlet weak var mss: UITextView!
    @IBOutlet weak var buttonLeft: UIButton!
    @IBOutlet weak var buttonRight: UIButton!
    var uid = ""
    var order = RM_OrderModel()
    var delegate:CancelOrderViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        uid = UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!
        if order.cancelflag == "N"{
            buttonRight.setTitle("撰寫Email", for: .normal)
        }else if order.cancelflag == "X"{
            buttonRight.setTitle("確定", for: .normal)
        }else if order.cancelflag == "R" || order.cancelflag == "C" {
            buttonRight.setTitle("取消訂單", for: .normal)
        }else{
            buttonRight.setTitle("確定", for: .normal)
        }
        
        mss.text = order.cancelalert
    }
    
    @IBAction func serviceAction(_ sender: UIButton) {
        var urlString = "https://www.callcar.com.tw/service-vanpool.html#vanpool-faq"
        if order.poolflag == "S"{
            urlString = "https://www.callcar.com.tw/service-carpool.html#carpool-faq"
        }else if order.poolflag == "P"{
            urlString = "https://www.callcar.com.tw/service-vip.html#vip-faq"
        }
        let url = URL(string: urlString)!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func buttonLeft(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buttonRight(_ sender: UIButton) {
        sender.isEnabled = false
        print("cancelflag:\(order.cancelflag)")
        if order.cancelflag == "N"{
            sender.titleLabel?.text = "撰寫Email"
            let url = URL(string: "mailto:support@ishareco.com")!
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }else if order.cancelflag == "X"{
            sender.titleLabel?.text = "確定"
            self.dismiss(animated: true, completion: nil)
        }else if order.cancelflag == "R" || order.cancelflag == "C" {
            let rno = order.rno
            print(rno)
            let vc = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "load")
            self.present(vc, animated: false, completion: nil)
            CallCarAPI.shared.cancelOrder(parameters: ["no":rno]) { (success, res, error,mss,sCode) in
                self.dismiss(animated: false, completion: nil)
                if sCode >= 900{
                    self.goRoot()
                    return
                }
                
                if error != nil {
                    print("error")
                    self.dismiss(animated: true, completion: nil)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        self.present(AlertViewModel.shared.showAlert(message: "請確認網路狀態"), animated: true, completion: nil)
                        return
                    }
                    
                }
                if !success {
                    self.dismiss(animated: true, completion: nil)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        self.present(AlertViewModel.shared.showAlert(message: mss), animated: true, completion: nil)
                        return
                    }
                }
                self.delegate?.cancelReload()
                self.dismiss(animated: true, completion: nil)
            }
        }else{
            self.dismiss(animated: true, completion: nil)
        }
        
    }
}
