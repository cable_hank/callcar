//
//  ReserveListViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/11.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
class ReserveListViewController: UIViewController,CancelOrderViewControllerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var s1Label: UILabel!
    @IBOutlet weak var s2Label: UILabel!
    @IBOutlet weak var sImage: UIImageView!
    @IBOutlet weak var s1ImageleftConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var loadingView: ShadowCornerView!
    
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var newOrderButton: UIButton!
    @IBOutlet weak var historyOrderButton: UIButton!
    
    
    var reserveArr = [RM_OrderModel]()
    var reserveHsArr = [RM_OrderHsModel]()
    var uid = ""
    var isNew = true
    var rno = ""
    var isCancel = false
    let formatter = DateFormatter()
    var isLoad = false
    var selectTag = 0
    var beanCarSelectTag = 0
    var refreshControl: UIRefreshControl!
    var timeList = [CarTimeModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.separatorStyle = .none
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black]
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        formatter.dateFormat = "yyyyMMddHHmm"
        
        uid = UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(getOrders), for: .valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        segment.selectedSegmentIndex = 0
//        segementView(isNew:true)
//        isNew = true
        if #available(iOS 13.0, *) {
            UIApplication.shared.statusBarStyle = .darkContent
        } else {
            UIApplication.shared.statusBarStyle = .default
        }
        UserDefaults.standard.setValue("0", forKey: UserInfo.statusColor.rawValue)
        reserveArr = RealmDB.shared.getOrder()
        reserveHsArr = RealmDB.shared.getHsOrder()
        setOrderValue()
        
        
//        self.tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }

    func setOrderValue() {
        if isNew{
            if self.reserveArr.count == 0{
                self.noDataView.isHidden = false
                self.tableView.isHidden = true
            }else{
                self.noDataView.isHidden = true
                self.tableView.isHidden = false
            }
        }else{
            if self.reserveHsArr.count == 0{
                self.noDataView.isHidden = false
                self.tableView.isHidden = true
            }else{
                self.noDataView.isHidden = true
                self.tableView.isHidden = false
            }
        }
        self.tableView.reloadData()
        self.loadingView.isHidden = true
        self.isLoad = false
        print("reload end")
        self.refreshControl.endRefreshing()
    }
    
    @objc func getOrders(){
        loadingView.isHidden = false
        isLoad = true
        CallCarAPI.shared.getForder(parameters: ["uid":uid]) { (success, res, error,sCode) in
            if sCode >= 900{
                self.goRoot()
                return
            }
            if success{
                UserNotificationTools().deleteAllNotifications()
                print(res)
                RealmDB.shared.saveOrder(json: res)
            }
            self.reserveArr = RealmDB.shared.getOrder()
            CallCarAPI.shared.getPorder(parameters: ["uid":self.uid]) { (success, res1, sCode,mss) in
                if sCode >= 900{
                    self.goRoot()
                    return
                }
                if success{
                    
                    RealmDB.shared.saveHsOrder(json: res1)
                }
                self.reserveHsArr = RealmDB.shared.getHsOrder()
                self.setOrderValue()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func orderSwitch(_ sender: UIButton) {
        print("orderSwitch")
        if (sender == newOrderButton && !isNew) || (sender == historyOrderButton && isNew){
            print("s")
            isNew = !isNew
            segementView(isNew:isNew)
            bgImageView.isHidden = !isNew
            tableView.reloadData()
            tableView.beginUpdates()
            tableView.setContentOffset(.zero, animated: true)
            tableView.endUpdates()
            
        }
        
    }
    
    @IBAction func segmentChange(_ sender: UISegmentedControl) {
        isNew = sender.selectedSegmentIndex == 0
        
        segementView(isNew:isNew)
        
        bgImageView.isHidden = !isNew
        tableView.reloadData()
        tableView.beginUpdates()
        tableView.setContentOffset(.zero, animated: true)
        tableView.endUpdates()
    }
    
    func segementView(isNew:Bool){
        if isNew{
            s1Label.textColor = UIColor(hex: 0x28ABE2)
            s2Label.textColor = UIColor(hex: 0x999999)
            UIView.animate(withDuration: 0.3) {
                self.s1ImageleftConstraint.constant = 20
                self.sImage.image = UIImage(named: "LineBlue")
                self.view.layoutIfNeeded()
            }
            if self.reserveArr.count == 0 {
                noDataView.isHidden = false
                self.tableView.isHidden = true
            }else{
                noDataView.isHidden = true
                self.tableView.isHidden = false
            }
        }else{
            s1Label.textColor = UIColor(hex: 0x999999)
            s2Label.textColor = UIColor(hex: 0xF5A623)
            UIView.animate(withDuration: 0.3) {
                self.s1ImageleftConstraint.constant = self.sImage.bounds.width + 60
                self.sImage.image = UIImage(named: "LineOrange")
                self.view.layoutIfNeeded()
            }
            if self.reserveHsArr.count == 0 {
                noDataView.isHidden = false
                self.tableView.isHidden = true
            }else{
                noDataView.isHidden = true
                self.tableView.isHidden = false
            }
        }
    }
    
    @objc func selectMoreButton(_ sender: BorderCornerButton){
        guard !isLoad else{
            print(isLoad)
            return
        }
        if isNew{
            rno = reserveArr[sender.tag].rno
            if reserveArr[sender.tag].pstage == "已取消"{
                isCancel = true
            }else{
                isCancel = false
            }
            if reserveArr[sender.tag].poolflag == "S"{
                self.performSegue(withIdentifier: "shareOrder", sender: nil)
            }else if reserveArr[sender.tag].poolflag == "P"{
                self.performSegue(withIdentifier: "vipOrder", sender: nil)
            }else{
                self.performSegue(withIdentifier: "vanOrder", sender: nil)
            }
        }else{
            rno = reserveHsArr[sender.tag].rno
            if reserveHsArr[sender.tag].poolflag == "S"{
                self.performSegue(withIdentifier: "shareOrder", sender: nil)
            }else if reserveHsArr[sender.tag].poolflag == "P"{
                self.performSegue(withIdentifier: "vipOrder", sender: nil)
            }else{
                self.performSegue(withIdentifier: "vanOrder", sender: nil)
            }
        }
    }
    @objc func selectBeanButton(_ sender: BorderCornerButton){
        print("baen車")
        self.performSegue(withIdentifier: "load", sender: nil)
        timeList = []
        let orderModel = reserveArr[sender.tag]
        let f = orderModel.ftime.replacingOccurrences(of: "-", with: "").replacingOccurrences(of: ":", with: "")
        
        let parmeters:[String:Any] = ["fdate":f.prefix(8),"ftime":f.suffix(4),"stype":orderModel.poolflag,"ftype":orderModel.stype,"lat":orderModel.lat,"lng":orderModel.lng,"airport":orderModel.airport,"ter":orderModel.terminal,"pcnt":orderModel.pcnt,"bcnt":orderModel.bcnt,"fno":orderModel.fno,"city":orderModel.city,"dist":orderModel.distinct]
        print(parmeters)
        CallCarAPI.shared.gVanTimeCar(parameters: parmeters, success: { (success, res,error,errorString,sCode) in
            for r in res.arrayValue{
                if orderModel.dno != r["dno"].stringValue{
                    
                    let carTime = CarTimeModel(carDate: r["date"].stringValue, carTime: r["time"].stringValue, dno:r["dno"].stringValue, city: r["city"].stringValue, distinct: r["distinct"].stringValue, rbcnt: r["rbcnt"].stringValue, rpcnt: r["rpcnt"].stringValue,carType: r["cartype"].stringValue,rno:r["rno"].arrayValue[0].stringValue)
                    self.timeList.append(carTime)
                }
                
            }
            self.dismiss(animated: true, completion: {
                print(self.timeList.count)
                if self.timeList.count > 0{
                    self.beanCarSelectTag = sender.tag
                    self.performSegue(withIdentifier: "beanCar", sender: nil)
                }else{
                    let alert = AlertViewModel.shared.showAlert(message: "目前無其他可併車趟次，請稍晚在嘗試。")
                    self.present(alert, animated: true, completion: nil)
                }
            })
            
        })
    }
    @objc func selectCencelButton(_ sender: BorderCornerButton){
        guard !isLoad else{
            print(isLoad)
            return
        }
        if self.reserveArr[sender.tag].pstage == "已取消"{
            return
        }
        selectTag = sender.tag
//        print(self.reserveArr[sender.tag].cancelflag)
//        var url = "https://www.callcar.com.tw/callcar/cancel-vip.html"
//        if self.reserveArr[sender.tag].poolflag == "S"{
//            url = "https://www.callcar.com.tw/callcar/cancel-carpool.html"
//        }
//        let alertController = UIAlertController(title: "提示",message: self.reserveArr[sender.tag].cancelalert,preferredStyle: .alert)
//        if self.reserveArr[sender.tag].cancelflag == "N"{
//            let regulationAction = UIAlertAction(title: "查看規定", style: .default) { (alertAction) in
//                let url = URL(string: url)!
//                if #available(iOS 10.0, *) {
//                    UIApplication.shared.open(url)
//                } else {
//                    UIApplication.shared.openURL(url)
//                }
//            }
//            alertController.addAction(regulationAction)
//            let emailAction = UIAlertAction(title: "撰寫Email", style: .default) { (alertAction) in
//                let url = URL(string: "mailto:support@ishareco.com")!
//                if #available(iOS 10.0, *) {
//                    UIApplication.shared.open(url)
//                } else {
//                    UIApplication.shared.openURL(url)
//                }
//            }
//            alertController.addAction(emailAction)
//            alertController.addAction(UIAlertAction(title: "稍後再說",style: .cancel,handler: nil))
//        }else if self.reserveArr[sender.tag].cancelflag == "X"{
//            let regulationAction = UIAlertAction(title: "查看規定", style: .default) { (alertAction) in
//                let url = URL(string: url)!
//                if #available(iOS 10.0, *) {
//                    UIApplication.shared.open(url)
//                } else {
//                    UIApplication.shared.openURL(url)
//                }
//            }
//            alertController.addAction(regulationAction)
//            alertController.addAction(UIAlertAction(title: "確定",style: .cancel,handler: nil))
//        }else if self.reserveArr[sender.tag].cancelflag == "R" || self.reserveArr[sender.tag].cancelflag == "C" {
//            let okActtion = UIAlertAction(title: "取消訂單", style: .default) { (UIAlertAction) in
//                self.performSegue(withIdentifier: "load", sender: nil)
//                print(sender.tag)
//                let rno = self.reserveArr[sender.tag].rno
//                print(rno)
//                CallCarAPI.shared.cancelOrder(parameters: ["no":rno]) { (success, res, error,mss) in
//                    print(success)
//
//                    if error != nil {
//                        print("error")
//                        self.dismiss(animated: true, completion: nil)
//                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//                            self.present(AlertViewModel.shared.showAlert(message: "請確認網路狀態"), animated: true, completion: nil)
//                            return
//                        }
//
//                    }
//                    if !success {
//                        self.dismiss(animated: true, completion: nil)
//                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//                            self.present(AlertViewModel.shared.showAlert(message: mss), animated: true, completion: nil)
//                            return
//                        }
//                    }
//                    CallCarAPI.shared.getForder(parameters: ["uid":self.uid]) { (success, res, error) in
//                        if success{
//                            UserNotificationTools().deleteAllNotifications()
//                            RealmDB.shared.saveOrder(json: res)
//                        }
//                        self.reserveArr = RealmDB.shared.getOrder()
//                        CallCarAPI.shared.getPorder(parameters: ["uid":self.uid]) { (success, res1, error,mss) in
//                            if success{
//                                RealmDB.shared.saveHsOrder(json: res1)
//                            }
//                            self.reserveHsArr = RealmDB.shared.getHsOrder()
//                            self.tableView.reloadData()
//                            self.dismiss(animated: true, completion: nil)
//                        }
//                    }
//                }
//            }
//
//            alertController.addAction(okActtion)
//            alertController.addAction(UIAlertAction(title: "稍後再說",style: .cancel,handler: nil))
//        }else{
//            alertController.addAction(UIAlertAction(title: "確定",style: .cancel,handler: nil))
//        }
//
//        self.present(alertController, animated: true, completion: nil)
        
        self.performSegue(withIdentifier: "cancel", sender: nil)
    }
    
    func cancelReload() {
        getOrders()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "shareOrder"{
            let vc = segue.destination as! ShareOrderTableViewController
            vc.rno = rno
            vc.isNew = isNew
            vc.isCancel = isCancel
        }else if segue.identifier == "vipOrder"{
            let vc = segue.destination as! VipOrderTableViewController
            vc.rno = rno
            vc.isNew = isNew
            vc.isCancel = isCancel
        }else if segue.identifier == "cancel"{
            let vc = segue.destination as! CancelOrderViewController
            vc.order = self.reserveArr[selectTag]
            vc.delegate = self
        }else if segue.identifier == "vanOrder"{
            let vc = segue.destination as! VanOrderTableViewController
            vc.rno = rno
            vc.isNew = isNew
            vc.isCancel = isCancel
        }else if segue.identifier == "beanCar"{
            
            let nav = segue.destination as! UINavigationController
            let vc = nav.topViewController as! VanBeanCarViewController
            vc.timeList = self.timeList
            vc.orderModel = reserveArr[beanCarSelectTag]
        }
    }
    
    func goPage(rno:String){
        print(rno)
        self.rno = rno
        if isNew{
            if let order = RealmDB.shared.getOrderByNo(rno: rno){
                if order.poolflag == "P"{
                    self.performSegue(withIdentifier: "vipOrder", sender: nil)
                }else if order.poolflag == "S"{
                    self.performSegue(withIdentifier: "shareOrder", sender: nil)
                }else{
                    self.performSegue(withIdentifier: "vanOrder", sender: nil)
                }
            }
        }else{
            if let order = RealmDB.shared.getHsOrderByNo(rno: rno){
                if order.poolflag == "P"{
                    self.performSegue(withIdentifier: "vipOrder", sender: nil)
                }else if order.poolflag == "S"{
                    self.performSegue(withIdentifier: "shareOrder", sender: nil)
                }else{
                    self.performSegue(withIdentifier: "vanOrder", sender: nil)
                }
            }
        }
    }
    
    @IBAction func goReserve(_ sender: BorderCornerButton) {
        print("tap")
        self.tabBarController?.selectedIndex = 0
    }
    
}


extension ReserveListViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isNew {
            return reserveArr.count
        }else{
            return reserveHsArr.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isNew{
            let cell = tableView.dequeueReusableCell(withIdentifier: "reserveCard")! as! NewReserveHsTableViewCell
            let reserve = reserveArr[indexPath.row]
            if reserve.stime != ""{
                cell.ttimeLabel.text = reserve.stime
            }else{
                cell.ttimeLabel.text = self.toTimeFormaater(timeString: reserve.ttime)
            }
            cell.tdateLabel.text = self.yyyyMMddToMM_Slash_dd(dateString: reserve.tdate)
            cell.paddressLabel.text = reserve.paddress
            cell.taddressLabel.text = reserve.taddress
            cell.moreButton.tag = indexPath.row
            cell.rnoLabel.text = "訂單編號:\(reserve.rno)"
//            print("tdate:\(reserve.tdate) pstage:\(reserve.pstage) pstageo:\(reserve.pstageo)")
            cell.moreButton.addTarget(self, action:  #selector(self.selectMoreButton(_:)), for: .touchUpInside)
            
            if reserve.pstage == "已取消"{
                cell.cancelButton.setTitle("已取消", for: .normal)
                cell.cancelButton.setTitleColor(UIColor(hex: 0x7F7F7F), for: .normal)
                cell.topView.backgroundColor = UIColor(hex: 0xF7F7F7)
            }else{
                cell.cancelButton.setTitle("取消預約", for: .normal)
                cell.cancelButton.setTitleColor(UIColor(hex: 0xF97B62), for: .normal)
                cell.topView.backgroundColor = .white
            }
            
            cell.cancelButton.tag = indexPath.row
            cell.cancelButton.addTarget(self, action:  #selector(self.selectCencelButton(_:)), for: .touchUpInside)
            
            if reserve.poolflag == "S"{
                cell.typeLabel.text = "共乘"
                cell.typeLabel.font = UIFont.systemFont(ofSize: 14)
                cell.typeLabel.textColor = UIColor(hex: 0x20B6FC)
                cell.typeView.borderColor = UIColor(hex: 0x20B6FC)
                cell.moreButton.backgroundColor = UIColor(hex: 0x20B6FC)
                cell.moreButton.borderColor = UIColor(hex: 0x20B6FC)
//                cell.BeanCarButton.backgroundColor = UIColor(hex: 0x20B6FC)
//                cell.BeanCarButton.borderColor = UIColor(hex: 0x20B6FC)
                cell.vanTitle.isHidden = true
                cell.rnoView.backgroundColor = UIColor(hex: 0x20B6FC)
                cell.BeanCarButton.isHidden = true
            }else if reserve.poolflag == "P"{
                cell.typeLabel.text = "專接"
                cell.typeLabel.font = UIFont.systemFont(ofSize: 14)
                cell.typeLabel.textColor = UIColor(hex: 0x3575C0)
                cell.typeView.borderColor = UIColor(hex: 0x3575C0)
                cell.moreButton.backgroundColor = UIColor(hex: 0x3575C0)
                cell.moreButton.borderColor = UIColor(hex: 0x3575C0)
//                cell.BeanCarButton.backgroundColor = UIColor(hex: 0x3575C0)
//                cell.BeanCarButton.borderColor = UIColor(hex: 0x3575C0)
                cell.vanTitle.isHidden = true
                cell.rnoView.backgroundColor = UIColor(hex: 0x3575C0)
                cell.BeanCarButton.isHidden = true
            }else{
                if reserve.vanflag == "S"{
                    cell.typeLabel.text = "併車"
                }else{
                    cell.typeLabel.text = "發起"
                }
                cell.vanTitle.isHidden = false
                
                if reserve.country == "JP"{
                    cell.typeLabel.font = UIFont.systemFont(ofSize: 12)
                    cell.typeLabel.textColor = UIColor(hex: 0xF18499)
                    cell.typeView.borderColor = UIColor(hex: 0xF18499)
                    cell.moreButton.backgroundColor = UIColor(hex: 0xFC989F)
                    cell.moreButton.borderColor = UIColor(hex: 0xFC989F)
                    cell.BeanCarButton.setTitleColor(UIColor(hex: 0xFC989F), for: .normal)
                    cell.BeanCarButton.borderColor = UIColor(hex: 0xFC989F)
                    cell.vanTitle.textColor = UIColor(hex: 0xFC989F)
                    cell.rnoView.backgroundColor = UIColor(hex: 0xFC989F)
                    cell.vanTitle.text = "日本共乘"
                }else{
                    cell.typeLabel.font = UIFont.systemFont(ofSize: 12)
                    cell.typeLabel.textColor = UIColor(hex: 0xFF5C00)
                    cell.typeView.borderColor = UIColor(hex: 0xFF8300)
                    cell.moreButton.backgroundColor = UIColor(hex: 0xF5A623)
                    cell.moreButton.borderColor = UIColor(hex: 0xF5A623)
                    cell.BeanCarButton.setTitleColor(UIColor(hex: 0xF5A623), for: .normal)
                    cell.BeanCarButton.borderColor = UIColor(hex: 0xF5A623)
                    cell.vanTitle.textColor = UIColor(hex: 0xF5A623)
                    cell.rnoView.backgroundColor = UIColor(hex: 0xF5A623)
                    cell.vanTitle.text = "TaiVAN共乘"
                }
                let ttime = "\(reserve.tdate)\(reserve.ttime)".formateDate(format: "yyyyMMddHHmm") ?? Date()
                if reserve.vanflag == "M" && Date(timeIntervalSinceNow: 60*60*24*3) < ttime && reserve.dispatch.count < 2 && reserve.pstage != "已取消"{
                    cell.BeanCarButton.isHidden = false //併車
                }else{
                    cell.BeanCarButton.isHidden = true
                }
                cell.BeanCarButton.tag = indexPath.row
                cell.BeanCarButton.addTarget(self, action: #selector(self.selectBeanButton(_:)), for: .touchUpInside)
            }
            cell.rno = reserve.rno
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "reserveHsCard")! as! ReserveHsTableViewCell
            let reserveHs = reserveHsArr[indexPath.row]
            cell.rnoLabel.text = "訂單編號:\(reserveHs.rno)"
            cell.ttimeLabel.text = toTimeFormaater(timeString: reserveHs.ttime)
            cell.tdateLabel.text = yyyyMMddToMM_Slash_dd(dateString: reserveHs.tdate)
            cell.paddressLabel.text = reserveHs.paddress
            cell.taddressLabel.text = reserveHs.taddress
            cell.moreButton.tag = indexPath.row
            cell.moreButton.addTarget(self, action:  #selector(self.selectMoreButton(_:)), for: .touchUpInside)
            if reserveHs.poolflag == "S"{
                cell.typeLabel.text = "共乘"
                cell.typeLabel.font = UIFont.systemFont(ofSize: 14)
                cell.typeLabel.textColor = UIColor(hex: 0x20B6FC)
                cell.typeView.borderColor = UIColor(hex: 0x20B6FC)
                cell.moreButton.backgroundColor = UIColor(hex: 0x20B6FC)
                cell.moreButton.borderColor = UIColor(hex: 0x20B6FC)
                cell.rnoView.backgroundColor = UIColor(hex: 0x20B6FC)
                cell.vanTitle.isHidden = true
            }else if reserveHs.poolflag == "P"{
                cell.typeLabel.text = "專接"
                cell.typeLabel.font = UIFont.systemFont(ofSize: 14)
                cell.typeLabel.textColor = UIColor(hex: 0x3575C0)
                cell.typeView.borderColor = UIColor(hex: 0x3575C0)
                cell.moreButton.backgroundColor = UIColor(hex: 0x3575C0)
                cell.moreButton.borderColor = UIColor(hex: 0x3575C0)
                cell.rnoView.backgroundColor = UIColor(hex: 0x3575C0)
                cell.vanTitle.isHidden = true
            }else{
                if reserveHs.vanflag == "S"{
                    cell.typeLabel.text = "併車"
                }else{
                    cell.typeLabel.text = "發起"
                }
                cell.vanTitle.isHidden = false
                
                if reserveHs.country == "JP"{
                    cell.typeLabel.font = UIFont.systemFont(ofSize: 12)
                    cell.typeLabel.textColor = UIColor(hex: 0xF18499)
                    cell.typeView.borderColor = UIColor(hex: 0xF18499)
                    cell.moreButton.backgroundColor = UIColor(hex: 0xFC989F)
                    cell.moreButton.borderColor = UIColor(hex: 0xFC989F)
                    cell.rnoView.backgroundColor = UIColor(hex: 0xFC989F)
                    cell.vanTitle.text = "日本共乘"
                }else{
                    cell.typeLabel.font = UIFont.systemFont(ofSize: 12)
                    cell.typeLabel.textColor = UIColor(hex: 0xFF5C00)
                    cell.typeView.borderColor = UIColor(hex: 0xFF8300)
                    cell.moreButton.backgroundColor = UIColor(hex: 0xF5A623)
                    cell.moreButton.borderColor = UIColor(hex: 0xF5A623)
                    cell.rnoView.backgroundColor = UIColor(hex: 0xF5A623)
                    cell.vanTitle.text = "TaiVAN共乘"
                }
            }
            if reserveHs.pstage == "已取消"{
                cell.topView.backgroundColor = UIColor(hex: 0xF7F7F7)
                cell.cancelLabel.isHidden = false
            }else{
                cell.topView.backgroundColor = .white
                cell.cancelLabel.isHidden = true
            }
            cell.rno = reserveHs.rno
            return cell
        }
        
    }
    
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        getOrders(id: uid)
//    }
}
