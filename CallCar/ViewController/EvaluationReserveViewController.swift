//
//  EvaluationReserveViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/10/16.
//  Copyright © 2018 Cablesoft. All rights reserved.
//

import UIKit
import Cosmos
class EvaluationReserveViewController: UIViewController {

    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var cosmosView: CosmosView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textView.layer.cornerRadius = 5
        textView.layer.borderWidth = 0.5
        textView.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    @IBAction func submit(_ sender: UIButton) {
        print(cosmosView.rating)
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
}
