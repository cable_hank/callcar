//
//  NoticeViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/12/19.
//  Copyright © 2018 Cablesoft. All rights reserved.
//

import UIKit
class NoticeViewController: UIViewController {
    var bulletinModelArr = [RM_BulletinModel]()
    
    @IBOutlet weak var noMissionView: UIStackView!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.separatorStyle = .none
        noMissionView.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let tabItems = tabBarController?.tabBar.items {
            // In this case we want to modify the badge number of the third tab:
            let tabItem = tabItems[3]
            tabItem.badgeValue = nil
        }
//        let loadvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "load")
//        loadvc.modalPresentationStyle = .overFullScreen
//        self.present(loadvc, animated: false, completion: nil)
        RM_BulletinModel().updateAllisLook()
        CallCarAPI.shared.getBulletin(parameters: ["uid":UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!,"platform":"iOS"]) { (success, res, error, errormss,sCode) in
//            self.dismiss(animated: true, completion: nil)
            if sCode >= 900{
                self.goRoot()
                return
            }
            print(res)
            if success {
                RM_BulletinModel().save(json: res)
            }
            self.bulletinModelArr = RM_BulletinModel().getAll()
            
            if self.bulletinModelArr.count > 0{
                self.tableView.isHidden = false
                self.noMissionView.isHidden = true
            }else{
                self.tableView.isHidden = true
                self.noMissionView.isHidden = false
            }
            self.tableView.reloadData()
        }
    }
    @IBAction func close(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    

}

extension NoticeViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bulletinModelArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! NoticeTableViewCell
        if bulletinModelArr[indexPath.row].type == "AD"{
            cell.icon.image = UIImage(named: "NewsGift")
        }else{
            cell.icon.image = UIImage(named: "NewsCar")
        }
        cell.title.text = bulletinModelArr[indexPath.row].title
        print(bulletinModelArr[indexPath.row].content)
        cell.body.attributedText = "<style>span {font-size:14px}</style>\(bulletinModelArr[indexPath.row].content)".htmlToAttributedString
        cell.date.text = bulletinModelArr[indexPath.row].stime
        cell.body.isUserInteractionEnabled = bulletinModelArr[indexPath.row].go == ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if bulletinModelArr[indexPath.row].go == "URL"{
            let url = URL(string: bulletinModelArr[indexPath.row].gouri)
            if let url = url{
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }else if bulletinModelArr[indexPath.row].go == "VIEW"{
//            self.tabBarController?.selectedIndex = 0
//            let vc = self.tabBarController?.customizableViewControllers?[0].children[0] as! ReserveListViewController
//            vc.goPage(rno: "20181206031")

        }else if bulletinModelArr[indexPath.row].go == "IMG"{
            let storyboard = UIStoryboard(name: "Reserve", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "noticeAlert") as! NoticeAlertViewController
            if let _ = URL(string: bulletinModelArr[indexPath.row].gouri){
                vc.url = bulletinModelArr[indexPath.row].gouri
                vc.modalPresentationStyle = .overFullScreen
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    
}
