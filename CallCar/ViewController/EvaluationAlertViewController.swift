//
//  EvaluationAlertViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/11/13.
//  Copyright © 2018 Cablesoft. All rights reserved.
//

import UIKit
protocol EvaluationAlertDelegate {
    func showNotice()
}
class EvaluationAlertViewController: UIViewController {
    
    var delegate:EvaluationAlertDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.delegate?.showNotice()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dislike(_ sender: UIButton) {
        weak var pvc = self.presentingViewController
        self.dismiss(animated: false, completion: {
            let storyboard = UIStoryboard(name: "TabBar", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "opinionAlert")
            pvc?.present(vc, animated: true, completion: nil)
        })
    }
    
    @IBAction func like(_ sender: UIButton) {
        let url = URL(string: "itms-apps://itunes.apple.com/tw/app/id1229543491?action=write-review")!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func close(_ sender: UIButton) {
        self.delegate?.showNotice()
        self.dismiss(animated: true, completion: nil)
    }
}
