//
//  HomeViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2019/11/26.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import UIKit
import Firebase
import SwiftyJSON

class HomeViewController: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var storeTableView: UITableView!
    @IBOutlet weak var orderCollectionView: UICollectionView!
    @IBOutlet weak var storeTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var bgImgTop: NSLayoutConstraint!
    @IBOutlet weak var orderView: UIView!
    
    @IBOutlet weak var pageScrollView: PageScrollView!
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    var productArr:[MarketModel.RtnObject] = []
    var product:ProductModel?
    var reserveArr = [RM_OrderModel]()
    var uid = ""
    var type = 0
    var noticeURL = ""
    var imgArr = [String]()
    var timer:Timer? = nil
    var promoteJSON = [JSON]()
    var isLoad = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        uid = UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!
        self.reserveArr = RealmDB.shared.getOrderInHome()
        bgImgTop.constant = -UIApplication.shared.statusBarFrame.height
        
        //STOMAP SDK
        if #available(iOS 10.0, *) {
            let id = UserDefaults.standard.getString(key: UserInfo.id.rawValue)
            STOMAPButterManager.shared().start(withApiKey: "7aafc69cc581497abc0439dc25da0528", aliasKey: id, aliasType: STOMAPButterflySDKAliasTypeOTHER)
            STOMAPButterManager.shared().delegate = self
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        titleLabel.text = "Hi!\(UserDefaults.standard.getString(key: UserInfo.fname.rawValue))~歡迎您的預約搭乘"
        isLoad = true
        
        //訂單更新 目前是在首頁執行 訂單list下拉refresh才會更新
        CallCarAPI.shared.getForder(parameters: ["uid":uid]) { (success, res, error,sCode) in
            if sCode >= 900{
                self.goRoot()
                return
            }
            if success{
                UserNotificationTools().deleteAllNotifications()
                print(res)
                RealmDB.shared.saveOrder(json: res)
            }
            self.reserveArr = RealmDB.shared.getOrderInHome()
            self.orderView.isHidden = self.reserveArr.count == 0
            self.orderCollectionView.reloadData()
            self.isLoad = false
        }
        CallCarAPI.shared.getPorder(parameters: ["uid":self.uid]) { (success, res1, sCode,mss) in
            if sCode >= 900{
                self.goRoot()
                return
            }
            if success{
                
                RealmDB.shared.saveHsOrder(json: res1)
            }
        }
        CallCarAPI.shared.getCategorySale(parameters:"Hot", success: { (success, data) in
            if let markets = data,success{
                self.productArr = markets.RtnObject
                self.storeTableView.reloadData()
                self.storeTableViewHeight.constant = self.getTableViewHeight()
                
            }
        })
        
        CallCarAPI.shared.getPromoteH { (success, res, error, errorString) in
            print(res)
            self.imgArr = []
            self.promoteJSON = res
            for r in res{
                self.imgArr.append(r["img"].stringValue)
            }
            self.pageScrollView.imgFrame = CGSize(width: UIScreen.main.bounds.width - 20, height: (UIScreen.main.bounds.width - 20) * 10/23)
            self.pageScrollView.imgUrlArr = self.imgArr
            self.pageControl.numberOfPages = self.imgArr.count
        }
        
        if checkInternetFunction(){
            CallCarAPI.shared.testAPI(parameters: [:]) { (success, res, error, mss) in
                if !success{
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "MaintainVC")
                    self.present(vc, animated: false, completion: nil)
                }
            }
        }
        CallCarAPI.shared.getTokenlize(parameters: ["acc":UserDefaults.standard.getString(key: UserInfo.account.rawValue),"pw":UserDefaults.standard.getString(key: UserInfo.pw.rawValue)]) { (success, rtn, error, errorString) in
            print(rtn)
            if rtn == "UnAuthorize"{
                RealmDB.shared.deleteAll()
                Messaging.messaging().unsubscribe(fromTopic: "iOS")
                Messaging.messaging().unsubscribe(fromTopic: UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!)
                UserNotificationTools().deleteAllNotifications()
                AnalyticsConfiguration.shared().setAnalyticsCollectionEnabled(false)
                UserDefaults.standard.removeObject(forKey: UserInfo.account.rawValue)
                UserDefaults.standard.removeObject(forKey: UserInfo.mobile.rawValue)
                UserDefaults.standard.removeObject(forKey: UserInfo.fname.rawValue)
                UserDefaults.standard.removeObject(forKey: UserInfo.lname.rawValue)
                UserDefaults.standard.removeObject(forKey: UserInfo.id.rawValue)
                UserDefaults.standard.removeObject(forKey: UserInfo.noticeDate.rawValue)
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
                let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDel.window?.rootViewController = vc
                return
            }
            UserDefaults.standard.set(rtn, forKey: UserInfo.tokenlize.rawValue)
            //取通知
            CallCarAPI.shared.getBulletin(parameters: ["uid":UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!,"platform":"iOS"]) { (success, res, error, errormss,sCode) in
                
                if success {
                    RM_BulletinModel().save(json: res)
                }
//                if let tabItems = self.tabBarController?.tabBar.items {
//                    let tabItem = tabItems[3]
//                    if RM_BulletinModel().getIfIsLookFalse(){
//                        tabItem.badgeValue = "!"
//                    }else{
//                        tabItem.badgeValue = nil
//                    }
//                }
            }
            
            //firebaseToken
            InstanceID.instanceID().instanceID(handler: { (result, error) in
                if let error = error {
                    print("Error fetching remote instange ID: \(error)")
                } else if let result = result {
                    print("Remote instance ID token: \(result.token)")
                    Messaging.messaging().subscribe(toTopic: "iOS")
                    Messaging.messaging().subscribe(toTopic: "TW")
                    Messaging.messaging().subscribe(toTopic: UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!)
                    let parameters:[String:Any] = ["token":result.token,"uid":Int(UserDefaults.standard.getString(key: UserInfo.id.rawValue))!,"os":"iOS","osver":UIDevice.current.systemVersion,"appver":Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String,"brand":"apple","model":UIDevice.current.model]
                    CallCarAPI.shared.updToken(parameters: parameters) { (success, res, error, mss,sCode) in
                        
                    }
                }
            })
            //評價
            if let isAlertStoreKit = UserDefaults.standard.string(forKey: UserInfo.isAlertStoreKit.rawValue){
                if isAlertStoreKit == "false"{
                    UserDefaults.standard.set("true", forKey: UserInfo.isAlertStoreKit.rawValue)
                    let storyboard = UIStoryboard(name: "TabBar", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "EvalutionAlert") as! EvaluationAlertViewController
                    vc.delegate = self
                    vc.modalPresentationStyle = .overFullScreen
                    self.present(vc, animated: true, completion: nil)
                }
            }else{
                if UserDefaults.standard.getString(key: UserInfo.missionOrder.rawValue) == "1"{
                    
                    UserDefaults.standard.set("true", forKey: UserInfo.isAlertStoreKit.rawValue)
                    let storyboard = UIStoryboard(name: "TabBar", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "EvalutionAlert") as! EvaluationAlertViewController
                    vc.delegate = self
                    //                vc.modalPresentationStyle = .overFullScreen
                    self.present(vc, animated: true, completion: nil)
                }
            }
            
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        UIApplication.shared.statusBarStyle = .lightContent
        UserDefaults.standard.setValue("1", forKey: UserInfo.statusColor.rawValue)
        timerSwitch(true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        timerSwitch(false)
    }
    
    func timerSwitch(_ bool:Bool){
        if bool{
            timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.scrollViewCarousel), userInfo: nil, repeats: true)
        }else{
            timer?.invalidate()
            timer = nil
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goReserve"{
            let vc = segue.destination as! ReserveViewController
            print("reserveType:\(type)")
            vc.reserveType = type
        }else if segue.identifier == "noticePresent"{
            let vc = segue.destination as! NoticeAlertViewController
            //            print(noticeURL)
            vc.url = noticeURL
        }else if segue.identifier == "openImg"{
            let vc = segue.destination as! ImgViewController
            vc.imgUrl = promoteJSON[pageScrollView.getPage()]["img"].stringValue
        }
    }
    @IBAction func goNotice(_ sender: UIButton) {
        let vc = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "notice")
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func goVan(_ sender: UIButton) {
        type = 2
        self.performSegue(withIdentifier: "goReserve", sender: nil)
    }
    
    @IBAction func goVip(_ sender: UIButton) {
        
        type = 1
        self.performSegue(withIdentifier: "goReserve", sender: nil)
    }
    @IBAction func goJp(_ sender: UIButton) {
        type = 3
        print("type:\(type)")
        self.performSegue(withIdentifier: "goReserve", sender: nil)
    }
    
    @IBAction func vanDescroption(_ sender: UIButton) {
        
    }
    
    @IBAction func vipDescription(_ sender: UIButton) {
        
    }
    
    @IBAction func tapPageScrollView(_ sender: UITapGestureRecognizer) {
        print(promoteJSON[pageScrollView.getPage()])
        let promote = promoteJSON[pageScrollView.getPage()]
        switch promote["GotoType"].stringValue{
        case "VIEW":
            switch promote["GotoUri"].stringValue{
            case "ORDER":
                self.tabBarController?.selectedIndex = 1
            case "STORE":
                self.tabBarController?.selectedIndex = 2
            case "PROFILE":
                self.tabBarController?.selectedIndex = 3
            default:
                print("?")
            }
        case "WEB":
            let url = URL(string: promote["GotoUri"].stringValue)!
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        case "IMG":
            self.performSegue(withIdentifier: "openImg", sender: nil)
        default:
            print("no GOTO")
        }
        
    }
    
    @IBAction func goOrderList(_ sender: UIButton) {
        self.tabBarController?.selectedIndex = 1
    }
    
    @objc func selectProductButton(_ sender: BorderCornerButton){
        self.tabBarController?.selectedIndex = 2
        let vc = self.tabBarController?.customizableViewControllers?[2].children[0] as! StoreViewController
        vc.goPage(id: productArr[sender.tag].MarketID)
    }
    
    func getTableViewHeight() -> CGFloat {
        var tableViewHeight:CGFloat = 0
        
        for i in 0..<tableView(storeTableView, numberOfRowsInSection: 0){
            tableViewHeight = tableViewHeight + tableView(storeTableView, cellForRowAt: IndexPath(row: i, section: 0)).frame.height
        }
        return tableViewHeight
    }
    
}

extension HomeViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "storeCell") as! StoreTableViewCell
        cell.setCell(product: productArr[indexPath.row])
        cell.goButton.tag = indexPath.row
        cell.goButton.addTarget(self, action:  #selector(self.selectProductButton(_:)), for: .touchUpInside)
        
        return cell
    }
}

extension HomeViewController:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return reserveArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "order", for: indexPath) as! HomeOrderCollectionViewCell
        let reserve = reserveArr[indexPath.row]
        if reserve.stime != ""{
            cell.timeLabel.text = reserve.stime
        }else{
            cell.timeLabel.text = self.toTimeFormaater(timeString: reserve.ttime)
        }
        cell.dateLabel.text = self.yyyyMMddToMM_Slash_dd(dateString: reserve.tdate)
        cell.addr1.text = reserve.paddress
        cell.addr2.text = reserve.taddress
        if reserve.poolflag == "P"{
            cell.titleImg.image = UIImage(named: "titlevip")
        }else{
            if reserve.country == "JP"{
                cell.titleImg.image = UIImage(named: "titlejp")
            }else{
                cell.titleImg.image = UIImage(named: "titlevanpool")
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath)
        if isLoad {
            return
        }
        self.tabBarController?.selectedIndex = 1
        let vc = self.tabBarController?.customizableViewControllers?[1].children[0] as! ReserveListViewController
        vc.goPage(rno: reserveArr[indexPath.row].rno)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 280, height: 175)
    }
}

extension HomeViewController:UIScrollViewDelegate{
    
    func scrollToPage(page:CGFloat){
        var frame = pageScrollView.frame
        frame.origin.x = frame.width * page
        frame.origin.y = 0
        pageControl.currentPage = Int(page)
        pageScrollView.scrollRectToVisible(frame, animated: true)
    }
    
    @objc func scrollViewCarousel(){
        if pageControl.currentPage == imgArr.count-1{
            scrollToPage(page:0)
        }else{
            scrollToPage(page:CGFloat(pageControl.currentPage+1))
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView != pageScrollView {return}
        if imgArr.count > 0{
            print("scrollViewDidEndDecelerating")
            print(self.pageScrollView.getPage())
            pageControl.currentPage = self.pageScrollView.getPage()
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if scrollView != pageScrollView {return}
        timerSwitch(false)
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if scrollView != pageScrollView {return}
        timerSwitch(true)
    }
    
}

extension HomeViewController:STOMAPButterManagerDelegate{
    func getLocationStatusIsAlways(_ value: Bool) {
        
    }
    
    func getResult(_ result: String) {
        
    }
    
    func getStatus(_ status: String) {
        
    }
    
    func getScanStatus(_ status: String) {
        
    }
    
    func getScanUUIDList(_ uuidLists: [Any]) {
        
    }
    
    func getScanFinalList(_ finalLists: [Any]) {
        
    }
    
    func getUpdateTime(_ updateTime: Int32) {
        
    }
    
    
}

extension HomeViewController:EvaluationAlertDelegate{
    func showNotice() {
        
    }
    
    
}
