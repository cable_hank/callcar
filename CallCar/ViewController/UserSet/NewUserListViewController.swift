//
//  NewUserListViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2019/5/16.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import UIKit
import Firebase
class NewUserListViewController: UIViewController {
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var version: UILabel!
    @IBOutlet weak var bonusLabel: UILabel!
    @IBOutlet weak var inviteLockButton: CornerButton!
    @IBOutlet weak var inviteNextImg: UIImageView!
    @IBOutlet weak var bgTop: NSLayoutConstraint!
    
    var invIsUnLock = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bgTop.constant = -UIApplication.shared.statusBarFrame.height
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
        UserDefaults.standard.setValue("1", forKey: UserInfo.statusColor.rawValue)
        navigationController?.setNavigationBarHidden(true, animated: false)
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            self.version.text = "版本號 \(version)"
        }
        userName.text = "\(UserDefaults.standard.getString(key: UserInfo.lname.rawValue))\(UserDefaults.standard.getString(key: UserInfo.fname.rawValue))"
        if let bonus = UserDefaults.standard.string(forKey: UserInfo.bonus.rawValue){
            bonusLabel.text = bonus
        }
        CallCarAPI.shared.getMarketing(parameters: ["uid":UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!]) { (success, res, error,sCode) in
            if sCode >= 900{
                self.goRoot()
                return
            }
            if success{
                self.bonusLabel.text = res["bonus"].stringValue
            }
        }
        CallCarAPI.shared.getMission(parameters: ["uid":UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!]) { (success, res, error,errorString,sCode) in
            if sCode >= 900{
                self.goRoot()
                return
            }
            if success{
                print("certCard:\(res["onetime"]["certCard"].stringValue)")
                print("newOrder:\(res["onetime"]["newOrder"].stringValue)")
                UserDefaults.standard.set(res["onetime"]["certCard"].stringValue, forKey: UserInfo.missionCard.rawValue)
                UserDefaults.standard.set(res["onetime"]["newOrder"].stringValue, forKey: UserInfo.missionOrder.rawValue)
            }
//            if let newOrder = UserDefaults.standard.string(forKey: UserInfo.missionOrder.rawValue) {
//                if newOrder == "1"{
//                    self.invIsUnLock = true
//                    self.inviteLockButton.setTitle("分享邀請碼", for: .normal)
//                    self.inviteLockButton.setImage(nil, for: .normal)
//                    self.inviteNextImg.isHidden = false
//                    if UserDefaults.standard.bool(forKey: UserInfo.unlockAlert.rawValue) == false{
//                        UserDefaults.standard.set(true, forKey: UserInfo.unlockAlert.rawValue)
//                        self.performSegue(withIdentifier: "showFirstUnlock", sender: nil)
//                    }
//                }
//            }
        }
        UserNotificationTools().checkNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        if #available(iOS 13.0, *) {
            UIApplication.shared.statusBarStyle = .darkContent
        } else {
            UIApplication.shared.statusBarStyle = .default
        }
        UserDefaults.standard.setValue("0", forKey: UserInfo.statusColor.rawValue)
    }
    
    @IBAction func goShop(_ sender: CornerButton) {
        self.tabBarController?.selectedIndex = 2
    }
    
    
    @IBAction func inviteLock(_ sender: CornerButton) {
        if invIsUnLock{
            UIPasteboard.general.string = "哈囉~送NT$50優惠金給你!\n讓你享受優質的機場接送體驗\n使用我的好友邀請碼\(UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!)就能獲得喔!https://goo.gl/3rENYe"
            let activityViewController = UIActivityViewController(activityItems: ["哈囉~送NT$50優惠金給你!\n讓你享受優質的機場接送體驗\n使用我的好友邀請碼\(UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!)就能獲得喔!https://goo.gl/3rENYe"], applicationActivities: nil)
            self.present(activityViewController, animated: true, completion: nil)
        }else{
            self.performSegue(withIdentifier: "invLockTips", sender: nil)
        }
    }
    
    @IBAction func goInvite(_ sender: UIButton) {
        if invIsUnLock{
            self.performSegue(withIdentifier: "goInv", sender: nil)
        }
    }
    
    @IBAction func goWeb(_ sender: UIButton) {
        var urlString = ""
        if sender.tag == 0{
            urlString = "http://www.callcar.com.tw/#service-faq"
        }else if sender.tag == 1{
            urlString = "https://www.callcar.com.tw/"
        }else if sender.tag == 2{
            urlString = "http://m.me/callcar.tw"
        }else if sender.tag == 3{
            urlString = "https://www.callcar.com.tw/policy.html"
        }
        let url = URL(string: urlString)!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func logout(_ sender: CornerButton) {
        let alertController = UIAlertController(title: "提示",message: "是否確定登出？",preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "返回",style: .cancel,handler: nil))
        let okActtion = UIAlertAction(title: "確定", style: .default) { (UIAlertAction) in
            InstanceID.instanceID().instanceID(handler: { (result, error) in
                if let error = error {
                    print("Error fetching remote instange ID: \(error)")
                } else if let result = result {
                    print("Remote instance ID token: \(result.token)")
                    let parameters:[String:Any] = ["token":result.token,"uid":Int(UserDefaults.standard.getString(key: UserInfo.id.rawValue))!]
                    CallCarAPI.shared.delToken(parameters: parameters, success: { (success, res, error, mss,sCode) in
                        if sCode >= 900{
                            self.goRoot()
                            return
                        }
                        if success {
                            RealmDB.shared.deleteAll()
                            Messaging.messaging().unsubscribe(fromTopic: "iOS")
                            Messaging.messaging().unsubscribe(fromTopic: UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!)
                            UserNotificationTools().deleteAllNotifications()
                            AnalyticsConfiguration.shared().setAnalyticsCollectionEnabled(false)
                            UserDefaults.standard.removeObject(forKey: UserInfo.account.rawValue)
                            UserDefaults.standard.removeObject(forKey: UserInfo.mobile.rawValue)
                            UserDefaults.standard.removeObject(forKey: UserInfo.fname.rawValue)
                            UserDefaults.standard.removeObject(forKey: UserInfo.lname.rawValue)
                            UserDefaults.standard.removeObject(forKey: UserInfo.id.rawValue)
                            UserDefaults.standard.removeObject(forKey: UserInfo.noticeDate.rawValue)
                            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
                            let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDel.window?.rootViewController = vc
                        }else{
                            self.present(AlertViewModel.shared.showAlert(message: "請確認網路狀態"), animated: true, completion: nil)
                        }
                        
                    })
                }
            })
            
        }
        alertController.addAction(okActtion)
        
        self.present(alertController, animated: true, completion: nil)
    }
}
