//
//  BonusViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/8/1.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit
import SwiftyJSON
class BonusViewController: UIViewController {

    @IBOutlet weak var effpointLabel: UILabel!
    @IBOutlet weak var uneffpointLabel: UILabel!
    @IBOutlet weak var expiredDateLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var noDataLabel: UILabel!
    var bonusList = [RM_BonusModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "乘車點數"
        //backbutton設定
        let backButton = UIBarButtonItem()
        backButton.title = " "
        backButton.tintColor = .black
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        self.tableView.separatorStyle = .none
        self.tabBarController?.tabBar.isHidden = true
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-12-31"
        expiredDateLabel.text = formatter.string(from: Date())
    }

    override func viewWillAppear(_ animated: Bool) {
        
        navigationController?.setNavigationBarHidden(false, animated: false)
        bonusList = RealmDB.shared.getBonus()
        effpointLabel.text = UserDefaults.standard.string(forKey: UserInfo.bonus.rawValue)
        uneffpointLabel.text = UserDefaults.standard.string(forKey: UserInfo.unbonus.rawValue)
        CallCarAPI.shared.getBonus(parameters: ["uid":UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!]) { (success, res, sCode, errorString) in
            if sCode >= 900{
                self.goRoot()
                return
            }
            print(res)
            if !success {
//                self.present(AlertViewModel.shared.showAlert(message: errorString), animated: true, completion: nil)
                return
            }
            var bonusL = [BonusModel]()
            self.effpointLabel.text = res["effpoint"].stringValue
            UserDefaults.standard.set(res["effpoint"].stringValue, forKey: UserInfo.bonus.rawValue)
            self.uneffpointLabel.text = res["uneffpoint"].stringValue
            UserDefaults.standard.set(res["uneffpoint"].stringValue, forKey: UserInfo.unbonus.rawValue)
            for r in res["bonus"].arrayValue{
                var bonusModel = BonusModel()
                bonusModel.addtype = r["addtype"].stringValue
                bonusModel.btype = r["btype"].stringValue
                bonusModel.relatedinfo = r["relatedinfo"].stringValue
                bonusModel.point = r["point"].stringValue
                bonusModel.cancel = r["cancel"].stringValue
                bonusModel.effdate = r["effdate"].stringValue
                bonusModel.getdate = r["getdate"].stringValue
                bonusModel.memo = r["memo"].stringValue
                bonusL.append(bonusModel)
            }
            RealmDB.shared.saveBonus(bonusModels: bonusL)
            self.bonusList = RealmDB.shared.getBonus()
            self.tableView.reloadData()
            if self.bonusList.count == 0 {
                self.noDataLabel.isHidden = false
            }else{
                self.noDataLabel.isHidden = true
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}

extension BonusViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bonusList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "bonusCell")! as! BonusTableViewCell
        if bonusList[indexPath.row].btype == "I"{
            cell.tiltle1Label.text = "邀請好友點數回饋"
            cell.title2Label.text = bonusList[indexPath.row].relatedinfo
            cell.point.text = "+\(bonusList[indexPath.row].point)"
        }else if bonusList[indexPath.row].btype == "O"{
            cell.tiltle1Label.text = "輸入邀請碼點數回饋"
            cell.title2Label.text = bonusList[indexPath.row].relatedinfo
            cell.point.text = "+\(bonusList[indexPath.row].point)"
        }else if bonusList[indexPath.row].btype == "R"{
            cell.tiltle1Label.text = "訂單編號\(bonusList[indexPath.row].relatedinfo)"
            if bonusList[indexPath.row].addtype == "NEW"{
                cell.title2Label.text = "點數回饋"
                cell.point.text = "+\(bonusList[indexPath.row].point)"
            }else{
                cell.title2Label.text = "使用點數"
                cell.point.text = "-\(bonusList[indexPath.row].point)"
            }
        }else{
            cell.tiltle1Label.text = bonusList[indexPath.row].memo
            cell.title2Label.text = ""
            cell.point.text = "+\(bonusList[indexPath.row].point)"
        }
        print(bonusList[indexPath.row].effdate)
        cell.date.text = toDateFormaaterSlash(dateString: bonusList[indexPath.row].effdate)
        return cell
    }
    
    
}
