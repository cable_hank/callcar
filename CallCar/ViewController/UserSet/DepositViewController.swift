//
//  DepositViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/8/9.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit

class DepositViewController: UIViewController {

    @IBOutlet weak var pointLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var noDataLabel: UILabel!
    
    var depositList = [RM_DepositModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "儲值金"
        //backbutton設定
        let backButton = UIBarButtonItem()
        backButton.title = " "
        backButton.tintColor = .black
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        self.tableView.separatorStyle = .none
        self.tabBarController?.tabBar.isHidden = true
    }

    override func viewWillAppear(_ animated: Bool) {
        depositList = RealmDB.shared.getDeposit()
        pointLabel.text = UserDefaults.standard.string(forKey: UserInfo.deposit.rawValue)
        CallCarAPI.shared.getDeposit(parameters: ["uid":UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!]) { (success, res, sCode,errorString) in
            if sCode >= 900{
                self.goRoot()
                return
            }
            print(res)
            if !success {
//                self.present(AlertViewModel.shared.showAlert(message: errorString), animated: true, completion: nil)
                return
            }
            var depositL = [DepositModel]()
            self.pointLabel.text = res["amt"].stringValue
            UserDefaults.standard.set(res["amt"].stringValue, forKey: UserInfo.deposit.rawValue)
            for r in res["deposits"].arrayValue{
                var depositModel = DepositModel()
                depositModel.source = r["source"].stringValue
                depositModel.type = r["type"].stringValue
                depositModel.getdate = r["getdate"].stringValue
                depositModel.amt = r["amt"].stringValue
                depositModel.cancel = r["cancel"].stringValue
                depositModel.relatedinfo = r["relatedinfo"].stringValue
                depositModel.memo = r["memo"].stringValue
                depositL.append(depositModel)
            }
            RealmDB.shared.saveDeposit(depositModels: depositL)
            self.depositList = RealmDB.shared.getDeposit()
            self.tableView.reloadData()
            if self.depositList.count == 0{
                self.noDataLabel.isHidden = false
            }else{
                self.noDataLabel.isHidden = true
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}

extension DepositViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return depositList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "depositCell")! as! BonusTableViewCell
        let dModel = depositList[indexPath.row]
        cell.date.text = dModel.getdate
        if dModel.type == "NEW"{
            cell.point.text = "+\(dModel.amt)"
            
            cell.tiltle1Label.text = "\(dModel.source) 儲值金編號:\(dModel.relatedinfo)"
        }else{
            cell.point.text = "-\(dModel.amt)"
            
            cell.tiltle1Label.text = "訂單儲值金折抵:\(dModel.relatedinfo)"
        }
        return cell
    }
}
