//
//  AccountEditViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/14.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit
class AccountEditViewController: UIViewController {
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var lnameTextField: LeftIconTextField!
    @IBOutlet weak var fnameTextField: LeftIconTextField!
    @IBOutlet weak var mobileTextField: LeftIconTextField!
    @IBOutlet weak var mbutton: UIButton!
    @IBOutlet weak var fbutton: UIButton!
    
    @IBOutlet weak var noAddrLabel: UILabel!
    @IBOutlet weak var addrTableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var shopUserInfoView: UIView!
    
    @IBOutlet weak var shopUserInfo: UILabel!
    
    @IBOutlet weak var noShopUserLabel: UILabel!
    var uid = ""
    var addrArr = [RM_AddrModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "帳號設定"
        //backbutton設定
        let backButton = UIBarButtonItem()
        backButton.title = " "
        backButton.tintColor = .black
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        self.addrTableView.separatorStyle = .none
        
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func selectDelButton(_ sender:UIButton){
        let alertController = UIAlertController(title: "提示",message: "是否確定刪除？",preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "取消",style: .cancel,handler: nil))
        let okActtion = UIAlertAction(title: "確定", style: .default) { (UIAlertAction) in
            RealmDB.shared.delAddr(id: self.addrArr[sender.tag].id)
            self.reloadAddr()
        }
        alertController.addAction(okActtion)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func reloadAddr(){
        addrArr = RealmDB.shared.getAddr()
        noAddrLabel.isHidden = addrArr.count > 0
        addrTableView.reloadData()
        tableViewHeight.constant = CGFloat(addrArr.count * 40)
    }
    @IBAction func selectMButton(_ sender: UIButton) {
        if mbutton.isSelected == false && fbutton.isSelected == false{
            sender.isSelected = true
            return
        }
        if !sender.isSelected{
            sender.isSelected = !sender.isSelected
            fbutton.isSelected = !fbutton.isSelected
        }
    }
    
    @IBAction func selectFButton(_ sender: UIButton) {
        if mbutton.isSelected == false && fbutton.isSelected == false{
            sender.isSelected = true
            return
        }
        if !sender.isSelected{
            sender.isSelected = !sender.isSelected
            mbutton.isSelected = !mbutton.isSelected
        }
    }
    
    @IBAction func delShopUserInfo(_ sender: UIButton) {
        let alertController = UIAlertController(title: "提示",message: "是否確定刪除？",preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "取消",style: .cancel,handler: nil))
        let okActtion = UIAlertAction(title: "確定", style: .default) { (UIAlertAction) in
            RealmShop.shared.delShopUser()
            self.noShopUserLabel.isHidden = false
            self.shopUserInfoView.isHidden = true
        }
        alertController.addAction(okActtion)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        navigationController?.setNavigationBarHidden(false, animated: false)
        reloadAddr()
        self.tabBarController?.tabBar.isHidden = true
        uid = UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!
        
        if let shopUser = RealmShop.shared.getShopUser(){
            noShopUserLabel.isHidden = true
            shopUserInfo.text = "\(shopUser.name) \(shopUser.mobile)\n\(shopUser.allAddr)"
        }else{
            noShopUserLabel.isHidden = false
            shopUserInfoView.isHidden = true
        }
        
        CallCarAPI.shared.getUserInfo(parameters: ["id":Int(uid)!]) { (success, res, sCode) in
            if sCode >= 900{
                self.goRoot()
                return
            }
            print(res)
            self.emailLabel.text = res["email"].stringValue
            print("lname:\(res["lname"].stringValue)")
            self.lnameTextField.text = res["lname"].stringValue
            self.fnameTextField.text = res["fname"].stringValue
            self.mobileTextField.text = res["mobile"].stringValue
            if res["gender"].stringValue == "0"{
                self.fbutton.isSelected = true
                self.mbutton.isSelected = false
            }else if res["gender"].stringValue == "1"{
                self.mbutton.isSelected = true
                self.fbutton.isSelected = false
            }else{
                self.mbutton.isSelected = false
                self.fbutton.isSelected = false
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        var gender = ""
        if mbutton.isSelected == true{
            gender = "1"
        }else if fbutton.isSelected == true{
            gender = "0"
        }else{
            gender = "2"
        }
        let data:[String : Any] = ["fname":fnameTextField.text!,"lname":lnameTextField.text!,"mobile":mobileTextField.text!,"nname":"","gender":gender,"mname":"","age":"","local":"","birthday":""]
        let paras:[String : Any]  = ["uid":"\(Int(uid)!)","data":"\(convertDictionaryToString(data))"]
        print(paras)
        
        CallCarAPI.shared.updateAccount(parameters: paras) { (success, res, sCode, mss) in
            if sCode >= 900{
                self.goRoot()
                return
            }
            if success{
                UserDefaults.standard.setValue(self.lnameTextField.text!, forKey: UserInfo.lname.rawValue)
                UserDefaults.standard.setValue(self.fnameTextField.text!, forKey: UserInfo.fname.rawValue)
                UserDefaults.standard.setValue(self.mobileTextField.text!, forKey: UserInfo.mobile.rawValue)
                print("updateAcc:\(res)")
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    var textFieldY:CGFloat = 0
    var isKeyboardShown = false
    
    @IBAction func textFieldDidBegin(_ sender: UITextField) {
        let relativePositionView = UIApplication.shared.keyWindow
        let relativeFrame = sender.superview?.convert(sender.frame, to: relativePositionView)
        let yPosition = relativeFrame?.minY;
        textFieldY = yPosition! + sender.frame.height * 3
        
    }
    
    @objc func keyboardWillShow(note: NSNotification) {
        if isKeyboardShown {
            return
        }
        let keyboardAnimationDetail = note.userInfo as! [String: AnyObject]
        let duration = TimeInterval(truncating: keyboardAnimationDetail[UIResponder.keyboardAnimationDurationUserInfoKey]! as! NSNumber)
        let keyboardFrameValue = keyboardAnimationDetail[UIResponder.keyboardFrameBeginUserInfoKey]! as! NSValue
        let keyboardFrame = keyboardFrameValue.cgRectValue
        
        if self.view.frame.height - keyboardFrame.size.height > textFieldY{
            return
        }
        
        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: -(keyboardFrame.size.height - (self.view.frame.height - self.textFieldY)))
        })
        isKeyboardShown = true
    }
    
    @objc func keyboardWillHide(note: NSNotification) {
        let keyboardAnimationDetail = note.userInfo as! [String: AnyObject]
        let duration = TimeInterval(truncating: keyboardAnimationDetail[UIResponder.keyboardAnimationDurationUserInfoKey]! as! NSNumber)
        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy:  -self.view.frame.origin.y)
        })
        isKeyboardShown = false
    }
}


func convertDictionaryToString(_ dictionary: [String : Any]) -> String {
    var result: String = ""
    
    do {
        // 將dictionary轉成JSON data
        let data: Data = try JSONSerialization.data(withJSONObject: dictionary, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        // 將data格式化成json格式
        let decoded = try JSONSerialization.jsonObject(with: data, options: [])
        
        if let dictFromJSON = decoded as? [String : Any] {
            // 將Data轉成字串
            let jsonData: Data = try JSONSerialization.data(withJSONObject: dictFromJSON, options: JSONSerialization.WritingOptions.prettyPrinted)
            result = String(data: jsonData, encoding: .utf8)!
        }
    } catch {
        print("convertDictionaryToString: \(error.localizedDescription)")
    }
    return result
}

extension AccountEditViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addrArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")! as! AddrDelTableViewCell
        cell.addrLabel.text = addrArr[indexPath.row].addr
        cell.delButton.tag = indexPath.row
        cell.delButton.addTarget(self, action:  #selector(self.selectDelButton(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
}
