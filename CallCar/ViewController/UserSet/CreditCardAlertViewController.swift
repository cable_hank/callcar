//
//  CreditCardAlertViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/24.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit
protocol CreditCardValueDelegate {
    func cardNo(no:String,date:String,thirdNo:String,id:String)
}
class CreditCardAlertViewController: UIViewController {
    @IBOutlet weak var cardNoTextField: LeftIconTextField!
    @IBOutlet weak var cardDateTextField: LeftIconTextField!
    @IBOutlet weak var cardTriNoTextField: LeftIconTextField!
    var delegate:CreditCardValueDelegate?
    
    var cardText = ""
    var uid = ""
    var cid = ""
    var isload = false
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        print(cid)
        uid = UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if isload{
            return
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func editCreditNo(_ sender: LeftIconTextField) {
        let cardno = sender.text!.replacingOccurrences(of: "-", with: "")
        if cardno.count >= 5 && cardno.count <= 8{
            sender.text! = "\(cardno.prefix(4))-\(cardno.suffix(cardno.count-4))"
        }
        if cardno.count >= 9 && cardno.count <= 12{
            sender.text! = "\(cardno.prefix(4))-\(cardno.subString(from: 4, to: 7))-\(cardno.suffix(cardno.count-8))"
        }
        if cardno.count >= 13 && cardno.count <= 16{
            sender.text! = "\(cardno.prefix(4))-\(cardno.subString(from: 4, to: 7))-\(cardno.subString(from: 8, to: 11))-\(cardno.suffix(cardno.count-12))"
        }
        if sender.text?.count == 19{
            sender.errorFunc(isError: false)
            cardDateTextField.select(nil)
        }else{
            sender.errorFunc(isError: true)
        }
    }
    
    @IBAction func editDate(_ sender: LeftIconTextField) {
        let date = sender.text!.replacingOccurrences(of: "/", with: "")
        if date.count >= 3{
            sender.text! = "\(date.prefix(2))/\(date.suffix(date.count-2))"
            
        }
        if sender.text?.count == 5{
            sender.errorFunc(isError: false)
            cardTriNoTextField.select(nil)
        }else{
            sender.errorFunc(isError: true)
        }
    }
    
    @IBAction func edit3No(_ sender: LeftIconTextField) {
        sender.text = "\(sender.text!.prefix(3))"
        if sender.text?.count == 3{
            sender.errorFunc(isError: false)
            self.view.endEditing(true)
        }else{
            sender.errorFunc(isError: true)
        }
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        if isload{
            return
        }
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func ok(_ sender: UIButton) {
        if isload{
            return
        }
        if cardNoTextField.text!.count > 0 && cardDateTextField.text!.count > 0 && cardTriNoTextField.text!.count > 0{
            isload = true
            let no = cardNoTextField.text!.replacingOccurrences(of: "-", with: "")
            let date = cardDateTextField.text!.replacingOccurrences(of: "/", with: "")
            let parameters:[String : Any] = ["uid":uid,"cno":no,"date":date,"code":cardTriNoTextField.text!]
            print(parameters)
            CallCarAPI.shared.certCard(parameters: parameters) { (success, res, sCode,mss) in
                if sCode >= 900{
                    self.goRoot()
                    return
                }
                print("RtnObject:\(res)")
                if success {
                    let parameters2:[String : Any] = ["uid":self.uid,"cno":no,"tid":res]
                    if self.cid == "" {
                        print(parameters2)
                        CallCarAPI.shared.addCard(parameters: parameters2) { (success, res, sCode,mss) in
                            if sCode >= 900{
                                self.goRoot()
                                return
                            }
                            print("RtnObject:\(res)")
                            self.isload = false
                            if success {
                                self.delegate?.cardNo(no: self.cardNoTextField.text!, date: date, thirdNo: self.cardTriNoTextField.text!, id: res)
                                self.dismiss(animated: true, completion: nil)
                            }else{
                                self.present(AlertViewModel.shared.showAlert(message: mss), animated: true, completion: nil)
                            }
                        }
                    }else{
                        CallCarAPI.shared.delCard(parameters: ["uid":self.uid,"cid":self.cid]) { (success, res, error,mss,sCode) in
                            if sCode >= 900{
                                self.goRoot()
                                return
                            }
                            if success{
                                CallCarAPI.shared.addCard(parameters: parameters2) { (success, res, sCode,mss) in
                                    if sCode >= 900{
                                        self.goRoot()
                                        return
                                    }
                                    print("RtnObject:\(res)")
                                    self.isload = false
                                    if success {
                                        self.delegate?.cardNo(no: self.cardNoTextField.text!, date: date, thirdNo: self.cardTriNoTextField.text!, id: res)
                                        self.dismiss(animated: true, completion: nil)
                                    }else{
                                        self.present(AlertViewModel.shared.showAlert(message: mss), animated: true, completion: nil)
                                    }
                                }
                            }else{
                               self.present(AlertViewModel.shared.showAlert(message: mss), animated: true, completion: nil)
                                self.isload = false
                            }
                        }
                    }
                    
                }else{
                    self.present(AlertViewModel.shared.showAlert(message: mss), animated: true, completion: nil)
                    self.isload = false
                }
            }
            
            
        }else{
            self.present(AlertViewModel.shared.showAlert(message: "請輸入完整的信用卡資訊"), animated: true, completion: nil)
        }
    }
    
    var textFieldY:CGFloat = 0
    var isKeyboardShown = false
    
    @IBAction func textFieldDidBegin(_ sender: UITextField) {
        let relativePositionView = UIApplication.shared.keyWindow
        let relativeFrame = sender.superview?.convert(sender.frame, to: relativePositionView)
        let yPosition = relativeFrame?.minY;
        textFieldY = yPosition! + sender.frame.height * 3
        
    }
    
    @objc func keyboardWillShow(note: NSNotification) {
        if isKeyboardShown {
            return
        }
        let keyboardAnimationDetail = note.userInfo as! [String: AnyObject]
        let duration = TimeInterval(truncating: keyboardAnimationDetail[UIResponder.keyboardAnimationDurationUserInfoKey]! as! NSNumber)
        let keyboardFrameValue = keyboardAnimationDetail[UIResponder.keyboardFrameBeginUserInfoKey]! as! NSValue
        let keyboardFrame = keyboardFrameValue.cgRectValue
        
        if self.view.frame.height - keyboardFrame.size.height > textFieldY{
            return
        }
        
        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: -(keyboardFrame.size.height - (self.view.frame.height - self.textFieldY)))
        })
        isKeyboardShown = true
    }
    
    @objc func keyboardWillHide(note: NSNotification) {
        let keyboardAnimationDetail = note.userInfo as! [String: AnyObject]
        let duration = TimeInterval(truncating: keyboardAnimationDetail[UIResponder.keyboardAnimationDurationUserInfoKey]! as! NSNumber)
        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy:  -self.view.frame.origin.y)
        })
        isKeyboardShown = false
    }
}


