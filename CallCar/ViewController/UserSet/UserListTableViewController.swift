//
//  UserListTableViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/15.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit
import Firebase
class UserListTableViewController: UITableViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var version: UILabel!
    @IBOutlet weak var bonusLabel: UILabel!
    
//    @IBOutlet weak var depositLabel: UILabel!
    
    @IBOutlet weak var inviteNo: UILabel!
    @IBOutlet weak var invTitleLabel: UILabel!
    @IBOutlet weak var invSubTitle: UILabel!
    @IBOutlet weak var lockButton: BorderButton!
    @IBOutlet weak var invCellContentView: UIView!
    
    var invIsLock = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = .white
    }

    override func viewWillAppear(_ animated: Bool) {
        // Get version and build
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            print(version)
            self.version.text = "版本號 \(version)"
        }
        if let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            print(build)
        }
        nameLabel.text = UserDefaults.standard.string(forKey: UserInfo.fname.rawValue)
        print(UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!)
        
        if let bonus = UserDefaults.standard.string(forKey: UserInfo.bonus.rawValue){
            bonusLabel.text = bonus
        }
        
//        if let deposit = UserDefaults.standard.string(forKey: UserInfo.deposit.rawValue){
//            depositLabel.text = deposit
//        }
        CallCarAPI.shared.getMarketing(parameters: ["uid":UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!]) { (success, res, error,sCode) in
            if sCode >= 900{
                self.goRoot()
                return
            }
            if success{
                self.bonusLabel.text = res["bonus"].stringValue
//                self.depositLabel.text = res["deposit"].stringValue
            }
        }
        CallCarAPI.shared.getMission(parameters: ["uid":UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!]) { (success, res, error,errorString,sCode) in
            if sCode >= 900{
                self.goRoot()
                return
            }
            if success{
                print("certCard:\(res["onetime"]["certCard"].stringValue)")
                print("newOrder:\(res["onetime"]["newOrder"].stringValue)")
                UserDefaults.standard.set(res["onetime"]["certCard"].stringValue, forKey: UserInfo.missionCard.rawValue)
                UserDefaults.standard.set(res["onetime"]["newOrder"].stringValue, forKey: UserInfo.missionOrder.rawValue)
            }
            if let newOrder = UserDefaults.standard.string(forKey: UserInfo.missionOrder.rawValue) {
                if newOrder == "1"{
                    self.invIsLock = true
                    self.invTitleLabel.textColor = .black
                    self.invSubTitle.textColor = .black
                    self.lockButton.isHidden = true
                    self.invCellContentView.backgroundColor = .white
                    if UserDefaults.standard.bool(forKey: UserInfo.unlockAlert.rawValue) == false{
                        UserDefaults.standard.set(true, forKey: UserInfo.unlockAlert.rawValue)
                        self.performSegue(withIdentifier: "showFirstUnlock", sender: nil)
                    }
                }
            }
        }
        UserNotificationTools().checkNotifications()
    }
    
    @IBAction func tapInvCell(_ sender: UITapGestureRecognizer) {
        if invIsLock{
            self.performSegue(withIdentifier: "showInv", sender: nil)
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func logOut(_ sender: UIButton) {
        let alertController = UIAlertController(title: "提示",message: "是否確定登出？",preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "返回",style: .cancel,handler: nil))
        let okActtion = UIAlertAction(title: "確定", style: .default) { (UIAlertAction) in
            InstanceID.instanceID().instanceID(handler: { (result, error) in
                if let error = error {
                    print("Error fetching remote instange ID: \(error)")
                } else if let result = result {
                    print("Remote instance ID token: \(result.token)")
                    let parameters:[String:Any] = ["token":result.token,"uid":Int(UserDefaults.standard.getString(key: UserInfo.id.rawValue))!]
                    CallCarAPI.shared.delToken(parameters: parameters, success: { (success, res, error, mss,sCode) in
                        if sCode >= 900{
                            self.goRoot()
                            return
                        }

                        if success {
                            RealmDB.shared.deleteAll()
                            Messaging.messaging().unsubscribe(fromTopic: "iOS")
                            Messaging.messaging().unsubscribe(fromTopic: UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!)
                            UserNotificationTools().deleteAllNotifications()
                            AnalyticsConfiguration.shared().setAnalyticsCollectionEnabled(false)
                            UserDefaults.standard.removeObject(forKey: UserInfo.account.rawValue)
                            UserDefaults.standard.removeObject(forKey: UserInfo.mobile.rawValue)
                            UserDefaults.standard.removeObject(forKey: UserInfo.fname.rawValue)
                            UserDefaults.standard.removeObject(forKey: UserInfo.lname.rawValue)
                            UserDefaults.standard.removeObject(forKey: UserInfo.id.rawValue)
                            UserDefaults.standard.removeObject(forKey: UserInfo.noticeDate.rawValue)
                            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
                            let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDel.window?.rootViewController = vc
                        }else{
                            self.present(AlertViewModel.shared.showAlert(message: "請確認網路狀態"), animated: true, completion: nil)
                        }
                        
                    })
                }
            })
            
        }
        alertController.addAction(okActtion)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row{
        case 3:
            self.performSegue(withIdentifier: "goAccountEdit", sender: nil)
        case 4:
            self.performSegue(withIdentifier: "goPayEdit", sender: nil)
        case 5:
            let url = URL(string: "http://www.callcar.com.tw/#service-faq")!
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        case 6:
            let url = URL(string: "http://m.me/callcar.tw")!
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        case 8:
            let url = URL(string: "https://www.callcar.com.tw/policy.html")!
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        case 9:
            let url = URL(string: "https://www.callcar.com.tw/")!
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        default: break
            
        }
        
        
    }
}
