//
//  InviteViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/8/14.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit

class InviteViewController: UIViewController {

    @IBOutlet weak var myIntro: CornerButton!
    @IBOutlet weak var isOKLabel: UILabel!
    @IBOutlet weak var invTextField: LeftIconTextField!
    @IBOutlet weak var submitButton: BorderButton!
    @IBOutlet weak var personCount: UILabel!
    @IBOutlet weak var totalPoint: UILabel!
    
    @IBOutlet weak var tipsView: ShadowCornerView!
    var isOK = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "邀請碼"
        //backbutton設定
        let backButton = UIBarButtonItem()
        backButton.title = " "
        backButton.tintColor = .black
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        self.tabBarController?.tabBar.isHidden = true
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: false)
        myIntro.setTitle(UserDefaults.standard.string(forKey: UserInfo.id.rawValue), for: .normal)
        CallCarAPI.shared.getUserInfo(parameters: ["id":UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!]) { (success, res, sCode) in
            if sCode >= 900{
                self.goRoot()
                return
            }
            print(res["intro"])
            if res["intro"].stringValue != "" && res["intro"].stringValue != "0"{
                self.disableSubmit(inv: res["intro"].stringValue)
            }
        }
        
        CallCarAPI.shared.getIntroList(parameters: ["uid":UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!]) { (success, res, error,errorString,sCode) in
            if sCode >= 900{
                self.goRoot()
                return
            }
            self.personCount.text = "\(res.count)"
            var price = 0
            for r in res.arrayValue{
                price = price + r["amt"].intValue
            }
            self.totalPoint.text = "\(price)"
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func copyMyIntro(_ sender: BorderButton) {
        UIPasteboard.general.string = UserDefaults.standard.string(forKey: UserInfo.id.rawValue)
        tipsView.isHidden = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) { // change 2 to desired number of seconds
            self.tipsView.isHidden = true
        }
    }
    
    @IBAction func shareMyIntro(_ sender: BorderButton) {
        UIPasteboard.general.string = "哈囉~送NT$50優惠金給你!\n讓你享受優質的機場接送體驗\n使用我的好友邀請碼\(UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!)就能獲得喔!https://goo.gl/3rENYe"
        let activityViewController = UIActivityViewController(activityItems: ["哈囉~送NT$50優惠金給你!\n讓你享受優質的機場接送體驗\n使用我的好友邀請碼\(UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!)就能獲得喔!https://goo.gl/3rENYe"], applicationActivities: nil)
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func inputIntro(_ sender: LeftIconTextField) {
        if sender.text?.count == 10{
            CallCarAPI.shared.chkInv(parameters: ["code":sender.text!]) { (success, res, error,sCode) in
                if sCode >= 900{
                    self.goRoot()
                    return
                }
                if success && sender.text != UserDefaults.standard.string(forKey: UserInfo.id.rawValue){
                    self.isOKLabel.isHidden = false
                    self.isOKLabel.text = "✔︎"
                    self.isOKLabel.textColor = UIColor(hex: 0x47B707)
                    self.isOK = true
                }else{
                    self.isOKLabel.isHidden = false
                    self.isOKLabel.text = "✖︎"
                    self.isOKLabel.textColor = UIColor(hex: 0xF97B62)
                    self.isOK = false
                }
            }
        }else{
            isOKLabel.isHidden = false
            isOKLabel.text = "✖︎"
            isOKLabel.textColor = UIColor(hex: 0xF97B62)
            isOK = false
        }
    }
    
    @IBAction func submitIntro(_ sender: BorderButton) {
        if isOK{
            sender.isEnabled = false
            CallCarAPI.shared.updateInv(parameters: ["uid":UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!,"code":invTextField.text!]) { (success, res, error, returnString,sCode) in
                if sCode >= 900{
                    self.goRoot()
                    return
                }
                if self.isApiError(error: error){
                    let alertController = UIAlertController(title: "提示",message: "連線失敗",preferredStyle: .alert)
                    let okActtion = UIAlertAction(title: "確定", style: .default) { (UIAlertAction) in
                        let storyboard = UIStoryboard(name: "TabBar", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "TabBarVC")
                        self.present(vc, animated: false, completion: nil)
                    }
                    alertController.addAction(okActtion)
                    self.present(alertController, animated: true, completion: nil)
                    return
                }
                if success {
                    self.disableSubmit(inv: self.invTextField.text!)
                    self.isOK = false
                    self.isOKLabel.isHidden = true
                    self.performSegue(withIdentifier: "alert", sender: nil)
                }else{
                    self.present(AlertViewModel.shared.showAlert(message: returnString), animated: true, completion: nil)
                }
                sender.isEnabled = true
            }
        }else{
            self.present(AlertViewModel.shared.showAlert(message: "請輸入正確的邀請碼"), animated: true, completion: nil)
        }
    }
    
    func disableSubmit(inv:String) {
        submitButton.backgroundColor = .gray
        submitButton.borderColor = .gray
        submitButton.bgColor = .gray
        submitButton.setImage(nil, for: .normal)
        submitButton.setTitle("已兌換點數", for: .normal)
        submitButton.isEnabled = false
        invTextField.isEnabled = false
        invTextField.text = inv
    }

    var textFieldY:CGFloat = 0
    var isKeyboardShown = false
    
    @IBAction func textFieldDidBegin(_ sender: UITextField) {
        let relativePositionView = UIApplication.shared.keyWindow
        let relativeFrame = sender.superview?.convert(sender.frame, to: relativePositionView)
        let yPosition = relativeFrame?.minY;
        textFieldY = yPosition! + sender.frame.height * 3
        
    }
    
    @objc func keyboardWillShow(note: NSNotification) {
        if isKeyboardShown {
            return
        }
        let keyboardAnimationDetail = note.userInfo as! [String: AnyObject]
        let duration = TimeInterval(truncating: keyboardAnimationDetail[UIResponder.keyboardAnimationDurationUserInfoKey]! as! NSNumber)
        let keyboardFrameValue = keyboardAnimationDetail[UIResponder.keyboardFrameBeginUserInfoKey]! as! NSValue
        let keyboardFrame = keyboardFrameValue.cgRectValue
        
        if self.view.frame.height - keyboardFrame.size.height > textFieldY{
            return
        }
        
        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: -(keyboardFrame.size.height - (self.view.frame.height - self.textFieldY)))
        })
        isKeyboardShown = true
    }
    
    @objc func keyboardWillHide(note: NSNotification) {
        let keyboardAnimationDetail = note.userInfo as! [String: AnyObject]
        let duration = TimeInterval(truncating: keyboardAnimationDetail[UIResponder.keyboardAnimationDurationUserInfoKey]! as! NSNumber)
        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy:  -self.view.frame.origin.y)
        })
        isKeyboardShown = false
    }
}


