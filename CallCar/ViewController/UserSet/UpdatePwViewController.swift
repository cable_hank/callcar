//
//  UpdatePwViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/6/4.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit

class UpdatePwViewController: UIViewController {
    
    @IBOutlet weak var oldPwTextField: LeftIconTextField!
    @IBOutlet weak var newPwTextField: LeftIconTextField!
    @IBOutlet weak var newPwdAgainTextField: LeftIconTextField!
    @IBOutlet weak var errorLabel: UILabel!
    
    var uid = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        uid =  UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func ok(_ sender: UIButton) {
        guard oldPwTextField.text! != "" || oldPwTextField.text! != "" || newPwdAgainTextField.text! != "" else{
            errorLabel.text = "請輸入密碼"
            errorLabel.isHidden = false
            return
        }
        
        guard newPwTextField.text! == newPwdAgainTextField.text! else{
            errorLabel.text = "密碼與確認密碼不符"
            errorLabel.isHidden = false
            return
        }
        
        guard newPwTextField.text!.count > 5 else{
            errorLabel.text = "密碼需大於六位數"
            errorLabel.isHidden = false
            return
        }
        CallCarAPI.shared.updatePw(parameters: ["id":uid,"old":oldPwTextField.text!,"newp":newPwTextField.text!]) { (success, res, error, msg,sCode) in
            if sCode >= 900{
                self.goRoot()
                return
            }
            if self.isApiError(error: error){
                let alertController = UIAlertController(title: "提示",message: "連線失敗",preferredStyle: .alert)
                let okActtion = UIAlertAction(title: "確定", style: .default) { (UIAlertAction) in
                    let storyboard = UIStoryboard(name: "TabBar", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "TabBarVC")
                    self.present(vc, animated: false, completion: nil)
                }
                alertController.addAction(okActtion)
                self.present(alertController, animated: true, completion: nil)
                return
            }
//            let alertController = UIAlertController(title: "提示",message: msg,preferredStyle: .alert)
//            let okActtion = UIAlertAction(title: "確定", style: .default) { (UIAlertAction) in
//                if success {
//                    print("success")
//                    self.dismiss(animated: true, completion: nil)
//                }else{
//                    print("no success")
//                }
//            }
//            alertController.addAction(okActtion)
//            self.present(alertController, animated: true, completion: nil)
            
            UserDefaults.standard.setValue(self.newPwTextField.text!, forKey: UserInfo.pw.rawValue)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    var textFieldY:CGFloat = 0
    var isKeyboardShown = false
    
    @IBAction func textFieldDidBegin(_ sender: UITextField) {
        let relativePositionView = UIApplication.shared.keyWindow
        let relativeFrame = sender.superview?.convert(sender.frame, to: relativePositionView)
        let yPosition = relativeFrame?.minY;
        textFieldY = yPosition! + sender.frame.height * 3
        
    }
    
    @objc func keyboardWillShow(note: NSNotification) {
        if isKeyboardShown {
            return
        }
        let keyboardAnimationDetail = note.userInfo as! [String: AnyObject]
        let duration = TimeInterval(truncating: keyboardAnimationDetail[UIResponder.keyboardAnimationDurationUserInfoKey]! as! NSNumber)
        let keyboardFrameValue = keyboardAnimationDetail[UIResponder.keyboardFrameBeginUserInfoKey]! as! NSValue
        let keyboardFrame = keyboardFrameValue.cgRectValue
        
        if self.view.frame.height - keyboardFrame.size.height > textFieldY{
            return
        }
        
        UIView.animate(withDuration: duration, animations: { () -> Void in
            print(-(keyboardFrame.size.height - (self.view.frame.height - self.textFieldY)))
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: -(keyboardFrame.size.height - (self.view.frame.height - self.textFieldY)))
        })
        isKeyboardShown = true
    }
    
    @objc func keyboardWillHide(note: NSNotification) {
        let keyboardAnimationDetail = note.userInfo as! [String: AnyObject]
        let duration = TimeInterval(truncating: keyboardAnimationDetail[UIResponder.keyboardAnimationDurationUserInfoKey]! as! NSNumber)
        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy:  -self.view.frame.origin.y)
        })
        isKeyboardShown = false
    }
}
