//
//  InvoiceAlertViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/24.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit

protocol InvoiceDelegate {
    func invoiceValue(invoiceAddr:String,invoiceNo:String,id:String)
}
class InvoiceAlertViewController: UIViewController {

    @IBOutlet weak var invoiceNo: LeftIconTextField!
    @IBOutlet weak var invoiceAddr: LeftIconTextField!
    @IBOutlet weak var invNoErrorLabel: UILabel!
    @IBOutlet weak var invAddrErrorLabel: UILabel!
    
    var uid = ""
    var iid = ""
    var delegate:InvoiceDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        iid = ""
        uid = UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!
        CallCarAPI.shared.getInv(parameters: ["uid":uid]) { (success, res, sCode) in
            if sCode >= 900{
                self.goRoot()
                return
            }
            if success{
                self.invoiceNo.text = res["ein"].stringValue
                self.invoiceAddr.text = res["address"].stringValue
                self.iid = res["iid"].stringValue
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func ok(_ sender: UIButton) {
        guard invoiceAddr.text! != "" else{
            invAddrErrorLabel.text = "請輸入地址"
            invAddrErrorLabel.isHidden = false
            return
        }
        if  invoiceNo.text! != ""{
            if  invoiceNo.text!.count != 8{
                invNoErrorLabel.isHidden = false
                return
            }
        }
        let parameters:[String:Any] = ["uid":uid,"title":"","ein":self.invoiceNo.text!,"address":self.invoiceAddr.text!]
        print(parameters)
        if iid != ""{
            CallCarAPI.shared.delInv(parameters: ["uid":self.uid,"iid":self.iid]) { (success, res, error,sCode) in
                if sCode >= 900{
                    self.goRoot()
                    return
                }
                if success{
                    CallCarAPI.shared.addInv(parameters: parameters) { (success, res, sCode) in
                        if sCode >= 900{
                            self.goRoot()
                            return
                        }
                        print("RtnObject:\(res)")
                        if success {
                            self.delegate?.invoiceValue(invoiceAddr: self.invoiceAddr.text!, invoiceNo: self.invoiceNo.text!,id:res)
                            self.dismiss(animated: true, completion: nil)
                        }else{
                            self.invAddrErrorLabel.text = "新增失敗"
                            self.invAddrErrorLabel.isHidden = false
                        }
                    }
                }
            }
        }else{
            CallCarAPI.shared.addInv(parameters: parameters) { (success, res, sCode) in
                if sCode >= 900{
                    self.goRoot()
                    return
                }
                print("RtnObject:\(res)")
                if success {
                    self.delegate?.invoiceValue(invoiceAddr: self.invoiceAddr.text!, invoiceNo: self.invoiceNo.text!,id:res)
                    self.dismiss(animated: true, completion: nil)
                }else{
                    self.invAddrErrorLabel.text = "新增失敗"
                    self.invAddrErrorLabel.isHidden = false
                }
            }
        }
        
        
        
    }
    @IBAction func editInvNo(_ sender: LeftIconTextField) {
        if sender.text?.count == 8{
            invNoErrorLabel.isHidden = true
        }
    }
    
    @IBAction func editAddr(_ sender: LeftIconTextField) {
        if sender.text!.count > 0{
            invAddrErrorLabel.isHidden = true
        }
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    var textFieldY:CGFloat = 0
    var isKeyboardShown = false
    
    @IBAction func textFieldDidBegin(_ sender: UITextField) {
        let relativePositionView = UIApplication.shared.keyWindow
        let relativeFrame = sender.superview?.convert(sender.frame, to: relativePositionView)
        let yPosition = relativeFrame?.minY;
        textFieldY = yPosition! + sender.frame.height * 3
        
    }
    
    @objc func keyboardWillShow(note: NSNotification) {
        if isKeyboardShown {
            return
        }
        let keyboardAnimationDetail = note.userInfo as! [String: AnyObject]
        let duration = TimeInterval(truncating: keyboardAnimationDetail[UIResponder.keyboardAnimationDurationUserInfoKey]! as! NSNumber)
        let keyboardFrameValue = keyboardAnimationDetail[UIResponder.keyboardFrameBeginUserInfoKey]! as! NSValue
        let keyboardFrame = keyboardFrameValue.cgRectValue
        
        if self.view.frame.height - keyboardFrame.size.height > textFieldY{
            return
        }
        
        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: -(keyboardFrame.size.height - (self.view.frame.height - self.textFieldY)))
        })
        isKeyboardShown = true
    }
    
    @objc func keyboardWillHide(note: NSNotification) {
        let keyboardAnimationDetail = note.userInfo as! [String: AnyObject]
        let duration = TimeInterval(truncating: keyboardAnimationDetail[UIResponder.keyboardAnimationDurationUserInfoKey]! as! NSNumber)
        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy:  -self.view.frame.origin.y)
        })
        isKeyboardShown = false
    }
}
