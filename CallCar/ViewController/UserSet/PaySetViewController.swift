//
//  PaySetViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/29.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit

class PaySetViewController: UIViewController,CreditCardValueDelegate,InvoiceDelegate {
    

    @IBOutlet weak var cardInfoView: UIView!
    @IBOutlet weak var invInfoView: UIView!
    @IBOutlet weak var cardInfoHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var invInfoHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var cardLabel: UILabel!
    @IBOutlet weak var cardInfoLabel: UILabel!
    @IBOutlet weak var invLabel: UILabel!
    @IBOutlet weak var invNoLabel: UILabel!
    @IBOutlet weak var invAddTLabel: UILabel!
    @IBOutlet weak var invAddrLabel: UILabel!
    
    @IBOutlet weak var cardButton: CornerButton!
    @IBOutlet weak var invButton: CornerButton!
    
    var cardId = ""
    var invId = ""
    var uid = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "付款設定"
        //backbutton設定
        let backButton = UIBarButtonItem()
        backButton.title = " "
        backButton.tintColor = .black
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }

    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: false)
        uid = UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!
        CallCarAPI.shared.getInv(parameters: ["uid":uid]) { (success, res, sCode) in
            if sCode >= 900{
                self.goRoot()
                return
            }
            if success{
                self.haveInv(have:true,ein: res["ein"].stringValue, address: res["address"].stringValue,id:res["iid"].stringValue )
            }
        }
        CallCarAPI.shared.getCard(parameters: ["uid":uid]) { (success, res, error) in
            print(res)
            if success{
                self.haveCard(have:true,no: res["CNo"].stringValue, date: res["Expire"].stringValue, thirdNo: res["Code"].stringValue,id:res["ID"].stringValue)
            }
        }
    }
    
    func haveCard(have:Bool,no:String,date:String,thirdNo:String,id:String){
        cardLabel.isHidden = !have
        cardInfoLabel.isHidden = !have
        cardInfoLabel.text = no
        cardId = id
        
        if have{
            cardButton.setTitle("移除信用卡", for: .normal)
            cardInfoHeightConstraint.constant = 60
            cardInfoView.layoutIfNeeded()
        }else{
            cardButton.setTitle("設定信用卡", for: .normal)
            cardInfoHeightConstraint.constant = 0
            cardInfoView.layoutIfNeeded()
        }
        
    }
    
    func haveInv(have:Bool,ein:String,address:String,id:String){
        invLabel.isHidden = !have
        invNoLabel.isHidden = !have
        invAddTLabel.isHidden = !have
        invAddrLabel.isHidden = !have
        invNoLabel.text = ein
        invAddrLabel.text = address
        invId = id
        if have{
            invButton.setTitle("移除發票", for: .normal)
            invInfoHeightConstraint.constant = 90
            invInfoView.layoutIfNeeded()
        }else{
            invButton.setTitle("設定發票", for: .normal)
            invInfoHeightConstraint.constant = 0
            invInfoView.layoutIfNeeded()
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.navigationController?.popViewController(animated: false);
    }
    
    @IBAction func cardButtonAction(_ sender: CornerButton) {
        if cardLabel.isHidden{
            self.performSegue(withIdentifier: "presentCard", sender: nil)
        }else{
            let alertController = UIAlertController(title: "提示",message: "確定刪除信用卡?",preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "返回",style: .cancel,handler: nil))
            let okActtion = UIAlertAction(title: "確定", style: .default) { (AlertAction) in
                CallCarAPI.shared.delCard(parameters: ["uid":self.uid,"cid":self.cardId]) { (success, res, error,mss,sCode) in
                    if sCode >= 900{
                        self.goRoot()
                        return
                    }
                    if self.isApiError(error: error){
                        let alertController2 = UIAlertController(title: "提示",message: "連線失敗",preferredStyle: .alert)
                        let okActtion2 = UIAlertAction(title: "確定", style: .default) { (UIAlertAction) in
                            let storyboard = UIStoryboard(name: "TabBar", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "TabBarVC")
                            self.present(vc, animated: false, completion: nil)
                        }
                        alertController2.addAction(okActtion2)
                        self.present(alertController2, animated: true, completion: nil)
                        return
                    }
                    if success{
                        self.haveCard(have: false, no: "", date: "", thirdNo: "",id:"")
                    }
                }
                
            }
            alertController.addAction(okActtion)
            
            self.present(alertController, animated: true, completion: nil)
            
            
            
        }
    }
    
    @IBAction func invButtonAction(_ sender: CornerButton) {
        if invLabel.isHidden{
            self.performSegue(withIdentifier: "presentInv", sender: nil)
        }else{
            let alertController = UIAlertController(title: "提示",message: "確定刪除發票?",preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "返回",style: .cancel,handler: nil))
            let okActtion = UIAlertAction(title: "確定", style: .default) { (AlertAction) in
                CallCarAPI.shared.delInv(parameters: ["uid":self.uid,"iid":self.invId]) { (success, res, error,sCode) in
                    if sCode >= 900{
                        self.goRoot()
                        return
                    }
                    if self.isApiError(error: error){
                        let alertController2 = UIAlertController(title: "提示",message: "連線失敗",preferredStyle: .alert)
                        let okActtion2 = UIAlertAction(title: "確定", style: .default) { (UIAlertAction) in
                            let storyboard = UIStoryboard(name: "TabBar", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "TabBarVC")
                            self.present(vc, animated: false, completion: nil)
                        }
                        alertController2.addAction(okActtion2)
                        self.present(alertController2, animated: true, completion: nil)
                        return
                    }
                    if success{
                        self.haveInv(have:false,ein: "", address: "",id:"")
                        self.invId = ""
                    }
                }
                
            }
            alertController.addAction(okActtion)
            
            self.present(alertController, animated: true, completion: nil)
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "presentCard"{
            let vc = segue.destination as! CreditCardAlertViewController
            vc.delegate = self
        }else if segue.identifier == "presentInv"{
            let vc = segue.destination as! InvoiceAlertViewController
            vc.delegate = self
        }
    }

    func cardNo(no: String, date: String, thirdNo: String,id:String) {
        CallCarAPI.shared.getCard(parameters: ["uid":uid]) { (success, res, error) in
            if success{
                self.haveCard(have:true,no: res["CNo"].stringValue, date: res["Expire"].stringValue, thirdNo: res["Code"].stringValue,id:res["ID"].stringValue)
            }
        }
    }
    
    func invoiceValue(invoiceAddr: String, invoiceNo: String,id:String) {
        CallCarAPI.shared.getInv(parameters: ["uid":uid]) { (success, res, sCode) in
            if sCode >= 900{
                self.goRoot()
                return
            }
            if success{
                self.haveInv(have:true,ein: res["ein"].stringValue, address: res["address"].stringValue,id:res["iid"].stringValue )
            }
        }
    }
}
