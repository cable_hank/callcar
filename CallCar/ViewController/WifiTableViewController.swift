//
//  WifiTableViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/6/1.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit

class WifiTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.barTintColor = .white
        self.tableView.separatorStyle = .none
         tableView.backgroundView = UIImageView(image: UIImage(named: "Bg"))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func web1(_ sender: UIButton) {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string : "https://www.gowifi.com.tw/event/callcar_gowifi.html")!)
        } else {
            let url = URL(string: "https://www.gowifi.com.tw/event/callcar_gowifi.html")!
            UIApplication.shared.openURL(url)
        }
    }
    @IBAction func web2(_ sender: UIButton) {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string : "https://www.wifihero.com.tw/zh/CallCar#wifihero")!)
        } else {
             let url = URL(string: "https://www.wifihero.com.tw/zh/CallCar#wifihero")!
            UIApplication.shared.openURL(url)
        }
    }
    
}
