//
//  ForgetPwdViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/11.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit

class ForgetPwdViewController: UIViewController {

    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var emailTextField: LeftIconTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func cancel(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func editEmail(_ sender: LeftIconTextField) {
        if !sender.isValidEmail(){
            sender.errorFunc(isError: true)
            errorLabel.isHidden = false
        }else{
            sender.errorFunc(isError: false)
            errorLabel.isHidden = true
        }
    }
    
    @IBAction func ok(_ sender: UIButton) {
        guard errorLabel.isHidden else{
            self.present(AlertViewModel.shared.showAlert(message: "E-mail格式有誤"), animated: true, completion: nil)
            return
        }
        CallCarAPI.shared.checkAccount(parameters: ["acc":emailTextField.text!]) { (res,error) in
            if self.isApiError(error: error){
                let alertController = UIAlertController(title: "提示",message: "連線失敗",preferredStyle: .alert)
                let okActtion = UIAlertAction(title: "確定", style: .default) { (UIAlertAction) in
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "loginNv")
                    self.present(vc, animated: false, completion: nil)
                }
                alertController.addAction(okActtion)
                self.present(alertController, animated: true, completion: nil)
                return
            }
            if let err = error{
                self.present(AlertViewModel.shared.showAlert(message: "錯誤代碼:\(err)"), animated: true, completion: nil)
                return
            }
            guard res else{
                self.present(AlertViewModel.shared.showAlert(message: "查無此信箱"), animated: true, completion: nil)
                
                return
            }
            self.performSegue(withIdentifier: "load", sender: nil)
            CallCarAPI.shared.forgetPw(parameters: ["email":self.emailTextField.text!]) { (success, res, sCode, msg) in
//                if sCode >= 900{
//                    self.goRoot()
//                    return
//                }

                if success{
                    self.dismiss(animated: true, completion: nil)
                    let alertController = UIAlertController(title: "提示",message: "新密碼已寄出",preferredStyle: .alert)
                    let okActtion = UIAlertAction(title: "確定", style: .default) { (UIAlertAction) in
                        self.dismiss(animated: true, completion: nil)
                    }
                    alertController.addAction(okActtion)
                    self.present(alertController, animated: true, completion: nil)
                }else{
                    self.present(AlertViewModel.shared.showAlert(message: msg), animated: true, completion: nil)
                }
            }
        }
        
        
    }
    var textFieldY:CGFloat = 0
    var isKeyboardShown = false
    
    @IBAction func textFieldDidBegin(_ sender: UITextField) {
        let relativePositionView = UIApplication.shared.keyWindow
        let relativeFrame = sender.superview?.convert(sender.frame, to: relativePositionView)
        let yPosition = relativeFrame?.minY;
        textFieldY = yPosition! + sender.frame.height * 3
        
    }
    
    @objc func keyboardWillShow(note: NSNotification) {
        if isKeyboardShown {
            return
        }
        let keyboardAnimationDetail = note.userInfo as! [String: AnyObject]
        let duration = TimeInterval(truncating: keyboardAnimationDetail[UIResponder.keyboardAnimationDurationUserInfoKey]! as! NSNumber)
        let keyboardFrameValue = keyboardAnimationDetail[UIResponder.keyboardFrameBeginUserInfoKey]! as! NSValue
        let keyboardFrame = keyboardFrameValue.cgRectValue
        
        if self.view.frame.height - keyboardFrame.size.height > textFieldY{
            return
        }
        
        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: -(keyboardFrame.size.height - (self.view.frame.height - self.textFieldY)))
        })
        isKeyboardShown = true
    }
    
    @objc func keyboardWillHide(note: NSNotification) {
        let keyboardAnimationDetail = note.userInfo as! [String: AnyObject]
        let duration = TimeInterval(truncating: keyboardAnimationDetail[UIResponder.keyboardAnimationDurationUserInfoKey]! as! NSNumber)
        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy:  -self.view.frame.origin.y)
        })
        isKeyboardShown = false
    }
   
}
