//
//  LoginViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/11.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit
import SwiftyJSON
import Firebase
class LoginViewController: UIViewController{

    @IBOutlet weak var email: LeftIconTextField!
    @IBOutlet weak var pwd: LeftIconTextField!
    @IBOutlet weak var errorEmail: UILabel!
    
    var textFieldY:CGFloat = 0
    var isKeyboardShown = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let backButton = UIBarButtonItem()
        backButton.title = " "
        backButton.tintColor = .black
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func login(_ sender: UIButton) {
        let acc = email.text!
        let pw = pwd.text!
        guard acc != "" && pw != "" else {
            self.present(AlertViewModel.shared.showAlert(message: "請輸入帳號或密碼"), animated: true, completion: nil)
            return
        }
        guard errorEmail.isHidden else{
            self.present(AlertViewModel.shared.showAlert(message: "E-mail格式有誤"), animated: true, completion: nil)
            return
        }
        
        print("acc:\(acc)  pw:\(pw)")
        CallCarAPI.shared.login(parameters: ["acc":acc,"pw":pw]) { (res,error,mss) in
            if self.isApiError(error: error){
                let alertController = UIAlertController(title: "提示",message: "連線失敗",preferredStyle: .alert)
                let okActtion = UIAlertAction(title: "確定", style: .default) { (UIAlertAction) in
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "loginNv")
                    self.present(vc, animated: false, completion: nil)
                }
                alertController.addAction(okActtion)
                self.present(alertController, animated: true, completion: nil)
                return
            }
            if  error != nil{
                self.present(AlertViewModel.shared.showAlert(message: "請確認網路狀態"), animated: true, completion: nil)
                return
            }
            guard res else{
                self.present(AlertViewModel.shared.showAlert(message: mss), animated: true, completion: nil)
                return
            }
            CallCarAPI.shared.checkAccount(parameters: ["acc":self.email.text!], success: { (success, res,error) in
                print(res["mobile"])
                
                UserDefaults.standard.setValue(res["mobile"].stringValue, forKey: UserInfo.mobile.rawValue)
                UserDefaults.standard.setValue(res["uid"].stringValue, forKey: UserInfo.id.rawValue)
                UserDefaults.standard.setValue(res["lname"].stringValue, forKey: UserInfo.lname.rawValue)
                UserDefaults.standard.setValue(res["fname"].stringValue, forKey: UserInfo.fname.rawValue)
                UserDefaults.standard.setValue(self.email.text, forKey: UserInfo.account.rawValue)
                UserDefaults.standard.setValue(self.pwd.text, forKey: UserInfo.pw.rawValue)
                
                CallCarAPI.shared.loadMission(parameters: ["uid":res["uid"].stringValue])
                CallCarAPI.shared.getMission(parameters: ["uid":res["uid"].stringValue]) { (success, res, error,errorString,sCode) in
                    if sCode >= 900{
                        self.goRoot()
                        return
                    }
                    if success{
//                        print(res["onetime"]["certCard"].stringValue)
                        UserDefaults.standard.set(res["onetime"]["certCard"].stringValue, forKey: UserInfo.missionCard.rawValue)
                        UserDefaults.standard.set(res["onetime"]["newOrder"].stringValue, forKey: UserInfo.missionOrder.rawValue)
                        if res["onetime"]["certCard"].stringValue == "1" && res["onetime"]["newOrder"].stringValue == "1"{
                            UserDefaults.standard.set(true, forKey: UserInfo.unlockAlert.rawValue)
                        }else{
                            UserDefaults.standard.set(false, forKey: UserInfo.unlockAlert.rawValue)
                        }
//                        print("-------------------------------------------------------")
//                        print(res["onetime"]["certCard"].stringValue == "1")
//                        print(res["onetime"]["newOrder"].stringValue == "1")
//                        print(UserDefaults.standard.bool(forKey: UserInfo.unlockAlert.rawValue))
                    }
                }
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                let vc = UIStoryboard(name: "TabBar", bundle: nil).instantiateInitialViewController()
                AnalyticsConfiguration.shared().setAnalyticsCollectionEnabled(true)
//                self.performSegue(withIdentifier: "loginPush", sender: self)
                appDelegate.window?.rootViewController = vc
                appDelegate.window!.makeKeyAndVisible()
                
            })
        }
        
    }
    @IBAction func emailValidate(_ sender: LeftIconTextField) {
        sender.text = sender.text?.replacingOccurrences(of: " ", with: "")
        if !sender.isValidEmail(){
            sender.errorFunc(isError: true)
            errorEmail.isHidden = false
        }else{
            sender.errorFunc(isError: false)
            errorEmail.isHidden = true
        }
    }
    
    @IBAction func textFieldDidBegin(_ sender: UITextField) {
        textFieldY = sender.frame.minY + sender.frame.height * 3
    }
    
    @objc func keyboardWillShow(note: NSNotification) {
        if isKeyboardShown {
            return
        }
        let keyboardAnimationDetail = note.userInfo as! [String: AnyObject]
        let duration = TimeInterval(truncating: keyboardAnimationDetail[UIResponder.keyboardAnimationDurationUserInfoKey]! as! NSNumber)
        let keyboardFrameValue = keyboardAnimationDetail[UIResponder.keyboardFrameBeginUserInfoKey]! as! NSValue
        let keyboardFrame = keyboardFrameValue.cgRectValue
        
        if self.view.frame.height - keyboardFrame.size.height > textFieldY{
            return
        }
        
        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: -(keyboardFrame.size.height - (self.view.frame.height - self.textFieldY)))
        })
        isKeyboardShown = true
    }
    
    @objc func keyboardWillHide(note: NSNotification) {
        let keyboardAnimationDetail = note.userInfo as! [String: AnyObject]
        let duration = TimeInterval(truncating: keyboardAnimationDetail[UIResponder.keyboardAnimationDurationUserInfoKey]! as! NSNumber)
        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy:  -self.view.frame.origin.y)
        })
        isKeyboardShown = false
    }
}
