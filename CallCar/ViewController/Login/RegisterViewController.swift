//
//  RegisterViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/16.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit
import SwiftyJSON
class RegisterViewController: UIViewController {
    @IBOutlet weak var emailErrorLabel: UILabel!
    @IBOutlet weak var emailTextField: LeftIconTextField!
    @IBOutlet weak var pwdTextField: LeftIconTextField!
    @IBOutlet weak var pwdAgainTextField: LeftIconTextField!
    @IBOutlet weak var submitBtn: CornerButton!
    @IBOutlet weak var load: UIActivityIndicatorView!
    var registerModel:RegisterModel = RegisterModel.shard
    override func viewDidLoad() {
        super.viewDidLoad()

        let backButton = UIBarButtonItem()
        backButton.title = " "
        backButton.tintColor = .black
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func emailValidate(_ sender: LeftIconTextField) {
        sender.text = sender.text?.lowercased()
        if !sender.isValidEmail(){
            sender.errorFunc(isError: true)
            emailErrorLabel.isHidden = false
        }else{
            sender.errorFunc(isError: false)
            emailErrorLabel.isHidden = true
        }
        
    }
    
    
    @IBAction func next(_ sender: UIButton) {
        let acc = emailTextField.text!
        let pw = pwdTextField.text!
        let pwA = pwdAgainTextField.text!
        guard emailErrorLabel.isHidden else{
            self.present(AlertViewModel.shared.showAlert(message: "E-mail格式有誤"), animated: true, completion: nil)
            return
        }
        guard acc != "" && pw != "" && pwA != "" else {
            self.present(AlertViewModel.shared.showAlert(message: "請輸入帳號或密碼"), animated: true, completion: nil)
            return
        }
        
        guard pw.count > 5 else{
            self.present(AlertViewModel.shared.showAlert(message: "密碼需大於六位數"), animated: true, completion: nil)
            return
        }
        guard pw.count < 13 else{
            self.present(AlertViewModel.shared.showAlert(message: "密碼需小於十二位數"), animated: true, completion: nil)
            return
        }
        guard pw == pwA else {
            self.present(AlertViewModel.shared.showAlert(message: "密碼與確認密碼需相同"), animated: true, completion: nil)
            return
        }
        submitBtn.isEnabled = false
        CallCarAPI.shared.checkAccount(parameters: ["acc":acc]) { (res,error) in
            self.submitBtn.isEnabled = true
            if self.isApiError(error: error){
                let alertController = UIAlertController(title: "提示",message: "連線失敗",preferredStyle: .alert)
                let okActtion = UIAlertAction(title: "確定", style: .default) { (UIAlertAction) in
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "loginNv")
                    self.present(vc, animated: false, completion: nil)
                }
                alertController.addAction(okActtion)
                self.present(alertController, animated: true, completion: nil)
                return
            }
            if error != nil{
                self.present(AlertViewModel.shared.showAlert(message: "請確認網路狀態"), animated: true, completion: nil)
                return
            }
            guard !res else{
                self.present(AlertViewModel.shared.showAlert(message: "此帳號已申請過"), animated: true, completion: nil)
                
                return
            }
            self.registerModel.email = acc
            self.registerModel.pw = pw
            self.performSegue(withIdentifier: "goRegisterDetail", sender: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goRegisterDetail"{
            let registerDetailVC = segue.destination as! RegisterDetailViewController
            registerDetailVC.registerModel = self.registerModel
        }
    }
    
    
    var textFieldY:CGFloat = 0
    var isKeyboardShown = false
    
    @IBAction func textFieldDidBegin(_ sender: UITextField) {
        let relativePositionView = UIApplication.shared.keyWindow
        let relativeFrame = sender.superview?.convert(sender.frame, to: relativePositionView)
        let yPosition = relativeFrame?.minY;
        textFieldY = yPosition! + sender.frame.height * 3
        
    }
    
    @objc func keyboardWillShow(note: NSNotification) {
        if isKeyboardShown {
            return
        }
        let keyboardAnimationDetail = note.userInfo as! [String: AnyObject]
        let duration = TimeInterval(truncating: keyboardAnimationDetail[UIResponder.keyboardAnimationDurationUserInfoKey]! as! NSNumber)
        let keyboardFrameValue = keyboardAnimationDetail[UIResponder.keyboardFrameBeginUserInfoKey]! as! NSValue
        let keyboardFrame = keyboardFrameValue.cgRectValue
        
        if self.view.frame.height - keyboardFrame.size.height > textFieldY{
            return
        }
        
        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: -(keyboardFrame.size.height - (self.view.frame.height - self.textFieldY)))
        })
        isKeyboardShown = true
    }
    
    @objc func keyboardWillHide(note: NSNotification) {
        let keyboardAnimationDetail = note.userInfo as! [String: AnyObject]
        let duration = TimeInterval(truncating: keyboardAnimationDetail[UIResponder.keyboardAnimationDurationUserInfoKey]! as! NSNumber)
        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy:  -self.view.frame.origin.y)
        })
        isKeyboardShown = false
    }
    
    
}
