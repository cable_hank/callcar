//
//  ViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/7.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var btn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.backgroundColor = .clear
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        CallCarAPI.shared.testAPI(parameters: [:]) { (success, res, error, mss) in
            if !success{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "MaintainVC")
                self.present(vc, animated: false, completion: nil)
            }
        }
        UserDefaults.standard.setValue("0", forKey: UserInfo.statusColor.rawValue)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

