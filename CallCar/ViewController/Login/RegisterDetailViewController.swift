//
//  RegisterDetailViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/17.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit
import Firebase

class RegisterDetailViewController: UIViewController {
    
    @IBOutlet weak var lNameTextField: LeftIconTextField!
    @IBOutlet weak var fNameTextField: LeftIconTextField!
    @IBOutlet weak var mobileTextField: LeftIconTextField!
    @IBOutlet weak var agreeButton: UIButton!
    
    var registerModel:RegisterModel = RegisterModel.shard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let backButton = UIBarButtonItem()
        backButton.title = " "
        backButton.tintColor = .black
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
       
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print(registerModel.email)
    }
    
    @IBAction func validateIntro(_ sender: UITextField) {
        
    }
    
    @IBAction func tapServiceButton(_ sender: UIButton) {
        let url = URL(string: "http://www.callcar.com.tw/#service-faq")!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func tapPrivacyButton(_ sender: UIButton) {
        let url = URL(string: "https://www.callcar.com.tw/policy.html")!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func selectButton(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func registerAndLogin(_ sender: UIButton) {
        guard agreeButton.isSelected else{
            self.present(AlertViewModel.shared.showAlert(message: "若要加入會員請同意服務條款"), animated: true, completion: nil)
            return
        }
        
        if mobileTextField.text! != ""{
            guard mobileTextField.text!.count == 10 && mobileTextField.text!.prefix(2) == "09"else{
                self.present(AlertViewModel.shared.showAlert(message: "請輸入手機正確格式"), animated: true, completion: nil)
                return
            }
        }
        let lName = lNameTextField.text!
        let fName = fNameTextField.text!
        let mobile = mobileTextField.text!
        
        
        registerModel.lname = lName
        registerModel.fname = fName
        registerModel.mobile = mobile
        registerModel.intro = "0"
        
        CallCarAPI.shared.register(parameters: registerModel.toArray()) { (res,error,mss) in
            if self.isApiError(error: error){
                let alertController = UIAlertController(title: "提示",message: "連線失敗",preferredStyle: .alert)
                let okActtion = UIAlertAction(title: "確定", style: .default) { (UIAlertAction) in
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "loginNv")
                    self.present(vc, animated: false, completion: nil)
                }
                alertController.addAction(okActtion)
                self.present(alertController, animated: true, completion: nil)
                return
            }
            if error != nil{
                self.present(AlertViewModel.shared.showAlert(message: "請確認網路狀態"), animated: true, completion: nil)
                return
            }
            guard res else{
                self.present(AlertViewModel.shared.showAlert(message: mss), animated: true, completion: nil)
                return
            }
            CallCarAPI.shared.checkAccount(parameters: ["acc":self.registerModel.email], success: { (success, res,error) in
                guard error == nil else{
                    self.present(AlertViewModel.shared.showAlert(message: "請確認網路狀態"), animated: true, completion: nil)
                    return
                }
                UserDefaults.standard.setValue(res["mobile"].stringValue, forKey: UserInfo.mobile.rawValue)
                UserDefaults.standard.setValue(res["uid"].stringValue, forKey: UserInfo.id.rawValue)
                UserDefaults.standard.setValue(res["lname"].stringValue, forKey: UserInfo.lname.rawValue)
                UserDefaults.standard.setValue(res["fname"].stringValue, forKey: UserInfo.fname.rawValue)
                UserDefaults.standard.setValue(self.registerModel.email, forKey: UserInfo.account.rawValue)
                UserDefaults.standard.setValue(self.registerModel.pw, forKey: UserInfo.pw.rawValue)
                CallCarAPI.shared.getMission(parameters: ["uid":res["uid"].stringValue]) { (success, res, error,errorString,sCode) in
//                    if sCode >= 900{
//                        self.goRoot()
//                        return
//                    }
                    if success{
                        print(res["onetime"]["certCard"].stringValue)
                        UserDefaults.standard.set(res["onetime"]["certCard"].stringValue, forKey: UserInfo.missionCard.rawValue)
                        UserDefaults.standard.set(res["onetime"]["newOrder"].stringValue, forKey: UserInfo.missionOrder.rawValue)
                        if res["onetime"]["certCard"].stringValue == "1" && res["onetime"]["newOrder"].stringValue == "1"{
                            UserDefaults.standard.set(true, forKey: UserInfo.unlockAlert.rawValue)
                        }else{
                            UserDefaults.standard.set(false, forKey: UserInfo.unlockAlert.rawValue)
                        }
                    }
                    
                    
                    let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                    let vc = UIStoryboard(name: "TabBar", bundle: nil).instantiateInitialViewController()
                    AnalyticsConfiguration.shared().setAnalyticsCollectionEnabled(true)
                    appDelegate.window?.rootViewController = vc
                    appDelegate.window!.makeKeyAndVisible()
                }
            })
        }
    }
    
    var textFieldY:CGFloat = 0
    var isKeyboardShown = false
    
    @IBAction func textFieldDidBegin(_ sender: UITextField) {
        let relativePositionView = UIApplication.shared.keyWindow
        let relativeFrame = sender.superview?.convert(sender.frame, to: relativePositionView)
        let yPosition = relativeFrame?.minY;
        textFieldY = yPosition! + sender.frame.height * 3
        
    }
    
    @objc func keyboardWillShow(note: NSNotification) {
        if isKeyboardShown {
            return
        }
        let keyboardAnimationDetail = note.userInfo as! [String: AnyObject]
        let duration = TimeInterval(truncating: keyboardAnimationDetail[UIResponder.keyboardAnimationDurationUserInfoKey]! as! NSNumber)
        let keyboardFrameValue = keyboardAnimationDetail[UIResponder.keyboardFrameBeginUserInfoKey]! as! NSValue
        let keyboardFrame = keyboardFrameValue.cgRectValue
        
        if self.view.frame.height - keyboardFrame.size.height > textFieldY{
            return
        }
        
        UIView.animate(withDuration: duration, animations: { () -> Void in            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: -(keyboardFrame.size.height - (self.view.frame.height - self.textFieldY)))
        })
        isKeyboardShown = true
    }
    
    @objc func keyboardWillHide(note: NSNotification) {
        let keyboardAnimationDetail = note.userInfo as! [String: AnyObject]
        let duration = TimeInterval(truncating: keyboardAnimationDetail[UIResponder.keyboardAnimationDurationUserInfoKey]! as! NSNumber)
        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy:  -self.view.frame.origin.y)
        })
        isKeyboardShown = false
    }
}
