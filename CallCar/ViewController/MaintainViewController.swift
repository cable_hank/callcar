//
//  MaintainViewController.swift
//  CallCar
//
//  Created by Cablesoft on 2018/10/24.
//  Copyright © 2018 Cablesoft. All rights reserved.
//

import UIKit

class MaintainViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var timeMssLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.hidesBackButton = true
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.barTintColor = UIColor(hex: 0xF7B028)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm"
        formatter.timeZone = TimeZone(identifier: "Asia/Taipei")!
        let nowDate = formatter.date(from: formatter.string(from: Date()))!
        CallCarAPI.shared.apiMantainMss(parameters: [:]) { (success, res, error, mss) in
            print(res)
            let array = res["maintain"].arrayValue
            if array.count == 0{
                self.titleLabel.text = "主機臨時維護"
                self.timeLabel.isHidden = true
                self.timeMssLabel.isHidden = true
            }else{
                for maintain in array{
                    if nowDate > formatter.date(from: maintain["begin"].stringValue)!  && nowDate < formatter.date(from: maintain["end"].stringValue)!{
                        self.titleLabel.text = maintain["content"].stringValue
                        self.timeLabel.text = maintain["end"].stringValue
                        self.timeLabel.isHidden = false
                        self.timeMssLabel.isHidden = false
                        return
                    }
                }
                
                self.titleLabel.text = "主機臨時維護"
                self.timeLabel.isHidden = true
                self.timeMssLabel.isHidden = true
            }
        }
    }
    
    @IBAction func back(_ sender: UIButton) {
        sender.isEnabled = false
        sender.backgroundColor = .lightGray
        CallCarAPI.shared.testAPI2(parameters: [:]) { (success, res, error, mss) in
            if success {
                if UserDefaults.standard.string(forKey: UserInfo.id.rawValue) != nil{
                    let storyboard = UIStoryboard(name: "TabBar", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "TabBarVC")
                    self.present(vc, animated: false, completion: nil)
                }else{
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "loginNv")
                    self.present(vc, animated: false, completion: nil)
                }
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5), execute: {
            sender.isEnabled = true
            sender.backgroundColor = UIColor(hex: 0xF7B028)
        })
    }
    
    
    
}
