//
//  PageZoomImgScrollView.swift
//  CallCar
//
//  Created by Cablesoft on 2019/3/13.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import UIKit
import Kingfisher

class PageZoomImgScrollView: UIScrollView {

    var imgUrlArr:[String] = []{
        didSet{
            initView()
        }
    }
    var scrollViewArr:[UIScrollView] = []
    var imgViewArr:[UIImageView] = []
    
    func initView(){
        //記住左邊的viewAnchor
        var iframe = CGRect(x: 0, y: 0, width: 0, height: 0)
        for index in 0..<imgUrlArr.count{
            iframe.origin.x = self.frame.size.width * CGFloat(index)
            iframe.size = self.frame.size
            
            let subScrollView = UIScrollView(frame: iframe)
            subScrollView.tag = index
            subScrollView.minimumZoomScale = 1.0
            subScrollView.maximumZoomScale = 6.0
            self.addSubview(subScrollView)
            let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: subScrollView.frame.size.width, height: subScrollView.frame.size.height))
            imgView.contentMode = .scaleAspectFit
            imgView.kf.setImage(with: URL(string: imgUrlArr[index]))
            subScrollView.addSubview(imgView)
            scrollViewArr.append(subScrollView)
            imgViewArr.append(imgView)
        }
        self.contentSize = CGSize(width: (self.frame.size.width * CGFloat(imgUrlArr.count)), height: self.frame.size.height)
    }
    
    func transition(page:Int){
        //螢幕翻轉時沒延遲的話，會取到翻轉前的frame
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
            //翻轉後長度改變
            self.contentSize = CGSize(width: (self.frame.size.width * CGFloat(self.imgUrlArr.count)), height: self.frame.size.height)
            var frame = self.frame;
            //翻轉至翻轉前頁數
            frame.origin.x = self.frame.width * CGFloat(page)
            frame.origin.y = 0
            self.scrollRectToVisible(frame, animated: true)
        }
        
    }
    
    //取頁數
    func getPage() -> Int {
        return Int(self.contentOffset.x / self.frame.size.width)
    }
    
    //刪除
    func removeSubView(){
        self.subviews.forEach({ $0.removeFromSuperview() })
        var frame = self.frame;
        frame.origin.x = 0
        frame.origin.y = 0
        self.scrollRectToVisible(frame, animated: false)
    }
    
    override func draw(_ rect: CGRect) {
        //scorllView設為頁面模式
        self.isPagingEnabled = true
    }

}
