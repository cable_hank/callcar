//
//  PageScrollView.swift
//  BaseProject
//
//  Created by Cablesoft on 2018/12/3.
//  Copyright © 2018 hank. All rights reserved.
//

import UIKit
import Kingfisher
class PageScrollView: UIScrollView {
    
    var imgFrame:CGSize?
    var imgUrlArr:[String] = []{
        didSet{
            initView()
        }
    }
    
    func initView(){
        //記住左邊的viewAnchor
        var iframe = CGRect(x: 0, y: 0, width: 0, height: 0)
        for index in 0..<imgUrlArr.count{
            if let imgFrame = imgFrame{
                print(iframe)
                iframe.origin.x = imgFrame.width * CGFloat(index)
                iframe.size = imgFrame
            }else{
                iframe.origin.x = UIScreen.main.bounds.width * CGFloat(index)
                iframe.size = CGSize(width: UIScreen.main.bounds.width, height: 150)
            }
            let imgView = UIImageView(frame: iframe)
            imgView.contentMode = .scaleAspectFit
            imgView.kf.setImage(with: URL(string: imgUrlArr[index]))
            self.addSubview(imgView)
        }
        if let imgFrame = imgFrame{
            self.contentSize = CGSize(width: (imgFrame.width * CGFloat(imgUrlArr.count)), height: imgFrame.height)
        }else{
            self.contentSize = CGSize(width: (UIScreen.main.bounds.width * CGFloat(imgUrlArr.count)), height: self.frame.size.height)
        }
    }
    
    func transition(page:Int){
        //螢幕翻轉時沒延遲的話，會取到翻轉前的frame
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
            //翻轉後長度改變
            self.contentSize = CGSize(width: (self.frame.size.width * CGFloat(self.imgUrlArr.count)), height: self.frame.size.height)
            var frame = self.frame;
            //翻轉至翻轉前頁數
            frame.origin.x = self.frame.width * CGFloat(page)
            frame.origin.y = 0
            self.scrollRectToVisible(frame, animated: true)
        }
        
    }
    
    //取頁數
    func getPage() -> Int {
        if let imgFrame = imgFrame{
            return Int(self.contentOffset.x / imgFrame.width)
        }else{
            return Int(self.contentOffset.x / UIScreen.main.bounds.width)
        }
    }
    
    //刪除
    func removeSubView(){
        self.subviews.forEach({ $0.removeFromSuperview() })
        var frame = self.frame;
        frame.origin.x = 0
        frame.origin.y = 0
        self.scrollRectToVisible(frame, animated: false)
    }
    
    override func draw(_ rect: CGRect) {
        //scorllView設為頁面模式
        self.isPagingEnabled = true
    }

    
    
}
