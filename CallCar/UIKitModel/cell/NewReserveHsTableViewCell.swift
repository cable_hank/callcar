//
//  NewReserveHsTableViewCell.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/30.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit

class NewReserveHsTableViewCell: UITableViewCell {

    @IBOutlet weak var tdateLabel: UILabel!
    @IBOutlet weak var ttimeLabel: UILabel!
    @IBOutlet weak var paddressLabel: UILabel!
    @IBOutlet weak var taddressLabel: UILabel!
    @IBOutlet weak var typeView: CornerView!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var moreButton: BorderCornerButton!
    @IBOutlet weak var cancelButton: BorderCornerButton!
    @IBOutlet weak var buttonStackView: UIStackView!
    
    @IBOutlet weak var vanTitle: UILabel!
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var rnoView: UIView!
    @IBOutlet weak var rnoLabel: UILabel!
    @IBOutlet weak var BeanCarButton: BorderCornerButton!
    
    
    var rno = ""
    var isShare = true
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
