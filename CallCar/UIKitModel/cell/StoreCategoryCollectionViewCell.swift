//
//  StoreCategoryCollectionViewCell.swift
//  CallCar
//
//  Created by Cablesoft on 2019/9/17.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import UIKit

class StoreCategoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var categoryTitle: UILabel!
    
    @IBOutlet weak var selectView: ShadowCornerView!
    
}
