//
//  HomeOrderCollectionViewCell.swift
//  CallCar
//
//  Created by Cablesoft on 2019/11/26.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import UIKit

class HomeOrderCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleImg: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var addr1: UILabel!
    @IBOutlet weak var addr2: UILabel!
    
}
