//
//  ReserveHsTableViewCell.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/31.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit

class ReserveHsTableViewCell: UITableViewCell {
    @IBOutlet weak var tdateLabel: UILabel!
    @IBOutlet weak var ttimeLabel: UILabel!
    @IBOutlet weak var paddressLabel: UILabel!
    @IBOutlet weak var taddressLabel: UILabel!
    @IBOutlet weak var typeView: CornerView!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var moreButton: BorderCornerButton!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var cancelLabel: UILabel!
    
    @IBOutlet weak var vanTitle: UILabel!
    
    @IBOutlet weak var rnoView: UIView!
    @IBOutlet weak var rnoLabel: UILabel!
    
    
    var rno = ""
    var isShare = true
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
