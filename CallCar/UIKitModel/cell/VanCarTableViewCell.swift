//
//  VanCarTableViewCell.swift
//  CallCar
//
//  Created by Cablesoft on 2019/6/13.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import UIKit

class VanCarTableViewCell: UITableViewCell {

    @IBOutlet weak var vanCornerView: CornerView!
    @IBOutlet weak var topLineView: UIView!
    @IBOutlet weak var bottomLineView: UIView!
    
    @IBOutlet weak var pCount: UILabel!
    @IBOutlet weak var bCount: UILabel!
    @IBOutlet weak var addr: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
