//
//  AddrFetcherTableViewCell.swift
//  CallCar
//
//  Created by Cablesoft on 2019/11/4.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import UIKit

class AddrFetcherTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
