//
//  CarTimeTableViewCell.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/22.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit
class CarTimeTableViewCell: UITableViewCell {

    @IBOutlet weak var carImg: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var radioButton: UIButton!
    @IBOutlet weak var carWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var cornerBackgroundView: CornerView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
