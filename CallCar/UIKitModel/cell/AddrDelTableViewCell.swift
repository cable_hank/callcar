//
//  AddrDelTableViewCell.swift
//  CallCar
//
//  Created by Cablesoft on 2018/6/4.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit

class AddrDelTableViewCell: UITableViewCell {

    @IBOutlet weak var delButton: UIButton!
    @IBOutlet weak var addrLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
