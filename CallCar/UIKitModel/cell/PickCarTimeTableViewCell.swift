//
//  PickCarTimeTableViewCell.swift
//  CallCar
//
//  Created by Cablesoft on 2019/6/10.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import UIKit

class PickCarTimeTableViewCell: UITableViewCell {

    @IBOutlet weak var radioButton: UIButton!
    
    @IBOutlet weak var carImg: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var carSeat: UILabel!
    @IBOutlet weak var personCount: UILabel!
    
    @IBOutlet weak var bagCount: UILabel!
    @IBOutlet weak var stopPlace: UILabel!
    @IBOutlet weak var cornerView: CornerView!
    @IBOutlet weak var shareCode: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
