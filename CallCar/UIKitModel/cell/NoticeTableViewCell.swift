//
//  NoticeTableViewCell.swift
//  CallCar
//
//  Created by Cablesoft on 2018/12/19.
//  Copyright © 2018 Cablesoft. All rights reserved.
//

import UIKit

class NoticeTableViewCell: UITableViewCell {
    @IBOutlet weak var icon: UIImageView!
    
    @IBOutlet weak var title: UILabel!
    
    
    @IBOutlet weak var date: UILabel!
    
    @IBOutlet weak var body: UITextView!
    
    @IBOutlet weak var bgView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setCell() {
        
    }
}
