//
//  BonusTableViewCell.swift
//  CallCar
//
//  Created by Cablesoft on 2018/8/1.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit

class BonusTableViewCell: UITableViewCell {

    @IBOutlet weak var tiltle1Label: UILabel!
    
    @IBOutlet weak var title2Label: UILabel!
    
    @IBOutlet weak var point: UILabel!
    
    @IBOutlet weak var date: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
