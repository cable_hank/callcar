//
//  BorderButton.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/11.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit

class BorderButton: UIButton {
    
    @IBInspectable var bgColor: UIColor = UIColor.white{
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.lightGray {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 5 {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var isShadaw: Bool = false {
        didSet {
            updateView()
        }
    }
    
    
    override func draw(_ rect: CGRect) {
        self.layer.borderWidth = 1
//        self.layer.masksToBounds = true
    }
    
    func updateView(){
        self.layer.cornerRadius = cornerRadius
        self.layer.borderColor = borderColor.cgColor
        if isShadaw{
            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowRadius = 2
            self.layer.shadowOffset = CGSize(width: 2, height: 2)
            self.layer.shadowOpacity = 0.3
            self.layer.backgroundColor = bgColor.cgColor
        }
        self.layer.masksToBounds = true
    }

}
