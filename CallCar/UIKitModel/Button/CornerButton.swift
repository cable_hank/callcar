//
//  CornerButton.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/8.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit

class CornerButton: UIButton {
    override func draw(_ rect: CGRect) {
        self.layer.cornerRadius = self.frame.height/2
        self.layer.masksToBounds = true
    }
}
