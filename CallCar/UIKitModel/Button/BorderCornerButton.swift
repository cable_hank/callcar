//
//  BorderCornerButton.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/8.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit

class BorderCornerButton: UIButton {
    
    @IBInspectable var borderColor: UIColor = UIColor.lightGray {
        didSet {
            updateView()
        }
    }
    
    
    override func draw(_ rect: CGRect) {
        self.layer.borderWidth = 1
        self.layer.cornerRadius = self.frame.height/2
        self.layer.masksToBounds = true
    }
    
    func updateView(){
        self.layer.borderColor = borderColor.cgColor
    }
}
