//
//  CornerView.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/10.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit

class CornerView: UIView {

    @IBInspectable var borderColor: UIColor = UIColor.lightGray {
        didSet {
            updateView()
        }
    }
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            updateView()
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            updateView()
        }
    }
    override func draw(_ rect: CGRect) {
    }
    func updateView(){
        self.layer.borderColor = borderColor.cgColor
        self.layer.cornerRadius = cornerRadius
        self.layer.borderWidth = borderWidth
        self.layer.masksToBounds = true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.endEditing(true)
    }

}
