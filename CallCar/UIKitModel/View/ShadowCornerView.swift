//
//  ShadowCornerView.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/11.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit

class ShadowCornerView: UIView {
    override func draw(_ rect: CGRect) {
        self.layer.shadowOffset = CGSize(width: 2, height: 2)
        self.layer.shadowOpacity = 0.3
        self.layer.shadowRadius = 2
        self.layer.shadowColor = UIColor.lightGray.cgColor
    }

}
