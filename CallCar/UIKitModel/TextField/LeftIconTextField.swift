//
//  LeftIconTextField.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/8.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit

class LeftIconTextField: UITextField{
    let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
    var isError = false
    var borderColor = UIColor(hex: 0xCCCCCC).cgColor
    var focusBorderColor = UIColor(hex: 0x20B6FC).cgColor
    var errorBorderColor = UIColor(hex: 0xF97B62).cgColor
    
    @IBInspectable var leftImage: UIImage? {
        didSet {
            initView()
        }
    }
    @IBInspectable var errorImage: UIImage?
    
    @IBInspectable var focusImage: UIImage?
    
    @IBInspectable var leftPadding: CGFloat = 0
   
    @IBInspectable var fColor: UIColor = UIColor(hex: 0x20B6FC) {
       didSet {
          initView()
       }
    }
   
    @IBInspectable var isWhite: Bool = false{
       didSet {
          initView()
       }
    }
   
    @IBInspectable var color: UIColor = UIColor(hex: 0xCCCCCC) {
        didSet {
            initView()
        }
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(UIResponderStandardEditActions.paste(_:)) || action == #selector(UIResponderStandardEditActions.cut(_:)) || action == #selector(UIResponderStandardEditActions.delete(_:)){
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }
    
    override func draw(_ layer: CALayer, in ctx: CGContext) {
        self.layer.cornerRadius = self.frame.height/2
        self.layer.borderWidth = 1
//        self.layer.borderColor = UIColor(hex: 0xCCCCCC).cgColor
        self.layer.masksToBounds = true
    }
    // Provides left padding for images
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.leftViewRect(forBounds: bounds)
        textRect.origin.x += leftPadding
        return textRect
    }
    
    
   func initView() {
      if isWhite{
         backgroundColor = UIColor.white
      }else{
         backgroundColor = UIColor(hex: 0xF7F7F7)
      }
      borderColor = backgroundColor?.cgColor ?? UIColor.white.cgColor
      self.layer.borderColor = borderColor
      //如果border顏色放到draw，textfield有字的時候邊框顏色會被這個顏色蓋掉
      updateView()
        
      // Placeholder text color
      attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: color])
   }
    
    func errorFunc(isError:Bool) {
        if isError {
            self.isError = true
            self.layer.borderColor = errorBorderColor
        }else{
            self.isError = false
            self.layer.borderColor = focusBorderColor
        }
        updateView()
    }
    
    func updateView(){
        focusBorderColor = fColor.cgColor
        if let image = leftImage {
            leftViewMode = UITextField.ViewMode.always
            imageView.contentMode = .scaleAspectFit
            if isError {
                imageView.image = errorImage
            }else{
                if isEditing{
                    imageView.image = focusImage
                }else{
                    imageView.image = image
                }
            }
            imageView.tintColor = color
            leftView = imageView
        } else {
            leftViewMode = UITextField.ViewMode.never
            leftView = nil
        }
    }
    
    override func becomeFirstResponder() -> Bool {
        let result = super.becomeFirstResponder()
        updateView()
        if isError{
            self.layer.borderColor = errorBorderColor
        }else{
            self.layer.borderColor = focusBorderColor
        }
        return result
    }
    
    
    
    override func resignFirstResponder() -> Bool {
        let result =  super.resignFirstResponder()
        updateView()
        if isError{
            self.layer.borderColor = errorBorderColor
        }else{
            self.layer.borderColor = borderColor
        }
        return result
    }
    
    func isValidEmail() -> Bool {
        let testStr = self.text
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}

