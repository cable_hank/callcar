//
//  CornerTextField.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/10.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit

class CornerTextField: UITextField {
    @IBInspectable var borderColor: UIColor = UIColor.lightGray {
        didSet {
            updateView()
        }
    }
    override func draw(_ rect: CGRect) {
        self.layer.cornerRadius = self.frame.height/2
        self.layer.borderWidth = 1
        self.layer.masksToBounds = true
    }
    func updateView(){
        self.layer.borderColor = borderColor.cgColor
    }
}
