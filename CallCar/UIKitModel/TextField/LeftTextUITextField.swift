//
//  LeftTextUITextField.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/7.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit

@IBDesignable
class LeftTextUITextField: UITextField {
    
    override func draw(_ layer: CALayer, in ctx: CGContext) {
        self.layer.cornerRadius = self.frame.height/2
        self.layer.borderWidth = 1
        self.layer.masksToBounds = true
    }
    
    // Provides left padding for images
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.leftViewRect(forBounds: bounds)
        textRect.origin.x += leftPadding
        return textRect
    }
    
    @IBInspectable var leftText: String? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var leftPadding: CGFloat = 0
    
    @IBInspectable var color: UIColor = UIColor.lightGray {
        didSet {
            updateView()
        }
    }
    
    func updateView() {
        if let bgc = backgroundColor{
            self.layer.borderColor = bgc.cgColor
        }else{
            self.layer.borderColor = UIColor(hex: 0xCCCCCC).cgColor
        } //如果border顏色放到draw，textfield有字的時候邊框顏色會被這個顏色蓋掉
        if let text = leftText {
            leftViewMode = UITextField.ViewMode.always
            
            let view = UIView(frame: CGRect(x: 0, y: 0, width: 35, height: 20))
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: 35, height: 20))
            view.contentMode = .scaleAspectFit
            label.contentMode = .scaleAspectFit
            label.text = text
            label.textColor = color
            label.font = UIFont.systemFont(ofSize: 14)
            view.addSubview(label)
            
            leftView = view
        } else {
            leftViewMode = UITextField.ViewMode.never
            leftView = nil
        }
        
        // Placeholder text color
//        attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSAttributedStringKey.foregroundColor: color])
    }
}

