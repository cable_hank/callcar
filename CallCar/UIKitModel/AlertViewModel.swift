//
//  AlertViewModel.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/17.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit

class AlertViewModel {
    static let shared : AlertViewModel = AlertViewModel()
    fileprivate let appdelegate = UIApplication.shared.delegate as? AppDelegate
    private init(){}
    
    func show(message : String) {
        let alert = UIAlertController(title: "提示", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "確定", style: UIAlertAction.Style.default, handler: nil))
        self.present(view: alert)
    }
    func show(alert : UIAlertController) {
        self.present(view: alert)
    }
    
    func showAlert(message:String) -> UIAlertController {
        let alert = UIAlertController(title: "提示", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "確定", style: UIAlertAction.Style.default, handler: nil))
        return alert
    }
    fileprivate func present<T:UIViewController>(view : T) {
        self.appdelegate?.window?.rootViewController?.present(view, animated: true, completion: nil)
    }
}
