//
//  AppDelegate.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/7.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import SwiftMessages
import RealmSwift
import UserNotifications
import Firebase


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate{

    var window: UIWindow?
    var date = ""
    var date2:Date?
    let formatter = DateFormatter()
    var messageTimer:Timer?
    func checkInternetFunction() -> Bool {
        let reachability = Reachability(hostName: "www.google.com.tw")
        if reachability?.currentReachabilityStatus().rawValue == 0 {
            return false
        }else {
            return true
        }
    }
    
    
    /// Show always scroll indicator in table view
    @objc func showInterNetMessage() {
//        print("showInterNetMessage")
        if !checkInternetFunction(){
            UIApplication.shared.statusBarStyle = .lightContent
            let view = MessageView.viewFromNib(layout: .statusLine)
            let iconStyle: IconStyle = .default
            view.configureTheme(.error, iconStyle: iconStyle)
            view.accessibilityPrefix = "error"
            view.configureContent(title: "錯誤", body: "目前無網路服務")
            var config = SwiftMessages.defaultConfig
            config.presentationContext = .window(windowLevel: UIWindow.Level(rawValue: UIWindow.Level.normal.rawValue))
            config.duration = .forever
            SwiftMessages.show(config: config, view: view)
        }else{
            if UserDefaults.standard.string(forKey: UserInfo.statusColor.rawValue)! == "0"{
                if #available(iOS 13.0, *) {
                    UIApplication.shared.statusBarStyle = .darkContent
                } else {
                    UIApplication.shared.statusBarStyle = .default
                }
            }else{
                UIApplication.shared.statusBarStyle = .lightContent
            }
            SwiftMessages.hide()
        }
    }
    
    @objc func hideInterNetMessage() {
        SwiftMessages.hide()
    }
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        CornerButton.methodExchange()
        BorderButton.methodExchange()
        BorderCornerButton.methodExchange()
        
        FirebaseApp.configure()
        AnalyticsConfiguration.shared().setAnalyticsCollectionEnabled(false)
        Messaging.messaging().delegate = self
        
        GMSPlacesClient.provideAPIKey("AIzaSyCmDSfdCv8XurR2G-ECzynxA_FgtdWFw60") //Google Place API Key
        GMSServices.provideAPIKey("AIzaSyCmDSfdCv8XurR2G-ECzynxA_FgtdWFw60") //Google Map API Key
        self.window?.backgroundColor = .white
        if UserDefaultModel.shared.isExist(key: "account") && UserDefaultModel.shared.isExist(key: "pw") {
            CallCarAPI.shared.loadMission(parameters: ["uid":UserDefaults.standard.string(forKey: UserInfo.id.rawValue)!])
            
            let storyboard = UIStoryboard(name: "TabBar", bundle: nil)
            let rootVC = storyboard.instantiateViewController(withIdentifier: "TabBarVC")
            self.window?.rootViewController = rootVC
            self.window?.makeKeyAndVisible()
            
        }
        // Inside your application(application:didFinishLaunchingWithOptions:)
        
        let config = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            // version (if you've never set a schema version before, the version is 0).
            schemaVersion: 19,
            
            // Set the block which will be called automatically when opening a Realm with
            // a schema version lower than the one set above
            migrationBlock: { migration, oldSchemaVersion in
                print("oldSchemaVersion:\(oldSchemaVersion)")
                // We haven’t migrated anything yet, so oldSchemaVersion == 0
                if (oldSchemaVersion < 1) {
                    // Nothing to do!
                    // Realm will automatically detect new properties and removed properties
                    // And will update the schema on disk automatically
                }
        })
        
        // Tell Realm to use this new configuration object for the default Realm
        Realm.Configuration.defaultConfiguration = config
        
        // Now that we've told Realm how to handle the schema change, opening the file
        // will automatically perform the migration
        let realm = try! Realm()
        print("fileURL: \(realm.configuration.fileURL!)")
        // 在程式一啟動即詢問使用者是否接受圖文(alert)、聲音(sound)、數字(badge)三種類型的通知
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.sound,.badge, .carPlay], completionHandler: { (granted, error) in
                if granted {
                    print("允許")
                } else {
                    print("不允許")
                }
            })
        } else {
            let type: UIUserNotificationType = [.badge, .alert, .sound]
            let setting = UIUserNotificationSettings(types: type, categories: nil)
            UIApplication.shared.registerUserNotificationSettings(setting)
        }
        application.registerForRemoteNotifications()
        
        return true
    }

    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        formatter.dateFormat = "yyyyMMdd"
        date = formatter.string(from: Date())
        date2 = Date()
        messageTimer?.invalidate()
        messageTimer = nil
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        CallCarAPI.shared.getVersion(parameters: [:]) { (success, ver, error, mss) in
            if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
                if ver != ""{
                    let appversion = Int(version.replacingOccurrences(of: ".", with: ""))!
                    let minversion = Int(ver.replacingOccurrences(of: ".", with: ""))!
                    print("now version \(appversion)")
                    print("min version \(minversion)")
                    if appversion < minversion{
                        let alertController = UIAlertController(title: "提示",message: "請更新至最新版本",preferredStyle: .alert)
                        let okActtion = UIAlertAction(title: "確定", style: .default) { (UIAlertAction) in
                            
                            let url = URL(string: "https://itunes.apple.com/tw/app/%E6%A9%9F%E5%A0%B4%E6%8E%A5%E9%A7%81/id1229543491?l=zh&mt=8")!
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(url)
                            } else {
                                UIApplication.shared.openURL(url)
                            }
                        }
                        alertController.addAction(okActtion)
                        self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
                    }
                }
                return
            }
        }
        UIApplication.shared.applicationIconBadgeNumber = 0
        print(date)
        if date2 != nil{
            date2 = Calendar.current.date(byAdding: Calendar.Component.hour, value: 1, to: date2!)
            print("oldDate:\(date2!)")
            print("newDate:\(Date())")
            if date2! < Date(){
                if UserDefaultModel.shared.isExist(key: "account") { AnalyticsConfiguration.shared().setAnalyticsCollectionEnabled(true)
                    let storyboard = UIStoryboard(name: "TabBar", bundle: nil)
                    let rootVC = storyboard.instantiateViewController(withIdentifier: "tabBarLaunch")
                    self.window?.rootViewController = rootVC
                }else{
                    AnalyticsConfiguration.shared().setAnalyticsCollectionEnabled(false)
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let rootVC = storyboard.instantiateViewController(withIdentifier: "loginNv")
                    self.window?.rootViewController = rootVC
                }
            }
        }
        date2 = nil
        
        messageTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.showInterNetMessage), userInfo: nil, repeats: true)
        
//        CallCarAPI.shared.testAPI(parameters: [:]) { (success, res, error, mss) in
//            if success{
//                let formatter = DateFormatter()
//                formatter.dateFormat = "yyyy-MM-dd HH:mm"
//                formatter.timeZone = TimeZone(identifier: "Asia/Taipei")!
//                print("date:\(formatter.string(from: Date()))")
//                let nowDate = formatter.date(from: formatter.string(from: Date()))!
//                if res["maintain"].arrayValue.count == 0{
//                    UserDefaults.standard.setValue(nil, forKeyPath: UserInfo.maintain.rawValue)
//                }else{
//                    let data = try! res.rawData()
//                    UserDefaults.standard.setValue(data, forKeyPath: UserInfo.maintain.rawValue)
//                }
//                for maintain in res["maintain"].arrayValue{
//                    print(formatter.date(from: maintain["begin"].stringValue)! )
//                    print(nowDate)
//                    print(formatter.date(from: maintain["end"].stringValue)! )
//                    if nowDate > formatter.date(from: maintain["begin"].stringValue)!  && nowDate < formatter.date(from: maintain["end"].stringValue)!{
//                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                        let vc = storyboard.instantiateViewController(withIdentifier: "MaintainVC")
//                        self.window?.rootViewController?.show(vc, sender: nil)
//                        return
//                    }
//                }
//            }
//        }

        
        
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    //進入讀取畫面
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        if let uid = UserDefaults.standard.string(forKey: UserInfo.id.rawValue){
            print("uid:\(uid)")
            CallCarAPI.shared.getForder(parameters: ["uid":uid]) { (success, res, error,sCode) in
                if success{
                    print("getForder success")
                    RealmDB.shared.saveOrder(json: res)
                }
            }
            CallCarAPI.shared.getPorder(parameters: ["uid":uid]) { (success, res1, error,mss) in
                if success{
                    print("getPorder success")
                    RealmDB.shared.saveHsOrder(json: res1)
                }
            }
            Thread.sleep(forTimeInterval: 3.0)
        }
        return false
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.badge, .sound, .alert])
        print("在前景收到通知...")
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        let planId = userInfo[AnyHashable("img")] as? String
        print("userInfo: \(userInfo)")
        print("planId: \(planId)")
        completionHandler()
        print("點擊通知觸發的事件")
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        let tokenParts = deviceToken.map { data -> String in
//            return String(format: "%02.2hhx", data)
//        }
//        let token = tokenParts.joined()
//        print(token)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
    }
    
}

extension AppDelegate:MessagingDelegate{
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("didReceiveRegistrationToken")
        print(fcmToken)
    }
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("didReceive")
    }
    
    
    
}
