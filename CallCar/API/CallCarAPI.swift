//
//  CallCarAPI.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/17.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit
import SwiftyJSON
class CallCarAPI : NSObject {
    static let shared : CallCarAPI = CallCarAPI()
    public func checkAccount( parameters : [String : String] , success : @escaping ( (Bool,Error?) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.checkAccount.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    
                    success(false,err)
                }
                return
            }
            let json = JSON(res)
            print(json)
            success(json["RtnObject"].stringValue != "",nil)
        }
        
    }
    public func checkAccount( parameters : [String : String] , success : @escaping ( (Bool,JSON,Error?) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.checkAccount.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    
                    success(false,"",err)
                }
                return
            }
            let json = JSON(res)
            let rtnObject = JSON.init(parseJSON: json["RtnObject"].stringValue)
//            print(json)
            success(json["RtnObject"].stringValue != "",rtnObject,nil)
        }
        
    }
    
    public func register( parameters : [String : String] , success : @escaping ( (Bool,Error?,String) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.GenAccAPPN.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,err,"")
                }
                return
            }
            let json = JSON(res)
//            print(json)
            success(json["Status"].intValue == 200,nil,json["DisplayMsg"].stringValue)
        }
        
    }
    
    public func login( parameters : [String : String] , success : @escaping ( (Bool,Error?,String) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.validateAccount.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,err,"")
                }
                return
            }
            let json = JSON(res)
            print(json)
            success(json["RtnObject"].stringValue == "True",nil,json["DisplayMsg"].stringValue)
        }
    }
    
    public func checkService(parameters : [String : String] , success : @escaping ( (Bool,JSON,Error?) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.checkService.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",err)
                }
                return
            }
            let json = JSON(res)
            print(json)
            let rtnObject = JSON.init(parseJSON: json["RtnObject"].stringValue)
            success(json["Status"].intValue == 200,rtnObject,nil)
        }
    }
    public func checkFlight(parameters : [String : String] , success : @escaping ( (Bool,JSON,Int) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.checkFlight.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",500)
                }
                return
            }
            let json = JSON(res)
            print("---------------")
            print(json)
            print("---------------")
            let rtnObject = JSON.init(parseJSON: json["RtnObject"].stringValue)
            success(json["Status"].intValue == 200,rtnObject,json["Status"].intValue)
        }
    }
    
    public func getCarTime(parameters : [String : Any] , success : @escaping ( (Bool,JSON,Error?,String,Int) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.getCarTime.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",err,"",500)
                }
                return
            }
            let json = JSON(res)
            print(json)
            let rtnObject = JSON.init(parseJSON: json["RtnObject"].stringValue)
            success(json["Status"].intValue == 200,rtnObject,nil,json["DisplayMsg"].stringValue,json["Status"].intValue)
        }
    }
    
    public func checkCoupon(parameters : [String : String] , success : @escaping ( (Bool,JSON,Int) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.checkCoupon.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",500)
                }
                return
            }
            let json = JSON(res)
//            print(json)
            let rtnObject = JSON.init(parseJSON: json["RtnObject"].stringValue)
            success(json["RtnObject"].stringValue == "True",rtnObject,json["Status"].intValue)
        }
    }
    
    public func getPrice(parameters : [String : Any] , success : @escaping ( (Bool,JSON,Int) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.getPrice.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",500)
                }
                return
            }
            let json = JSON(res)
//            print(json)
            let rtnObject = JSON.init(parseJSON: json["RtnObject"].stringValue)
            success(json["Status"].intValue == 200,rtnObject,json["Status"].intValue)
        }
    }
    public func getUserInfo(parameters : [String : Any] , success : @escaping ( (Bool,JSON,Int) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.getUserInfo.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",500)
                }
                return
            }
            let json = JSON(res)
//            print(json)
            let rtnObject = JSON.init(parseJSON: json["RtnObject"].stringValue)
            success(json["Status"].intValue == 200,rtnObject,json["Status"].intValue)
        }
    }
    public func addCard(parameters : [String : Any] , success : @escaping ( (Bool,String,Int,String) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.addCard.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",500,"")
                }
                return
            }
            let json = JSON(res)
//            print(json)
            let rtnObject = json["RtnObject"].stringValue
            success(json["Status"].intValue == 200,rtnObject,json["Status"].intValue,json["DisplayMsg"].stringValue)
        }
    }
    public func getVipDate(success : @escaping ( (Bool,JSON,Error?) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.getVipDate.rawValue) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",err)
                }
                return
            }
            let json = JSON(res)
//            print(json)
            let rtnObject = JSON.init(parseJSON: json["RtnObject"].stringValue)
            success(json["Status"].intValue == 200,rtnObject,nil)
        }
    }
    public func addInv(parameters : [String : Any] , success : @escaping ( (Bool,String,Int) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.addInv.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",500)
                }
                return
            }
            let json = JSON(res)
//            print(json)
            let rtnObject = json["RtnObject"].stringValue
            success(json["Status"].intValue == 200,rtnObject,json["Status"].intValue)
        }
    }
    
    public func getInv(parameters : [String : Any] , success : @escaping ( (Bool,JSON,Int) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.getInv.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",500)
                }
                return
            }
            let json = JSON(res)
//            print(json)
            let rtnObject = JSON.init(parseJSON: json["RtnObject"].stringValue)
            guard json["Status"].intValue == 200 else{
                success(false,rtnObject,json["Status"].intValue)
                return
            }
            success(json["RtnObject"].stringValue != "",rtnObject,json["Status"].intValue)
        }
    }
    
    public func getCard(parameters : [String : Any] , success : @escaping ( (Bool,JSON,Int) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.getCard.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",500)
                }
                return
            }
            let json = JSON(res)
//            print(json)
            let rtnObject = JSON.init(parseJSON: json["RtnObject"].stringValue)
            guard json["Status"].intValue == 200 else{
                success(false,rtnObject,json["Status"].intValue)
                return
            }
            success(json["RtnObject"].stringValue != "",rtnObject,json["Status"].intValue)
        }
    }
    
    public func delInv(parameters : [String : Any] , success : @escaping ( (Bool,JSON,Error?,Int) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.delInv.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",err,500)
                }
                return
            }
            let json = JSON(res)
//            print(json)
            let rtnObject = JSON.init(parseJSON: json["RtnObject"].stringValue)
            success(json["Status"].intValue == 200,rtnObject,nil,json["Status"].intValue)
        }
    }
    public func delCard(parameters : [String : Any] , success : @escaping ( (Bool,JSON,Error?,String,Int) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.delCard.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",err,"",500)
                }
                return
            }
            let json = JSON(res)
//            print(json)
            let rtnObject = JSON.init(parseJSON: json["RtnObject"].stringValue)
            success(json["Status"].intValue == 200,rtnObject,nil,json["DisplayMsg"].stringValue,json["Status"].intValue)
        }
    }
    
    public func addOrder(parameters : [String : Any] , success : @escaping ( (Bool,String,Error?,String,Int) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.addOrder.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",err,"",500)
                }
                return
            }
            let json = JSON(res)
            print(json)
            let rtnObject = json["RtnObject"].stringValue
            success(json["Status"].intValue == 200,rtnObject,nil,json["DisplayMsg"].stringValue,json["Status"].intValue)
        }
    }
    
    public func getForder(parameters : [String : Any] , success : @escaping ( (Bool,JSON,Error?,Int) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.getForder.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",err,500)
                }
                return
            }
            let json = JSON(res)
//            print(json)
            let rtnObject = JSON.init(parseJSON: json["RtnObject"].stringValue)
            success(json["RtnObject"].stringValue != "",rtnObject,nil,json["Status"].intValue)
        }
    }
    public func getPorder(parameters : [String : Any] , success : @escaping ( (Bool,JSON,Int,String) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.getPorder.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",500,"")
                }
                return
            }
            
            let json = JSON(res)
//            print(json)
            let rtnObject = JSON.init(parseJSON: json["RtnObject"].stringValue)
            success(json["RtnObject"].stringValue != "",rtnObject,json["Status"].intValue,json["DisplayMsg"].stringValue)
        }
    }
    public func cancelOrder(parameters : [String : Any] , success : @escaping ( (Bool,JSON,Error?,String,Int) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.cancelOrder.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",err,"",500)
                }
                return
            }
            let json = JSON(res)
//            print(json)
            let rtnObject = JSON.init(parseJSON: json["RtnObject"].stringValue)
            success(json["Status"].intValue == 200,rtnObject,nil,json["DisplayMsg"].stringValue,json["Status"].intValue)
        }
    }
    public func forgetPw(parameters : [String : Any] , success : @escaping ( (Bool,JSON,Int,String) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.forgetPw.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",500,"")
                }
                return
            }
            let json = JSON(res)
//            print(json)
            let rtnObject = JSON.init(parseJSON: json["RtnObject"].stringValue)
            success(json["Status"].intValue == 200,rtnObject,json["Status"].intValue,json["DisplayMsg"].stringValue)
        }
    }
    
    public func updatePw(parameters : [String : Any] , success : @escaping ( (Bool,JSON,Error?,String,Int) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.updatePw.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",err,"",500)
                }
                return
            }
            let json = JSON(res)
//            print(json)
            success(json["RtnObject"].stringValue == "True",json,nil,json["DisplayMsg"].stringValue,json["Status"].intValue)
        }
    }
    
    public func updateAccount(parameters : [String : Any] , success : @escaping ( (Bool,JSON,Int,String) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.updateAccount.rawValue, parameters: parameters) { (response, error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",500,"")
                }
                return
            }
            print("updateAccount")
            let json = JSON(res)
//            print(json)
            let rtnObject = JSON.init(parseJSON: json["RtnObject"].stringValue)
            success(json["Status"].intValue == 200,rtnObject,json["Status"].intValue,json["DisplayMsg"].stringValue)
        }
    }
    
    public func certCard(parameters : [String : Any] , success : @escaping ( (Bool,String,Int,String) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.certCard.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",500,"")
                }
                return
            }
            let json = JSON(res)
//            print(json)
            let rtnObject = json["RtnObject"].stringValue
            success(json["Status"].intValue == 200,rtnObject,json["Status"].intValue,json["DisplayMsg"].stringValue)
        }
    }
   
    public func addPickOrder(parameters : [String : Any] , success : @escaping ( (Bool,String,Error?,String,Int) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.addPickOrder.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",err,"",500)
                }
                return
            }
            let json = JSON(res)
            print(json)
            let rtnObject = json["RtnObject"].stringValue
            success(json["Status"].intValue == 200,rtnObject,nil,json["DisplayMsg"].stringValue,json["Status"].intValue)
        }
    }
    public func chkVip(parameters : [String : Any] , success : @escaping ( (Bool,JSON,Error?,String,Int) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.chkVip.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",err,"",500)
                }
                return
            }
            let json = JSON(res)
//            print(json)
            success(json["RtnObject"] == "True",json,nil,json["DisplayMsg"].stringValue,json["Status"].intValue)
        }
    }
    
    public func getVipTime(parameters : [String : Any] , success : @escaping ( (Bool,JSON,Int,String) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.getVipTime.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",500,"")
                }
                return
            }
            let json = JSON(res)
//            print(json)
            let rtnObject = JSON.init(parseJSON: json["RtnObject"].stringValue)
//            print(rtnObject != "")
            success(json["RtnObject"].stringValue != "",rtnObject,json["Status"].intValue,json["DisplayMsg"].stringValue)
        }
    }
    
    public func getAirPort(parameters : [String : Any] , success : @escaping ( (Bool,JSON,Int,String) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.getAirPort.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",500,"")
                }
                return
            }
            let json = JSON(res)
//            print(json)
            let rtnObject = JSON.init(parseJSON: json["RtnObject"].stringValue)
//            print(rtnObject != "")
            success(json["RtnObject"].stringValue != "" && json["Status"].intValue == 200,rtnObject,json["Status"].intValue,json["DisplayMsg"].stringValue)
        }
    }
    
    public func getBonus(parameters : [String : Any] , success : @escaping ( (Bool,JSON,Int,String) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.getBonus.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",500,"無網路連線")
                }
                return
            }
            let json = JSON(res)
//            print(json)
            let rtnObject = JSON.init(parseJSON: json["RtnObject"].stringValue)
//            print(rtnObject != "")
            success(json["RtnObject"].stringValue != "" && json["Status"].intValue == 200,rtnObject,json["Status"].intValue,json["DisplayMsg"].stringValue)
        }
    }
    
    
    public func getMarketing(parameters : [String : Any] , success : @escaping ( (Bool,JSON,Error?,Int) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.getMarketing.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",err,500)
                }
                return
            }
            let json = JSON(res)
//            print(json)
            let rtnObject = JSON.init(parseJSON: json["RtnObject"].stringValue)
//            print(rtnObject != "")
            success(json["RtnObject"].stringValue != "" && json["Status"].intValue == 200,rtnObject,nil,json["Status"].intValue)
        }
    }
    public func getDeposit(parameters : [String : Any] , success : @escaping ( (Bool,JSON,Int,String) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.getDeposit.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",500,"無網路連線")
                }
                return
            }
            let json = JSON(res)
//            print(json)
            let rtnObject = JSON.init(parseJSON: json["RtnObject"].stringValue)
//            print(rtnObject != "")
            success(json["RtnObject"].stringValue != "" && json["Status"].intValue == 200,rtnObject,json["Status"].intValue,json["DisplayMsg"].stringValue)
        }
    }
    public func chkInv(parameters : [String : Any] , success : @escaping ( (Bool,String,Error?,Int) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.checkInv.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",err,500)
                }
                return
            }
            let json = JSON(res)
//            print(json)
            let rtnObject = json["RtnObject"].stringValue
//            print(rtnObject != "")
            success(json["RtnObject"].stringValue == "True" && json["Status"].intValue == 200,rtnObject,nil,json["Status"].intValue)
        }
    }
    public func updateInv(parameters : [String : Any] , success : @escaping ( (Bool,JSON,Error?,String,Int) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.updateInv.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",err,"無網路連線",500)
                }
                return
            }
            let json = JSON(res)
//            print(json)
            let rtnObject = JSON.init(parseJSON: json["RtnObject"].stringValue)
//            print(rtnObject != "")
            success(json["Status"].intValue == 200,rtnObject,nil,json["DisplayMsg"].stringValue,json["Status"].intValue)
        }
    }
    
    public func getMission(parameters : [String : Any] , success : @escaping ( (Bool,JSON,Error?,String,Int) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.getMission.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",err,"無網路連線",500)
                }
                return
            }
            let json = JSON(res)
//            print(json)
            let rtnObject = JSON.init(parseJSON: json["RtnObject"].stringValue)
//            print(rtnObject != "")
            success(json["Status"].intValue == 200,rtnObject,nil,json["DisplayMsg"].stringValue,json["Status"].intValue)
        }
    }
    
    public func loadMission(parameters : [String : Any]) {
        NetworkTools.postRequestData(URLString: APIRoute.loadMission.rawValue, parameters: parameters) { (response,error) in
            print("loadMission")
        }
    }
    
    public func getIntroList(parameters : [String : Any] , success : @escaping ( (Bool,JSON,Error?,String,Int) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.getIntroList.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",err,"無網路連線",500)
                }
                return
            }
            let json = JSON(res)
//            print(json)
            let rtnObject = JSON.init(parseJSON: json["RtnObject"].stringValue)
//            print(rtnObject != "")
            success(json["Status"].intValue == 200,rtnObject,nil,json["DisplayMsg"].stringValue,json["Status"].intValue)
        }
    }
    
    public func testAPI(parameters : [String : Any] , success : @escaping ( (Bool,JSON,Error?,String) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.testAPI.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",err,"無網路連線")
                }
                return
            }
            let json = JSON(res)
            print(json)
            success(json["TestiSharecoAPIResult"].stringValue == "Alive",json,nil,json.stringValue)
        }
    }
    
    public func testAPI2(parameters : [String : Any] , success : @escaping ( (Bool,JSON,Error?,String) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.testAPI2.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",err,"無網路連線")
                }
                return
            }
            let json = JSON(res)
//            print(json)
            success(json["status"].stringValue == "Alive",json,nil,json.stringValue)
        }
    }
    public func apiMantainMss(parameters : [String : Any] , success : @escaping ( (Bool,JSON,Error?,String) -> () ) ) {
        NetworkTools.getRequestData(URLString: APIRoute.apiMantainMss.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",err,"無網路連線")
                }
                return
            }
            let json = JSON(res)
            print("apiMantainMss:\(json)")
            success(true,json,nil,json.stringValue)
        }
    }
    
    public func getVersion(parameters : [String : Any] , success : @escaping ( (Bool,String,Error?,String) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.getVersion.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",err,"無網路連線")
                }
                return
            }
            let json = JSON(res)
            var iosVersion = ""
            for j in json.arrayValue{
                if j["os"].stringValue == "iOS"{
                    iosVersion = j["minver"].stringValue
                }
            }
//            print(json)
            success(true,iosVersion,nil,json.stringValue)
        }
    }
    public func getPromote(parameters : [String : Any] , success : @escaping ( (Bool,String,Error?,String) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.getPromote.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",err,"無網路連線")
                }
                return
            }
            let json = JSON(res)
            
            success(json["RtnObject"].stringValue != "",json["RtnObject"].stringValue,nil,json.stringValue)
        }
    }
    
    public func getPromoteH( success : @escaping ( (Bool,[JSON],Error?,String) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.getPromoteH.rawValue, parameters: [:]) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,[],err,"無網路連線")
                }
                return
            }
            let json = JSON(res)
            let rtnObject = JSON.init(parseJSON: json["RtnObject"].stringValue)
            success(json["RtnObject"].arrayValue.count > 0,rtnObject.arrayValue,nil,json.stringValue)
        }
    }
    public func sendEval(parameters : [String : Any] , success : @escaping ( (Bool,String,Error?,String) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.sendEval.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",err,"無網路連線")
                }
                return
            }
            let json = JSON(res)
//            print(json)
            success(true,json["RtnObject"].stringValue,nil,json.stringValue)
        }
    }
    
    public func updToken(parameters : [String : Any] , success : @escaping ( (Bool,String,Error?,String,Int) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.updToken.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",err,"無網路連線",500)
                }
                return
            }
            let json = JSON(res)
            print(json)
            success(true,json["RtnObject"].stringValue,nil,json.stringValue,json["Status"].intValue)
        }
    }
    public func delToken(parameters : [String : Any] , success : @escaping ( (Bool,String,Error?,String,Int) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.delUserToken.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",err,"無網路連線",500)
                }
                return
            }
            let json = JSON(res)
                        print(json)
            success(json["Status"].intValue == 200,json["RtnObject"].stringValue,nil,json.stringValue,json["Status"].intValue)
        }
    }
    public func updUserLoc(parameters : [String : Any] , success : @escaping ( (Bool,String,Error?,String) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.updUserLoc.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",err,"無網路連線")
                }
                return
            }
            let json = JSON(res)
            print("updLoc:\(json)")
            success(true,json["RtnObject"].stringValue,nil,json.stringValue)
        }
    }
    public func getBulletin(parameters : [String : Any] , success : @escaping ( (Bool,JSON,Error?,String,Int) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.getBulletin.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",err,"無網路連線",500)
                }
                return
            }
            let json = JSON(res)
            
            let rtnObject = JSON.init(parseJSON: json["RtnObject"].stringValue)
            success(json["Status"].intValue == 200,rtnObject,nil,json.stringValue,json["Status"].intValue)
        }
    }
    
    public func getMarket(success : @escaping ( (Bool,MarketModel?) -> () ) ) {
        print("getMarket")
        NetworkTools.getRequestBodyData(URLString: APIRoute.getMarket.rawValue) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,nil)
                }
                return
            }
//            print(JSON(res))
            let decoder = JSONDecoder()
            if let marketModel = try? decoder.decode(MarketModel.self, from: res){
                success(marketModel.Status == 200,marketModel)
            }else{
                success(false,nil)
            }
        }
    }
    
    public func addStoreOrder(parameters : String , success : @escaping ( (Bool,JSON) -> () ) ) {
        NetworkTools.postRequestBodyData(URLString: APIRoute.addStoreOrder.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"")
                }
                return
            }
            let json = JSON(res)
            print(json)
            success(json["Status"].intValue == 200,json)
        }
    }
    public func findStoreOrder(parameter : String , success : @escaping ( (Bool,StoreOrderHsModel?) -> () ) ) {
        NetworkTools.getRequestBodyData(URLString: "\(APIRoute.findStoreOrder.rawValue)/\(parameter)") { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,nil)
                }
                return
            }
            
            let decoder = JSONDecoder()
            if let storeOrderHsModel = try? decoder.decode(StoreOrderHsModel.self, from: res){
                success(storeOrderHsModel.Status == 200,storeOrderHsModel)
                
            }else{
                success(false,nil)
            }
        }
    }
    
    public func cancelStoreOrder(parameters : String , success : @escaping ( (Bool,JSON) -> () ) ) {
        NetworkTools.postRequestBodyData(URLString: APIRoute.cancelStoreOrder.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"")
                }
                return
            }
            let json = JSON(res)
            print(json)
            success(json["Status"].intValue == 200,json)
        }
    }
    public func searchStoreInv(parameters : String , success : @escaping ( (Bool,JSON) -> () ) ) {
        NetworkTools.getRequestBodyData(URLString: "\(APIRoute.searchStoreInv.rawValue)\(parameters)") { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"")
                }
                return
            }
            let json = JSON(res)
            print(json)
            success(json["Status"].intValue == 200,json)
        }
    }
    public func getMarketP(parameters : String , success : @escaping ( (Bool,ProductModel?) -> () ) ) {
        print("\(APIRoute.getMarketP.rawValue)\(parameters)")
        NetworkTools.getRequestBodyData(URLString: "\(APIRoute.getMarketP.rawValue)\(parameters)") { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,nil)
                }
                return
            }
            let decoder = JSONDecoder()
            if let productModel = try? decoder.decode(ProductModel.self, from: res){
                success(productModel.Status == 200,productModel)
            }else{
                success(false,nil)
            }
        }
    }
    
    
    public func checkShopCoupon(parameters : String , success : @escaping ( (Bool,JSON) -> () ) ) {
        NetworkTools.postRequestBodyData(URLString: APIRoute.checkShopCoupon.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"")
                }
                return
            }
            let json = JSON(res)
            print(json)
            success(json["Status"].intValue == 200,json)
        }
    }
    public func getShopPrice(parameters : String , success : @escaping ( (Bool,JSON) -> () ) ) {
        NetworkTools.postRequestBodyData(URLString: APIRoute.getShopPrice.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"")
                }
                return
            }
            let json = JSON(res)
            print(json)
            success(json["Status"].intValue == 200,json)
        }
    }
    
    public func checkMarketId(parameters : String , success : @escaping ( (Bool) -> () ) ) {
        NetworkTools.getRequestBodyData(URLString: "\(APIRoute.checkMarketId.rawValue)\(parameters)") { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false)
                }
                return
            }
            let json = JSON(res)
            print(json)
            success(json["RtnObject"].boolValue)
        }
    }
    public func checkShopOrder(parameter : String , success : @escaping ( (Bool) -> () ) ) {
        NetworkTools.getRequestBodyData(URLString: "\(APIRoute.checkShopOrder.rawValue)/\(parameter)") { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false)
                }
                return
            }
            let json = JSON(res)
            print(json)
            success(json["RtnObject"].stringValue == "0")
        }
    }
    
    public func getMarketPs(parameters : String , success : @escaping ( (Bool,ProductsModel?) -> () ) ) {
        NetworkTools.postRequestBodyData(URLString: APIRoute.getMarketPs.rawValue ,parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,nil)
                }
                return
            }
            print("--------------------")
            print(JSON(res))
            print("--------------------")
            let decoder = JSONDecoder()
            if let productsModel = try? decoder.decode(ProductsModel.self, from: res){
                success(productsModel.Status == 200,productsModel)
            }else{
                success(false,nil)
            }
        }
    }
    
    public func shopUncharge(parameters : String , success : @escaping ( (Bool,JSON) -> () ) ) {
        NetworkTools.postRequestBodyData(URLString: APIRoute.shopUncharge.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"")
                }
                return
            }
            let json = JSON(res)
            print(json)
            success(json["Status"].intValue == 200,json["RtnObject"])
        }
    }
    public func lanShopUncharge(parameters : String , success : @escaping ( (Bool,String,String) -> () ) ) {
        NetworkTools.postRequestBodyData(URLString: APIRoute.lanUncharge.rawValue, parameters: parameters,isForm: true) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"","網路連線錯誤")
                }
                return
            }
            let json = JSON(res)
            print("lanShopUncharge\(json.description)")
            success(json["Status"].stringValue == "SUCCESS",json.description,json["Message"].stringValue)
        }
    }
    public func lanCancel(parameters : String , success : @escaping ( (Bool,String,String) -> () ) ) {
        NetworkTools.postRequestBodyData(URLString: APIRoute.lanCancel.rawValue, parameters: parameters,isForm: true) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"","網路連線錯誤")
                }
                return
            }
            let json = JSON(res)
            print(json)
            success(json["Status"].stringValue == "SUCCESS",json.description,json["Message"].stringValue)
        }
    }
    public func shopRefundrecord(parameters : String) {
        NetworkTools.postRequestBodyData(URLString: APIRoute.shopRefundrecord.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                }
                return
            }
            let json = JSON(res)
            print(json)
        }
    }
    
    public func getTokenlize(parameters : [String : Any] , success : @escaping ( (Bool,String,Error?,String) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.getTokenlize.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",err,"")
                }
                return
            }
            let json = JSON(res)
            print(json)
            let rtnObject = json["RtnObject"].stringValue
            success(json["Status"].intValue == 200,rtnObject,nil,json["DisplayMsg"].stringValue)
        }
    }
    
    public func getVANOTime(parameters : [String : Any] , success : @escaping ( (Bool,JSON,Int,String) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.getVANOTime.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",500,"")
                }
                return
            }
            let json = JSON(res)
                        print(json)
            let rtnObject = JSON.init(parseJSON: json["RtnObject"].stringValue)
            //            print(rtnObject != "")
            success(json["RtnObject"].stringValue != "",rtnObject,json["Status"].intValue,json["DisplayMsg"].stringValue)
        }
    }
    public func gVanTimeCar(parameters : [String : Any] , success : @escaping ( (Bool,JSON,Error?,String,Int) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.gVanTimeCar.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",err,"",500)
                }
                return
            }
            let json = JSON(res)
            print(json)
            let rtnObject = JSON.init(parseJSON: json["RtnObject"].stringValue)
            success(json["Status"].intValue == 200,rtnObject,nil,json["DisplayMsg"].stringValue,json["Status"].intValue)
        }
    }
    
    public func getCategory(success : @escaping ( (Bool,CategoryModel?) -> () ) ) {
        NetworkTools.getRequestBodyData(URLString: APIRoute.getCategory.rawValue) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,nil)
                }
                return
            }
            let decoder = JSONDecoder()
            if let categoryModel = try? decoder.decode(CategoryModel.self, from: res){
                success(categoryModel.Status == 200,categoryModel)
            }else{
                success(false,nil)
            }
        }
    }
    public func getCategorySale(parameters : String , success : @escaping ( (Bool,MarketModel?) -> () ) ) {
        NetworkTools.getRequestBodyData(URLString: "\(APIRoute.onSaleCategory.rawValue)\(parameters)") { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,nil)
                }
                return
            }
            let decoder = JSONDecoder()
            if let marketModel = try? decoder.decode(MarketModel.self, from: res){
                success(marketModel.Status == 200,marketModel)
            }else{
                success(false,nil)
            }
        }
    }
    public func cVANOrder(parameters : [String : Any] , success : @escaping ( (Bool,JSON,Error?,String,Int) -> () ) ) {
        NetworkTools.postRequestData(URLString: APIRoute.cVANOrder.rawValue, parameters: parameters) { (response,error) in
            guard let res = response else {
                if let err = error {
                    print(err.localizedDescription)
                    success(false,"",err,"",500)
                }
                return
            }
            let json = JSON(res)
            print(json)
            let rtnObject = JSON.init(parseJSON: json["RtnObject"].stringValue)
            success(json["Status"].intValue == 200,rtnObject,nil,json["DisplayMsg"].stringValue,json["Status"].intValue)
        }
    }
}
