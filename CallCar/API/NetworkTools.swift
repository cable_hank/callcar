//
//  NetworkTools.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/8.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit
import Alamofire
enum MethodType {
    case get
    case post
}

class NetworkTools {
    class func requestData(_ type : MethodType, URLString : String, parameters : [String : Any]? = nil, finishedCallback :  @escaping (_ result : Any?, _ error :Error?) -> ()) {
        
        let method = type == .get ? HTTPMethod.get : HTTPMethod.post
        
        Alamofire.request(URLString, method: method, parameters: parameters).responseData { (response) in
            guard let result = response.result.value else {
                print(response.result.error!)
                finishedCallback(nil,response.result.error!)
                return
            }
            finishedCallback(result,nil)
        }
    }
    class func requestData(_ type : MethodType, URLString : String, parameters : [String : Any]? = nil, finishedCallback :  @escaping (_ result : Any?) -> ()) {
        
        let method = type == .get ? HTTPMethod.get : HTTPMethod.post
        Alamofire.request(URLString, method: method, parameters: parameters).responseData { (response) in
            guard let result = response.result.value else {
                print(response.result.error!)
                return
            }
            finishedCallback(result)
        }
    }
    class func requestJson(_ type : MethodType, URLString : String, parameters : [String : Any]? = nil, finishedCallback :  @escaping (_ result : Any) -> ()) {
        
        let method = type == .get ? HTTPMethod.get : HTTPMethod.post
        
        Alamofire.request(URLString, method: method, parameters: parameters).responseJSON { (response) in
            
            guard let result = response.result.value else {
                print(response.result.error!)
                return
            }
            
            finishedCallback(result)
        }
    }
    
    class func postRequestData(URLString : String, parameters : [String : Any]? = nil, finishedCallback :  @escaping (_ result : Any?,Error?) -> ()) {
        var p = parameters
        if p != nil{
            p!["appLocale"] = "TW"
        }
        
        Alamofire.request(URLString, method: .post, parameters: p,encoding: JSONEncoding.default,headers: ["Authorization":UserDefaults.standard.getString(key: UserInfo.tokenlize.rawValue)]).responseData { (response) in
            guard let result = response.result.value else {
                print(response.result.error!)
                finishedCallback(nil,response.result.error!)
                
                return
            }
            
//            print(response.response!.statusCode)
            if response.response!.statusCode == 200{
                finishedCallback(result,nil)
            }else{
                let error:Error = NSError(domain: "404or500", code: 999, userInfo: nil)
                finishedCallback(nil,error)
            }
        }
        
    }
    
    class func getRequestData(URLString : String, parameters : [String : Any]? = nil, finishedCallback :  @escaping (_ result : Data?,Error?) -> ()) {
        var p = parameters
        if p != nil{
            p!["appLocale"] = "TW"
        }
        Alamofire.request(URLString, method: .get, parameters: p,encoding: JSONEncoding.default,headers: ["Authorization":UserDefaults.standard.getString(key: UserInfo.tokenlize.rawValue)]).responseData { (response) in
            guard let result = response.result.value else {
                print(response.result.error!)
                finishedCallback(nil,response.result.error!)
                
                return
            }
            
            if response.response!.statusCode == 200{
                finishedCallback(result,nil)
            }else{
                let error:Error = NSError(domain: "404or500", code: 999, userInfo: nil)
                finishedCallback(nil,error)
            }
        }
    }
    
    
    class func postNative(path:String,paras:String, finishedCallback :  @escaping (_ result : Any?,Error?) -> ()){
        print("postNative")
        var res:Any?
        //創建URL對象
        let urlString:String = path
        let url = URL(string:urlString)
        //創建請求對象
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.httpBody = paras.data(using: .utf8)
        let session = URLSession.shared
//        let semaphore = DispatchSemaphore(value: 1)
        let dataTask = session.dataTask(with: request, completionHandler: {(data, response, error) -> Void in
            if error != nil{
                print("error:\(error!)")
                finishedCallback(nil,error!)
                return
            }else{
                res = String(data: data!, encoding: String.Encoding.utf8)
                
                finishedCallback(res,nil)
            }
//            semaphore.signal()
            
        }) as URLSessionTask
        //使用resume方法啟動任務
        dataTask.resume()
        
//        _ = semaphore.wait(timeout: DispatchTime.now() + DispatchTimeInterval.seconds(2))
    }
    
    class func postRequestBodyData(URLString : String, parameters : [String : Any]? = nil, finishedCallback :  @escaping (_ result : Data?,Error?) -> ()) {
        
        let url = URL(string: URLString)!
        
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.post.rawValue
        
        request.setValue("text/plain", forHTTPHeaderField: "Content-Type")
        if let json = NetworkTools().json(from: parameters!){
            let jsonData = "\(json)".data(using: .utf8, allowLossyConversion: false)!
            request.httpBody = jsonData
        }else{
            finishedCallback(nil,nil)
            return
        }
        Alamofire.request(request).responseData { (response) in
            guard let result = response.result.value else {
                print(response.result.error!)
                finishedCallback(nil,response.result.error!)
                
                return
            }
            
            if response.response!.statusCode == 200{
                finishedCallback(result,nil)
            }else{
                let error:Error = NSError(domain: "404or500", code: 999, userInfo: nil)
                finishedCallback(nil,error)
            }
        }

        
    }
    
    class func postRequestBodyData(URLString : String, parameters : String,isForm:Bool = false, finishedCallback :  @escaping (_ result : Data?,Error?) -> ()) {
        
        let url = URL(string: URLString)!
        
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.post.rawValue
        if isForm{
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        }else{
            request.setValue("text/plain", forHTTPHeaderField: "Content-Type")
        }
        print(parameters)
        let jsonData = "\(parameters)".data(using: .utf8, allowLossyConversion: false)!
        request.httpBody = jsonData
        Alamofire.request(request).responseData { (response) in
            guard let result = response.result.value else {
                print(response.result.error!)
                finishedCallback(nil,response.result.error!)
                
                return
            }
            print(response.response!.statusCode)
            if response.response!.statusCode == 200{
                finishedCallback(result,nil)
            }else{
                let error:Error = NSError(domain: "404or500", code: 999, userInfo: nil)
                finishedCallback(nil,error)
            }
        }
        
        
    }
    class func getRequestBodyData(URLString : String, finishedCallback :  @escaping (_ result : Data?,Error?) -> ()) {
        let url = URL(string: URLString)!
        
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.get.rawValue
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        Alamofire.request(request).responseData { (response) in
            guard let result = response.result.value else {
                print(response.result.error!)
                finishedCallback(nil,response.result.error!)
                
                return
            }
            
            if response.response!.statusCode == 200{
                finishedCallback(result,nil)
            }else{
                let error:Error = NSError(domain: "404or500", code: 999, userInfo: nil)
                finishedCallback(nil,error)
            }
        }
        
        
    }
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
}

