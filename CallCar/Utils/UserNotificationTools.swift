//
//  UserNotificationTools.swift
//  CallCar
//
//  Created by Cablesoft on 2018/8/16.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit
import UserNotifications

class UserNotificationTools {
    func addNotification(identifier:String,dateTime:String,badgeInt:Int) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMddHHmm"
        formatter.timeZone = TimeZone(identifier: "Asia/Taipei")!
        let dateTime1 = formatter.date(from: dateTime)!
        let date = Calendar.current.date(byAdding: Calendar.Component.hour, value: -10, to: dateTime1)!
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year,.month,.day, .hour, .minute,.second],
                                                 from: date)
        let title = "CallCar 機場接送溫馨提醒"
        let body = "CallCar 感謝您預約入境接機往市區的服務。提醒您，務必再次確認抵達台灣機場之航班資訊是否正確或有延遲等。如果因班機取消或延誤，而無法使用本服務，請務必於服務前 180 分鐘來信 support@ishareco.com 申請取消您的用車（若不符合取消期限或未通知，將依照服務條款收取費用）。\n\n iShareco 艾雪科技團隊。"
        if #available(iOS 10.0, *) {
            let content = UNMutableNotificationContent()
            content.title = title
            content.body = body
            content.badge = NSNumber(integerLiteral: badgeInt + 1)
            content.sound = UNNotificationSound.default
            
            let identifier = identifier
            let trigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: false)
            let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
            UNUserNotificationCenter.current().add(request, withCompletionHandler: {error in
                print("成功建立通知...")
            })
        } else {
            // ios 9
            let notification = UILocalNotification()
            notification.fireDate = date
            notification.alertBody = body
            notification.alertTitle = title
            notification.userInfo = ["identifier":identifier]
//            notification.timeZone = TimeZone(identifier: "Asia/Taipei")
            notification.soundName = UILocalNotificationDefaultSoundName
            UIApplication.shared.scheduleLocalNotification(notification)
            print("成功建立通知...")
        }
    }
    
    func deletNotification(identifier:String) {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [identifier])
        } else {
            if let localNotifications = UIApplication.shared.scheduledLocalNotifications {
                for oneEvent in localNotifications {
                    let notification = oneEvent as UILocalNotification
                    if let userInfoCurrent = notification.userInfo as? [String:AnyObject],
                        let identifier = userInfoCurrent["identifier"] as? String {
                        if identifier == identifier {
                            //Cancelling local notification
                            UIApplication.shared.cancelLocalNotification(notification)
                            break
                        }
                    }
                }
            }
        }
    }
    
    func deleteAllNotifications(){
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        } else {
            UIApplication.shared.cancelAllLocalNotifications()
        }
    }
    
    func checkNotifications(){
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.getPendingNotificationRequests(completionHandler: { requests in
                print(requests.count)
//                for request in requests {
//                    print(request.identifier)
//                    print(request.trigger)
//                }
            })
        } else {
            if let localNotifications = UIApplication.shared.scheduledLocalNotifications {
                
                print(localNotifications.count)
                for oneEvent in localNotifications {
                    let notification = oneEvent as UILocalNotification
                    if let userInfoCurrent = notification.userInfo as? [String:AnyObject],
                        let identifier = userInfoCurrent["identifier"] as? String {
//                        print(identifier)
//                        print(notification.fireDate)
                    }
                }
            }
        }
    }
}
