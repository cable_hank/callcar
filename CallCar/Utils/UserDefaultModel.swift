//
//  UserDefaultModel.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/15.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import Foundation

class UserDefaultModel {
    static let shared : UserDefaultModel = UserDefaultModel()
    fileprivate let userDefault = UserDefaults.standard
    private init(){}
    
    public func isExist(key : String) -> Bool {
        return ( (self.userDefault.string(forKey: key)) != nil)
    }
    public func readString(key : String) -> String? {
        return self.userDefault.string(forKey: key)
    }
    public func setValue(key : String , value : Any) {
        self.userDefault.set(value, forKey: key)
        self.userDefault.synchronize()
    }
    public func remove(key : String){
        self.userDefault.removeObject(forKey: key)
    }
}
