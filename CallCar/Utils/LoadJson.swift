//
//  LoadJson.swift
//  CallCar
//
//  Created by Cablesoft on 2019/1/24.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import Foundation
import SwiftyJSON

class LoadJson{
    func getJson(path:String) -> JSON? {
        if let path = Bundle.main.path(forResource: path, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                let jsonObj = try JSON(data: data)
                print("jsonData:\(jsonObj)")
                return jsonObj
            } catch let error {
                print("parse error: \(error.localizedDescription)")
                return nil
            }
        } else {
            print("Invalid filename/path.")
            return nil
        }
    }
    
    func getPostalCodeModel(path:String) -> [PostalCodeModel] {
        if let path = Bundle.main.path(forResource: path, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                
                let decoder = JSONDecoder()
                if let marketModel = try? decoder.decode([PostalCodeModel].self, from: data){
                    return marketModel
                }else{
                    return [PostalCodeModel]()
                }
            } catch let error {
                print("parse error: \(error.localizedDescription)")
                return [PostalCodeModel]()
            }
        } else {
            print("Invalid filename/path.")
            return [PostalCodeModel]()
        }
    }
    
}
