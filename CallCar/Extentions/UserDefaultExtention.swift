//
//  UserDefaultExtention.swift
//  CallCar
//
//  Created by Cablesoft on 2018/11/20.
//  Copyright © 2018 Cablesoft. All rights reserved.
//

import Foundation
extension UserDefaults{
    func getString(key:String) -> String{
        return string(forKey: key) != nil ? string(forKey: key)! : ""
        
    }
}
