//
//  UIViewControllerExtention.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/8.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit
import SwiftMessages
import SwiftyJSON
import Firebase

extension UIViewController{
    
    func goRoot(){
        let storyboard = UIStoryboard(name: "TabBar", bundle: nil)
        let rootVC = storyboard.instantiateViewController(withIdentifier: "tabBarLaunch")
        self.present(rootVC, animated: false, completion: nil)
    }
    
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func checkInternetFunction() -> Bool {
        let reachability = Reachability(hostName: "www.apple.com")
        if reachability?.currentReachabilityStatus().rawValue == 0 {
            print("no internet connected.")
            return false
        }else {
            print("internet connected successfully.")
            return true
        }
    }
    
    
    /// Show always scroll indicator in table view
    @objc func showInterNetMessage() {
        print("doNetMessage")
        if !checkInternetFunction(){
            UIApplication.shared.statusBarStyle = .lightContent
            let view = MessageView.viewFromNib(layout: .statusLine)
            let iconStyle: IconStyle = .default
            view.configureTheme(.error, iconStyle: iconStyle)
            view.accessibilityPrefix = "error"
            view.configureContent(title: "錯誤", body: "目前無網路服務")
            var config = SwiftMessages.defaultConfig
            config.presentationContext = .window(windowLevel: UIWindow.Level(rawValue: UIWindow.Level.normal.rawValue))
            config.duration = .forever
            SwiftMessages.show(config: config, view: view)
        }else{
            UIApplication.shared.statusBarStyle = .default
            SwiftMessages.hide()
        }
    }
    
    @objc func hideInterNetMessage() {
        SwiftMessages.hide()
    }
    func adjustUITextViewHeight(arg : UITextView){
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        arg.isScrollEnabled = false
    }
    
//    func testAPI(dateString:String) -> Bool {
//        if let jsonData = UserDefaults.standard.data(forKey: UserInfo.maintain.rawValue){
//
//            let formatter = DateFormatter()
//            formatter.dateFormat = "yyyy-MM-dd HH:mm"
//            formatter.timeZone = TimeZone(identifier: "Asia/Taipei")!
//            var date = formatter.string(from: Date())
//            print(date)
//            if dateString != ""{
//                date = dateString
//            }
//            let nowDate = formatter.date(from: date)!
//            let json = try! JSON(data: jsonData)
//            for maintain in json["maintain"].arrayValue{
//                print(maintain["begin"].stringValue)
//                print(nowDate)
//                print(maintain["end"].stringValue)
//                if nowDate > formatter.date(from: maintain["begin"].stringValue)!  && nowDate < formatter.date(from: maintain["end"].stringValue)!{
//
//                    return false
//
//                }
//            }
//        }
//        return true
//    }
    
    
    func isApiError(error:Error?) -> Bool {
        if !checkInternetFunction(){
            return false
        }
        if error != nil{
            return true
        }
        return false
    }
    
    func toTimeFormaater(timeString:String) -> String{
        if timeString.count == 4{
            return "\(timeString.prefix(2)):\(timeString.suffix(2))"
        }else{
            return timeString
        }
    }
    
    func toDateFormaater(dateString:String) -> String{
        if dateString.count == 8{
            return "\(dateString.prefix(4))-\(dateString.subString(from: 4, to: 5))-\(dateString.suffix(2))"
        }else{
            return dateString
        }
    }
    func toDateFormaaterSlash(dateString:String) -> String{
        if dateString.count != 8{
            return dateString
        }
        return "\(dateString.prefix(4))/\(dateString.subString(from: 4, to: 5))/\(dateString.suffix(2))"
    }
    func yyyyMMddToMM_Slash_dd(dateString:String) -> String{
        if dateString.count != 8{
            return dateString
        }
        return "\(dateString.subString(from: 4, to: 5))/\(dateString.suffix(2))"
    }
    
    
}

extension UIViewController:UITextFieldDelegate{
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension UIScrollView{
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        endEditing(true)
    }
}
extension String {
    func subString(from: Int, to: Int) -> String {
        let startIndex = self.index(self.startIndex, offsetBy: from)
        let endIndex = self.index(self.startIndex, offsetBy: to)
        return String(self[startIndex...endIndex])
    }
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    func lastIndex(of string: String) -> Int? {
        guard let index = range(of: string, options: .backwards) else { return nil }
        return self.distance(from: self.startIndex, to: index.lowerBound)
    }
    func formateDate(format:String) -> Date? {
        let f = DateFormatter()
        f.dateFormat = format
        return f.date(from: self)
    }
}
extension Date{
    func formateString(format:String) -> String {
        let f = DateFormatter()
        f.dateFormat = format
        return f.string(from: self)
    }
}

extension UIImageView {
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
        }.resume()
    }
    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit){
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
    
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit, completion: @escaping ()->()) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
                completion()
            }
        }.resume()
    }
    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit, completion: @escaping ()->()){
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode) { () in
            completion()
        }
    }
}


extension UIView{
    func blink() {
        self.alpha = 0.5
        UIView.animate(withDuration: 0.3, delay: 0.0, options: [.curveLinear, .repeat, .autoreverse], animations: {
            UIView.setAnimationRepeatCount(2)
            self.alpha = 1.0
        }, completion: nil)
    }
}
// MARK: - 按钮的反复点击问题 交换方法
extension UIButton {
    
    
    /// 对外交换方法的方法 AppDelegate Launch中使用
    public  static func methodExchange() {
        DispatchQueue.once(token: "UIButton") {
            let originalSelector = Selector.sysFunc
            let swizzledSelector = Selector.myFunc
            changeMethod(originalSelector, swizzledSelector, self)
        }
    }
    
    
    /// Runtime方法交换
    ///
    /// - Parameters:
    ///   - original: 原方法
    ///   - swizzled: 交换方法
    ///   - object: 对象
    private static func changeMethod(_ original: Selector, _ swizzled: Selector, _ object: AnyClass) -> () {
        
        guard let originalMethod = class_getInstanceMethod(object, original),
            let swizzledMethod = class_getInstanceMethod(object, swizzled) else {
                return
        }
        
        let didAddMethod = class_addMethod(object, original, method_getImplementation(swizzledMethod), method_getTypeEncoding(swizzledMethod))
        if didAddMethod {
            class_replaceMethod(object, swizzled, method_getImplementation(originalMethod), method_getTypeEncoding(originalMethod))
        } else {
            method_exchangeImplementations(originalMethod, swizzledMethod)
        }
    }
    
    
    /// 结构体静态key
    private struct UIButtonKey {
        static var isEventUnavailableKey = "isEventUnavailableKey"
        static var eventIntervalKey = "eventIntervalKey"
    }
    
    /// 触发事件的间隔
    var eventInterval: TimeInterval {
        get {
            return (objc_getAssociatedObject(self, &UIButtonKey.eventIntervalKey) as? TimeInterval) ?? 0.5
        }
        set {
            objc_setAssociatedObject(self, &UIButtonKey.eventIntervalKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    /// 是否可以触发事件
    fileprivate var isEventUnavailable: Bool {
        get {
            return (objc_getAssociatedObject(self, &UIButtonKey.isEventUnavailableKey) as? Bool) ?? false
        }
        set {
            objc_setAssociatedObject(self, &UIButtonKey.isEventUnavailableKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    /// 手写的set方法
    ///
    /// - Parameter isEventUnavailable: 事件是否可用
    @objc private func setIsEventUnavailable(_ isEventUnavailable: Bool) {
        self.isEventUnavailable = isEventUnavailable
    }
    
    /// mySendAction
    @objc fileprivate func mySendAction(_ action: Selector, to target: Any?, for event: UIEvent?) {
        print("交换了按钮事件的方法")
        
        if isEventUnavailable == false {
            isEventUnavailable = true
            mySendAction(action, to: target, for: event)
            perform(#selector(setIsEventUnavailable(_: )), with: false, afterDelay: eventInterval)
        }
    }
}

fileprivate extension Selector {
    static let sysFunc = #selector(UIButton.sendAction(_:to:for:))
    static let myFunc = #selector(UIButton.mySendAction(_:to:for:))
}

extension DispatchQueue {
    private static var onceTracker = [String]()
    
    open class func once(token: String, block:() -> ()) {
        //注意defer作用域，调用顺序——即一个作用域结束，该作用域中的defer语句自下而上调用。
        objc_sync_enter(self)
        defer {
            print("线程锁退出")
            objc_sync_exit(self)
        }
        
        if onceTracker.contains(token) {
            return
        }
        onceTracker.append(token)
        block()
        defer {
            print("block执行完毕")
        }
    }
}
