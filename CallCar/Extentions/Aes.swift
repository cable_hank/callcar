//
//  Aes.swift
//  BaseProject
//
//  Created by Cablesoft on 2018/12/6.
//  Copyright © 2018 hank. All rights reserved.
//

import Foundation
import CryptoSwift

extension String {
    
    /// AES加密
    ///
    /// - Parameters:
    ///   - key: key
    ///   - iv: iv
    /// - Returns: String
    func aesEncrypt(key: String, iv: String) -> String? {
        var result: String?
        do {
            // 用UTF8的编碼方式將字串轉成Data
            let data: Data = self.data(using: String.Encoding.utf8, allowLossyConversion: true)!
            // 用AES的方式將Data加密
            let aecEnc: AES = try AES(key: key, iv: iv)
            let enc = try aecEnc.encrypt(data.bytes)
            // 使用hex編碼方式將Data轉回字串
            let encData: Data = Data(bytes: enc, count: enc.count)
            result = encData.toHexString()
        } catch {
            print("\(error.localizedDescription)")
        }
        
        return result
    }
    
    /// AES解密
    ///
    /// - Parameters:
    ///   - key: key
    ///   - iv: iv
    /// - Returns: String
    func aesDecrypt(key: String, iv: String) -> String? {
        var result: String?
        do {
            // 使用hex的解碼方式將字串解碼後再轉换Data
            
            let data = Data(hex: self)
            print(data)
            // 用AES方式將Data解密
            //            let aesDec: AES = try AES(key: Array(key.utf8), blockMode: CBC(iv: Array(iv.utf8)))
            let aesDec: AES = try AES(key: key, iv: iv)
            let dec = try aesDec.decrypt(data.bytes)
            
            // 用UTF8的編碼方式將解完密的Data轉回字串
            let desData: Data = Data(bytes: dec, count: dec.count)
            result = String(data: desData, encoding: .utf8)
        } catch {
            print("\(error.localizedDescription)")
        }
        
        return result
    }
    
    func toSha256() -> String{
        return self.sha256().uppercased()
    }
    
}
