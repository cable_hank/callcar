//
//  RealmDB.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/30.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON
class RealmDB{
    static let shared : RealmDB = RealmDB()
    
    func deleteAll(){
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
    }
    
    func saveBonus(bonusModels:[BonusModel]){
        var rmBounusArr = [RM_BonusModel]()
        for bonus in bonusModels{
            let rmBonus = RM_BonusModel()
            rmBonus.addtype = bonus.addtype
            rmBonus.btype = bonus.btype
            rmBonus.relatedinfo = bonus.relatedinfo
            rmBonus.point = bonus.point
            rmBonus.cancel = bonus.cancel
            rmBonus.effdate = bonus.effdate
            rmBonus.getdate = bonus.getdate
            rmBonus.memo = bonus.memo
            rmBounusArr.append(rmBonus)
        }
        let realm = try! Realm()
        
        try! realm.write {
            realm.delete(realm.objects(RM_BonusModel.self))
            realm.add(rmBounusArr)
        }
    }
    
    func getBonus() -> [RM_BonusModel] {
        let realm = try! Realm()
        let bonusArr = Array(realm.objects(RM_BonusModel.self))
        return bonusArr
    }
    
    func saveDeposit(depositModels:[DepositModel]){
        var rmDepositArr = [RM_DepositModel]()
        for deposit in depositModels{
            let rmDeposit = RM_DepositModel()
            rmDeposit.source = deposit.source
            rmDeposit.type = deposit.type
            rmDeposit.getdate = deposit.getdate
            rmDeposit.amt = deposit.amt
            rmDeposit.cancel = deposit.cancel
            rmDeposit.relatedinfo = deposit.relatedinfo
            rmDeposit.memo = deposit.memo
            rmDepositArr.append(rmDeposit)
        }
        let realm = try! Realm()
        try! realm.write {
            realm.delete(realm.objects(RM_DepositModel.self))
            realm.add(rmDepositArr)
        }
    }
    
    func getDeposit() -> [RM_DepositModel] {
        let realm = try! Realm()
        let depositArr = Array(realm.objects(RM_DepositModel.self))
        return depositArr
    }
    
    func saveOrder(json:JSON){
        var orderArr = [RM_OrderModel]()
        for o in json.arrayValue{
            let orderModel = RM_OrderModel()
            orderModel.rno = o["rno"].stringValue
            orderModel.stype = o["stype"].stringValue
            orderModel.tdate = o["tdate"].stringValue
            orderModel.ttime = o["ttime"].stringValue
            orderModel.stime = o["stime"].stringValue
            orderModel.poolflag = o["poolflag"].stringValue
            orderModel.fno = o["fno"].stringValue
            orderModel.ftime = o["ftime"].stringValue
            orderModel.paddress = o["paddress"].stringValue
            orderModel.taddress = o["taddress"].stringValue
            orderModel.pcnt = o["pcnt"].intValue
            orderModel.bcnt = o["bcnt"].intValue
            orderModel.addcnt = o["addcnt"].intValue
            orderModel.price = o["price"].intValue
            orderModel.distype = o["distype"].stringValue
            orderModel.coupon = o["coupon"].stringValue
            orderModel.disamt = o["disamt"].intValue
            orderModel.bonus = o["bonus"].intValue
            orderModel.pstage = o["pstage"].stringValue
            orderModel.pname = o["pname"].stringValue
            orderModel.pphone = o["pphone"].stringValue
            orderModel.otime = o["otime"].stringValue
            orderModel.dname = o["dname"].stringValue
            orderModel.dphone = o["dphone"].stringValue
            orderModel.carno = o["carno"].stringValue
            orderModel.cartype = o["cartype"].stringValue
            orderModel.price1 = o["price1"].intValue
            orderModel.cancelflag = o["cancelflag"].stringValue
            orderModel.cancelalert = o["cancelalert"].stringValue
            orderModel.pstageo = o["pstageo"].stringValue
            orderModel.vanflag = o["vanflag"].stringValue
            orderModel.noshow = o["noshow"].stringValue
            orderModel.lat = o["lat"].doubleValue
            orderModel.lng = o["lng"].doubleValue
            orderModel.airport = o["airport"].stringValue
            orderModel.terminal = o["terminal"].stringValue
            orderModel.city = o["city"].stringValue
            orderModel.distinct = o["distinct"].stringValue
            orderModel.dno = o["dno"].stringValue
            orderModel.country = o["country"].stringValue
            for dispatch in o["dispatch"].arrayValue{
                let rmdispatch = rm_dispatch()
                rmdispatch.time = dispatch["time"].stringValue
                rmdispatch.rno = dispatch["rno"].stringValue
                rmdispatch.seq = dispatch["seq"].intValue
                rmdispatch.distinct = dispatch["distinct"].stringValue
                rmdispatch.dno = dispatch["dno"].stringValue
                rmdispatch.city = dispatch["city"].stringValue
                rmdispatch.pcnt = dispatch["pcnt"].intValue
                rmdispatch.bcnt = dispatch["bcnt"].intValue
                orderModel.dispatch.append(rmdispatch)
            }
            orderArr.append(orderModel)
            if orderModel.pstage != "已取消" && orderModel.pstage != "已完成" && orderModel.stype == "I" && orderModel.poolflag == "S"{
                let dateTime = "\(orderModel.tdate)\(orderModel.ttime)"
                UserNotificationTools().addNotification(identifier: orderModel.rno, dateTime: dateTime, badgeInt: UIApplication.shared.applicationIconBadgeNumber)
            }
        }
        let realm = try! Realm()
        
        try! realm.write {
            realm.delete(realm.objects(RM_OrderModel.self))
            realm.add(orderArr)
        }
    }
    
    func saveHsOrder(json:JSON){
        var orderArr = [RM_OrderHsModel]()
        for o in json.arrayValue{
            let orderModel = RM_OrderHsModel()
            orderModel.rno = o["rno"].stringValue
            orderModel.stype = o["stype"].stringValue
            orderModel.tdate = o["tdate"].stringValue
            orderModel.ttime = o["ttime"].stringValue
            orderModel.stime = o["stime"].stringValue
            orderModel.poolflag = o["poolflag"].stringValue
            orderModel.fno = o["fno"].stringValue
            orderModel.ftime = o["ftime"].stringValue
            orderModel.paddress = o["paddress"].stringValue
            orderModel.taddress = o["taddress"].stringValue
            orderModel.pcnt = o["pcnt"].intValue
            orderModel.bcnt = o["bcnt"].intValue
            orderModel.addcnt = o["addcnt"].intValue
            orderModel.price = o["price"].intValue
            orderModel.distype = o["distype"].stringValue
            orderModel.coupon = o["coupon"].stringValue
            orderModel.disamt = o["disamt"].intValue
            orderModel.bonus = o["bonus"].intValue
            orderModel.pstage = o["pstage"].stringValue
            orderModel.pname = o["pname"].stringValue
            orderModel.pphone = o["pphone"].stringValue
            orderModel.otime = o["otime"].stringValue
            orderModel.dname = o["dname"].stringValue
            orderModel.dphone = o["dphone"].stringValue
            orderModel.carno = o["carno"].stringValue
            orderModel.cartype = o["cartype"].stringValue
            orderModel.price1 = o["price1"].intValue
            orderModel.vanflag = o["vanflag"].stringValue
            orderModel.noshow = o["noshow"].stringValue
            orderModel.country = o["country"].stringValue
            orderArr.append(orderModel)
        }
        let realm = try! Realm()
        
        try! realm.write {
            realm.delete(realm.objects(RM_OrderHsModel.self))
            realm.add(orderArr)
        }
    }
    func getOrder() -> [RM_OrderModel] {
        let realm = try! Realm()
        let sortProperties = [SortDescriptor(keyPath: "pstageo", ascending: true), SortDescriptor(keyPath: "tdate", ascending: false)]
        let orderArr = Array(realm.objects(RM_OrderModel.self).sorted(by: sortProperties))
        return orderArr
    }
    func getOrderInHome() -> [RM_OrderModel] {
        let realm = try! Realm()
        let sortProperties = [SortDescriptor(keyPath: "pstageo", ascending: true), SortDescriptor(keyPath: "tdate", ascending: false)]
        let orderArr = Array(realm.objects(RM_OrderModel.self).filter("pstageo == '0' OR pstageo == 'A'").sorted(by: sortProperties))
        return orderArr
    }
    
    func getHsOrder() -> [RM_OrderHsModel] {
        let realm = try! Realm()
        let orderArr = Array(realm.objects(RM_OrderHsModel.self).sorted(byKeyPath: "tdate", ascending: false))
        return orderArr
    }
    
    func getOrderByNo(rno:String) -> RM_OrderModel?{
        let realm = try! Realm()
        let order = realm.objects(RM_OrderModel.self).filter("rno == '\(rno)'").first
        return order
    }
    
    func getHsOrderByNo(rno:String) -> RM_OrderHsModel?{
        let realm = try! Realm()
        let order = realm.objects(RM_OrderHsModel.self).filter("rno == '\(rno)'").first
        return order
    }
    
    func saveAddr(addr:String,lat:Double,lng:Double){
        let realm = try! Realm()
        let addrModel = RM_AddrModel()
        addrModel.addr = addr
        addrModel.lat = lat
        addrModel.lng = lng
        try! realm.write {
            realm.add(addrModel)
        }
        
    }
    func getAddr() -> [RM_AddrModel]{
        let realm = try! Realm()
        let addr = Array(realm.objects(RM_AddrModel.self).sorted(byKeyPath: "date", ascending: false))
        return addr
    }
    func delAddr(id:String){
        let realm = try! Realm()
        try! realm.write {
            let obj = realm.objects(RM_AddrModel.self).filter("id == '\(id)'").first
//            print(obj)
            if let o = obj {
                realm.delete(o)
            }
            
        }
    }
}
