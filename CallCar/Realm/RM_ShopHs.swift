//
//  RM_ShopHs.swift
//  CallCar
//
//  Created by Cablesoft on 2019/2/18.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import Foundation
import RealmSwift
class RM_ShopHs:Object {
    @objc private(set) dynamic var id:String = UUID().uuidString
    @objc dynamic var ono:String = ""
    @objc dynamic var otime:String = ""
    @objc dynamic var uid:Int = 0
    @objc dynamic var shipp:Int = 0
    @objc dynamic var orderp:Int = 0
    @objc dynamic var dtype:String = ""
    @objc dynamic var damt:Int = 0
    @objc dynamic var coupon:String = ""
    @objc dynamic var totalp:Int = 0
    @objc dynamic var payamt:Int = 0
    @objc dynamic var oname:String = ""
    @objc dynamic var omobile:String = ""
    @objc dynamic var rname:String = ""
    @objc dynamic var rmobile:String = ""
    @objc dynamic var rzip:String = ""
    @objc dynamic var rcity:String = ""
    @objc dynamic var rdist:String = ""
    @objc dynamic var raddress:String = ""
    @objc dynamic var ostatus:String = ""
    @objc dynamic var rstatus:String = ""
    @objc dynamic var eddate:String = ""
    @objc dynamic var ddate:String = ""
    @objc dynamic var oprocess:String = ""
    @objc dynamic var invoice:String = ""
    @objc dynamic var invoiceno:String = ""
    var process = List<rm_process>()
    var odetail = List<rm_odetail>()
    //設置索引主鍵
    override static func primaryKey() -> String {
        return "id"
    }
}

class rm_process:Object {
    @objc dynamic var Seq:Int = 0
    @objc dynamic var Stage:String = ""
    @objc dynamic var StageTime:String = ""
    @objc dynamic var Display:String = ""
}

class rm_odetail:Object {
    @objc dynamic var ono:String = ""
    @objc dynamic var dno:String = ""
    @objc dynamic var marketid:String = ""
    @objc dynamic var mid:String = ""
    @objc dynamic var format:String = ""
    @objc dynamic var unitp:Int = 0
    @objc dynamic var amt:Int = 0
    @objc dynamic var unit:String = ""
    @objc dynamic var totalp:Int = 0
    @objc dynamic var title:String = ""
    @objc dynamic var img:String = ""
    @objc dynamic var damt:Int = 0
    @objc dynamic var process:String = ""
}
