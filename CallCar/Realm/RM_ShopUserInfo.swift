//
//  RM_ShopUserInfo.swift
//  CallCar
//
//  Created by Cablesoft on 2019/2/27.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import Foundation
import RealmSwift

class RM_ShopUserInfo : Object {
    
    @objc private(set) dynamic var id:String = UUID().uuidString
    @objc dynamic var email = ""
    @objc dynamic var userName = ""
    @objc dynamic var userMobile = ""
    @objc dynamic var name = ""
    @objc dynamic var mobile = ""
    @objc dynamic var level1 = 0
    @objc dynamic var level2 = 0
    @objc dynamic var addr = ""
    @objc dynamic var allAddr = ""
    
    //設置索引主鍵
    override static func primaryKey() -> String {
        return "id"
    }
    
}
