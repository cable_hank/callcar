//
//  RM_BonusModel.swift
//  CallCar
//
//  Created by Cablesoft on 2018/8/14.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit
import RealmSwift
class RM_BonusModel : Object{
    @objc private(set) dynamic var id:String = UUID().uuidString
    @objc dynamic var addtype:String = ""
    @objc dynamic var btype:String = ""
    @objc dynamic var relatedinfo:String = ""
    @objc dynamic var point:String = ""
    @objc dynamic var cancel:String = ""
    @objc dynamic var effdate:String = ""
    @objc dynamic var getdate:String = ""
    @objc dynamic var memo:String = ""
}
