//
//  RM_ShopCar.swift
//  CallCar
//
//  Created by Cablesoft on 2019/1/23.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import Foundation
import RealmSwift

class RM_ShopCar : Object {
    
    @objc private(set) dynamic var id:String = UUID().uuidString
    @objc dynamic var rno = ""
    
    @objc dynamic var MarketID = ""
    @objc dynamic var PID = ""
    @objc dynamic var STime = ""
    @objc dynamic var ETime = ""
    @objc dynamic var OnFlag = ""
    @objc dynamic var SaleName = ""
    @objc dynamic var Title1 = ""
    @objc dynamic var Title2 = ""
    @objc dynamic var ImageUrl = ""
    @objc dynamic var SaleInfo = ""
    @objc dynamic var Video = ""
    @objc dynamic var FormatId = ""
    @objc dynamic var Format = ""
    @objc dynamic var Price = 0
    @objc dynamic var SalePrice = 0
    @objc dynamic var Unit = ""
    @objc dynamic var ShippingFlag = ""
    @objc dynamic var ShippingCost = 0
    @objc dynamic var BonusFlag = ""
    @objc dynamic var BonusLimit = 0
    @objc dynamic var CreateTime = ""
    @objc dynamic var UpdTime = ""
    @objc dynamic var MaxBuy = 0
    @objc dynamic var pCount = 0
    @objc dynamic var shopCarBonus = 0
    @objc dynamic var saleq = 0
    @objc dynamic var soldq = 0
    
    //設置索引主鍵
    override static func primaryKey() -> String {
        return "id"
    }
    
}
