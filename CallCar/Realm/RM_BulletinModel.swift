//
//  RM_BulletinModel.swift
//  CallCar
//
//  Created by Cablesoft on 2018/12/24.
//  Copyright © 2018 Cablesoft. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
class RM_BulletinModel : Object{
    @objc private(set) dynamic var id:String = UUID().uuidString
    @objc dynamic var nid:String = ""
    @objc dynamic var stime:String = ""
    @objc dynamic var etime:String = ""
    @objc dynamic var type:String = ""
    @objc dynamic var title:String = ""
    @objc dynamic var content:String = ""
    @objc dynamic var go:String = ""
    @objc dynamic var gouri:String = ""
    @objc dynamic var isLook:Bool = false
    
    func save(json:JSON) {
        var bulletinArr = [RM_BulletinModel]()
        for bulletinJson in json.arrayValue{
            let bulletin = RM_BulletinModel()
            bulletin.nid = bulletinJson["nid"].stringValue
            bulletin.stime = bulletinJson["stime"].stringValue
            bulletin.etime = bulletinJson["etime"].stringValue
            bulletin.type = bulletinJson["type"].stringValue
            bulletin.title = bulletinJson["title"].stringValue
            bulletin.content = bulletinJson["content"].stringValue
            bulletin.go = bulletinJson["go"].stringValue
            bulletin.gouri = bulletinJson["gouri"].stringValue
            let realm = try! Realm()
            let oldBulletin = realm.objects(RM_BulletinModel.self).filter("nid == '\(bulletinJson["nid"].stringValue)'").first
            if let oldBulletin = oldBulletin{
                bulletin.isLook = oldBulletin.isLook
            }else{
                bulletin.isLook = false
            }
            bulletinArr.append(bulletin)
        }
        let realm = try! Realm()
        try! realm.write {
            realm.delete(realm.objects(RM_BulletinModel.self))
            realm.add(bulletinArr)
        }
    }
    func getIfIsLookFalse() -> Bool{
        let realm = try! Realm()
        let bulletin = realm.objects(RM_BulletinModel.self).filter("isLook == %@",false).first
        if bulletin != nil{
            return true
        }else{
            return false
        }
    }
    func delete() {
        let realm = try! Realm()
        try! realm.write {
            realm.delete(realm.objects(RM_BulletinModel.self))
        }
    }
    
    func updateIsLook(bulletin:RM_BulletinModel) {
        let realm = try! Realm()
        
        try! realm.write {
            bulletin.isLook = true
        }
    }
    
    func updateAllisLook(){
        let realm = try! Realm()
        let bulletinArr = Array(realm.objects(RM_BulletinModel.self))
        
        for bulletin in bulletinArr{
            try! realm.write {
                bulletin.isLook = true
            }
        }
    }
    
    func getAll() -> [RM_BulletinModel]{
        let realm = try! Realm()
        let bulletinArr = Array(realm.objects(RM_BulletinModel.self).sorted(byKeyPath: "stime", ascending: false))
        return bulletinArr
    }
}
