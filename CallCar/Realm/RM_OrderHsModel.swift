//
//  RM_OrderHsModel.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/31.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit
import RealmSwift

class RM_OrderHsModel : Object {
    
    @objc private(set) dynamic var id:String = UUID().uuidString
    @objc dynamic var rno = ""
    @objc dynamic var stype = ""
    @objc dynamic var tdate = ""
    @objc dynamic var ttime = ""
    @objc dynamic var stime = ""
    @objc dynamic var poolflag = ""
    @objc dynamic var fno = ""
    @objc dynamic var ftime = ""
    @objc dynamic var paddress = ""
    @objc dynamic var taddress = ""
    @objc dynamic var pcnt = 0
    @objc dynamic var bcnt = 0
    @objc dynamic var addcnt = 0
    @objc dynamic var price = 0
    @objc dynamic var distype = ""
    @objc dynamic var coupon = ""
    @objc dynamic var disamt = 0
    @objc dynamic var bonus = 0
    @objc dynamic var pstage = ""
    @objc dynamic var pname = ""
    @objc dynamic var pphone = ""
    @objc dynamic var otime = ""
    @objc dynamic var dname = ""
    @objc dynamic var dphone = ""
    @objc dynamic var carno = ""
    @objc dynamic var cartype = ""
    @objc dynamic var price1 = 0
    @objc dynamic var vanflag = ""
    @objc dynamic var noshow = ""
    @objc dynamic var country = ""
    //設置索引主鍵
    override static func primaryKey() -> String {
        return "id"
    }
    
}

