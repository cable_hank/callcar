//
//  RealmShop.swift
//  CallCar
//
//  Created by Cablesoft on 2019/1/23.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import Foundation
import RealmSwift
class RealmShop{
    static let shared : RealmShop = RealmShop()
    func saveShopCar(marketModel:ProductModel.RtnObject,format:ProductModel.RtnObject.Format?,count:Int,canExceed:Bool) -> Bool{
        let shopCar = RM_ShopCar()
        if let format = format{
            shopCar.FormatId = format.mid
            shopCar.Format = format.format
            shopCar.soldq = format.soldq
            shopCar.saleq = format.saleq
        }else{
            shopCar.FormatId = ""
            shopCar.Format = ""
        }
        
        
        shopCar.MarketID = marketModel.MarketID
        shopCar.PID = marketModel.PID
        shopCar.STime = marketModel.STime
        shopCar.ETime = marketModel.ETime
        shopCar.OnFlag = marketModel.OnFlag
        shopCar.SaleName = marketModel.SaleName
        shopCar.Title1 = marketModel.Title1
        shopCar.Title2 = marketModel.Title2
        shopCar.ImageUrl = marketModel.Images[0].Url
        shopCar.SaleInfo = marketModel.SaleInfo
        shopCar.Video = marketModel.Video
        shopCar.Price = marketModel.Price
        shopCar.SalePrice = marketModel.SalePrice
        shopCar.Unit = marketModel.Unit
        shopCar.ShippingFlag = marketModel.ShippingFlag
        shopCar.ShippingCost = marketModel.ShippingCost
        shopCar.BonusFlag = marketModel.BonusFlag
        shopCar.BonusLimit = marketModel.BonusLimit
        shopCar.CreateTime = marketModel.CreateTime
        shopCar.UpdTime = marketModel.UpdTime
        shopCar.MaxBuy = marketModel.MaxBuy
        shopCar.pCount = count
        let realm = try! Realm()
        
        if let s = self.isRepeat(marketId: marketModel.MarketID, formatId: shopCar.FormatId){
            if s.pCount + count > s.MaxBuy{
                if canExceed{
                    self.updateCount(shopCar: s, count: s.MaxBuy)
                }
                return false
            }else{
                self.updateCount(shopCar: s, count: s.pCount + count)
                return true
            }
        }else{
            try! realm.write {
                realm.add(shopCar)
            }
            return true
        }
    }
    
    func updateShopCar(marketModel:ProductsModel.RtnObject,format:ProductsModel.RtnObject.Format?,shopCar:RM_ShopCar){
        
        let realm = try! Realm()
        try! realm.write {
            if let format = format{
                shopCar.FormatId = format.mid
                shopCar.Format = format.format
            }else{
                shopCar.FormatId = ""
                shopCar.Format = ""
            }
            
            
            shopCar.MarketID = marketModel.MarketID
            shopCar.PID = marketModel.PID
            shopCar.STime = marketModel.STime
            shopCar.ETime = marketModel.ETime
            shopCar.OnFlag = marketModel.OnFlag
            shopCar.SaleName = marketModel.SaleName
            shopCar.Title1 = marketModel.Title1
            shopCar.Title2 = marketModel.Title2
            shopCar.ImageUrl = marketModel.Images[0].Url
            shopCar.SaleInfo = marketModel.SaleInfo
            shopCar.Video = marketModel.Video
            shopCar.Price = marketModel.Price
            shopCar.SalePrice = marketModel.SalePrice
            shopCar.Unit = marketModel.Unit
            shopCar.ShippingFlag = marketModel.ShippingFlag
            shopCar.ShippingCost = marketModel.ShippingCost
            shopCar.BonusFlag = marketModel.BonusFlag
            shopCar.BonusLimit = marketModel.BonusLimit
            shopCar.CreateTime = marketModel.CreateTime
            shopCar.UpdTime = marketModel.UpdTime
            shopCar.MaxBuy = marketModel.MaxBuy
        }
        
        
    }
    
    func getShopCars() -> [RM_ShopCar] {
        let realm = try! Realm()
        let shopCarArr = Array(realm.objects(RM_ShopCar.self))
        return shopCarArr
    }
    
    func getShopCarsPrice() -> Int {
        let realm = try! Realm()
        let shopCarArr = Array(realm.objects(RM_ShopCar.self))
        var price = 0
        for shopCar in shopCarArr{
            price = price + (shopCar.SalePrice * shopCar.pCount)
        }
        return price
    }
    func delShopCar(shopCar:RM_ShopCar) {
        let realm = try! Realm()
        try! realm.write {
            realm.delete(shopCar)
        }
    }
    
    func delAllShopCars() {
        let realm = try! Realm()
        try! realm.write {
            realm.delete(realm.objects(RM_ShopCar.self))
        }
    }
    
    func updateCount(shopCar:RM_ShopCar,count:Int){
        let realm = try! Realm()
        try! realm.write {
            shopCar.pCount = count
        }
    }
    
    func updateShopCarBonus(shopCar:RM_ShopCar,bonus:Int){
        let realm = try! Realm()
        try! realm.write {
            shopCar.shopCarBonus = bonus
        }
    }
    func updateAllShopCarBonusTo0(){
        let realm = try! Realm()
        let shopCarArr = Array(realm.objects(RM_ShopCar.self))
        try! realm.write {
            for shopCar in shopCarArr{
                shopCar.shopCarBonus = 0
            }
        }
    }
    
    func isRepeat(marketId:String,formatId:String) -> RM_ShopCar?{
        let realm = try! Realm()
        let shopCar = realm.objects(RM_ShopCar.self).filter("MarketID == '\(marketId)' AND FormatId == '\(formatId)'").first
        return shopCar
    }
    
    func saveShopHs(OrderHs:[StoreOrderHsModel.RtnObject]){
        var rm_ShopHss = [RM_ShopHs]()
        for order in OrderHs{
            let rm_ShopHs = RM_ShopHs()
            rm_ShopHs.ono = order.ono
            rm_ShopHs.otime = order.otime
            rm_ShopHs.uid = order.uid
            rm_ShopHs.shipp = order.shipp
            rm_ShopHs.orderp = order.orderp
            rm_ShopHs.dtype = order.dtype
            rm_ShopHs.damt = order.damt
            rm_ShopHs.coupon = order.coupon
            rm_ShopHs.totalp = order.totalp
            rm_ShopHs.payamt = order.payamt
            rm_ShopHs.oname = order.oname
            rm_ShopHs.omobile = order.omobile
            rm_ShopHs.rname = order.rname
            rm_ShopHs.rmobile = order.rmobile
            rm_ShopHs.rzip = order.rzip
            rm_ShopHs.rcity = order.rcity
            rm_ShopHs.rdist = order.rdist
            rm_ShopHs.raddress = order.raddress
            rm_ShopHs.ostatus = order.ostatus
            rm_ShopHs.rstatus = order.rstatus
            rm_ShopHs.eddate = order.eddate
            rm_ShopHs.ddate = order.ddate
            rm_ShopHs.oprocess = order.oprocess
            rm_ShopHs.invoice = order.invoice
            rm_ShopHs.invoiceno = order.invoiceno
            for process in order.process{
                let rm_shopHsProcess = rm_process()
                rm_shopHsProcess.Seq = process.Seq
                rm_shopHsProcess.Stage = process.Stage
                rm_shopHsProcess.StageTime = process.StageTime
                rm_shopHsProcess.Display = process.Display
                rm_ShopHs.process.append(rm_shopHsProcess)
            }
            for odetail in order.odetail{
                let rm_shopHsodetail = rm_odetail()
                rm_shopHsodetail.ono = odetail.ono
                rm_shopHsodetail.dno = odetail.dno
                rm_shopHsodetail.marketid = odetail.marketid
                rm_shopHsodetail.mid = odetail.mid
                rm_shopHsodetail.format = odetail.format
                rm_shopHsodetail.unitp = odetail.unitp
                rm_shopHsodetail.amt = odetail.amt
                rm_shopHsodetail.unit = odetail.unit
                rm_shopHsodetail.totalp = odetail.totalp
                rm_shopHsodetail.title = odetail.title
                rm_shopHsodetail.img = odetail.img
                rm_shopHsodetail.damt = odetail.damt
                rm_shopHsodetail.process = odetail.process
                rm_ShopHs.odetail.append(rm_shopHsodetail)
            }
            rm_ShopHss.append(rm_ShopHs)
        }
        
        delAllShopHs()
        let realm = try! Realm()
        try! realm.write {
            realm.add(rm_ShopHss)
        }
    }
    func delAllShopHs() {
        let realm = try! Realm()
        try! realm.write {
            realm.delete(realm.objects(RM_ShopHs.self))
        }
    }
    
    func getShopHs() -> [RM_ShopHs] {
        let realm = try! Realm()
        let shopHsArr = Array(realm.objects(RM_ShopHs.self))
        return shopHsArr
    }
    
    func getShopHs(ono:String) -> RM_ShopHs? {
        let realm = try! Realm()
        let shopHs = realm.objects(RM_ShopHs.self).filter("ono == '\(ono)'").first
        return shopHs
    }
    
    func saveShopUser(shopUser:RM_ShopUserInfo) {
        let realm = try! Realm()
        try! realm.write {
            realm.delete(realm.objects(RM_ShopUserInfo.self))
            realm.add(shopUser)
        }
    }
    
    func getShopUser() -> RM_ShopUserInfo? {
        let realm = try! Realm()
        let shopUserInfo = Array(realm.objects(RM_ShopUserInfo.self)).first
        return shopUserInfo
    }
    
    func delShopUser() {
        let realm = try! Realm()
        try! realm.write {
            realm.delete(realm.objects(RM_ShopUserInfo.self))
        }
    }
}
