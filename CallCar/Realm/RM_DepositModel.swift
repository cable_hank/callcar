//
//  RM_DepositModel.swift
//  CallCar
//
//  Created by Cablesoft on 2018/8/14.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit
import RealmSwift
class RM_DepositModel : Object{
    @objc private(set) dynamic var id:String = UUID().uuidString
    @objc dynamic var source:String = ""
    @objc dynamic var type:String = ""
    @objc dynamic var getdate:String = ""
    @objc dynamic var amt:String = ""
    @objc dynamic var cancel:String = ""
    @objc dynamic var relatedinfo:String = ""
    @objc dynamic var memo:String = ""
}
