//
//  RM_OrderModel.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/30.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit
import RealmSwift

class RM_OrderModel : Object {
    
    @objc private(set) dynamic var id:String = UUID().uuidString
    @objc dynamic var rno = ""
    @objc dynamic var stype = ""
    @objc dynamic var tdate = ""
    @objc dynamic var ttime = ""
    @objc dynamic var stime = ""
    @objc dynamic var poolflag = ""
    @objc dynamic var fno = ""
    @objc dynamic var ftime = ""
    @objc dynamic var paddress = ""
    @objc dynamic var taddress = ""
    @objc dynamic var pcnt = 0
    @objc dynamic var bcnt = 0
    @objc dynamic var addcnt = 0
    @objc dynamic var price = 0
    @objc dynamic var distype = ""
    @objc dynamic var coupon = ""
    @objc dynamic var disamt = 0
    @objc dynamic var bonus = 0
    @objc dynamic var pstage = ""
    @objc dynamic var pname = ""
    @objc dynamic var pphone = ""
    @objc dynamic var otime = ""
    @objc dynamic var dname = ""
    @objc dynamic var dphone = ""
    @objc dynamic var carno = ""
    @objc dynamic var cartype = ""
    @objc dynamic var price1 = 0
    @objc dynamic var cancelflag = ""
    @objc dynamic var cancelalert = ""
    @objc dynamic var pstageo = ""
    @objc dynamic var vanflag = ""
    @objc dynamic var noshow = ""
    @objc dynamic var lat = 0.0
    @objc dynamic var lng = 0.0
    @objc dynamic var airport = ""
    @objc dynamic var terminal = ""
    @objc dynamic var city = ""
    @objc dynamic var distinct = ""
    @objc dynamic var dno = ""
    @objc dynamic var country = ""
    var dispatch = List<rm_dispatch>()
    //設置索引主鍵
    override static func primaryKey() -> String {
        return "id"
    }
}

class rm_dispatch:Object {
    @objc dynamic var time:String = ""
    @objc dynamic var rno:String = ""
    @objc dynamic var seq:Int = 0
    @objc dynamic var distinct:String = ""
    @objc dynamic var dno:String = ""
    @objc dynamic var city:String = ""
    @objc dynamic var pcnt:Int = 0
    @objc dynamic var bcnt:Int = 0
}
