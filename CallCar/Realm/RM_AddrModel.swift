//
//  RM_AddrModel.swift
//  CallCar
//
//  Created by Cablesoft on 2018/6/1.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import UIKit
import RealmSwift

class RM_AddrModel : Object {
    
    @objc private(set) dynamic var id:String = UUID().uuidString
    @objc dynamic var addr = ""
    @objc dynamic var lat:Double = 0.0
    @objc dynamic var lng:Double = 0.0
    @objc dynamic var date = Date()
    
    //設置索引主鍵
    override static func primaryKey() -> String {
        return "id"
    }
    
}

