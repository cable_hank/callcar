//
//  OrderHsModel.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/30.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import Foundation

struct OrderHsModel{
    var rno = ""
    var stype = ""
    var tdate = ""
    var ttime = ""
    var stime = ""
    var poolflag = ""
    var fno = ""
    var ftime = ""
    var paddress = ""
    var taddress = ""
    var pcnt = 0
    var bcnt = 0
    var addcnt = 0
    var price = 0
    var distype = ""
    var coupon = ""
    var disamt = 0
    var bonus = 0
    var patage = ""
    var pname = ""
    var pphone = ""
    var otime = ""
    var dname = ""
    var dphone = ""
    var carno = ""
    
}
