//
//  RegisterModel.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/17.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import Foundation

struct RegisterModel {
    static let shard:RegisterModel = RegisterModel()
    var source:String = "APP"
    var sid = ""
    var email:String = ""
    var pw:String = ""
    var fname:String = ""
    var lname:String = ""
    var mobile = ""
    var intro:String = ""
    
    func toArray() -> [String:String] {
        let arr:[String:String] = ["source":source,
                                   "sid":sid,
                                   "email":email,
                                   "pw":pw,
                                   "fname":fname,
                                   "lname":lname,
                                   "mobile":mobile,
                                   "intro":intro]
        return arr
    }
}
