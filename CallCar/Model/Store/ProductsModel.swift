//
//  ProductsModel.swift
//  CallCar
//
//  Created by Cablesoft on 2019/4/11.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import Foundation
struct ProductsModel:Codable {
    var Status:Int
    var DebugMsg:String?
    var DisplayMsg:String?
    var RtnObject:[RtnObject]
    struct RtnObject:Codable{
        var MarketID:String
        var PID:String
        var STime:String
        var ETime:String
        var OnFlag:String
        var SaleName:String
        var Title1:String
        var Title2:String
        var Images:[Images]
        struct Images:Codable {
            var Url:String
            var Main:String
        }
        var SaleInfo:String
        var Video:String
        var Format:[Format]
        struct Format:Codable {
            var mid:String
            var format:String
            var saleflag:String
            var saleq:Int
            var soldq:Int
        }
        var Price:Int
        var SalePrice:Int
        var Unit:String
        var ShippingFlag:String
        var ShippingCost:Int
        var BonusFlag:String
        var BonusLimit:Int
        var CreateTime:String
        var UpdTime:String
        var MaxBuy:Int
    }
    
}
