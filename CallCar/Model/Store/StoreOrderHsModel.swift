//
//  StoreOrderHsModel.swift
//  CallCar
//
//  Created by Cablesoft on 2019/1/28.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import Foundation
struct StoreOrderHsModel:Codable {
    var Status:Int
    var DebugMsg:String?
    var DisplayMsg:String?
    var RtnObject:[RtnObject]
    struct RtnObject:Codable {
        var ono:String
        var otime:String
        var uid:Int
        var shipp:Int
        var orderp:Int
        var dtype:String
        var damt:Int
        var coupon:String
        var totalp:Int
        var payamt:Int
        var oname:String
        var omobile:String
        var rname:String
        var rmobile:String
        var rzip:String
        var rcity:String
        var rdist:String
        var raddress:String
        var ostatus:String
        var rstatus:String
        var eddate:String
        var ddate:String
        var oprocess:String
        var invoice:String
        var invoiceno:String
        var process:[process]
        struct process:Codable {
            var Seq:Int
            var Stage:String
            var StageTime:String
            var Display:String
        }
        var odetail:[odetail]
        struct odetail:Codable {
            var ono:String
            var dno:String
            var marketid:String
            var mid:String
            var format:String
            var unitp:Int
            var amt:Int
            var unit:String
            var totalp:Int
            var title:String
            var img:String
            var damt:Int
            var process:String
        }
    }
}
