//
//  CategoryModel.swift
//  CallCar
//
//  Created by Cablesoft on 2019/9/18.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import Foundation
struct CategoryModel :Codable{
    var Status:Int
    var DebugMsg:String?
    var DisplayMsg:String?
    var RtnObject:[RtnObject]
    struct RtnObject:Codable{
        var cid:String
        var desc:String
        var sort:Int
    }
}
