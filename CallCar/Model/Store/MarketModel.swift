//
//  MarketModel.swift
//  CallCar
//
//  Created by Cablesoft on 2019/1/17.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import Foundation
struct MarketModel:Codable {
    var Status:Int
    var DebugMsg:String?
    var DisplayMsg:String?
    var RtnObject:[RtnObject]
    struct RtnObject:Codable{
        var MarketID:String
        var STime:String
        var ETime:String
        var OnFlag:String
        var Title1:String
        var Title2:String
        var Images:[Images]
        struct Images:Codable {
            var Url:String
            var Main:String
        }
        var Price:Int
        var SalePrice:Int
        var CreateTime:String
        var BonusLimit:Int
        var BonusFlag:String
    }
    
}
