//
//  StoreOrderParameterModel.swift
//  CallCar
//
//  Created by Cablesoft on 2019/1/28.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import Foundation
struct StoreOrderParameterModel:Codable {
    var uid:String
    var oprice:Int
    var sprice:Int
    var tprice:Int
    var dtype:String
    var damt:Int
    var pamt:Int
    var coupon:String
    var oname:String
    var ophone:String
    var rname:String
    var rphone:String
    var zip:String
    var city:String
    var distinct:String
    var address:String
    var market:[market]
    struct market:Codable{
        var marketid:String
        var mid:String
        var title:String
        var format:String
        var uprice:Int
        var qty:Int
        var unit:String
        var dtamt:Int
        var damt:Int
    }
    var einv:einv
    struct einv:Codable{
        var category:String
        var etype:String
        var ubn:String
        var Address:String
        var ctype:String
        var cnum:String
        var email:String
    }
    
    var appLocale:String
}
