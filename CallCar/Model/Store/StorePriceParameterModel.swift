//
//  StorePriceParameterModel.swift
//  CallCar
//
//  Created by Cablesoft on 2019/3/8.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

struct StorePriceParameterModel:Codable {
    var uid:String
    var coupon:String
    var bonus:Int
    
    var market:[market]
    struct market:Codable{
        var mid:String
        var amt:Int
    }
    
    var appLocale:String
}
