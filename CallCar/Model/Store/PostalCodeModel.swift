//
//  PostalCodeModel.swift
//  CallCar
//
//  Created by Cablesoft on 2019/1/24.
//  Copyright © 2019 Cablesoft. All rights reserved.
//

import Foundation
struct PostalCodeModel:Codable {
    var name:String
    var districts:[districts]
    struct districts:Codable{
        var zip:String
        var name:String
    }
    
    
}
