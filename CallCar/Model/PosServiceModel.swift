//
//  PosServiceModel.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/21.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import Foundation
import SwiftyJSON
struct PosServiceModel{
    static let shared : PosServiceModel = PosServiceModel()
    var share:Share = Share()
    var vip:Vip = Vip()
    var tshare:Tshare = Tshare()
    struct Share{
        var Status = ""
        var Price = ""
        var MinPassenger = ""
        var Promote = ""
    }
    struct Vip{
        var Status = ""
        var Price = ""
        var MinPassenger = ""
        var Promote = ""
        var PromotePrice = ""
    }
    struct Tshare{
        var Status = ""
        var Price = ""
        var MinPassenger = ""
        var Promote = ""
        var PromotePrice = ""
    }
    
    mutating func jsonToModel(_ json:JSON) {
        self.share.Status = json["share"]["Status"].stringValue
        self.share.Price = json["share"]["Price"].stringValue
        self.share.MinPassenger = json["share"]["MinPassenger"].stringValue
        self.share.Promote = json["share"]["Promote"].stringValue
        self.vip.Status = json["vip"]["Status"].stringValue
        self.vip.Price = json["vip"]["Price"].stringValue
        self.vip.MinPassenger = json["vip"]["MinPassenger"].stringValue
        self.vip.Promote = json["vip"]["Promote"].stringValue
        self.vip.PromotePrice = json["vip"]["PromotePrice"].stringValue
        self.tshare.Status = json["tshare"]["Status"].stringValue
        self.tshare.Price = json["tshare"]["Price"].stringValue
        self.tshare.MinPassenger = json["tshare"]["MinPassenger"].stringValue
        self.tshare.Promote = json["tshare"]["Promote"].stringValue
        self.tshare.PromotePrice = json["tshare"]["PromotePrice"].stringValue
    }
}
