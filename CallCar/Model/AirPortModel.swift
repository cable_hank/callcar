//
//  AirPortModel.swift
//  CallCar
//
//  Created by Cablesoft on 2018/6/14.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import Foundation

struct AirPortModel {
    static let shard:AirPortModel = AirPortModel()
    var code:String = ""
    var name:String = ""
    var vip:String = ""
    var share:String = ""
    var lat:Double = 0.0
    var lng:Double = 0.0
    var city:String = ""
    var distinct:String = ""
    var village:String = ""
    var address:String = ""
    var country:String = ""
}
