//
//  BulletinModel.swift
//  CallCar
//
//  Created by Cablesoft on 2018/12/24.
//  Copyright © 2018 Cablesoft. All rights reserved.
//

import Foundation
struct BulletinModel {
    var nid:String = ""
    var stime:String = ""
    var etime:String = ""
    var type:String = ""
    var title:String = ""
    var content:String = ""
    var go:String = ""
    var gouri:String = ""
}
