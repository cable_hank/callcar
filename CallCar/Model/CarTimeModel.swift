//
//  CarTimeModel.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/23.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import Foundation

struct CarTimeModel {
    var carDate = ""
    var carTime = ""
    var dno = ""
    var city = ""
    var distinct = ""
    var rbcnt = ""
    var rpcnt = ""
    var carType = ""
    var rno = ""
    
    init(carDate:String,carTime:String,dno:String,city:String,distinct:String,rbcnt:String,rpcnt:String,carType:String,rno:String) {
        self.carDate = carDate
        self.carTime = carTime
        self.dno = dno
        self.city = city
        self.distinct = distinct
        self.rbcnt = rbcnt
        self.rpcnt = rpcnt
        self.carType = carType
        self.rno = rno
    }
}
