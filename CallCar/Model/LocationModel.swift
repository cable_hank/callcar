//
//  LocationModel.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/24.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import Foundation

struct LocationModel {
   var city = ""
   var distinct = ""
   var village = ""
   var address = ""
   var lat:Double = 0.0
   var lng:Double = 0.0
}
