//
//  UserInfo.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/17.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import Foundation

enum UserInfo:String{
    case account = "account"
    case pw = "pw"
    case id = "id"
    case fname = "fname"
    case lname = "lname"
    case mobile = "mobile"
    case statusColor = "statusColor"
    case tokenlize = "tokenlize"
    
    case bonus = "bonus"
    case unbonus = "unbonus"
    case deposit = "deposit"
    
    
    case missionOrder = "missionOrder"
    case missionCard = "missionCard"
    case unlockAlert = "unlockAlert"
    
    case maintain = "maintain"
    case noticeDate = "noticeDate"
    
    case isAlertStoreKit = "isAlertStoreKit"
    case isTeach = "isTeach"
}
