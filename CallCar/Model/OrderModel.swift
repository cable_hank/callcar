//
//  OrderModel.swift
//  CallCar
//
//  Created by Cablesoft on 2018/5/22.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import Foundation

struct OrderModel{
    var uid:Int?
    var stype:String = ""
    var sdate:String = ""
    var ptime:String = ""
    var ctype:String = ""
    var pcartype:String = ""
    var fno:String = ""
    var fdate:String = ""
    var ftime:String = ""
    var pcity:String = ""
    var pdistinct:String = ""
    var pvillage:String = ""
    var paddress:String = ""
    var plat:Double = 0
    var plng:Double = 0
    var tcity:String = ""
    var tdistinct:String = ""
    var tvillage:String = ""
    var taddress:String = ""
    var tlat:Double = 0
    var tlng:Double = 0
    var pcnt:Int = 0
    var bcnt:Int = 0
    var max:String = ""
    var idata:String = ""
    var cdata:String = ""
    var price:Int = 0
    var airport:String = ""
    var terminal:String = ""
    var coupon:String = ""
    var pname:String = ""
    var pmobile:String = ""
    var dtype:String = ""
    var damt:Int = 0
    var cpflag:String = ""
    var cartype:String = ""
    var udamt:Int = 0
    var pdamt:Int = 0
    
    func getParameters() -> [String:Any] {
        let parameters:[String:Any] = ["uid":uid!,
                                       "stype":stype,
                                       "sdate":sdate,
                                       "ptime":ptime,
                                       "ctype":ctype,
                                       "pcartype":pcartype,
                                       "fno":fno,
                                       "fdate":fdate,
                                       "ftime":ftime,
                                       "pcity":pcity,
                                       "pdistinct":pdistinct,
                                       "pvillage":pvillage,
                                       "paddress":paddress,
                                       "plat":plat,
                                       "plng":plng,
                                       "tcity":tcity,
                                       "tdistinct":tdistinct,
                                       "tvillage":tvillage,
                                       "taddress":taddress,
                                       "tlat":tlat,
                                       "tlng":tlng,
                                       "pcnt":pcnt,
                                       "bcnt":bcnt,
                                       "max":max,
                                       "idata":idata,
                                       "cdata":cdata,
                                       "price":price,
                                       "airport":airport,
                                       "terminal":terminal,
                                       "coupon":coupon,
                                       "pname":pname,
                                       "pmobile":pmobile,
                                       "dtype":dtype,
                                       "damt":damt,
                                       "cpflag":cpflag,
                                       "cartype":cartype,
                                       "udamt":udamt,
                                       "pdamt":pdamt]
        return parameters
    }
    
    func getPickParameters() -> [String:Any] {
        let parameters:[String:Any] = ["uid":uid!,
                                       "stype":stype,
                                       "sdate":sdate,
                                       "ptime":ptime,
                                       "ctype":ctype,
                                       "pcartype":pcartype,
                                       "fno":fno,
                                       "fdate":fdate,
                                       "ftime":ftime,
                                       "pcity":pcity,
                                       "pdistinct":pdistinct,
                                       "pvillage":pvillage,
                                       "paddress":paddress,
                                       "plat":plat,
                                       "plng":plng,
                                       "tcity":tcity,
                                       "tdistinct":tdistinct,
                                       "tvillage":tvillage,
                                       "taddress":taddress,
                                       "tlat":tlat,
                                       "tlng":tlng,
                                       "pcnt":pcnt,
                                       "bcnt":bcnt,
                                       "max":max,
                                       "idata":idata,
                                       "cdata":cdata,
                                       "price":price,
                                       "airport":airport,
                                       "terminal":terminal,
                                       "coupon":coupon,
                                       "pname":pname,
                                       "pmobile":pmobile,
                                       "dtype":dtype,
                                       "damt":damt,
                                       "cpflag":cpflag,
                                       "cartype":cartype,
                                       "udamt":udamt,
                                       "pdamt":pdamt]
        return parameters
    }
}
