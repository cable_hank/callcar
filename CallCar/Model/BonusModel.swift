//
//  BonusModel.swift
//  CallCar
//
//  Created by Cablesoft on 2018/8/1.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import Foundation

struct BonusModel {
    var addtype:String = ""
    var btype:String = ""
    var relatedinfo:String = ""
    var point:String = ""
    var cancel:String = ""
    var effdate:String = ""
    var getdate:String = ""
    var memo:String = ""
}
