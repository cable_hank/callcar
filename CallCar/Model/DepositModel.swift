//
//  DepositModel.swift
//  CallCar
//
//  Created by Cablesoft on 2018/8/9.
//  Copyright © 2018年 Cablesoft. All rights reserved.
//

import Foundation

struct DepositModel {
    var source:String = ""
    var type:String = ""
    var getdate:String = ""
    var amt:String = ""
    var cancel:String = ""
    var relatedinfo:String = ""
    var memo:String = ""
}
